var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

var paths = {
  appScripts: ['./www/js/*/*.js'],
  vendorScripts: ['./www/js/public/*.js'],
  sass: ['./scss/**/*.scss'],
  css: ['./www/css/public/*.css']
};

gulp.task('default', ['sass','app-scripts', 'vendor-scripts', 'css']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('css', function () {
  gulp.src(paths.css)
    .pipe(concat('common.css'))
    .pipe(gulp.dest('www/css'))
});

gulp.task('vendor-scripts', function () {
  gulp.src(paths.vendorScripts)
    .pipe(concat('common.js'))
    .pipe(gulp.dest('www/js'))
});

gulp.task('app-scripts', function () {
  gulp.src(paths.appScripts)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('www/js'))
});


gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.css, ['css']);
  gulp.watch(paths.appScripts, ['app-scripts']);
  gulp.watch(paths.vendorScripts, ['vendor-scripts']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
