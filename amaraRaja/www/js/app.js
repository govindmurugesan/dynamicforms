// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('amaraja', ['ionic', 'ngCordova', 'ui.sortable', 'ngFileUpload', 'ui.bootstrap', 'ionic-timepicker', 'ionic-datepicker','AngularPrint'])



.run(function($ionicPlatform, $cordovaSQLite, constantService, $http, $window, $cordovaNetwork) {
		$ionicPlatform.ready(function() {
			if (window.cordova && window.cordova.plugins.Keyboard) {
				// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
				// for form inputs)
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

				// Don't remove this line unless you know what you are doing. It stops the viewport
				// from snapping when text inputs are focused. Ionic handles this internally for
				// a much nicer keyboard experience.
				cordova.plugins.Keyboard.disableScroll(true);


			}
			$window.localStorage.removeItem("userDept");
			    $window.localStorage.removeItem("userType");
			    $window.localStorage.removeItem("userName");
			var deviceInformation = ionic.Platform.isWebView();
			document.addEventListener("deviceready", onDeviceReady, false);
			function onDeviceReady() {
	
		   							
					db = window.openDatabase("amaraja.db", "1", "Sqlite", "2000");
					//$cordovaSQLite.execute(db, 'DROP TABLE workflowItems');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_DEPT');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_GRP');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_LC');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_PARAM');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_PROC');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_CUST');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_MAC');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_MBNO_IP');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_MPNO_IP');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_PNO_OP');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTR_USERS');
					$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS DF_MSTRS');
					/*$cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS approveTable');*/
					/*$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Tables (rowid INTEGER PRIMARY KEY AUTOINCREMENT, tableName TEXT, tableRecord JSON)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Users (rowid INTEGER PRIMARY KEY AUTOINCREMENT, userName TEXT, password TEXT, userType Text, userDept TEXT)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS department (id INTEGER PRIMARY KEY AUTOINCREMENT, department TEXT)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS forms (id INTEGER PRIMARY KEY AUTOINCREMENT, forms TEXT)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS workflowMaster (rowid INTEGER PRIMARY KEY AUTOINCREMENT, workflowid INTEGER, workflowname TEXT, levels INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS workflowItems (rowid INTEGER PRIMARY KEY AUTOINCREMENT, workflowid INTEGER, department TEXT, levelform TEXT, level TEXT, levelname TEXT)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS approveTable (rowid INTEGER PRIMARY KEY AUTOINCREMENT, approved TEXT, tableName TEXT, recordId TEXT, deptName Type, formName Type)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_DEPT (rowid INTEGER PRIMARY KEY AUTOINCREMENT, deptName TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_LGN (rowid INTEGER PRIMARY KEY AUTOINCREMENT, empCode TEXT, empName TEXT, deptName TEXT, role TEXT, supervisor TEXT, email TEXT, mobile TEXT, plant TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_GRP (rowid INTEGER PRIMARY KEY AUTOINCREMENT, company TEXT, plant TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_LC (rowid INTEGER PRIMARY KEY AUTOINCREMENT, process TEXT, lossCode TEXT, codeName TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_PARAM (rowid INTEGER PRIMARY KEY AUTOINCREMENT, process TEXT, parameters TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_PROC (rowid INTEGER PRIMARY KEY AUTOINCREMENT, process TEXT, detail TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_CUST (rowid INTEGER PRIMARY KEY AUTOINCREMENT, plant TEXT, customer TEXT, customerName TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_MAC (rowid INTEGER PRIMARY KEY AUTOINCREMENT, plant TEXT, process TEXT, machine TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_MBNO_IP (rowid INTEGER PRIMARY KEY AUTOINCREMENT, partNo TEXT, descr TEXT, unom TEXT, purpose TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_MPNO_IP (rowid INTEGER PRIMARY KEY AUTOINCREMENT, partNo TEXT, descr TEXT, unom TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_PNO_OP (rowid INTEGER PRIMARY KEY AUTOINCREMENT, itemCode TEXT, descr TEXT, unom TEXT, customer TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_USERS (rowid INTEGER PRIMARY KEY AUTOINCREMENT, empCode TEXT, empName TEXT, deptName TEXT, role TEXT, supervisor TEXT, email TEXT, mobile TEXT, plant TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTRS (rowid INTEGER PRIMARY KEY AUTOINCREMENT, tableName TEXT, colName TEXT)');*/

					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_DYNTABLES (rowid INTEGER PRIMARY KEY AUTOINCREMENT, tableName TEXT, tableRecord JSON)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Users (rowid INTEGER PRIMARY KEY AUTOINCREMENT, userName TEXT, password TEXT, userType Text, userDept TEXT)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_WORKFLOW (workflowId INTEGER PRIMARY KEY AUTOINCREMENT, workflowName TEXT, levels INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_WORKFLOW_I (workflowId INTEGER, department TEXT, levelForm TEXT, lev TEXT, levelName TEXT)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_TBL_APR (approved TEXT, tableName TEXT, recordId TEXT, deptName TEXT, formName TEXT, updatedBy TEXT, modifiedOn datetime default current_timestamp, status TEXT )');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_DEPT (deptName TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_LGN (empCode TEXT, empName TEXT, deptName TEXT, role TEXT, supervisor TEXT, email TEXT, mobile TEXT, plant TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_GRP (company TEXT, plant TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_LC (process TEXT, lossCode TEXT, codeName TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_PARAM (process TEXT, parameters TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_PROC (process TEXT, detail TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_CUST (plant TEXT, customer TEXT, customerName TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_MAC (plant TEXT, process TEXT, machine TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_MBNO_IP (partNo TEXT, descr TEXT, unom TEXT, purpose TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_MPNO_IP (partNo TEXT, descr TEXT, unom TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_PNO_OP (itemCode TEXT, descr TEXT, unom TEXT, customer TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTR_USERS (empCode TEXT, empName TEXT, deptName TEXT, role TEXT, supervisor TEXT, email TEXT, mobile TEXT, plant TEXT, inActive INTEGER)');
					$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS DF_MSTRS (tableName TEXT, colName TEXT)');

					if (window.StatusBar) {
						StatusBar.styleDefault();
					}

					/*$cordovaSQLite.execute(db, 'INSERT INTO Users (userName, password, userType, userDept) VALUES ("100101","123123123","admin","")');
					$cordovaSQLite.execute(db, 'INSERT INTO Users (userName, password, userType, userDept) VALUES ("100102","123123123","Engnieering","operator")');
					$cordovaSQLite.execute(db, 'INSERT INTO Users (userName, password, userType, userDept) VALUES ("100103","123123123","Engnieering","manager")');
					$cordovaSQLite.execute(db, 'INSERT INTO Users (userName, password, userType, userDept) VALUES ("100104","123123123","Production","operator")');
					$cordovaSQLite.execute(db, 'INSERT INTO Users (userName, password, userType, userDept) VALUES ("100105","123123123","Production","manager")');
					$cordovaSQLite.execute(db, 'INSERT INTO Users (userName, password, userType, userDept) VALUES ("100106","123123123","QA-process","operator")');
					$cordovaSQLite.execute(db, 'INSERT INTO Users (userName, password, userType, userDept) VALUES ("100107","123123123","QA-process","operator")');
					
					$cordovaSQLite.execute(db, 'INSERT INTO department (department) VALUES ("Engnieering")');
					$cordovaSQLite.execute(db, 'INSERT INTO department (department) VALUES ("QA-process")');
					$cordovaSQLite.execute(db, 'INSERT INTO department (department) VALUES ("Production")');*/
				if($cordovaNetwork.isOnline()){
					var deptreq = {
		                method: 'POST',
		                url: constantService.serverURL + 'SelectAllDepartments',
		                headers: {
		                  'Content-Type': 'application/json'
		                }
		               }
			    $http(deptreq).then(function(response){
			      var result = response.data.SelectAllDepartmentsResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_DEPT (deptName, inActive) VALUES ("' + value.deptName +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});
			   	var grupreq = {
		                method: 'POST',
		                url: constantService.serverURL + 'SelectAllGroups',
		                headers: {
		                  'Content-Type': 'application/json'
		                }
		               }
			    $http(grupreq).then(function(response){
			      var result = response.data.SelectAllGroupsResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_GRP (company, plant, inActive) VALUES ("' + value.company +'", "'+ value.plant +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

		   		var losscodereq = {
		              method: 'POST',
		              url: constantService.serverURL + 'SelectAllVisualDefects',
		              headers: {
		                'Content-Type': 'application/json'
		              }
		             }
			    $http(losscodereq).then(function(response){
			      var result = response.data.SelectAllVisualDefectsResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_LC (process, lossCode, codeName, inActive) VALUES ("' + value.process +'", "'+ value.lossCode +'", "'+ value.codeName +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

			   	var paramreq = {
		              method: 'POST',
		              url: constantService.serverURL + 'SelectAllCheckingParams',
		              headers: {
		                'Content-Type': 'application/json'
		              }
		             }
			    $http(paramreq).then(function(response){
			      var result = response.data.SelectAllCheckingParamsResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_PARAM (process, parameters, inActive) VALUES ("' + value.process +'", "'+ value.parameters +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

			   	var processreq = {
		              method: 'POST',
		              url: constantService.serverURL + 'SelectAllProcess',
		              headers: {
		                'Content-Type': 'application/json'
		              }
		             }
			    $http(processreq).then(function(response){
			      var result = response.data.SelectAllProcessResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_PROC (process, detail, inActive) VALUES ("' + value.process +'", "'+ value.detail +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

			   	var custreq = {
		              method: 'POST',
		              url: constantService.serverURL + 'SelectAllCustomers',
		              headers: {
		                'Content-Type': 'application/json'
		              }
		             }
			    $http(custreq).then(function(response){
			      var result = response.data.SelectAllCustomersResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_CUST (plant, customer, customerName, inActive) VALUES ("' + value.plant +'", "'+ value.customer +'", "'+ value.customerName +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

			   	var mastreq = {
		                method: 'POST',
		                url: constantService.serverURL + 'SelectAllTableColumns',
		                headers: {
		                  'Content-Type': 'application/json'
		                }
		               }
			    $http(mastreq).then(function(response){
			      var result = response.data.SelectAllTableColumnsResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTRS (tableName, colName) VALUES ("' + value.tableName +'", "'+ value.colName +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});
			   	var machreq = {
		                method: 'POST',
		                url: constantService.serverURL + 'SelectAllMachines',
		                headers: {
		                  'Content-Type': 'application/json'
		                }
		               }
			    $http(machreq).then(function(response){
			      var result = response.data.SelectAllMachinesResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_MAC (plant, process, machine, inActive) VALUES ("' + value.plant +'","' + value.process +'","' + value.machine +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

			  var batchreq = {
		                method: 'POST',
		                url: constantService.serverURL + 'SelectAllMatBatchNoIP',
		                headers: {
		                  'Content-Type': 'application/json'
		                }
		               }
			    $http(batchreq).then(function(response){
			      var result = response.data.SelectAllMatBatchNoIPResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_MBNO_IP (partNo, descr, unom, purpose, inActive) VALUES ("' + value.partNo +'","' + value.descr +'","' + value.unom +'","' + value.purpose +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

			   	var partreq = {
		                method: 'POST',
		                url: constantService.serverURL + 'SelectAllMatPartNoIP',
		                headers: {
		                  'Content-Type': 'application/json'
		                }
		               }
			    $http(partreq).then(function(response){
			      var result = response.data.SelectAllMatPartNoIPResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_MPNO_IP (partNo, descr, unom, inActive) VALUES ("' + value.partNo +'","' + value.descr +'","' + value.unom +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

			   	var partnoreq = {
		                method: 'POST',
		                url: constantService.serverURL + 'SelectAllPartNoOP',
		                headers: {
		                  'Content-Type': 'application/json'
		                }
		               }
			    $http(partnoreq).then(function(response){
			      var result = response.data.SelectAllPartNoOPResult;
			      angular.forEach(result, function(value, index){
				      $cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_PNO_OP (itemCode, descr, unom, customer, inActive) VALUES ("' + value.itemCode +'","' + value.descr +'","' + value.unom +'","' + value.customer +'", "'+ value.inActive +'")');
			      })
			      
			 		}, function(error){
			      console.log('error',error);
			       
			   	});

			   	var userreq = {
		                method: 'POST',
		                url: constantService.serverURL + 'SelectAllUsers',
		                headers: {
		                  'Content-Type': 'application/json'
		                }
		               }
		    $http(userreq).then(function(response){
		      var result = response.data.SelectAllUsersResult;
		      angular.forEach(result, function(value, index){
		      	$cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_USERS (empCode, empName, deptName, role, supervisor, email, mobile, plant, inActive) VALUES ("' + value.empCode +'", "'+ value.empName +'", "'+ value.deptName +'", "'+ value.role +'", "'+ value.supervisor +'", "'+ value.email +'", "'+ value.mobile +'", "'+ value.plant +'", "'+ value.inActive + '")');
		      })
		   }, function(error){
		      console.log('error',error);
		   });
	   	}
			

    //ionic.Platform.isFullScreen = true;
     /* if(ionic.Platform.isAndroid()){console.log('android');
          $ionicPlatform.registerBackButtonAction(function(e){
              e.preventDefault();
          },100);
      }*/
    }
			
		});
	})
	.directive('elementDraggable', ['$document', function($document) {
		return {
			link: function(scope, element, attr) {
				element.on('dragstart', function(event) {

					event.originalEvent.dataTransfer.setData('templateIdx', $(element).data('index'));
				});
			}
		};
	}])

.directive('elementDrop', ['$document', function($document) {
	return {
		link: function(scope, element, attr) {

			element.on('dragover', function(event) {
				event.preventDefault();
			});

			$('.drop').on('dragenter', function(event) {
				event.preventDefault();
			})
			element.on('drop', function(event) {
				event.stopPropagation();
				var self = $(this);
				scope.$apply(function() {
					var idx = event.originalEvent.dataTransfer.getData('templateIdx');
					var insertIdx = self.data('index')
					scope.addElement(scope.dragElements[idx], insertIdx);
				});
			});
		}
	};
}])

.directive('convertToNumber', convertToNumber);

  function convertToNumber() {
      return {
          require: 'ngModel',
          link: function (scope, element, attrs, ngModel) {
              ngModel.$parsers.push(function(val) {
                  return parseInt(val, 10);
              });
              ngModel.$formatters.push(function (val) {
                  return '' + val;
              });
          }
      };
  }



