(function() {
	'use strict';

	angular
		.module('amaraja')
		.config(function ($httpProvider) {
		  $httpProvider.defaults.headers.common = {};
		  $httpProvider.defaults.headers.post = {};
		  $httpProvider.defaults.headers.put = {};
		  $httpProvider.defaults.headers.patch = {};
		})
		.config(config)
	config.$inject = ['$stateProvider', '$urlRouterProvider', 'ionicTimePickerProvider', 'ionicDatePickerProvider']

	function config($stateProvider, $urlRouterProvider, ionicTimePickerProvider, ionicDatePickerProvider) {
		$stateProvider
			.state('workflow', {
				url: '/',
				templateUrl: 'templates/workflow.html',
				controller: 'workflowController',
				cache: false,
				controllerAs: 'vm'
			})
		.state('createForm', {
			url: '/createForm',
			templateUrl: 'templates/createForms.html',
			cache: false,
			controller: 'createFormsController',
			controllerAs: 'vm'
		})
		.state('editForms', {
				url: '/editForms/:formName',
				templateUrl: 'templates/editForms.html',
				cache: false,
				controller: 'createFormsController',
				controllerAs: 'vm'
			})
		.state('getForms', {
			url: '/getForms/:getFormName/:type/:selectedId',
			templateUrl: 'templates/getForms.html',
			cache: false,
			controller: 'getFormsController',
			controllerAs: 'vm'
		})
		.state('getFormRecords', {
			url: '/getFormRecords/:getFormName/:type',
			templateUrl: 'templates/getFormRecords.html',
			cache: false,
			controller: 'getFormsRecordsCtrls',
			controllerAs: 'vm'
		})
		.state('admin', {
			url: '/admin',
			templateUrl: 'templates/admin.html'
		})
		.state('user', {
			url: '/user',
			templateUrl: 'templates/user.html',
			cache: false,
			controller: 'userController',
			controllerAs: 'vm'
		})
		.state('login', {
			url: '/login',
			templateUrl: 'templates/login.html',
			cache: false,
			controller: 'loginController',
			controllerAs: 'vm'
		})
		.state('editWorkflow', {
			url: '/editWorkflow/:workName',
			templateUrl: 'templates/editWorkflow.html',
			cache: false,
			controller: 'workflowController',
			controllerAs: 'vm'
		})
		.state('trackRecord', {
			url: '/trackRecord',
			templateUrl: 'templates/trackRecord.html',
			cache: false,
			controller: 'trackRecordController',
			controllerAs: 'vm'
		})
		.state('getAllForms', {
			url: '/getAllForms',
			templateUrl: 'templates/allForms.html',
			cache: false,
			controller: 'getAllFormsController',
			controllerAs: 'vm'
		})
		.state('report', {
			url: '/report',
			templateUrl: 'templates/report.html',
			cache: false,
			controller: 'reportController',
			controllerAs: 'vm'
		})
		$urlRouterProvider.otherwise('/login');


	var timePickerObj = {
      inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
      format: 12,
      step: 15,
      setLabel: 'Set',
      closeLabel: 'Close'
    };
    ionicTimePickerProvider.configTimePicker(timePickerObj);


    var datePickerObj = {
      inputDate: new Date(),
      titleLabel: 'Select a Date',
      setLabel: 'Set',
      todayLabel: 'Today',
      closeLabel: 'Close',
      mondayFirst: false,
      weeksList: ["S", "M", "T", "W", "T", "F", "S"],
      monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
      templateType: 'popup',
      from: new Date(2012, 8, 1),
      to: new Date(2018, 8, 1),
      showTodayButton: true,
      dateFormat: 'dd MMMM yyyy',
      closeOnSelect: true,
      disableWeekdays: []
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);
	}
})();