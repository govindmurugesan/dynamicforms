angular.module('amaraja').controller('createFormsController', createFormsController);

createFormsController.$inject = ['$scope', '$rootScope', '$location', '$http', '$state', '$window', 'createFormsService', 'notificationService', '$ionicSideMenuDelegate','constantService', 'workflowService', '$ionicPopup', '$parse'];

function createFormsController($scope, $rootScope, $location, $http, $state, $window, createFormsService, notificationService, $ionicSideMenuDelegate, constantService, workflowService, $ionicPopup, $parse) {
	var vm = this;
	var guid = 1;
	vm.getBase64 = getBase64;
	vm.changeSelectedMaster = changeSelectedMaster;
	vm.changeSelectedColumn = changeSelectedColumn;
	vm.selectionChange = selectionChange;
	vm.setDynamicTable = setDynamicTable;
	vm.getDynamicTableColuman = getDynamicTableColuman;
	vm.setColCondition = setColCondition;
	vm.setCondition = setCondition;
	vm.setTableColumnRef = setTableColumnRef;
	vm.valuesTables = [];
	var colSpans = ['2', '3', '4', '6', '8', '9', '10', '12'];
	var tableid = "";
	vm.populate = populate;
	vm.masterCheckChange = masterCheckChange;
	var uniqueInternalName = [];
	vm.getDepartments = getDepartments;
	var previousForm = "";
	vm.selectCondTable = selectCondTable;
	var selecttableName = "";
	vm.selectCondColumn = selectCondColumn;
	vm.addCondition = addCondition;
	vm.addSelectCondition = addSelectCondition;
	vm.getAllColumnList= getAllColumnList;
	vm.addColDept = addColDept;
	vm.showCondition = false;
	vm.cancel = cancel;
	vm.changingColumanName = changingColumanName;
	vm.addColTimer = addColTimer;
	vm.addColDate = addColDate;
	vm.dynamicTableList = [];
	vm.checkLogin = checkLogin;

  checkLogin()
	function checkLogin(){
		if($window.localStorage.userDept == '' || $window.localStorage.userDept == undefined  )
      $state.go('login');
	}

	vm.OperatoList = ['Engineering Operator','Engineering Approver','Production Operator', 'Production Approver', 'QA Operator', 'QA Approver']
	
	$scope.$watch(function() {
    return $state.params;
  });
  var editFormName = $state.params.formName;
  $ionicSideMenuDelegate.canDragContent(false);
	$scope.dragElements = [{
		'Name': "Radio",
		'Type': "radio",
		'Image': 'ion-android-radio-button-on',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text',
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Choice',
				'Type': 'dropdown_increment',
				'PossibleValue': [{
					'Text': 'Choice 1',
				}, {
					'Text': 'Choice 2'
				}]
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""
			}

		]
	}, {
		'Name': "Checkbox",
		'Type': "checkbox",
		'Image': 'fa fa-check-square-o',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Choice',
				'Type': 'dropdown_increment',
				'PossibleValue': [{
					'Text': 'Choice 1',
					'Value': 'check1'
				}, {
					'Text': 'Choice 2',
					'Value': 'check2'
				}]
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""

			}

		]
	}, {
		'Name': "Input",
		'Type': "text",
		'Image': 'fa fa-font',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Max Input Length',
				'Value': '50',
				'Type': 'text'
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				}, {
					'Name': 'Unique',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""
			}

		]
	}, {
		'Name': "Date",
		'Type': "date",
		'Image': 'fa fa-calendar',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Type',
				'Value': '',
				'Type': 'radio',
				'PossibleValue': [{
					'Text': 'Date',
					'Checked': true
				}, {
					'Text': 'Current Date',
					'Checked': false
				}]
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""
			}

		]
	}, {
		'Name': "Select Box",
		"Type": "dropdown",
		'Image': 'fa fa-caret-down',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Choice',
				'Type': 'select_drop_increment',
				'MasterData':[{
					'TableName': '',
					'ColumnName': ''
				}],
				'PossibleValue': [{
					'Text': 'Choice 1'
				}, {
					'Text': 'Choice 2'
				}],
				'MasterRef': false,
				'ConditionData':[{
					'condColumn':'',
					'condTableName': '',
					'condColumnName': '',
					'condValue': '',
					'condChange': '',
					'condEquals': 'IN'
				}],	
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': "",
				'Cavity':""
			}

		]
	}, {
		'Name': "Text Area",
		"Type": "textarea",
		'Image': 'ion-compose',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
          'Name': 'Row Size',
          'Value': 4,
          'Type': 'number'
      }, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""
			}

		]
	}/*, {
		'Name': "Image",
		"Type": "button",
		'Image': 'fa fa-picture-o',
		'Settings': [{
				'Name': 'Field Type',
				'Value': 'Image',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': 'Internal Name',
				'Type': 'text'
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'Base64',
				'Value': '',

			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				}]
			}

		]
	}*/, {
		'Name': "Table",
		"Type": "table",
		'Image': 'fa fa-table',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'Table',
				'Type': 'table',
				'PossibleValue': [{
					'columns': [{
						'value': 'column1',
						'value1':'column1',
						'readonly' : false,
						'showTime': false,
						'colRef':{
							 'referMaster':false,
							 'dropdown':false,
							 'PossibleValue':[]
						}
					}, {
						'value': 'column2',
						'value1':'column2',
						'readonly' : false,
						'colRef':{			
							'referMaster':false,	
							'dropdown':false,			 
							'PossibleValue':[]
						}
					}, {
						'value': 'column3',
						'value1':'column3',
						'readonly' : false,
						'colRef':{
							 'referMaster':false,	
							 'dropdown':false,
							 'PossibleValue':[]
						}
					}],
					'rows': [{
						'cells': [{
							'value': 'row01'
						}, {
							'value': 'row02'
						}, {
							'value': 'row03'
						}]
					}]
				}]
			},{
				'Name': 'Choice',
				'Type': 'select_table_drop_increment',
				'MasterData':[{
					'TableName': '',
					'ColumnName': '',
					'column': ''
				}],	
				'PossibleValue': [{
					'Text': 'Choice 1'
				}, {
					'Text': 'Choice 2'
				}],
				'MasterRef': false,
				'ConditionData':[{
					'condColumn':'',
					'condTableName': '',
					'condColumnName': '',
					'condValue': '',
					'condChange': '',
					'condEquals': 'IN'
				}],	
				'DynamicTableRef': false,
				'DynaicRef':{ 
					'TableId':'',
					'RefCol':'',
					'FilterCol':''
				}
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': "",
				'ColmRestrict': [],				
				'ColCondValue': {"Condtion":[]},
			}
		]
	}];

	$scope.formFields = [];

	$scope.current_field = {};

	var createNewField = function() {
		return {
			'id': ++guid,
			'Name': '',
			'Settings': [],
			'Active': true,
			'ChangeFieldSetting': function(Value, SettingName) {
				switch (SettingName) {
					case 'Field Label':
						{
							$scope.current_field.Name = Value;
							$scope.current_field.Settings[0].Value = $scope.current_field.Name;
							break;
						}					
					case 'Internal Name':
						{
							$scope.current_field.Settings[1].Value = Value.replace(/\s/g, '_');
							break;
						}

					default:
						break;
				}
			},
			'GetFieldSetting': function(settingName) {
				var result = {};
				var settings = this.Settings;
				$.each(settings, function(index, set) {
					if (set.Name == settingName) {
						result = set;
						return;
					}
				});
				if (!Object.keys(result).length) {
					//Continue to search settings in the checkbox zone
					$.each(settings[settings.length - 1].Options, function(index, set) {
						if (set.Name == settingName) {
							result = set;
							return;
						}
					});
				}
				return result;
			}
		};
	}

/*	$scope.changeFieldName = function(Value) {
		$scope.current_field.Name = Value;
		$scope.current_field.Settings[0].Value = $scope.current_field.Name;
		$scope.current_field.Settings[1].Value = $scope.current_field.Name;
		$scope.current_field.Settings[2].Value = 'x' + $scope.current_field.Name.replace(/\s/g, '_');
	}*/

	$scope.removeElement = function(idx) {
		if ($scope.formFields[idx].Active) {
			$('#addFieldTab_lnk').tab('show');
			$scope.current_field = {};

		}
		$scope.formFields.splice(idx, 1);

	};

	$scope.addElement = function(ele, idx) {
    $('#formBuilderContent').animate({scrollTop: $('#formBuilderContent').prop("scrollHeight")}, 200);
		vm.showButtons = true;
		$scope.current_field.Active = false;
		$scope.current_field = createNewField();
		//Merge setting from template object
		angular.merge($scope.current_field, ele);

		if (typeof idx == 'undefined') {
			$scope.formFields.push($scope.current_field);
		} else {
			$scope.formFields.splice(idx, 0, $scope.current_field);
			$('#fieldSettingTab_lnk').tab('show');
		}
	};

	$scope.activeField = function(f) {
		$scope.current_field.Active = false;
		$scope.current_field = f;
		f.Active = true;
		$('#fieldSettingTab_lnk').tab('show');
	};

	$scope.addChoice = function(set) {
		if (set.PossibleValue[set.PossibleValue.length - 1].Text != '') {
			set.PossibleValue.push({
				'Text': ''
			});
		}
	}

	$scope.removeChoice = function(set, index) {
		if (set.PossibleValue.length > 1) {  
			set.PossibleValue.splice(index, 1);
		}
	}
	$scope.formbuilderSortableOpts = {
		'ui-floating': true,
	};
	/*table add and remove items */
	$scope.removeCol = function(set, index) {
		if (index > -1 && set.Settings[3].PossibleValue[0].columns.length > 1) {
			set.Settings[3].PossibleValue[0].columns.splice(index, 1);
			var len = set.Settings[3].PossibleValue[0].rows.length
			for (var i = 0; i < len; i++) {
				set.Settings[3].PossibleValue[0].rows[i].cells.splice(index, 1);
			}
		}
	};

	$scope.removeRow = function($index, set) {
		if ($index > -1 && set.Settings[3].PossibleValue[0].rows.length > 1) {
			set.Settings[3].PossibleValue[0].rows.splice($index, 1);
		}
	};

	$scope.addCol = function(value) {
		var leng = value.PossibleValue[0].columns.length+1;
		value.PossibleValue[0].columns.push({
			'value': 'column'+leng,
			'value1': 'column'+leng,
			'readonly' : false,
			'showTime': false,
			'colRef':{
							 'referMaster':false,	
							 'dropdown':false,
							 'PossibleValue':[]
						}
		})
		var rowLen = value.PossibleValue[0].rows.length;
		for (var i = 0; i < rowLen; i++) {
			value.PossibleValue[0].rows[i].cells.push({
				value: ''
			});
		}
	};

	$scope.addRow = function(value) {
		var row = {
				cells: []
			},
			colLen = value.PossibleValue[0].columns.length;
		for (var i = 0; i < colLen; i++) {
			row.cells.push({
				value: ''
			});
		}
		value.PossibleValue[0].rows.push(row);
	};

	$scope.addTableJSON = function(record) {
		if (record != "" && (vm.formName != undefined && vm.formName != "")) {
			angular.forEach(record, function(value, key) {
				angular.forEach(value.Settings, function(value1, key1) {
					if (value1.Name == "Internal Name") {
						var intName = value1.Value.replace(/\s/g, '_');
						if(value1.Value != '' && value1.Value != undefined && uniqueInternalName.indexOf(intName) == -1){
							uniqueInternalName.push(intName);
						}						
					}
				});
			});

			if(uniqueInternalName.length == record.length){
				uniqueInternalName = [];
				vm.tableColums = [];
				angular.forEach(record, function(value, key) {
					delete value['GetFieldSetting'];
					delete value['ChangeFieldSetting'];
					delete value['$$hashKey'];
					delete value['id'];
					angular.forEach(value.Settings, function(value1, key1) {
						delete value1['$$hashKey'];
						if (value1.Name == "Internal Name") {
	            vm.tableColums.push({'Type':value.Type, 'ColName': value1.Value.replace(/\s/g, '_')});
	          }
					});
				});
				notificationService.showSpinner();
				createFormsService.addTableJSON(vm.formName, record).then(function(req) {
					createFormsService.createTable(req.rowid, vm.tableColums).then(function(requ){
							createFormsService.createInnerTable(req.rowid, requ.rowid, record).then(function(reque){
									notificationService.hideLoad();	
									notificationService.alert('', 'Form Saved Successfully', function() {
										$scope.formFields = [];
										vm.formName = "";
										vm.selectedTable="";
										vm.showButtons = false;
										$scope.current_field = [];
										$('#addFieldTab_lnk').tab('show');
									});
							});
					})
					
				})
			}else{
    		notificationService.alert('', 'Internal Name should be Required && Unique', function() { 
    			uniqueInternalName = [];
    		});
    	}
		}else{
			if(vm.formName == undefined || vm.formName != "" ){
				notificationService.alert('', 'Enter the Form Name', function() {	});
			}else{
				notificationService.alert('', 'Add atleast one filed to the form', function() { });
			}
		}
	}
	$scope.updateTable = function(record){
		if (record != "" && vm.formName != undefined) {
			notificationService.showSpinner();
		  createFormsService.updateTableJSON(editFormName, vm.formName, record).then(function(req) {
		  	if(previousForm != vm.formName){
		  		createFormsService.updateFormName(vm.formName, previousForm).then(function(req) {
		  		})
		  	}

		  	notificationService.hideLoad();
				notificationService.alert('', 'Form Updated Successfully', function() {	
					$scope.formFields = [];
					vm.formName = "";
					vm.selectedTable ="";
					vm.showButtons = false;
					$scope.current_field = [];
					$('#addFieldTab_lnk').tab('show');
				});
			})
		}
	}
	
  getState();
	function getState(){
		if($state.current.name == 'editForms'){
			 //getForms();
			 getTableByName(editFormName);
		}
	}

	/*function getForms() {
		vm.tableName = [];
		createFormsService.getTableJSON().then(function(data) {
			angular.forEach(data.rows, function(value, key) {
				vm.tableName.push(value);
			});
		});
	}*/
	vm.showButtons = false;
	function getTableByName(selectedTable) {
		$scope.formFields = [];
		createFormsService.getTableByName(selectedTable).then(function(record) {
			if($state.current.name == 'editForms'){
				vm.formName = record.rows[0].tableName;
				previousForm = record.rows[0].tableName
			}
			angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
				
				$scope.addElement(value);
			})
		});
	}
	$scope.cancel = function(){
		$scope.formFields = [];
		vm.formName = "";
		vm.selectedTable="";
		vm.showButtons = false;
		$scope.current_field = [];
		$('#addFieldTab_lnk').tab('show');
	}

	function getBase64(field) {
		var logoImage = document.getElementById('logo_image');
		imgData = getBase64Image(logoImage);
		field.Settings[3].Value = imgData;
	}

	function getBase64Image(img) {
		var canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;

		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);

		var dataURL = canvas.toDataURL("image/jpeg");

		return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
	}

	function changeSelectedMaster(value){
		vm.columnNames = [];
		vm.OtherRef = false;
		angular.forEach(vm.masterTableNames, function(data, key) {
			//Modified to dynamic table
			if(data.tableName == value || data.rowid == value){
				console.log("value  ", value, value.substring(0,7))
				if(value == 'Other'){
					vm.OtherRef = true;
					vm.columnNames.push("Auto-increment");
				}else if((value.substring(0,7)) == 'DF_MSTR'){
					createFormsService.getMasterByName(data.tableName).then(function(record) {
						angular.forEach(record.rows, function(value, key) {
							vm.columnNames.push(value.colName);
						})
					});
				}else{
					tableid = data.rowid;
					createFormsService.getTableByName(data.rowid).then(function(record) {
						console.log("record  ---------------", record)
						angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
							for(var i=0;i<value.Settings.length;i++){
								if(value.Settings[i].Name == 'Internal Name'){
									vm.columnNames.push(value.Settings[i].Value);
								}
							}
						})
					});
				}
			}
		});
	}

	function changeSelectedColumn(currentField, masterdata, conditionData){
		console.log('masterdata000',masterdata);
		vm.valuesTables = [];
		var dynamicTableName = "";
		if(tableid != "")
			dynamicTableName = constantService.tableName+tableid;
		else
			dynamicTableName = masterdata.TableName;
		createFormsService.getColumnValuesCondition(masterdata.ColumnName, dynamicTableName, conditionData).then(function(record) {
			if(masterdata.ColumnName == 'empName'){
				angular.forEach(record.rows, function(value, key) {
					vm.valuesTables.push({'Text' : value[masterdata.ColumnName], 'Text1':value['empCode']});
				})
			}else{
				angular.forEach(record.rows, function(value, key) {
					vm.valuesTables.push({'Text' : value[masterdata.ColumnName]});
				})
			}
			$scope.current_field.Settings[2].PossibleValue = vm.valuesTables;
		})
	}

	function selectionChange(set, current_field){
		set.PossibleValue = [];
		set.MasterRef = !set.MasterRef;

		if(!set.MasterRef){
			$ionicPopup.show({
	    templateUrl: 'templates/masterPopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	          set.MasterRef = false;
	          set.MasterData[0].TableName = '';
						set.MasterData[0].ColumnName = '';
			  		vm.tableNames = [];
						set.PossibleValue.push({
							'Text': 'Choice 1'
						}, {
							'Text': 'Choice 2'
						});
	        } 
	      },
	      {
	        text: '<b>Populate</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	          changeSelectedColumn(current_field, set.MasterData[0], set.ConditionData)
	        }
	      }
	    ]
	  });
			set.MasterRef = true;
			vm.masterShow = true;
			vm.masterTableNames = [];
  		createFormsService.getTableJSON().then(function(data) {
				angular.forEach(data.rows, function(value, key) {
					vm.masterTableNames.push(value);
				});
				createFormsService.getTableMaster().then(function(masterData) {
					angular.forEach(masterData.rows, function(mdata, key) {
						vm.masterTableNames.push(mdata);
					});
				})
			});
		}else{
			set.MasterRef = false;
			set.MasterData[0].TableName = '';
			set.MasterData[0].ColumnName = '';
  		vm.tableNames = [];
			set.PossibleValue.push({
				'Text': 'Choice 1'
			}, {
				'Text': 'Choice 2'
			});
		}
	}
	/*	'DynamicTableRef': false,
				'DynaicRef':{ 
					'Table':'',
					'RefCol':'',
					'FilterCol':''
				}*/


	function setDynamicTable(set, current_field){
		$ionicPopup.show({
	    templateUrl: 'templates/dynamicTablePopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	          set.DynamicTableRef = false;
	          set.DynaicRef.TableId = '';
						set.DynaicRef.RefCol = '';
						set.DynaicRef.FilterCol = '';
	        } 
	      },
	      {
	        text: '<b>Set</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	         
	        }
	      }
	    ]
	  });
		vm.dynamicTableList =[];
	  createFormsService.getTableJSON().then(function(data) {
			angular.forEach(data.rows, function(value, key) {
				vm.dynamicTableList.push(value);
			});
			createFormsService.getTableMaster().then(function(masterData) {
				angular.forEach(masterData.rows, function(mdata, key) {
					vm.dynamicTableList.push(mdata);
				});
			})
		});
	}

	vm.dynamicTableColList = [];
	function getDynamicTableColuman(id){
		vm.dynamicTableColList = [];
		createFormsService.getDynamicTableColName(id).then(function(data) {
			angular.forEach(data.data, function(value, key) {
				vm.dynamicTableColList.push(value);
			});
		});
	}

	vm.getDynamicsameTableColuman = getDynamicsameTableColuman;
	vm.dynamicsameTableColList = [];
	function getDynamicsameTableColuman(id){
		vm.dynamicsameTableColList = [];
		createFormsService.getDynamicTableColName(id).then(function(data) {
			angular.forEach(data.data, function(value, key) {
				vm.dynamicsameTableColList.push(value);
			});
		});
	}


/*'colRef':{
							 'colRefence': false,
							 'masterTable':'',
							 'masterCol':'',
							 'masterRefCol':'',
							 'PossibleValue':[]
						}*/
  function setTableColumnRef(set, current_field){
  	console.log("set ",set)
  	console.log("current fielf ", current_field)

	 /* set.PossibleValue = [];
		set.MasterRef = !set.MasterRef;*/
			
			$ionicPopup.show({
	    templateUrl: 'templates/tableColRefPopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {	          
						
	        } 
	      },
	      {
	        text: '<b>Save</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	         
	        }
	      }
	    ]
	  });
			vm.masterTableNames = [];
  		createFormsService.getTableJSON().then(function(data) {
				angular.forEach(data.rows, function(value, key) {
					vm.masterTableNames.push(value);
				});
				createFormsService.getTableMaster().then(function(masterData) {
					angular.forEach(masterData.rows, function(mdata, key) {
						vm.masterTableNames.push(mdata);
					});
				})
			});
  }









	function populate(set){
		var index = "";
		vm.valuesTables = [];
		var dynamicTableName = "";
		if(tableid != "")
			dynamicTableName = constantService.tableName+tableid;
		else
			dynamicTableName = set.Settings[4].MasterData[0].TableName;
		var count = 0;
		angular.forEach(set.Settings[3].PossibleValue[0].columns, function(data, key) {
			if(data.value == set.Settings[4].MasterData[0].column){
				index = key;
				if(vm.OtherRef){
					vm.autoIncrIndex = index;
					count = vm.autoIncrement != undefined?vm.autoIncrement:0;
				}
			}
		})
		for(var z = 0; z < set.Settings[3].PossibleValue[0].rows.length; z++){
			angular.forEach(set.Settings[3].PossibleValue[0].rows[z].cells, function(value , key){
				if(vm.autoIncrIndex == key){
					count = count + 1 ;
					set.Settings[3].PossibleValue[0].rows[z].cells[key].value = count;
				}
			})
		}
		
		/*createFormsService.getColumnValues(set.Settings[4].MasterData[0].ColumnName, dynamicTableName).then(function(record) {*/
		createFormsService.getColumnValuesCondition(set.Settings[4].MasterData[0].ColumnName, dynamicTableName, set.Settings[4].ConditionData).then(function(record) {

		//createFormsService.getColumnValuesCondition(set.Settings[4].MasterData[0].ColumnName, dynamicTableName, set.Settings[4].ConditionData[0].condColumn, set.Settings[4].ConditionData[0].TableName, set.Settings[4].ConditionData[0].ColumnName, set.Settings[4].ConditionData[0].value).then(function(record) {

			var startPoint = 0;
			var existcells = [];
			for(var i = 0; i < set.Settings[3].PossibleValue[0].rows.length; i++ ){
				if(set.Settings[3].PossibleValue[0].rows[i].cells[index].value != ""){
					startPoint = i+1;
				}
			}
			angular.forEach(record.rows, function(value, key) {
				if(set.Settings[3].PossibleValue[0].rows.length > 0){
					if(startPoint == set.Settings[3].PossibleValue[0].rows.length){
						var cells = [];
						for(var j = 0; j < set.Settings[3].PossibleValue[0].rows[0].cells.length; j++ ){
							if(index == j){
								cells.push({'value': value[set.Settings[4].MasterData[0].ColumnName]});
							}else if(vm.autoIncrIndex == j){
								count = count + 1;
								cells.push({'value': count});
							}else{
								cells.push({'value': ''});
							}
						}
						set.Settings[3].PossibleValue[0].rows.push({'cells': cells});
						startPoint++;
					}else{
						var cells = [];
						var m = 0;
						for(var k = 0; k < set.Settings[3].PossibleValue[0].rows[0].cells.length; k++ ){
							if(index == k){
								cells.push({'value': value[set.Settings[4].MasterData[0].ColumnName]});
							}else if(vm.autoIncrIndex == j){
								count = count + 1;
								cells.push({'value': count});
							}else{	
								cells.push({'value': set.Settings[3].PossibleValue[0].rows[startPoint].cells[m].value});
							}
							m++;
						}
						set.Settings[3].PossibleValue[0].rows[startPoint]['cells']= cells;
						startPoint++;
					}
				}else{
					var cells = [];
					for(var n = 0; n < set.Settings[3].PossibleValue[0].rows[0].cells.length; n++ ){
						if(index == n){
							cells.push({'value': value[set.Settings[4].MasterData[0].ColumnName]});
						}else if(vm.autoIncrIndex == j){
								count = count + 1;
								cells.push({'value': count});
						}else{								
							cells.push({'value': ''});
						}
					}
					set.Settings[3].PossibleValue[0].rows.push({'cells': cells});
				}

			})
		})
	}

	function masterCheckChange(set, current_field){
		set.PossibleValue = [];
		set.MasterRef = !set.MasterRef;
		if(!set.MasterRef){
			$ionicPopup.show({
	    templateUrl: 'templates/tablePopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	          set.MasterRef = false;
	        } 
	      },
	      {
	        text: '<b>Populate</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	          populate(current_field);
	        }
	      }
	    ]
	  });
			set.MasterRef = true;
			vm.masterTableNames = [];
  		createFormsService.getTableJSON().then(function(data) {
				angular.forEach(data.rows, function(value, key) {
					vm.masterTableNames.push(value);
				});
				createFormsService.getTableMaster().then(function(masterData) {
					angular.forEach(masterData.rows, function(mdata, key) {
						vm.masterTableNames.push(mdata);
					});
				})
				vm.masterTableNames.push({"rowid":'',tableName:'Other'});
			});
		}else{
			set.MasterRef = false;
			set.MasterData[0].TableName = '';
			set.MasterData[0].ColumnName = '';
		}
	}


	function setColCondition(set, current_field){
		console.log("current_field ", current_field)
		current_field.Settings[5].ColCondValue = ''
		current_field.Settings[5].ColCondValue['resultCol'] = '';
		current_field.Settings[5].ColCondValue.Condtion = [];
		getAllColumnList(current_field);
		console.log("set in table condition ", current_field)				
		$ionicPopup.show({
	    templateUrl: 'templates/tableColConditionPopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	          
	        } 
	      },
	      {
	        text: '<b>Ok</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	         
	        }
	      }
	    ]
	  });
			
		vm.masterTableNames = [];
		
		
	}
	
	function setCondition(set, value, index){		
		console.log("set ", set)
		if(set.Condtion == undefined){
			set.Condtion = [];
		}
			
		set.Condtion[index] = value;
		
		
				console.log("setrttt ", set)
	}



/*'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				}],
				'Restrict': "",
				'ColmRestrict': [],
				'ColCondition': false,
				'ColCondValue': {"Condtion":[]},*/






  vm.colList = [];
	function  getAllColumnList(current_field){
		console.log("jo ", current_field)
		angular.forEach($scope.formFields, function(value, key){
			if(value.Type == "table" && current_field.Name == value.Name){			
			console.log("value ", value)			
				angular.forEach(value.Settings[3].PossibleValue[0].columns, function(value1, key1){
					var count = 0;
					angular.forEach(vm.colList, function(val2, key2){
						if(val2.value1 != value1.value1 || val2.value != value1.value){
							count++
						}
					})
					if(count == vm.colList.length){
						value.Settings[5].ColmRestrict.push({column: value1.value1, depts:[]});
						vm.colList.push({'value':value1.value, 'value1':value1.value1});
					}
				});
			}
		})
		getDepartments('true');
	}

	function addColDept(value, colname, set){		
		angular.forEach(set.ColmRestrict, function(val1, key1){
			if(colname == val1.column){
				if(val1.depts.indexOf(value) == -1) {					
		      val1.depts.push(value);
		    }else{
		    	 val1.depts.splice(val1.depts.indexOf(value), 1)
		    }
			}
		})
	}

	function addColTimer(value,  curentField){		
		angular.forEach(curentField.Settings[3].PossibleValue[0].columns, function(val1, key1){
			if(value.value == val1.value){
				if(val1.showTime != undefined){
					val1.showTime = true;
				}else{
					val1['showTime'] = true;
				}
			}
		});
	}

	function addColDate(value,  curentField){		
		angular.forEach(curentField.Settings[3].PossibleValue[0].columns, function(val1, key1){
			if(value.value == val1.value){
				if(val1.showDate != undefined){
					val1.showDate = true;
				}else{
					val1['showDate'] = true;
				}
			}
		});
	}




	function getDepartments(rest){
		if(rest){
			vm.departments = [];
			workflowService.getDepartments().then(function(data) {
				angular.forEach(data.rows, function(value, key){
					vm.departments.push(value);
				})
			});
		}
	}

	function selectCondTable(value, colIndex){
		var the_string = "condColumnNames"+colIndex;
	  var model = $parse(the_string);
	  model.assign($scope, true);
		$scope[the_string] = [];
		selecttableName = value;
		angular.forEach(vm.masterTableNames, function(data, key) {
			//Modified to dynamic table
			if(data.tableName == value){
				if((value.substring(0,7)) == 'DF_MSTR'){
					createFormsService.getMasterByName(data.tableName).then(function(record) {
						angular.forEach(record.rows, function(value, key) {
							$scope[the_string].push(value.colName);
						})
					});
				}else{
					tableid = data.rowid;
					createFormsService.getTableByName(data.rowid).then(function(record) {
						angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
							for(var i=0;i<value.Settings.length;i++){
								if(value.Settings[i].Name == 'Internal Name'){
									$scope[the_string].push(value.Settings[i].Value);
								}
							}
						})
					});
				}
			}
		});
		//colIndex = colIndex+1;
	}

	$scope.getValues = function (index) {
	  return $scope["condColumnNames"+index];
	}
	$scope.getDataValues = function (index) {
	  return $scope["dataValues"+index];
	}
	function changingColumanName(val){


		console.log("asd ", val)
	}
	function selectCondColumn(value, valIndex){
		var dataval = "dataValues"+valIndex;
	  var model = $parse(dataval);
	  model.assign($scope, true);
		$scope[dataval] = [];
		//vm.dataValues =[];
		createFormsService.getColumnValues(value, selecttableName).then(function(record) {
			angular.forEach(record.rows, function(data, key) {
				$scope[dataval].push(data[value]);
			})

		})
		//valIndex = valIndex+1;
	}

	function addCondition(current_field){
		current_field.Settings[4].ConditionData.push({
					'condColumn':'',
					'condTableName': '',
					'condColumnName': '',
					'condValue': '',
					'condChange': '',
					'condEquals': 'IN'
				})
		vm.showCondition = true;
	}

	function addSelectCondition(current_field){
		current_field.Settings[2].ConditionData.push({
					'condColumn':'',
					'condTableName': '',
					'condColumnName': '',
					'condValue': '',
					'condChange': '',
					'condEquals': 'IN'
				})
		vm.showCondition = true;
	}

	function cancel(){
		notificationService.confirm('Are you sure want to cancel', 'Yes', 'No',function(){
      $scope.formFields = [];
			vm.formName = "";
			vm.selectedTable="";
			vm.showButtons = false;
			$scope.current_field = [];
			$('#addFieldTab_lnk').tab('show');
    },function(){
    })
	}

}

$(function() {
	// Code here
	var dh = $(document).height();
	$('#sidebar-tab-content').height(dh - 115);
	$('#main-content').height(dh - 10);
});