(function() {
	'use strict';

	angular
		.module('amaraja')
		.controller('getAllFormsController', getAllFormsController);

	getAllFormsController.$inject = ['workflowService', '$cordovaSQLite', 'notificationService', '$state', '$scope', '$q'];

	function getAllFormsController(workflowService, $cordovaSQLite, notificationService, $state, $scope, $q) {
		var vm = this;
		var workflowNames = [];
		var promiseColl = [];
		var previousValue = "";
		var cond = true;

		workflowService.getAllDetails().then(function(data) {
			vm.workflowNames = [];
			angular.forEach(data.rows, function(value, key) {
				if(key > 0 && cond){
					previousValue = data.rows[key-1].workflowName;
					cond = false;
				}
				if(value.workflowName == previousValue){
					value.workflowName = "";
					value.levels = "";
				}else{
					cond = true;
				}
      	vm.workflowNames.push(value);
      });
     
    })

	}

})();