angular.module('amaraja').controller('getFormsController', getFormsController);

getFormsController.$inject = ['$scope', '$rootScope', '$location', '$q', '$state', '$http', '$window', 'createFormsService', 'getFormsService', 'notificationService', '$ionicSideMenuDelegate', 'constantService', '$timeout', 'ionicTimePicker', 'ionicDatePicker', 'SendEmailService', 'getFormsRecordsService', 'recordDetailsService'];

function getFormsController($scope, $rootScope, $location, $q, $state, $http, $window, createFormsService, getFormsService, notificationService, $ionicSideMenuDelegate, constantService, $timeout, ionicTimePicker, ionicDatePicker, SendEmailService, getFormsRecordsService, recordDetailsService) {
  var vm = this;
  vm.getTableJSON = getTableJSON;
  vm.getTableByName = getTableByName;
  vm.saveFormsData = saveFormsData;
  vm.valuationDatePickerOpen = valuationDatePickerOpen;
  vm.getValueDynamicaly = getValueDynamicaly;
  vm.dynamicLoad = dynamicLoad;
  vm.ColumanCondition = ColumanCondition;
  vm.getTime = getTime;
  vm.getDate = getDate;
  vm.checkDept = checkDept;
  vm.cancel = cancel;
  vm.dynamicColChange = dynamicColChange;
  // vm.sendmailNotifiction = sendmailNotifiction;
  vm.removeRow = removeRow;
  vm.logoShow = false;
  vm.hideButtons = false;
  vm.tableName = [];
  vm.recordName = ''
  vm.tableId = '';
  vm.tableRecord = '';
  vm.TableJson = ''
  vm.record = '';
  vm.returnSelectedId = '';
  vm.serviceMailInformation = '';
  vm.showError = false;
  vm.deptpartment = $window.localStorage.userDept;
  vm.showComment = false;
  vm.showOption = false;
  vm.SqlDateFormate = SqlDateFormate;
  vm.getRecordBasedOnSelectedId = getRecordBasedOnSelectedId;
  vm.notifid = 0;
  vm.isFlamabityOrInspection = false;

  vm.userDept = $window.localStorage.userDept;
  vm.userRole = $window.localStorage.userType;
  if ($state.params.getFormName == "Flammability Report" || $state.params.getFormName == "Inspection Report"|| $state.params.getFormName == "Engg Process Specification for IMM")
    vm.isFlamabityOrInspection = true;


  function getTime(cell, col, celll, tablejson) {
    var timmm = ''
    var ipObj1 = {
      callback: function(val) { //Mandatory
        if (typeof(val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);

          timmm = selectedTime.getUTCHours();
          console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
          if (tablejson != null) {
            cell.value = selectedTime.getUTCHours() + " : " + selectedTime.getUTCMinutes()
            vm.ColumanCondition(col, celll, tablejson);
          } else {
            vm.record[cell] = selectedTime.getUTCHours() + " : " + selectedTime.getUTCMinutes()
          }
        }
      },
      inputTime: 12 * 60 * 60, //Optional
      format: 12, //Optional
      step: 1, //Optional
      setLabel: 'Ok' //Optional
    };
    ionicTimePicker.openTimePicker(ipObj1);
  }


  function getDate(cell, col, celll, tablejson) {
    var ipObj2 = {
      callback: function(val) { //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        if (celll == 'selectdate') {
          vm.record[cell] = dateConvert(new Date(val));
        } else {
          cell.value = dateConvert(new Date(val));
          vm.ColumanCondition(col, celll, tablejson);
        }
      },
      /*disabledDates: [            //Optional
        new Date(2016, 2, 16),
        new Date(2015, 3, 16),
        new Date(2015, 4, 16),
        new Date(2015, 5, 16),
        new Date('Wednesday, August 12, 2015'),
        new Date("08-16-2016"),
        new Date(1439676000000)
      ],*/
      from: new Date(2000, 1, 1), //Optional
      to: new Date(2200, 10, 30), //Optional
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      disableWeekdays: [0], //Optional
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };
    ionicDatePicker.openDatePicker(ipObj2);
  }


  $scope.$watch(function() {
    return $state.params;
  });
  FormName = $state.params.getFormName;
  getTableJSON();

  function getTableJSON() {
    if ($window.localStorage.userDept == '' || $window.localStorage.userDept == undefined)
      $state.go('login');

    vm.tableName = [];
    createFormsService.getTableJSON().then(function(data) {
      angular.forEach(data.rows, function(value, key) {
        vm.tableName.push(value);
        if (value.tableName == FormName) {
          getTableByName(value.rowid);
        }
      });
    });
  }

  function getTableByName(id) {
    vm.selectedTable = id;
    var promiscall = [];
    var deferred = $q.defer();
    $('#border_getform').addClass('border-getForm');
    vm.logoShow = true;
    vm.showOption = true;
    vm.hideButtons = true;
    for (var i = 0; i < vm.tableName.length; i++) {
      if (vm.selectedTable == vm.tableName[i].rowid) {
        vm.selectedTableShow = vm.tableName[i].tableName;
        var matches = vm.selectedTableShow.match(/\b(\w)/g);
        vm.recordName = matches.join('') + '-2017-';
      }
    }

    createFormsService.getTableByName(vm.selectedTable).then(function(record) {
      if (record.rows[0].tableRecord != '') {
        vm.tableColName = [];
        var tableData = [];
        var PossibleValue = [];
        var promiseColl = [];
        var data = {};
        var ColCondValue = "";
        var autofillUser = ''
        vm.TableJson = JSON.parse(record.rows[0].tableRecord).data;

        promiscall.push(
          angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
            data.Name = value.Name;
            data.Type = value.Type;
            if (value.setTime) {
              data.setTime = value.setTime;
            }
            angular.forEach(value.Settings, function(value1, key1) {
              if (value1.Name == "Choice") {
                if (value1.DynamicTableRef) {
                  data.DynamicTableRef = value1.DynamicTableRef;
                  data.DynaicRef = value1.DynaicRef;
                  data.FieldOption = [];
                } else {
                  if (value1.MasterData != undefined && value1.MasterData.length > 0 && value1.MasterData[0].TableName == 'DF_MSTR_USERS') {
                    angular.forEach(value1.PossibleValue, function(value2, key2) {
                      PossibleValue.push({
                        "Text": value2.Text,
                        "Text1": value2.Text1
                      });
                    });
                  } else {
                    angular.forEach(value1.PossibleValue, function(value2, key2) {
                      PossibleValue.push({
                        "Text": value2.Text
                      });
                    });
                  }
                  data.FieldOption = PossibleValue;
                }
              } else if (value1.Name == "General Options") {
                if (value1.Remark != '' && value1.Remark != undefined) data.Remark = value1.Remark;
                else value1.Remark = 'false'
                if (value.Type == 'dropdown' && value1.Cavity != '') {
                  data.Cavity = value1.Cavity;
                }

                if (value1.colCondition) {
                  data.colCondition = value1.colCondition;
                  data.ColCondValue = value1.ColCondValue;
                }
                angular.forEach(value1.Options, function(value2, key2) {
                  if (value2.Name == "Required") {
                    data.Required = value2.Value;
                  }
                  if (value2.Name == "Readonly") {
                    data.ReadOnly = value2.Value;
                  }

                });
                data.Restrict = [];
                angular.forEach(value1.Restrict, function(value2, key2) {
                  if (value2) {
                    data.Restrict.push(key2);
                  }
                });
                if (value.Type == 'text' && value1.AutoFill != '' && value1.AutoFill != undefined) {
                  if (value1.AutoFill == $window.localStorage.userDept + " " + $window.localStorage.userType) {
                    data.value = $window.localStorage.empCode + " " + $window.localStorage.empName;
                  } else if (value1.AutoFill.includes('QA') && FormName.toUpperCase().includes("IN-PROCESS") && $window.localStorage.userDept == 'QA') {
                    data.value = $window.localStorage.empCode + " " + $window.localStorage.empName;
                  }
                }

              } else if (value1.Name == "Internal Name") {
                vm.tableColName.push(value1.Value);
                data.InternalName = value1.Value;
                if (value1.Value == 'specno' && value.Type == 'text') {
                  data.value = 'PLD-PRS-';
                } else if (value1.Value == 'applicablespecification') {
                  data.value = 'MPPW-PLS-TSP-004 (UL94)';
                } else if (value1.Value == 'fromcustomer' && value.Type == 'text') {
                  data.value = 'MIL-2 PLD';
                }
              } else if (value1.Name == "Column Span") {
                data.ColSpan = value1.Value;
              } else if (value1.Name == "Base64") {
                data.base64 = value1.Value;
              } else if (value1.Name == "Table") {
                data.Table = value1.PossibleValue[0];
                vm.valuationDate = new Date();
                vm.valuationDatePickerIsOpen = false;
                vm.opens = [];

                $scope.$watch(function() {
                  return vm.valuationDatePickerIsOpen;
                }, function(value) {
                  vm.opens.push("valuationDatePickerIsOpen: " + value + " at: " + new Date());
                });
              }
            })

            tableData.push(data);
            PossibleValue = [];
            data = {};
          })
        )
        $q.all(promiscall).then(function(datavalue) {
          vm.tableRecord = tableData;
          dynamicLoad(vm.tableRecord);
          vm.showComment = true;
          vm.getRecordBasedOnSelectedId($state.params);

        })
      }
    });

  }

  function settableLeakTesting() {
    if (FormName.toUpperCase().includes('LEAK TESTING') && $state.params.type.toUpperCase() == "RETURN") {
      angular.forEach(vm.tableRecord, function(value, key) {
        if (value.InternalName == 'process') {
          getValueDynamicaly(value, vm.tableRecord, vm.record, vm.record['process'])
        } else if (value.Type == 'table') {
          setColumnInOrder(value.Table.rows, vm.record, value.InternalName)
        }

      });
    }
    if (FormName.toUpperCase().includes('DAILY') && $state.params.type.toUpperCase() == "RETURN") {
      angular.forEach(vm.tableRecord, function(value, key) {
        if (value.InternalName == 'machine') {
          getValueDynamicaly(value, vm.tableRecord, vm.record, vm.record['machine'])
        } else if (value.Type == 'table') {

          setColumnInOrder(value.Table.rows, vm.record, value.InternalName)
        }
      });
    }
  }

  function saveFormsData(record) {
    console.log('record saave', record);
    if (record.recordName != undefined) {
      var deptName = vm.deptpartment;
      console.log("===========================", FormName.toUpperCase().includes("LEAK TEST"), $window.localStorage.userDept, vm.approved, $window.localStorage.userType)
      if (FormName.toUpperCase().includes("LEAK TEST") && $window.localStorage.userDept == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Approver') {
        deptName = 'QA';
      }
      //updating record preeating record
      var flag = true;
      var tabcount = 0;
      console.log('enter save formsdata');
      angular.forEach(vm.tableRecord, function(val, key) {
        if (val.Type == 'table') {
          angular.forEach(record[val.InternalName].rows, function(val1, key1) {
            var col = '';
            var insertCol = "'" + vm.subtableId + "',";
            angular.forEach(val1.cells, function(val2, key2) {
              col = col + 'column' + (parseInt(key2) + parseInt(1)) + "='" + val2.value + "',";
              insertCol = insertCol + "'" + val2.value + "',"
              if (key2 == val1.cells.length - 1) {
                var query = "UPDATE " + constantService.innerTableName + vm.selectedTable + '_' + tabcount + " SET " + col.trim().slice(0, -1) + " WHERE tableid='" + vm.subtableId + "' and column1='" + val1.cells[0].value + "'";
                if (val1.cells[0].value != undefined) {
                  if (tabcount == '1') {
                    if (flag) {
                      flag = false;
                      var query4 = "delete from " + constantService.innerTableName + vm.selectedTable + '_1' + " WHERE tableid='" + vm.subtableId + "' and column1=''";
                      getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {});
                    }
                    var query1 = "Select * From " + constantService.innerTableName + vm.selectedTable + '_1' + " WHERE tableid='" + vm.subtableId + "' and column1='" + val1.cells[0].value + "'";
                    getFormsRecordsService.getCompletedRecordinOneLevel(query1).then(function(reqVal1) {
                      if (reqVal1.data.length > 0) {
                        var query3 = "UPDATE " + constantService.innerTableName + vm.selectedTable + '_1' + " SET " + col.trim().slice(0, -1) + " WHERE tableid='" + vm.subtableId + "' and column1='" + val1.cells[0].value + "'";
                        getFormsRecordsService.getCompletedRecordinOneLevel(query3).then(function(reqVal) {});
                      } else {
                        var query2 = "insert into  " + constantService.innerTableName + vm.selectedTable + '_1' + " values (" + insertCol.trim().slice(0, -1) + ")";
                        getFormsRecordsService.getCompletedRecordinOneLevel(query2).then(function(reqVal) {});

                      }
                    });
                  } else {
                    getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {});
                  }

                  //var query = "insert into DF_TBL_APR (approved, tableName,recordId,deptName, formName,updatedBy,status) values ('"+vm.approved+"', '"+vm.selectedTable+"', '"+''+"', '"+vm.deptpartment+"','"+FormName+"','"+ vm.deptpartment+"', 'a' )";
                  var query = "update DF_TBL_APR set approved='" + vm.approved + "', deptName='" + deptName + "' where formName = '" + FormName + "' and recordId ='" + vm.returnSelectedId + "'";
                  console.log("query ", query)
                  getFormsService.updateApprovalTable(query).then(function(reqData) {})

                  if (vm.comment != '' && vm.comment != undefined) {
                    var query = "INSERT INTO DF_TBL_FMH (userName , userId , role , formLevel , formName, status, comment ,recordId) VALUES ('" + $window.localStorage.empName + "','" + $window.localStorage.empCode + "','" + $window.localStorage.userDept + "','1','" + vm.selectedTable + "','" + vm.approved + "','" + vm.comment + "','" + '' + "')"
                    console.log("query ", query)
                    getFormsService.updateApprovalTable(query).then(function(reqData) {})
                  }
                }
              }
            });
          });


          tabcount++;
        }
      })
      notificationService.hideLoad();
      notificationService.alert('', 'Data insertd Successfully', function() {
        console.log("Notification 1:", record);
        getFormsRecordsService.getApproverDetails(vm.selectedTable, vm.returnSelectedId).then(function(data) {
          vm.serviceMailInformation = {
            serviceRecordId: vm.returnSelectedId,
            serviceFirstPerson: data.firstPerson,
            serviceSecondPerson: data.secondPerson,
            serviceThirdPerson: data.thirdPerson
          };
          recordDetailsService.getStatusInformation(vm.selectedTable, vm.tableRowId).then(function(response) {
            SendEmailService.sendmailNotifiction(vm.serviceMailInformation, response);
          });
        });
        // sendmailNotifiction(record);
        $rootScope.$emit("CallParentMethod", {});
      });

    } else {

      console.log('enter save formsdata 2');
      vm.showError = false;
      angular.forEach(vm.tableRecord, function(valValue, valKey) {
        if (valValue.Name.includes('Supervisor')) {
          console.log("Notification 2:", "DF_TBL_PGR_" + vm.selectedTable);
          constantService.selectedTableName = "DF_TBL_PGR_" + vm.selectedTable;
          vm.notifid = 2;
          // sendmailNotifiction(record[valValue.InternalName])
        }
        if (valValue.Required == true) {
          if (record[valValue.InternalName] == undefined || record[valValue.InternalName] == '') {
            vm.showError = true;
            return false;
          }
        }
      })

      if (!vm.showError) {
        var promiscall = [];
        var deferred = $q.defer();
        var colums = '';
        var tableValues = '';
        var InnertableData = [];
        var tableCount = 0;
        notificationService.showSpinner();
        angular.forEach(vm.tableColName, function(value, key) {
          if (record[value] == undefined) {
            tableValues = tableValues + " " + "''" + ",";
            colums = colums + " " + value + ",";
          } else {
            angular.forEach(vm.tableRecord, function(value1, key1) {
              if (value1.InternalName == value) {
                if (value1.Type == "table") {
                  InnertableData.push(value1);
                  promiscall.push(
                    getFormsService.getTableRecordCount(vm.selectedTable).then(function(rescount) {
                      if (rescount.rows.length > 0) {
                        colums = colums + " " + value + ",";
                        tableValues = tableValues + " " + (parseInt(rescount.rows[0]['dbcount']) + 1) + ",";
                        tableCount = parseInt(rescount.rows[0]['dbcount']) + 1;
                        vm.tableId = parseInt(rescount.rows[0]['dbcount']) + 1;
                      }
                    })
                  )
                } else if (value1.Type == "checkbox") {
                  tableValues = tableValues + " " + "'" + JSON.stringify(record[value]) + "',";
                  colums = colums + " " + value + ",";
                } else if (value1.Type == "date") {
                  tableValues = tableValues + " " + "'" + DBdateConvert(record[value]) + "',";
                  colums = colums + " " + value + ",";
                } else {
                  tableValues = tableValues + " '" + record[value] + "',";
                  colums = colums + " " + value + ",";
                }
              }
            });
          }
        })
        $q.all(promiscall).then(function(datavalue) {
          colums = "recordName, " + colums;
          tableValues = "'" + vm.recordName + tableCount + "'," + tableValues;
          $rootScope.codeNameOfRecord = vm.recordName;
          var recordIdForMailService = '';
          // adding data to respective form tables ex. _2096
          getFormsService.addTableRecord(vm.selectedTable, colums.trim().slice(0, -1), tableValues.trim().slice(0, -1)).then(function(data) {
            recordIdForMailService = data.data.CreateDynamicTableResult[0].rowid;
            getFormsService.getWorkFlowLevel(vm.selectedTable).then(function(result) {
              if (result.data != '' && (result.data[0].lev.replace('level', '').trim() > 1 || vm.selectedTableShow.toUpperCase().includes("IN-PROCESS"))) {
                var depts = vm.deptpartment;
                if ((vm.selectedTableShow.toUpperCase().includes("PACK NOTE") || vm.selectedTableShow.toUpperCase().includes("IN-PROCESS")) && $window.localStorage.userDept == 'Production' && vm.approved == 'approved') {
                  depts = 'QA';
                  if (vm.selectedTableShow.toUpperCase().includes("IN-PROCESS SET UP APPROVAL"))
                    depts = 'Production';
                  console.log('depts', depts);
                } else if (vm.selectedTableShow.toUpperCase().includes("SHIFT") && $window.localStorage.userDept == 'Production' && vm.approved == 'approved') {
                  vm.approved = 'done'
                }
                var query = "insert into DF_TBL_APR (approved, tableName,recordId,deptName, formName,updatedBy,status) values ('" + vm.approved + "', '" + vm.selectedTable + "', '" + data.data.CreateDynamicTableResult[0].rowid + "', '" + depts + "','" + FormName + "','" + vm.deptpartment + "', 'a' )";
                getFormsService.updateApprovalTable(query).then(function(reqData) {})

                if (vm.comment != '' && vm.comment != undefined) {
                  var query = "INSERT INTO DF_TBL_FMH (userName , userId , role , formLevel , formName, status, comment ,recordId) VALUES ('" + $window.localStorage.empName + "','" + $window.localStorage.empCode + "','" + $window.localStorage.userDept + "','1','" + vm.selectedTable + "','" + vm.approved + "','" + vm.comment + "','" + data.data.CreateDynamicTableResult[0].rowid + "')"
                  getFormsService.updateApprovalTable(query).then(function(reqData) {})
                }
              } else if (result.data != '' && result.data[0].lev.replace('level', '').trim() > 0 && $window.localStorage.userType == 'Operator') {
                console.log('vm.approved yea', vm.approved);
                if (vm.approved == undefined)
                  vm.approved = 'approved';
                if (vm.selectedTableShow.toUpperCase().includes("SHIFT") && $window.localStorage.userDept == 'Production' && vm.approved == 'approved') {
                  if (vm.approved != "Draft")
                    vm.approved = 'done'
                }
                if (vm.selectedTableShow.toUpperCase().includes("PACK NOTE") && $window.localStorage.userDept == 'Production' && vm.approved == 'approved') {
                  vm.deptpartment = 'QA'
                }
                var query = "insert into DF_TBL_APR (approved, tableName,recordId,deptName, formName,updatedBy,status) values ('" + vm.approved + "', '" + vm.selectedTable + "', '" + data.data.CreateDynamicTableResult[0].rowid + "', '" + vm.deptpartment + "','" + FormName + "','" + vm.deptpartment + "', 'a' )";

                getFormsService.updateApprovalTable(query).then(function(reqData) {})

                if (vm.comment != '' && vm.comment != undefined) {

                  var query = "INSERT INTO DF_TBL_FMH (userName , userId , role , formLevel , formName, status, comment ,recordId) VALUES ('" + $window.localStorage.empName + "','" + $window.localStorage.empCode + "','" + $window.localStorage.userDept + "','1','" + vm.selectedTable + "','" + vm.approved + "','" + vm.comment + "','" + data.data.CreateDynamicTableResult[0].rowid + "')"
                  getFormsService.updateApprovalTable(query).then(function(reqData) {})
                }
              }
            })
            var inTbleValues = " ";
            var subid = 0;
            angular.forEach(InnertableData, function(value, key) {
              var inTblecolm = " ";
              angular.forEach(value.Table.columns, function(value1, key1) {
                if (!value1.cavity) {
                  inTblecolm = inTblecolm + value1.value + ",";
                } else {}

              })
              angular.forEach(value.Table.rows, function(value1, key1) {
                  angular.forEach(value1.cells, function(value2, key2) {
                    if (!value.Table.columns[key2].cavity) {
                      inTbleValues = inTbleValues + "'" + value2.value + "',";
                    } else {
                      inTbleValues = inTbleValues.trim().slice(0, -2) + "_" + value2.value + "',";
                    }

                  })
                  getFormsService.addInnerTableRecord(vm.selectedTable, subid, vm.tableId, inTblecolm.trim().slice(0, -1), inTbleValues.trim().slice(0, -1)).then(function(req) {})
                  inTbleValues = "";
                })
                ++subid;
            })
          });
          notificationService.hideLoad();
          notificationService.alert('', 'Data insertd Successfully', function() {
            console.log("Notification 3:", constantService.selectedTableName);


            if (vm.approved != "draft") {

              getFormsRecordsService.getApproverDetails(vm.selectedTable, recordIdForMailService).then(function(data) {
                vm.serviceMailInformation = {
                  serviceRecordId: recordIdForMailService,
                  serviceFirstPerson: data.firstPerson,
                  serviceSecondPerson: data.secondPerson,
                  serviceThirdPerson: data.thirdPerson
                };
                recordDetailsService.getStatusInformation(vm.selectedTable, recordIdForMailService).then(function(response) {
                  SendEmailService.sendmailNotifiction(vm.serviceMailInformation, response);
                });
              });
            }



            // sendmailNotifiction(record);
            $rootScope.$emit("CallParentMethod", {});
          });

          deferred.resolve(datavalue);
        }, function(error) {
          deferred.reject(error);
        });
        return deferred.promise;
      }

    }
  }

  function valuationDatePickerOpen($event) {
    if ($event) {
      $event.preventDefault();
      $event.stopPropagation(); // This is the magic
    }
    this.valuationDatePickerIsOpen = true;
  }

  function getValueDynamicaly(tablecol, tableRecord, record, recorvalue) {
    var flag = true;
    if (tablecol.InternalName != constantService.partNO && $window.localStorage.userDept != 'Engineering') {
      console.log('1');
      if (tablecol.InternalName == constantService.shift) {
        console.log('1 1');
        var hoursCol = 0;
        angular.forEach(tableRecord, function(value, key) {
          if (value.Type == 'table') {
            var tFlag = false;
            angular.forEach(value.Table.columns, function(value1, key1) {
              if (value1.value1 == 'Hours') {
                tFlag = true;
                hoursCol = key1;
              }
            });
            if (tFlag) {
              for (i = 0; i < 8; i++) {
                if (value.Table.rows[i] != undefined) {
                  value.Table.rows[i].cells[hoursCol].value = constantService[recorvalue][i]
                } else {
                  var cells = [];
                  for (j = 0; j < value.Table.columns.length; j++) {
                    if (hoursCol == j) {
                      cells.push({ 'value': constantService[recorvalue][i] })
                    } else {
                      cells.push({ 'value': '' })
                    }
                  }
                  value.Table.rows.push({ "cells": cells })
                }
              }
            }
          }
        });
      } else if (tablecol.InternalName == 'process' && $window.localStorage.userDept != 'Engineering') {

        console.log('1 2');
        angular.forEach(tableRecord, function(value, key) {
          if (value.DynamicTableRef && value.DynaicRef.RefCol == tablecol.InternalName) {
            var query = "select * From  DF_TBL_PGR_" + value.DynaicRef.TableId + " where " + tablecol.InternalName + " like '" + record[tablecol.InternalName] + "'";
            constantService.selectedTableName = "DF_TBL_PGR_" + value.DynaicRef.TableId;
            getFormsService.getTableRecordBasedOnQuery(query).then(function(dynResponse) {
              if (dynResponse.data.length > 0) {
                var fflag = true;
                var cells = []
                var flag2 = true;
                if (FormName.toUpperCase().includes('MATERIAL PRE')) {
                  angular.forEach(tableRecord, function(value2, key2) {
                    if (value2.Type == 'table') {
                      angular.forEach(dynResponse.data, function(value5, key5) {
                        getFormsService.getInnerTable(tablecol.DynaicRef.TableId, dynResponse.data[key5][value2.InternalName], 0, FormName).then(function(dyntabResponse) {
                          if (fflag) {
                            vm.record[value2.InternalName].rows = [];
                            fflag = false;
                          }
                          angular.forEach(dyntabResponse.data, function(value3, key3) {
                            angular.forEach(vm.record[value2.InternalName].columns, function(value4, key4) {
                              if (flag2) {
                                value4.colRef.PossibleValue = [];
                                flag2 = false;
                              }
                              if (value4.value != 'rowid' && value4.value != 'tableid' && key3 == 0 && key5 == dynResponse.data.length - 1) {
                                value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                                cells.push({ 'value': value3[value4.value] != undefined ? value3[value4.value] : '' })
                              } else {
                                if (value4.colRef.PossibleValue.indexOf(value3[value4.value]) == -1) {
                                  value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                                }
                              }

                            });
                            if (cells.length > 0) vm.record[value2.InternalName].rows.push({ 'cells': cells });
                            cells = [];
                          });
                        });
                      });

                    }
                  });
                } else {
                  angular.forEach(tableRecord, function(value2, key2) {
                    if (value2.Type == 'table') {
                      console.log("dynResponse   ", dynResponse)
                      getFormsService.getInnerTable(tablecol.DynaicRef.TableId, dynResponse.data[dynResponse.data.length - 1][value2.InternalName], 0, FormName).then(function(dyntabResponse) {
                        console.log("dynResponse11   ", dyntabResponse)
                        vm.record[value2.InternalName].rows = [];
                        var cells = []
                        angular.forEach(dyntabResponse.data, function(value3, key3) {
                          angular.forEach(vm.record[value2.InternalName].columns, function(value4, key4) {

                            if (FormName.toUpperCase().includes('MATERIAL PRE')) {

                              if (key3 == 0 && key4 == 0) value4.colRef.PossibleValue = [];
                              if (value4.value != 'rowid' && value4.value != 'tableid' && key3 == 0) {
                                console.log("----aaa query value3[value4.value]  ", value3[value4.value])
                                value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                                cells.push({ 'value': value3[value4.value] != undefined ? value3[value4.value] : '' })
                              } else {
                                value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                              }




                            } else {
                              if (key3 == 0 && key4 == 0) value4.colRef.PossibleValue = [];
                              if (value4.value != 'rowid' && value4.value != 'tableid') {
                                value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                                cells.push({ 'value': value3[value4.value] != undefined ? value3[value4.value] : '' })
                              }
                            }
                          });
                          if (cells.length > 0) vm.record[value2.InternalName].rows.push({ 'cells': cells });
                          cells = [];
                        });
                      });
                    }
                  });
                }




              }
            });
          } else {
            if (FormName.toUpperCase().includes('LEAK TESTING')) {
              if (value.Type == 'table') {
                if (value.InternalName.includes(((vm.record.process).split(" ")[0]).toLowerCase())) {
                  value.Restrict[0] = "Production";
                } else {
                  value.Restrict[0] = "Engineering";
                }
              } else if (value.Type == 'dropdown' || value.Type == 'text') {
                if ((value.InternalName.toUpperCase()).includes('BUSH') || (value.InternalName.toUpperCase()).includes('LEAK') || (value.InternalName.toUpperCase()).includes('GUMM')) {
                  if (value.InternalName.includes(((vm.record.process).split(" ")[0]).toLowerCase())) {
                    value.Restrict[0] = "Production";
                  } else {
                    value.Restrict[0] = "Engineering";
                  }
                }
              }
            } else if (FormName.toUpperCase().includes('DAILY')) {
              if (value.DynamicTableRef && value.DynaicRef.FilterCol == tablecol.InternalName) {
                var query = "select * from DF_MSTR_PROC where detail = '" + vm.record[tablecol.InternalName] + "';"
                constantService.selectedTableName = "DF_MSTR_PROC";
                getFormsService.getTableRecordBasedOnQuery(query).then(function(dynResponse) {
                  if (dynResponse.data.length > 0) {
                    var query1 = "select * From " + value.DynaicRef.TableId + " where " + tablecol.InternalName + " like '" + dynResponse.data[0].process + "'";
                    constantService.selectedTableName = value.DynaicRef.TableId;
                    getFormsService.getTableRecordBasedOnQuery(query1).then(function(dynResponse1) {
                      value.FieldOption = []
                      angular.forEach(dynResponse1.data, function(valueMac, keyMac) {
                        value.FieldOption.push({ "Text": valueMac.machine })
                      })
                    });
                  }
                });
              }
            }
          }
        });
      } else if (tablecol.InternalName == 'drawingno') {

        console.log('1 3');
        var query = "select * From  DF_MSTR_DRWNO WHERE drawingNo='" + record[tablecol.InternalName] + "'";
        constantService.selectedTableName = "DF_MSTR_DRWNO";
        getFormsService.getTableRecordBasedOnQuery(query).then(function(dynResponse) {
          if (dynResponse.data.length > 0) {
            vm.record['drawingdesc'] = dynResponse.data[0].descr;
          }
        });
      } else {

        console.log('1 4');
        angular.forEach(tableRecord, function(value, key) {
          if (value.DynamicTableRef && value.DynaicRef.FilterCol == tablecol.InternalName) {

            console.log('1 4 1');
            value.FieldOption = [];
            getFormsService.getBasedOnColumnName(value.DynaicRef.TableId, value.DynaicRef.RefCol, value.DynaicRef.FilterCol, record[tablecol.InternalName], '').then(function(dynResponse) {
              angular.forEach(dynResponse.data, function(value2, key2) {

                value.FieldOption.push({
                  "Text": value2[value.DynaicRef.RefCol]
                });
              })
            });
          }

          if (value.Type == 'table') {
            angular.forEach(value.Table.columns, function(value1, key1) {
              if (value1.colRef != undefined && value1.colRef.referMaster && value1.colRef.filterCol == constantService.machine && value1.colRef.filterCol == tablecol.InternalName) {
                if ((value1.colRef.masterTable).split(' ').length > 0) {
                  angular.forEach(vm.tableName, function(value2, key2) {
                    if (value2.tableName == value1.colRef.masterTable) {
                      value1.colRef.masterTable = 'DF_TBL_PGR_' + value2.rowid
                    }
                  })
                }
                getFormsService.getBasedOnColumnName(value1.colRef.masterTable, value1.colRef.masterCol, value1.colRef.filterCol, record[tablecol.InternalName], 'table').then(function(dynResponse) {
                  value1.colRef.PossibleValue = [];
                  if (FormName.toUpperCase().includes('DAILY')) {

                    angular.forEach(dynResponse.data, function(value4, key4) {
                      if (value4.partno != '' && value4.partno != undefined && dynResponse.data.length - 1 == key4) {

                        value1.colRef.PossibleValue.push(value4.partno);
                      }
                    });
                  } else {
                    angular.forEach(dynResponse.data, function(value4, key4) {
                      if (value4.partno != '' && value4.partno != undefined) {
                        value1.colRef.PossibleValue.push(value4.partno);
                      }
                    });
                  }

                });
              }
            });
          } else if (value.Type == 'dropdown' && value.Cavity != undefined && value.Cavity != '' && parseInt(record[value.InternalName]) > 0) {
            console.log('drop do');
            var cavityCount = 0;
            var statIndex = 0;
            angular.forEach(tableRecord, function(val, key) {
              if (val.Type == 'table' && val.Cavity != '') {
                angular.forEach(val.Table.columns, function(val1, key1) {
                  if (val1.value == value.Cavity) {
                    statIndex = key1 + 1;
                  }
                  if (val1.cavity) {
                    cavityCount++;
                  }
                })
                var count = recorvalue - 1;

                if (cavityCount == 0) {
                  angular.forEach(val.Table.columns, function(val1, key1) {
                    if (val1.value == value.Cavity) {
                      /*var count = recorvalue;  */
                      for (i = 0; i < count; i++) {
                        val.Table.columns.splice(statIndex + cavityCount + i, 0, { 'value1': val.Table.columns[statIndex - 1].value1 + '_' + (i + 1), 'value': 'cavity' + (parseInt(statIndex) + parseInt(i)), 'cavity': true });
                        if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) + 1;
                      }
                      angular.forEach(val.Table.rows, function(val2, key2) {
                        for (i = 0; i < count; i++) {
                          val2.cells.splice(statIndex + cavityCount + i, 0, { 'value': '', 'cavity': true });
                        }
                      });
                    }
                  });
                } else if (cavityCount < count) {
                  var countval = 1;
                  for (i = cavityCount; i < count; i++) {
                    val.Table.columns.splice(parseInt(statIndex) + parseInt(countval), 0, { 'value1': val.Table.columns[statIndex - 1].value1 + '_' + (i + 1), 'value': 'cavity' + parseInt(statIndex) + parseInt(countval) - 1, 'cavity': true });
                    if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) + 1;
                    countval++;
                  }
                  angular.forEach(val.Table.rows, function(val2, key2) {
                    for (i = cavityCount; i < count; i++) {
                      val2.cells.splice(parseInt(statIndex) + parseInt(countval), 0, { 'value': '', 'cavity': true });
                    }
                  });
                } else if (cavityCount > count) {
                  var countval = 1;
                  for (i = cavityCount - count; i > 0; i--) {
                    var tot = statIndex + cavityCount - countval;
                    if (val.Table.columns[tot].cavity) {
                      val.Table.columns.splice(tot, 1);
                      if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) - 1;
                    }
                    countval++;
                  }
                  angular.forEach(val.Table.rows, function(val2, key2) {
                    for (i = cavityCount - count; i > 0; i--) {
                      val2.cells.splice(tot, 1);
                    }
                  });
                }
              }
            });
          } else if (value.Type == 'dropdown' && FormName.toUpperCase().includes("FLAMMABILITY")) {

            console.log('1 4 2');
            if (recorvalue == 'V0 Material') {
              angular.forEach(record, function(fvalue, fkey) {
                if (fkey.includes('2')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    fvalue1.cells[3].value = constantService.V0[fkey1];
                  });
                } else if (fkey.includes('1')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    if (fkey1 != 6 && fkey1 != 0 && fkey1 != 7) fvalue1.cells[3].value = 'T1+T2 ≤ 50';
                    else fvalue1.cells[3].value = ''
                  });
                }
              })
            } else if (recorvalue == 'New V0 Material') {
              angular.forEach(record, function(fvalue, fkey) {
                if (fkey.includes('2')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    fvalue1.cells[3].value = constantService.NEWV0[fkey1];
                  });
                } else if (fkey.includes('1')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    if (fkey1 != 6 && fkey1 != 0 && fkey1 != 7) fvalue1.cells[3].value = 'T1+T2 ≤ 50';
                    else fvalue1.cells[3].value = ''
                  });
                }
              })
            } else if (recorvalue == 'V2 Material') {
              angular.forEach(record, function(fvalue, fkey) {
                if (fkey.includes('2')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    fvalue1.cells[3].value = constantService.V2[fkey1];
                  });
                } else if (fkey.includes('1')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    if (fkey1 != 6 && fkey1 != 0 && fkey1 != 7) fvalue1.cells[3].value = 'T1+T2 ≤ 250';
                    else fvalue1.cells[3].value = ''
                  });
                }
              })
            }
          }
        })
      }
    } else if (tablecol.InternalName == constantService.partNO || tablecol.InternalName == 'customerpartno') {
      vm.showComment = true;
      if ($window.localStorage.userDept == 'Engineering' || tablecol.DynaicRef == undefined) {
        var tablename = '';
        var tablefield = '';
        if (tablecol.InternalName == 'customerpartno') {
          tablename = 'DF_MSTR_PNO_OP'
          tablefield = 'itemCode';
        } else {
          tablename = 'DF_MSTR_PNO_INT';
          tablefield = 'partNo';
        }
        getFormsService.getEngneeRecord(tablename, tablefield, recorvalue).then(function(dynResponse) {
          if (dynResponse.data.length > 0) {
            angular.forEach(tableRecord, function(value, key) {
              if (tablecol.InternalName == 'customerpartno') {
                if (value.InternalName == 'customerpartdesc') {
                  record['customerpartdesc'] = dynResponse.data[dynResponse.data.length - 1]['descr'];
                }
              } else {
                if (value.InternalName == 'partdesc') {
                  record['partdesc'] = dynResponse.data[dynResponse.data.length - 1]['descr'];
                }
              }
            });
          }
        });
      } else {
        getFormsService.getRecordByMachinPatNo(tablecol.DynaicRef.TableId, tablecol.DynaicRef.RefCol, recorvalue, FormName, vm.record['machine']).then(function(dynResponse) {
          var count = -1;
          angular.forEach(tableRecord, function(value, key) {
            angular.forEach(dynResponse.data[dynResponse.data.length - 1], function(value1, key1) {
              if (value.Type != 'table' && value.InternalName == key1 && key1 != 'datee' && key1 != 'approvedby' && key1 != 'preparedby' && key1 != 'machine' && key1 != 'partno' && key1 != 'rowid' && key1 != 'shift' && !key1.toUpperCase().includes('INSPECTOR')) {
                vm.record[key1] = value1;
              } else if (value.Type == 'table' && key1 == value.InternalName && vm.record[key1]) {
                count++;
                getFormsService.getInnerTable(tablecol.DynaicRef.TableId, value1, count, FormName).then(function(dyntabResponse) {
                  vm.record[key1].rows = [];
                  var cells = [];
                  angular.forEach(dyntabResponse.data, function(value2, key2) {
                    angular.forEach(vm.record[key1].columns, function(value3, key3) {
                      if (value3.value != 'rowid' && value3.value != 'tableid') {
                        cells.push({ 'value': value2[value3.value] != undefined ? value2[value3.value] : '' })
                      }
                    });
                    vm.record[key1].rows.push({ 'cells': cells });
                    cells = [];
                  });
                });
              }
            });

            if (value.DynamicTableRef && value.InternalName == "cavitydetails") {
              value.FieldOption = [];
              getFormsService.getBasedOnColumnName(value.DynaicRef.TableId, value.DynaicRef.RefCol, value.DynaicRef.FilterCol, record[tablecol.InternalName], '').then(function(dynResponse) {
                angular.forEach(dynResponse.data, function(value2, key2) {
                  value.FieldOption.push({
                    "Text": value2[value.DynaicRef.RefCol]
                  });
                })
              });
            }
          })
        });
      }
    }

    if (($state.params.getFormName).toUpperCase().includes("CHEMICAL")) {
      if (tablecol.InternalName != constantService.process) {
        switch (recorvalue) {
          case 'Die Coat Chemical Mixing':
            angular.forEach(vm.tableRecord, function(value, key) {
              if (value.Type == 'table') {
                angular.forEach(value.Table.columns, function(value1, key1) {
                  if (key1 < 3) {
                    value1.showColumn = false;
                  } else {
                    value1.showColumn = true;
                  }
                });
              }
            })
            break;
          case 'Gum Mixing':
            angular.forEach(vm.tableRecord, function(value, key) {
              if (value.Type == 'table') {
                angular.forEach(value.Table.columns, function(value1, key1) {
                  if (key1 < 3) {
                    value1.showColumn = true;
                  } else {
                    value1.showColumn = false;
                  }
                });
              }
            })
            break;

          case 'Cork Mixing':
            angular.forEach(vm.tableRecord, function(value, key) {
              if (value.Type == 'table') {
                angular.forEach(value.Table.columns, function(value1, key1) {
                  if (key1 < 3) {
                    value1.showColumn = true;
                  } else {
                    value1.showColumn = false;
                  }
                });
              }
            })
            break;
          default:
        }
      }
    }
  }

  function dynamicLoad(tableRecord) {
    angular.forEach(tableRecord, function(value, key) {
      if (value.DynamicTableRef) {
        getFormsService.getBasedOnColumnName(value.DynaicRef.TableId, value.DynaicRef.RefCol, null, null, null).then(function(dynResponse) {
          angular.forEach(dynResponse.data, function(value2, key2) {
            value.FieldOption.push({
              "Text": value2[value.DynaicRef.RefCol]
            });
          })
        });
      }

      if (value.Type == 'table') {
        if ((FormName.toUpperCase()).includes("SHIFT RELEIVING SHEET")) {

          var query = '';
          value.Table.rows = []
          angular.forEach(value.Table.columns, function(value1, key1) {
            var tablename = '';
            if (value1.colRef != undefined && value1.colRef.referMaster && value1.colRef.filterCol == undefined) {
              tablename = value1.colRef.masterTable;
              angular.forEach(vm.tableName, function(nValue, nKey) {
                if (nValue.tableName == value1.colRef.masterTable) {
                  tablename = nValue.rowid;
                }
              });
              if (isNaN(tablename)) {
                tableeName = tablename;
              } else {
                tableeName = constantService.tableName + tablename;
              }

              query = "select * from " + tableeName + " where rowid in (select max(rowid) from " + tableeName + " group by machine)";
              console.log("query  ", query)
              constantService.selectedTableName = tableeName;
              getFormsService.getTableRecordBasedOnQuery(query).then(function(dynResponse) {
                angular.forEach(dynResponse.data, function(value2, key2) {
                  var cell = [];
                  for (i = 0; i < value.Table.columns.length; i++) {
                    if (i == 0) {
                      cell.push({ 'value': parseInt(key2) + 1 })
                    } else if (i == 1) {
                      cell.push({ 'value': value2.machine })
                      value.Table.columns[i].colRef.PossibleValue.push(value2.machine)
                    } else if (i == 2) {
                      cell.push({ 'value': value2.partno })
                      value.Table.columns[i].colRef.PossibleValue.push(value2.partno)
                    } else if (i == 3) {
                      cell.push({ 'value': value2.partdesc })
                      value.Table.columns[i].colRef.PossibleValue.push(value2.partdesc)
                    } else cell.push({ 'value': '' })

                  }
                  value.Table.rows.push({ 'cells': cell })
                });
              });
            }
          });
        } else {
          angular.forEach(value.Table.columns, function(value1, key1) {
            if (value1.colRef != undefined && value1.colRef.referMaster) {
              getFormsService.getBasedOnColumnName(value1.colRef.masterTable, value1.colRef.masterCol, null, null, 'table').then(function(dynResponse) {
                angular.forEach(dynResponse.data, function(value2, key2) {
                  value1.colRef.PossibleValue.push(value2[value1.colRef.masterCol])
                });
              });
            }
          });

        }

        if ((FormName.toUpperCase()).includes("INSPECTION REPORT") || (FormName.toUpperCase()).includes("SHIFT")) {
          angular.forEach(value.Table.columns, function(value1, key1) {
            if (value1.value == 'column1') {
              value1.showColumn = true;
            } else {
              value1.showColumn = false;
            }
          });
        }
      }
    })
  }

  function ColumanCondition(col, celll, tablejson) {
    var flag = false;
    if (tablejson.colCondition) {
      var colIndex = tablejson.ColCondValue.resultCol;
      var aa = tablejson.ColCondValue.Condtion;

      var index = '';
      if (col.includes('cavity')) {
        index = 10;
      } else {
        index = aa.indexOf(col);
      }
      var TableCavityCount = 1;
      angular.forEach(tablejson.Table.columns, function(cellvalue, cellKey) {
        if (cellvalue.cavity) TableCavityCount++;
      })


      if (index >= 0 && aa.length > 3) {
        var a = celll[aa[0].replace("column", '') - 1].value;
        var b = celll[aa[2].replace("column", '') - 1].value;
        var c = ''
        if (col.includes('cavity')) {
          c = celll[parseInt(col.replace("cavity", ''))].value;
        } else {
          c = celll[aa[4].replace("column", '') - 1].value;
        }
        if (aa[1] == "+/-") {
          if (TableCavityCount == 1) {
            var count = 0;
            angular.forEach(a.split("/"), function(value, key) {
              if (b.split("/").length > 1) {
                if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b.split("/")[1])).toFixed(2) && parseFloat(c.split("/")[key]) >= (parseFloat(value) + parseFloat(b.split("/")[0])).toFixed(2)) {
                  count++;
                } else {
                  celll[tablejson.Remark].value = false
                }
              } else {
                if (parseFloat(c.split("/")[key]) <= parseFloat((parseFloat(value) + parseFloat(b)).toFixed(2)) && parseFloat(c.split("/")[key]) >= parseFloat((value - b).toFixed(2))) {
                  count++;
                } else {
                  celll[tablejson.Remark].value = false
                }
              }
            })
            if (c.split("/").length > a.split("/").length) {
              celll[parseInt(tablejson.Remark) + 3].Value = false;
            } else if (count == a.split("/").length) {
              celll[tablejson.Remark].value = true
            } else if (b.toUpperCase() == 'REF') {
              celll[tablejson.Remark].value = true
            } else {
              celll[tablejson.Remark].value = false
            }
          } else if (TableCavityCount > 1) {
            var cavityStartNumber = 0;
            var flagg = true;
            var count = 0;
            var noofcavityColumn = 0;
            angular.forEach(tablejson.Table.columns, function(cellvalue, cellKey) {
              if (aa[4].replace("column", '') - 1 == cellKey || cellvalue.cavity) {
                if (flagg) {
                  cavityStartNumber = cellvalue.value.replace("column", '') - 1;
                  flagg = false;
                }
                c = celll[cavityStartNumber].value;
                cavityStartNumber++;

                angular.forEach(a.split("/"), function(value, key) {
                  if (b.split("/").length > 1) {
                    if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b.split("/")[1])).toFixed(2) && parseFloat(c.split("/")[key]) >= (parseFloat(value) + parseFloat(b.split("/")[0])).toFixed(2)) {
                      count++;
                    } else {
                      celll[tablejson.Remark].value = false
                    }
                  } else {
                    if (parseFloat(c.split("/")[key]) <= parseFloat((parseFloat(value) + parseFloat(b)).toFixed(2)) && parseFloat(c.split("/")[key]) >= parseFloat((value - b).toFixed(2))) {
                      count++;
                    } else {
                      celll[tablejson.Remark].value = false
                    }
                  }
                })
                noofcavityColumn++;

              }
              var qq = a.split('/').length
              if (cellKey == tablejson.Table.columns.length - 1 && count / (a.split('/').length) == noofcavityColumn) {
                celll[tablejson.Remark].value = true
              } else if (b.toUpperCase() == 'REF') {
                celll[tablejson.Remark].value = true
              } else {
                celll[tablejson.Remark].value = false
              }
            })
          }





        } else if (aa[1] == "+") {
          if ((a - b) == c) {
            celll[tablejson.Remark].value = true
          } else {
            celll[tablejson.Remark].value = false
          }
        } else if (aa[1] == "-") {
          if ((parseInt(a) + parseInt(b) == c)) celll[colIndex.replace("column", '') - 1].value = true
          else celll[tablejson.Remark].value = false
        } else {}
      } else {
        var a = celll[aa[0].replace("column", '')].value;
        var b = celll[aa[2].replace("column", '')].value;
        var time1 = (a.split(":")[0] * 60 * 60) + (a.split(":")[1] * 60);
        var time2 = (b.split(":")[0] * 60 * 60) + (b.split(":")[1] * 60);
        if (time1 >= 0 && time2 >= 0) {
          var settime = (time2 - time1) / (60 * 60);
          var timemin = (settime.toString().split(".")[1]) != undefined ? (settime.toString().split(".")[1]) * (60) / 100 : '00'
          var finalTime = settime.toString().split(".")[0] + ":" + timemin;

          celll[colIndex.replace("column", '')].value = finalTime;
        }
      }
    } else if ((FormName.toUpperCase()).includes("CHEMICAL MIXING")) {
      var colnumber = col.replace('column', '');
      switch (col) {
        case 'column8':
          var colVal = celll[parseInt(colnumber) - 1].value
          var hours = parseInt(colVal.split(':')[0]) + 12
          if (hours > 24) {
            hours = hours - 24;
          }
          celll[parseInt(colnumber)].value = hours + ":" + colVal.split(':')[1]
          break;
        case 'column12':
          var colVal = celll[parseInt(colnumber) - 1].value
          var hours = parseInt(colVal.split(':')[0]) + 12
          if (hours > 24) {
            hours = hours - 24;
          }
          celll[parseInt(colnumber)].value = hours + ":" + colVal.split(':')[1]
          break;
        default:
      }

      /*if(parseInt(colnumber) > 5 && parseInt(colnumber) < 14){
        if(colnumber%2 == 0){
          var a =  celll[parseInt(colnumber)-1].value;
          var b =  celll[parseInt(colnumber)].value;
          var time1 = (a.split(" ")[0]*60*60) + (a.split(" ")[1]*60);        
          var time2= (b.split(" ")[0]*60*60) + (b.split(" ")[1]*60);
          var totaltime =  (time2 - time1);
          if(time1 >= 0 && time2 >= 0){
            if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('STIRRRING') && totaltime < 2700){
              celll[colnumber-1].value = '';
            }else if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('SOAKING') && totaltime > 14400){
              celll[colnumber-1].value = '';
            }else if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('MIXING') && totaltime < 2700){
              celll[colnumber-1].value = '';
            }else { }
          }
        }else{
          var a =  celll[parseInt(colnumber)-2].value;
          var b =  celll[parseInt(colnumber)-1].value;
          var time1 = (a.split(" ")[0]*60*60) + (a.split(" ")[1]*60);        
          var time2= (b.split(" ")[0]*60*60) + (b.split(" ")[1]*60);
          var totaltime =  (time2 - time1);
          if(time1 >= 0 && time2 >= 0){
            if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('STIRRRING') && totaltime > 2700){
              celll[colnumber-1].value = '';
            }else if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('SOAKING') && totaltime > 14400){
              celll[colnumber-1].value = '';
            }else if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('MIXING') && totaltime > 2700){
              celll[colnumber-1].value = '';
            }else { }
          }
        }
      }*/
    } else if ((FormName.toUpperCase()).includes("FLAMMABILITY")) {
      if ((tablejson.InternalName).includes('2')) {
        if (col == 'column5') {
          if (isNaN(celll[4].value)) {
            if (celll[4].value.toUpperCase() == celll[3].value.toUpperCase()) {
              celll[5].value = true;
            } else {
              celll[5].value = false;
            }
          } else {
            if (celll[4].value <= celll[3].value) {
              celll[5].value = true;
            } else {
              celll[5].value = false;
            }
          }

        }
      } else if ((tablejson.InternalName).includes('1')) {
        if (celll[0].value <= 6) {
          celll[6].value = parseInt(celll[4].value) + parseInt(celll[5].value)
          var cal = '';
          if (vm.record['nameofthesample'] != undefined && vm.record['nameofthesample'].includes('V2')) {
            cal = 250;
          } else {
            cal = 50
          }
          if (celll[6].value <= cal) {
            celll[8].value = true;
          } else {
            celll[8].value = false;
          }
          var a = 0;
          var b = 0;
          var c = 0;
          var d = 0;
          var e = 0;
          var f = 0;
          angular.forEach(tablejson.Table.rows, function(cValue, ckey) {
            if (cValue.cells[0].value > 1 && cValue.cells[0].value <= 6 && cValue.cells[0].value != '' && cValue.cells[0].value != undefined) {
              if (parseInt(cValue.cells[4].value) != NaN) a = a + parseInt(cValue.cells[4].value);
              if (parseInt(cValue.cells[5].value) != NaN) b = b + parseInt(cValue.cells[5].value);
              if (parseInt(cValue.cells[6].value) != NaN) c = c + parseInt(cValue.cells[6].value);
              if (parseInt(cValue.cells[7].value) != NaN) d = d + parseInt(cValue.cells[7].value);
              if (parseInt(cValue.cells[5].value) != NaN && parseInt(cValue.cells[5].value) > e) e = parseInt(cValue.cells[5].value);
              if (parseInt(cValue.cells[7].value) != NaN && parseInt(cValue.cells[7].value) > f) f = parseInt(cValue.cells[7].value);
            } else if (cValue.cells[0].value != 7 && cValue.cells[0].value != 1) {
              cValue.cells[4].value = a;
              cValue.cells[5].value = b;
              cValue.cells[6].value = c;
              cValue.cells[7].value = d;

              var cal = '';
              if (vm.record['nameofthesample'] != undefined && vm.record['nameofthesample'].includes('V2')) {
                cal = 250;
              } else {
                cal = 50
              }
              if (cValue.cells[6].value <= cal) {
                cValue.cells[8].value = true;
              } else {
                cValue.cells[8].value = false;
              }

            } else if (cValue.cells[0].value == 7 && cValue.cells[0].value != 1) {
              //cValue.cells[4].value = a;
              cValue.cells[5].value = e;
              //cValue.cells[6].value = c;
              cValue.cells[7].value = f;

              var cal = '';
              if (vm.record['nameofthesample'] != undefined && vm.record['nameofthesample'].includes('V2')) {
                cal = 60;
              } else {
                cal = 30
              }
              if (parseInt(cValue.cells[5].value) + parseInt(cValue.cells[7].value) <= cal) {
                cValue.cells[8].value = true;
              } else {
                cValue.cells[8].value = false;
              }
            }
          })
        } else if (celll[0].value == 7) {
          angular.forEach(tablejson.Table.rows, function(cValue, ckey) {})
        } else {}
      }
    }
  }

  function dynamicColChange(colm, colmSelected, cellvla, allColumn, row) {
    console.log("-------------11212")
    var flag = false;
    angular.forEach(cellvla.Table.columns, function(val, key) {
      if (colm.referMaster && val.colRef != undefined && colm.masterCol == val.colRef.filterCol) {
        flag = true;
      }
    })
    if (flag) {
      if (FormName.toUpperCase().includes("MATERIAL PRE")) {
        if (colm.masterCol == "process") {
          //material pre heating on change ro get vale , master material preaheating admin anf process column
          var query = "select * from DF_TBL_PGR_TBL_" + colm.masterTable + "_0 where column1='" + colmSelected.value + "'";
          getFormsService.getTableRecordBasedOnQuery(query).then(function(resul1) {
            if (resul1.data.length > 0) {
              angular.forEach(resul1.data, function(value3, key3) {
                if (key3 == resul1.data.length - 1) {
                  angular.forEach(cellvla.Table.rows, function(value4, key4) {
                    angular.forEach(value4.cells, function(value5, key5) {
                      if (value5.value == colmSelected.value && value4.value != 'rowid' && value4.value != 'tableid') {
                        var i = 0;
                        angular.forEach(value3, function(value6, key6) {
                          if (key6 != 'rowid' && key6 != 'tableid') {
                            value4.cells[i].value = value6;
                            i++;
                          }
                        });
                      }
                    });
                  });
                }
              });

              angular.forEach(cellvla.Table.columns, function(val, key) {
                if (val.colRef.masterCol != undefined && val.colRef.masterCol == 'descr') {
                  row[key].value = resul1.data[0].descr;
                }
              });
            }
          })
        }
      } else {
        getFormsService.getEngneeRecords(colm.masterTable, colmSelected.value, colm.masterCol).then(function(resul) {
          angular.forEach(cellvla.Table.columns, function(val, key) {
            if (colm.referMaster && val.colRef != undefined && colm.masterCol == val.colRef.filterCol) {
              row[key].value = resul.data[resul.data.length - 1][val.colRef.masterCol];
              if (colm.masterCol == 'machine' && val.colRef.masterCol == 'partno' && cellvla.Table.columns[parseInt(key) + 1].colRef.masterCol == 'partdesc') {
                row[parseInt(key) + 1].value = resul.data[resul.data.length - 1][cellvla.Table.columns[parseInt(key) + 1].colRef.masterCol];
              }
              if (FormName.toUpperCase().includes("PACK NOTE")) {
                row[parseInt(key)].value = resul.data[resul.data.length - 1]['partdesc'];
              }
            } else if (val.value1.includes("Cycle Time S")) {
              var query = "select * from DF_TBL_PGR_TBL_" + colm.masterTable.split("_")[colm.masterTable.split("_").length - 1] + "_0 where tableid=" + resul.data[resul.data.length - 1].tableeng1 + " and column2 like '%Cycle time%'"
              getFormsService.getTableRecordBasedOnQuery(query).then(function(resul1) {
                if (resul1.data.length > 0) {
                  row[key].value = resul1.data[resul1.data.length - 1].column4;
                  var aaa = ((3600 * row[4].value) / (resul1.data[resul1.data.length - 1].column4)).toFixed(2);
                  if (aaa != NaN && aaa != 'Infinity') row[11].value = aaa;
                }
              });
            } else {}
          });
        })
      }
    }

    if (FormName.toUpperCase().includes("MATERIAL PRE")) {

      if (colm.masterCol == 'itemCode' || colm.masterCol == 'partNo') {
        var query = "select descr from " + colm.masterTable + " where " + colm.masterCol + "='" + (colmSelected.value).trim() + "'";
        console.log("query query  ", query)
        getFormsService.getTableRecordBasedOnQuery(query).then(function(resul1) {
          if (resul1.data.length > 0) {
            angular.forEach(cellvla.Table.columns, function(val, key) {
              if (val.colRef.masterCol != undefined && val.colRef.masterCol == 'descr') {
                row[key].value = resul1.data[0].descr;
              }
            });
          }
        })
      }
    } else {}




    return true;
  }

  vm.subtableId = '';

  function getRecordBasedOnSelectedId(values) {
    var formName = values.getFormName;
    var recordType = values.type;
    var recordId = values.selectedId;

    var query = "select * from   " + constantService.tableName + vm.selectedTable + " where recordName='" + recordId + "'"
    console.log("query  ", query);
    constantService.selectedTableName = constantService.tableName + vm.selectedTable;
    getFormsService.getTableRecordBasedOnQuery(query).then(function(data) {
      var i = 0;
      if (data.data.length > 0) {
        vm.returnSelectedId = data.data[0].rowid;
        angular.forEach(vm.tableRecord, function(value, key) {
          if (value.Type == 'date') {
            vm.record[value.InternalName] = SqlDateFormate(data.data[0][value.InternalName]);
          } else if (value.Type != 'table') {
            vm.record[value.InternalName] = data.data[0][value.InternalName];
            if (value.InternalName == 'machine') {
              var query = "select * from   " + constantService.tableName + value.DynaicRef.TableId + " where " + value.DynaicRef.RefCol + "='" + data.data[0][value.InternalName] + "'";
              constantService.selectedTableName = constantService.tableName + value.DynaicRef.TableId;
              getFormsService.getTableRecordBasedOnQuery(query).then(function(data1) {
                angular.forEach(vm.tableRecord, function(value2, key2) {
                  if (value2.Type == 'table') {
                    angular.forEach(value2.Table.columns, function(value3, key3) {
                      if (value3.colRef.masterCol == "partno") {
                        value3.colRef.PossibleValue = [];
                        angular.forEach(data1.data, function(value1, key1) {
                          value3.colRef.PossibleValue.push(value1.partno)
                        });
                      }
                    });
                  }
                });
              });
            }
            vm.record['recordName'] = recordId;
          } else if (value.Type == 'table') {
            vm.subtableId = data.data[0][value.InternalName];
            value.Table.rows = [];
            var query = "select * from  " + constantService.innerTableName + vm.selectedTable + "_" + i + " where tableid='" + data.data[0][value.InternalName] + "'";
            i++;
            getFormsService.getTableRecordBasedOnQuery(query).then(function(data1) {
              angular.forEach(data1.data, function(value1, key1) {
                var cell = [];
                angular.forEach(value1, function(value2, key2) {
                  if (key2 != 'tableid' && key2 != 'rowid') {
                    cell.push({ "value": value2 })
                  }
                });
                value.Table.rows.push({ "cells": cell })
              });

              if (key == vm.tableRecord.length - 1) { settableLeakTesting(); }

            });
          } else {

          }
        })
      }
    });
  }

  $scope.openMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  function checkDept(depts) {
    console.log('depts',depts);
    if (depts.length > 0) {
      if (depts.indexOf(vm.deptpartment) >= 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  function dateConvert(istdate) {
    var date = new Date(istdate);
    var newDate = (date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear());
    return newDate;
  }

  function DBdateConvert(date) {
    var date1 = '';
    if (date.split(' ')[0].split('/').length > 1) {
      var val = date.split(' ')[0].split('/')
      date1 = val[1] + '-' + val[0] + '-' + val[2]
    } else {
      var val = date.split(' ')[0].split('-')
      date1 = val[1] + '-' + val[0] + '-' + val[2]
    }
    return date1;
  }


  function SqlDateFormate(date) {
    var date1 = '';
    if (date.split(' ')[0].split('/').length > 1) {
      var val = date.split(' ')[0].split('/')
      date1 = val[1] + '-' + val[0] + '-' + val[2]
    } else {
      var val = date.split(' ')[0].split('-')
      date1 = val[0] + '-' + val[1] + '-' + val[2]
    }
    return date1;
  }

  $scope.addRow = function(value) {
    var flag = false;
    if (FormName.toLowerCase().includes('inspection') && FormName.toLowerCase().includes('admin')) {
      flag = true;
    }

    var row = {
        cells: []
      },
      colLen = value.Table.columns.length;
    for (var i = 0; i < colLen; i++) {
      if (i == 0 && flag) {
        row.cells.push({ value: parseInt(value.Table.rows.length) + 1 });
      } else {
        row.cells.push({ value: '' });
      }
    }
    value.Table.rows.push(row);
  };

  function cancel() {
    notificationService.confirm('Are you sure want to cancel', 'Yes', 'No', function() {
      $state.reload();
    }, function() {})
  }

  // function sendmailNotifiction(name) {
  //   var empCodee = '';
  //   var no = "";
  //   var cou = 0;
  //   var promiseCall = [];
  //   if (name != undefined && name != '') {
  //     var query1 = "select * from " + constantService.selectedTableName + " where rowid = (select max(rowid) from " + constantService.selectedTableName + ")";
  //     if (constantService.selectedTableName != undefined && constantService.selectedTableName != '') {
  //       promiseCall.push(
  //         getFormsService.getTableRecordBasedOnQuery(query1).then(function(datas) {
  //           console.log("Notifid:", vm.notifid);
  //           if (datas.data.length > 0) {
  //             if (vm.notifid === 2) {
  //               var arr = datas.data[0].recordName.split('-');

  //               no = arr[0] + "-" + arr[1] + "-" + (parseInt(arr[2]) + 1);
  //               vm.notifid = 0;
  //             } else {
  //               no = datas.data[0].recordName;
  //             }
  //           }
  //         })
  //       )
  //       $q.all(promiseCall).then(function(datavalue) {
  //         if (name.approverby == undefined) {
  //           if (typeof name === 'string') {
  //             empCodee = name.split(' ')[name.split(' ').length - 1];
  //           }
  //         } else {
  //           empCodee = name.approverby.split(' ')[name.approverby.split(' ').length - 1];
  //         }

  //         var query = "select * from DF_MSTR_USERS where empCode = '" + empCodee + "'";

  //         getFormsService.getTableRecordBasedOnQuery(query).then(function(data) {
  //           if (data.data.length > 0) {
  //             var email = data.data[0].email == '' ? "mazharudeen@saptalabs.com" : data.data[0].email;
  //             var msg = "Dear Sir/Madam \n\n Please Approve the document with Request No: " + no;
  //             var sub = "Online Process Approval Request";
  //             var req = { "email": email, "subj": sub, "msg": msg }
  //             SendEmailService.sendEmail(req).then(function(aa) {})
  //           }
  //         })
  //       });
  //     }
  //   }
  // }

  function setColumnInOrder(rows, record, inter) {
    var rowww = [];

    var flag = false;
    if (record.shift != undefined && rows.length > 6) {

      angular.forEach(constantService[record.shift], function(value, key) {
        angular.forEach(rows, function(value1, key1) {
          if (value == value1.cells[0].value) {
            flag = true;
            rowww.push(value1);
          }

          if (flag && key == constantService[record.shift].length - 1 && rowww.length == rows.length) {
            console.log("record record 00000000000", rowww)
            vm.record[inter].rows = []
            vm.record[inter].rows = rowww;
            console.log("record  vm.record 00000000000", vm.record)
          }
        });
      });
    }
  }




  function removeRow(index, r, allRecord) {
    allRecord.splice(index, 1);

    angular.forEach(allRecord, function(value, key) {
      if (FormName.toUpperCase().includes("ENGG")) value.cells[0].value = key + 1;
    })
  }
}
