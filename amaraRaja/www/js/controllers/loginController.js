angular.module('amaraja').controller('loginController', loginController);

loginController.$inject = ['$scope' ,'$state', '$window', 'notificationService', 'loginService', '$http', 'constantService', '$ionicSideMenuDelegate', '$rootScope'];

function loginController($scope, $state, $window, notificationService, loginService, $http, constantService, $ionicSideMenuDelegate, $rootScope) {
	var vm = this;
	vm.login = login;
	vm.cancel = cancel;
  vm.logout = logout;
  var flag = true;
  console.log('loginController 1');

  $ionicSideMenuDelegate.canDragContent(false);
  function login(){
    if(vm.user.userName != undefined && vm.user.password != undefined && flag){
      flag = false;
      console.log('vm.user',vm.user);
      var request = {"empCode":vm.user.userName,"pass":vm.user.password};
      loginService.userLogin(request).then(function(data){
        console.log('data',data);
        /*if(data.rows.length  > 0){*/
          $window.localStorage.userDept = "";
          $window.localStorage.userType = "";
          $window.localStorage.empName = "";
          $window.localStorage.empCode = "";
          $window.localStorage.email = "";
          if(data.IS_SUCCESS == true){

          $window.localStorage.empName = data.empName;
          $window.localStorage.empCode = data.empCode;
          $window.localStorage.userDept = data.deptName;
          $window.localStorage.userType = data.role;
          $window.localStorage.email = data.email;
          $rootScope.$emit("CallParentMethod", {});
          if((data.role).toUpperCase() == 'ADMIN'){
             $state.go('createForm');
          }else if(data.role == 'Approver'){
            if(data.deptName == 'Engineering'){
              $state.go('getFormRecords', {reload: true});
            }else if(data.deptName == 'Production'){
              $state.go('getFormRecords', {reload: true});
            }else{
              $state.go('getFormRecords', {reload: true});
            }           

          }else if(data.role == 'Supervisor'){
            if(data.deptName == 'Engineering'){
               $state.go('getFormRecords', {reload: true});
            }else if(data.deptName == 'Production'){
              $state.go('getFormRecords', {reload: true});
            }else{
              $state.go('getFormRecords', {reload: true});
            }
          }else if(data.role == 'Operator'){
            if(data.deptName == 'Engineering'){
              console.log('OperatorEngineering')
               $state.go('getForms', {reload: true});
            }else if(data.deptName == 'Production'){
              console.log('OperatorEngineering')
              $state.go('getForms', {reload: true});
            }else{
              $state.go('getFormRecords', {reload: true});
            }
          }else{
            
          }
          flag = true;
        }else{
          flag = true;
          notificationService.alert('', 'UserName and Password does not match', function() { });
        }
        
      });
    }
    	
  }

  function cancel(user){
    flag = true;
  	vm.user = "";
  }

  function logout(){console.log('logout');
    $window.localStorage.removeItem("userDept");
    $window.localStorage.removeItem("userType");
    $window.localStorage.removeItem("userName");
    $state.reload();
    $state.go('login', {reload: true});
  }

        
}


