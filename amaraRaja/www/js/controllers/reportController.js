angular.module('amaraja').controller('reportController', reportController);

reportController.$inject = ['$scope','$state','$window','$q', '$rootScope', '$location', '$http', 'createFormsService','reportService','notificationService','ionicDatePicker', '$ionicPopup', 'constantService','$ionicSideMenuDelegate','$cordovaPrinter'];

function reportController($scope, $state, $window, $q, $rootScope, $location, $http, createFormsService, reportService,notificationService,   ionicDatePicker, $ionicPopup, constantService, $ionicSideMenuDelegate,$cordovaPrinter) {
	var vm = this;
	vm.getTableByName = getTableByName;
	vm.getFormsNames = getFormsNames;
	vm.setColCondition = setColCondition;
	vm.getAllRecord = getAllRecord;
	vm.CheckforTable = CheckforTable;
	vm.getValueDynamicaly = getValueDynamicaly;
	vm.getDate = getDate;
	vm.getPdf = getPdf;
	vm.columns = [];
	vm.rows = [];
	vm.resultJson = [];
	vm.selectedValue = {};
	vm.dateConvert = dateConvert;
	vm.DBdateConvert = DBdateConvert;
	vm.tableName = [];
	notificationService.showSpinner();
	vm.filter = true;

	$scope.openMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };
	
	 function getDate(cell, col, type){
   var ipObj2 = {
      callback: function (val) {  //Mandatory
      		if(col[cell] == undefined){
      			col[cell] = {};
      		}
      		if(type == 'to'){
      			if(col[cell].from == undefined || col[cell].from == ''){
      				col[cell][type] = '';
      			}else{
      				var aa = col[cell].from;
      				var bb = aa.split("-");
      				var d1 = new Date(bb[2], bb[1] - 1, bb[0])
      				if(d1 <= val){
      					col[cell][type] = dateConvert(new Date(val));
      				}
      			}
      		}else{
      			col[cell][type] = dateConvert(new Date(val));
      		}
      },
      from: new Date(2000, 1, 1), //Optional
      to: new Date(2200, 10, 30), //Optional
      inputDate: new Date(),      //Optional
      mondayFirst: true,          //Optional
            //Optional
      closeOnSelect: false,       //Optional
      templateType: 'popup'       //Optional
    }; 
    ionicDatePicker.openDatePicker(ipObj2);
  }

  function dateConvert(istdate){
    var date = new Date(istdate);
    var newDate = ( date.getDate() + '-' +  (date.getMonth() + 1) + '-' +  date.getFullYear());
    return newDate;
  }

  function DBdateConvert(date){
    var date1 = '';
    if(date.split('/').length > 1){
      var val = date.split('/')
      date1 = val[1]+'-'+val[0]+'-'+val[2]
    }else{
      var val = date.split('-')
      date1 = val[1]+'-'+val[0]+'-'+val[2]
    }
    return date1;
  }



	getFormsNames();
	function getFormsNames(){
		vm.selectedValue = {}
		createFormsService.getTableJSON().then(function(data) {
    	angular.forEach(data.rows, function(value, key) {
    		if(!value.tableName.includes('Admin')){
					value['selected'] = true;
				  vm.tableName.push(value);
    		}
        notificationService.hideLoad();
      });
    });
	}

	function getTableByName(){
		vm.columns = [];
		vm.rows = [];
		vm.selectedValue = {}
		notificationService.showSpinner();
		vm.recordData = [];
		vm.tableData = [];
		createFormsService.getTableByName(vm.selectedTable).then(function(data) {
			if(data.rows.length > 0){
				vm.tableJson = JSON.parse(data.rows[0].tableRecord).data;
				angular.forEach(JSON.parse(data.rows[0].tableRecord).data, function(value, key){
					if(value.Type != 'table') {
						value['selected'] = true;
						if(value.Settings[1].Value == 'partno' || value.Settings[1].Value == 'process' || value.Settings[1].Value == 'machine' ){
							reportService.getByRowName(vm.selectedTable,  value.Settings[1].Value).then(function(result){
								value.Settings[2].PossibleValue = [];
								angular.forEach(result.rows, function(vall, keyy){
									value.Settings[2].PossibleValue.push({'Text': vall[value.Settings[1].Value]})
								})
								vm.recordData.push(value);
							})
						}else{
							vm.recordData.push(value);
						}
					}else{
						angular.forEach(value.Settings[3].PossibleValue[0].columns, function(value1, key1){
							value1['selected'] = true;
						});
						vm.recordData.push(value);
					} 
				})
			}
			notificationService.hideLoad();
		});
	}

	function setColCondition(x, type){
		vm.tableJson = vm.recordData;
		vm.type = type;
		$ionicPopup.show({
	    templateUrl: 'templates/selectColumnPopup.html',
	    title: 'Select Column to be displayed',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	        } 
	      },
	      {
	        text: '<b>Ok</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	        }
	      }
	    ]
	  });
	}

	function getAllRecord(){
		vm.resultJson = [];
		var subId = 0;
		
		var promiseArray=[];
		var subcell = [];
		vm.columns = [];
		vm.rows = [];
		vm.result = {};
		var condition = '';
		console.log("vm.recordData  ", vm.recordData)
		angular.forEach(vm.recordData, function(value, key){
			if(value.Settings[1].Value == 'partno' || value.Settings[1].Value == 'machine' || value.Settings[1].Value == 'process' || value.Settings[1].Value == 'datee'){
				if(vm.selectedValue[value.Settings[1].Value] != undefined){
					if(value.Settings[1].Value == 'datee'){
						var aa1 = vm.selectedValue[value.Settings[1].Value].from
						condition = condition + value.Settings[1].Value+" BETWEEN '"+ DBdateConvert(aa1) +"' and ";
						if(vm.selectedValue[value.Settings[1].Value].to != undefined && vm.selectedValue[value.Settings[1].Value].to != ''){
							var aa = vm.selectedValue[value.Settings[1].Value].to;      				
							condition = condition +"'"+DBdateConvert(aa) +"' and ";
						}else{
							var aa =  new Date();
							var newDate = aa.getDate() + '-' +  (aa.getMonth() + 1) + '-' +  aa.getFullYear();
							condition = condition +"'"+ DBdateConvert(newDate)+"' and ";
						}
					}else{
						if(vm.selectedValue[value.Settings[1].Value] != '' && vm.selectedValue[value.Settings[1].Value] != undefined){
							condition = condition + value.Settings[1].Value+"='"+vm.selectedValue[value.Settings[1].Value]+"' and ";
						}
					}
				}
			}
		});
		reportService.getTableRecords(vm.selectedTable, condition.trim().slice(0,-4)).then(function(data){
				var promiscall1= [];
			angular.forEach(data.rows, function(value, key){
				var i=0;
				var cell = [];
				subId = 0;
				var flag = true;
				promiseArray = [];
				var result = {};
				var promiscall = [];

				var count = 0;
				promiscall.push(
					angular.forEach(vm.tableJson, function(value1, key1){
						if( value1.selected != false)  value1.selected = true
						if(value1.Type != 'table' && value1.selected){
							if(key == 0){
								vm.columns.push(value1.Name);
								subcell.push('');
							}
							var bbb = value1.Settings[1].Value;
							cell.push(value[bbb])	
							var aaaa = value1.Name;
							result[aaaa] = value[bbb]	
						}else if(value1.Type == 'table'){
							var flag = false;
							var bbb = value1.Settings[1].Value;
						
							if(vm.selectedTable == '3097'  ){
							  	var qq = result.Process;
							  	var ww = (qq.split(" ")[0]).toLowerCase()
							  	if(bbb.includes(ww)){
							  		flag = true;
							  	}
							  }else{
							  	flag = true;
							  }
							  if(flag){
						  	  	count++;
						  		  //result['table'+count] = {};

									var retVal = reportService.getInnerTableRecords(vm.selectedTable,i++, value[bbb]).then(function(inData){
										count++;
										/*result['table'+count] = {}
										result['table'+count]['columns'] = [];
										result['table'+count]['rows'] = []*/


										var tablev = {};
										var column = []
										var rows = []

										tablev['rows'] = []; //
										tablev['column'] = [];
										var loopData = '';
										 if(vm.selectedTable == '3097' || vm.selectedTable == '2093' ){
										 		var valuess = [];
										    var flag = false;

										    var ii = Object.keys(inData.rows[0])
										    if(result.Shift != undefined && ii.length > 6){
										      angular.forEach(constantService[result.Shift], function(value, key){
										        angular.forEach(inData.rows, function(value1, key1){
										          if(value == value1.column1){
										            flag = true;
										            valuess.push(value1);
										          }
										          if(flag && key == constantService[result.Shift].length-1) {
										           	console.log(flag, key, valuess.length);
										            inData.rows = []
										            inData.rows = valuess;
										            loopData = inData.rows
										          }
										        });
										      });
										    }else{
										    	loopData = inData.rows;
										    }
										 }else{	
										 	 loopData = inData.rows;
										 }

										angular.forEach(loopData, function(colValue, ColKey){
											rows = []
											angular.forEach(value1.Settings[3].PossibleValue[0].columns, function(resValue, resKey){
												angular.forEach(colValue, function(colValue1, ColKey1){
													if(resValue.selected != false)resValue.selected = true;
													if(resValue.value == ColKey1 && resValue.selected){
														if(ColKey == 0){
															//result['table'+count]['columns'].push(resValue.value1);
															column.push(resValue.value1);//
														}
														rows.push(colValue1);
													}
												})
											});
											tablev['rows'].push(rows);//


											//return result['table'+count]['rows'].push(rows);
											//result['table'+count]['rows'].push(rows);
										})
										tablev['column'] = column;//

										return tablev;//
									})

									result['table'+count] = retVal;
								
									/*var aaaa = Promise.resolve(retVal);
									aaaa.then(function(v) {
										console.log("2131 3   ", v);
									})*/
										
							  }

					  	}
					  })
				  )
			
					$q.all(promiscall).then(function(datavalue){
					 	vm.rows.push(cell);
						vm.resultJson.push(result);
						
						count = 0;
						if(data.rows.length-1 == key){
							aaa(vm.resultJson)
						}
					});
					
			});
		})
	}


	vm.aaa = aaa;
	function aaa(resultJson){	
		angular.forEach(vm.resultJson, function(jValue, jKey){			
			angular.forEach(jValue, function(jValue1, jKey1){
				if(jKey1.includes('table')){					
					var aaaa  = Promise.resolve(jValue1);
					aaaa.then(function(v) {		
						jValue[jKey1]  = '';
						jValue[jKey1] = v;		

						if(jKey == vm.resultJson.length-1 ){
							angular.element('#filter').triggerHandler('click');
						}
					})					
				}
			})
		})
	}


	function getValueDynamicaly(tableid, colname, colValue){
		angular.forEach(vm.recordData, function(value, key){
			if(value.Type != 'table') {
				switch(colname){
					case 'process':
									if(value.Settings[1].Value == 'partno' || value.Settings[1].Value == 'machine'){
										var condition = 
										reportService.getByRowNamewithcondition(vm.selectedTable,  value.Settings[1].Value, colname,  colValue).then(function(result){
											value.Settings[2].PossibleValue = [];
											angular.forEach(result.rows, function(vall, keyy){
												value.Settings[2].PossibleValue.push({'Text': vall[value.Settings[1].Value]})
											})
										
										})
									}
									break;
					case 'machine':
									if(value.Settings[1].Value == 'partno'  ){
										var condition = 
										reportService.getByRowNamewithcondition(vm.selectedTable,  value.Settings[1].Value).then(function(result){
											value.Settings[2].PossibleValue = [];
											angular.forEach(result.rows, function(vall, keyy){
												value.Settings[2].PossibleValue.push({'Text': vall[value.Settings[1].Value]})
											})
											
										})
									}
									break;
					case 'partno':
									
					 				break
					default:
					 	break;
			}
		}
	})
}


	function getPdf(){
		printDiv();
		//window.print()
	}
 vm.printDiv = printDiv;
	function printDiv() { 
				var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
				if ( app ) {					 
					    var printContents = document.getElementById('prinyy').innerHTML;
						  cordova.plugins.printer.print('<html><head><style type="text/css"> @media all {.page-break	{ display: none; }} @media print {.page-break	{ display: block; page-break-before: always; }}</style><link href="css/style.css" rel="stylesheet"></head><body>' + printContents + '</body></html>', 'BB10');
				} else {
					  var printContents = document.getElementById('prinyy').innerHTML; 
				   	var popupWin =  window.open('', '_blank', 'width=300,height=300')
					  popupWin.document.open();
			  	  popupWin.document.write('<html><head><style type="text/css"> @media all {.page-break	{ display: none; }} @media print {.page-break	{ display: block; page-break-before: always; }}</style><link href="css/style.css" rel="stylesheet"></head><body onload="window.print()">' + printContents + '</body></html>');
				    popupWin.document.close();
				} 


				 // popupWin.focus();
				  //popupWin.close();
				} 
				



	function CheckforTable(x){
		if(x.includes('table')){
			return false;
		}else{
			return true;
		}	

	}
	


}