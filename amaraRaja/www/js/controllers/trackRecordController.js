angular.module('amaraja').controller('trackRecordController', trackRecordController);

trackRecordController.$inject = ['$scope', '$ionicSideMenuDelegate', '$state', '$window', 'getFormsService','trackRecordService'];

function trackRecordController($scope, $ionicSideMenuDelegate, $state, $window, getFormsService, trackRecordService) {
	var vm = this;

	vm.getTableRecords = getTableRecords;
	vm.getFormRecords = getFormRecords;
	vm.showRecordDetails = showRecordDetails;

	vm.formList = [];
	vm.formRecords = [];
	vm.approveCount = 0;
	vm.rejectedCount = 0;
	vm.inprocessCount = 0;
	vm.comletedCount = 0;
	vm.completedrecords = 0;
	vm.allDetails = [];
	vm.rejectedList = [];

	getTableRecords();
	function getTableRecords(){
		trackRecordService.getFormNames().then(function(response){			
			trackRecordService.getWorkflow().then(function(resDate){
				angular.forEach(response.data, function(value, key){
					angular.forEach(resDate.data, function(value1, key1){
						if(value.tableName == value1.levelForm){
							vm.formList.push(value)
						}
					})
				})
			})
		});
	}

	function getFormRecords(){
		vm.formRecords = [];
		trackRecordService.getFormRecords(vm.selectedTable).then(function(response){
			angular.forEach(response.data, function(value, key){
				vm.formRecords.push(value)
			})
		});
	}

	function showRecordDetails(record){
		console.log("record --=- ", record);
		vm.allDetails = [];
		vm.showtable = true;
		trackRecordService.getRecordDetail(vm.selectedTable, record.rowid).then(function(response){
			console.log("asd ", response)
			angular.forEach(response.data, function(value, key){
				vm.allDetails.push(value);
				if(value.status == 'done'){
					vm.comletedCount = vm.comletedCount+1;
					vm.completedrecords.push(value);
				}else if(value.status == 'rejected'){
					vm.rejectedCount = vm.rejectedCount + 1;
					vm.rejectedList.push(value);
				}else{

				}
			})
		});

	}
	$scope.openMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };


}