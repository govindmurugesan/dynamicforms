angular.module('amaraja').controller('userController', userController);

userController.$inject = ['$scope', '$ionicSideMenuDelegate', '$state', '$window', 'getFormsService', 'createFormsService', 'workflowService', '$ionicHistory', '$rootScope'];

function userController($scope, $ionicSideMenuDelegate, $state, $window, getFormsService, createFormsService, workflowService, $ionicHistory, $rootScope) {
	var vm = this;
  vm.getWorkFlowTableList = getWorkFlowTableList;
  vm.getFormName = getFormName;
  vm.getTableRecords = getTableRecords;
  vm.Rejected = Rejected;
	$scope.active1 = false;
  $scope.active2 = false;
  $scope.active3 = false;
  vm.createForm = createForm;
  vm.selectedTableName = selectedTableName;
  vm.selectedWorkName = selectedWorkName;
  vm.trackRecordDetails = vtrackRecordDetails;
  vm.logouthead = logouthead;
  vm.getFormNames = getFormNames;
  vm.inProcess = inProcess;
  vm.gotoWorkflow = gotoWorkflow;
  vm.completed1 = completed1;
  vm.logout = logout;
  vm.pdf = pdf;
  vm.record = [];
  vm.transferList = [];
  vm.returnList = [];
  vm.tableName = [];
  vm.workName = [];
  vm.userType = "";
  vm.userDept = "";
  vm.rejectList = [];
  vm.completed = [];
  vm.inFlowList = [];

  $rootScope.$on("CallParentMethod", function(){
     hideMenu();
  });
  //hideMenu();
  function hideMenu(){
    vm.userType = $window.localStorage.userType;
    vm.userDept = $window.localStorage.userDept;
    getWorkFlowTableList();
  }
  console.log('$window.localStorage.userType',vm.userType);

  function getWorkFlowTableList(){
    vm.record = [];
    getFormsService.getRejectedList(vm.userDept).then(function(result){
      vm.rejectList=[];
      console.log('rejectList ',result);
      if(result.rows.length > 0){
        angular.forEach(result.rows, function(value, key){
          getFormsService.getTableName(value.tableName).then(function(result1){
            // console.log('rejectList result1',result1)
            angular.forEach(result1.data,function(val){
            vm.rejectList.push(val);
          });
          })
        })
      }
    });

    getFormsService.getInFLowList(vm.userDept).then(function(result){
      console.log('inFlowList',result);
      vm.inFlowList =[];
      angular.forEach(result.data, function(value, key){
        // if(vm.userDept=='QA')
          queryVal=value.tableName
        // else
        //   queryVal=value.formName;
        getFormsService.getTableName(queryVal).then(function(result1){
      console.log('inFlowList result1',result1);
          if(result1.data.length > 0){
            var FormName = result1.data[0].tableName;
            if(vm.userType != 'Operator' && vm.userDept == 'QA'){
              if((FormName.toUpperCase().includes("INSPECTION") || FormName.toUpperCase().includes("FLAM")) && !FormName.toUpperCase().includes("PACK") && !FormName.toUpperCase().includes("ADMIN")){
                 if(result1.data.length > 0) vm.inFlowList.push(result1.data[0]);
              }
            }else{
              if(result1.data.length > 0) vm.inFlowList.push(result1.data[0]);
            }
          }
        })
      })
    })
    getFormsService.getWorkFlowTableList(vm.userDept).then(function(data){
      getFormsService.getCompletedList().then(function(result){
        console.log('completed list',result)
        vm.completed =[];
        angular.forEach(result.data, function(value, key){   
          angular.forEach(data.rows, function(value1, key1){
            if(value.formName == value1.levelForm ){
               var FormName = value.formName;

              if(vm.userType != 'Operator' && vm.userDept == 'QA'){
                if((FormName.toUpperCase().includes("INSPECTION") || FormName.toUpperCase().includes("FLAM")) && !FormName.toUpperCase().includes("PACK") && !FormName.toUpperCase().includes("ADMIN")){
                  vm.completed.push(value);
                }
              }else{
                vm.completed.push(value);
              }
            }
          })
        })
      })

     
      vm.transferList =[];
      vm.returnList = [];
      if(data.rows.length > 0){
        if(vm.userDept == 'Engineering'){
          angular.forEach(data.rows, function(value, key){
            vm.record.push(value);
             getFormsService.getApproved(value.levelForm).then(function(response){
              if(response.rows.length > 0){
                angular.forEach(response.rows, function(valueRes, keyRes){
                   if(valueRes.approved == "transfer"){
                      vm.transferList.push(value);
                    }else if(valueRes.approved == "return"){
                      vm.returnList.push(value);
                    }else{

                    }
                });

        console.log('transfer List',vm.transferList)
        console.log('return List',vm.returnList)
              }
            });
          })
        }else if(vm.userDept == 'QA' || vm.userDept == 'Production'){
          vm.record = [];
          vm.transferList = [];
          vm.returnList = [];
          angular.forEach(data.rows, function(value, key){
            getFormsService.getApproved(value.levelForm).then(function(response){
              if(response.rows.length > 0){  
               var aaa = value.levelForm != undefined?value.levelForm:value.formName;
              if($window.localStorage.empName == "Production admin" || $window.localStorage.empName == "QA admin"){
                if(aaa.toUpperCase().includes("ADMIN") ) {         
                  vm.record.push(value); 
                }
              }else{
                if(!aaa.toUpperCase().includes("ADMIN")){
                  var FormName = aaa;
                  if(vm.userType != 'Operator' && vm.userDept == 'QA'){
                    if((FormName.toUpperCase().includes("INSPECTION") || FormName.toUpperCase().includes("FLAM")) && !FormName.toUpperCase().includes("PACK")  && !FormName.toUpperCase().includes("ADMIN")){
                      vm.record.push(value);
                    }
                  }else{
                    vm.record.push(value);
                  }
                }
              }


              angular.forEach(response.rows, function(valueRes, keyRes){
                 if(valueRes.approved == "approved"){
                  }else if(valueRes.approved == "transfer"){
                    vm.transferList.push(value);
                  }else if(valueRes.approved == "return"){
                    vm.returnList.push(value);
                  }/*else if(valueRes.approved != "done" && valueRes.approved != "reject"){
                     vm.returnList.push(value);
                  }*/else{

                  }
              });
              
        console.log('transfer List',vm.transferList)
        console.log('return List',vm.returnList)
              }else if($window.localStorage.userType == "Operator"){
                var aaa = value.levelForm != undefined?value.levelForm:value.formName;
                if(($window.localStorage.empName == "Production admin" || $window.localStorage.empName == "QA admin")) {
                  if(aaa.toUpperCase().includes("ADMIN")) {  
                    vm.record.push(value); 
                  }
                }else{
                  if(!aaa.toUpperCase().includes("ADMIN")){
                     var FormName = value.formName;
                      if(vm.userType != 'Operator' && vm.userDept == 'QA'){
                        if((FormName.toUpperCase().includes("INSPECTION") || FormName.toUpperCase().includes("FLAM")) && !FormName.toUpperCase().includes("PACK") && !FormName.toUpperCase().includes("ADMIN")){
                          vm.record.push(value);
                        }
                      }else{
                       vm.record.push(value);
                      }
                  }
                }
              }
            });             
          })
        }else if(vm.userDept == 'Maintenance'){
          angular.forEach(data.rows, function(value, key){
            vm.record.push(value);
          })
        }else if(vm.userDept == 'Mold Maintenance'){
          angular.forEach(data.rows, function(value, key){
            vm.record.push(value);
          })
        }
      }
    });   
  }
  
  function getFormName(x){
    var query = " select lev from DF_WORKFLOW_I where levelForm='"+x+"' and department ='"+vm.userDept+"'"
    getFormsService.getTableRecordBasedOnQuery(query).then(function(data){
      if(data.data.length > 0){
        if(data.data[0].lev.replace("level", '').trim() > 1 ){
          $state.go('getFormRecords', { 'getFormName': x}, {reload:true});
        }else{
          $state.go('getForms', { 'getFormName': x}, {reload:true});
        }
      }
    })
  }

  function getTableRecords(x){
    $state.go('getFormRecords', { 'getFormName': x, 'type': ''},  {reload:true});
  }

  /*user page content*/
  $scope.openMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };

  function createForm(){
  	$state.go('createForm');
  }
  function gotoWorkflow(){
    $state.go('workflow');
  }
  function Rejected(x, type){  
    $state.go('getFormRecords', {  'getFormName': x, 'type': type}, {reload:true});   
  }
  function inProcess(x,type){
    if (type==undefined) {
     $state.go('getFormRecords', {  'getFormName': x, 'type': 'inprocess'}, {reload:true}); 
    }
    else      
     $state.go('getFormRecords', {  'getFormName': x, 'type': type}, {reload:false}); 
  }
  function completed1(x){
    $state.go('getFormRecords', {  'getFormName': x, 'type': 'done'}, {reload:true}); 
  }

  createFormsService.getTableJSON().then(function(data) {
    angular.forEach(data.rows, function(value, key) {
      vm.tableName.push(value);
    });
  });

  workflowService.getWorkflow().then(function(data) {
    angular.forEach(data.rows, function(value, key) {
      vm.workName.push(value);
    });
  })

  function selectedTableName(form){
    $state.go('editForms', { 'formName': form.rowid});
  }

  function selectedWorkName(work){
    $state.go('editWorkflow', { 'workName': JSON.stringify(work)});
  }
  $scope.opts = [
        {text: '1st' },
        {text: '2nd' },
        {text: '1st' },
        {text: '1st' },
        {text: '1st' },
    ];

  function logout(){
    $window.localStorage.removeItem("userDept");
    $window.localStorage.removeItem("userType");
    $window.localStorage.removeItem("userName");
    $window.localStorage.removeItem("email");    
    $ionicSideMenuDelegate.toggleLeft();
    vm.record = [];
    vm.transferList = [];
    vm.returnList = [];
    vm.tableName = [];
    vm.workName = [];
    vm.rejectList = [];
    vm.completed = [];
    vm.inFlowList = [];
    $state.go('login', {reload: true});
  }


  function pdf(){
    $state.go('report', {reload: true});
  }


  function logouthead(){
    $state.go('login', {reload: true});
  }

  function vtrackRecordDetails(){
    $state.go('trackRecord', {reload: true});
  }

  function getFormNames(){
    $state.go('getAllForms');
  }

}