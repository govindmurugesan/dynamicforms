angular.module('amaraja').controller('getFormsRecordsCtrls', getFormsRecordsCtrls);

getFormsRecordsCtrls.$inject = ['$scope', '$timeout', '$rootScope', '$interval', '$state', '$window', '$q', '$rootScope', '$location', '$http', 'getFormsService', 'createFormsService', 'getFormsRecordsService', 'notificationService', '$ionicSideMenuDelegate', 'ionicDatePicker', 'ionicTimePicker', 'trackRecordService', 'constantService', 'SendEmailService', 'recordDetailsService'];

function getFormsRecordsCtrls($scope, $timeout, $rootScope, $interval, $state, $window, $q, $rootScope, $location, $http, getFormsService, createFormsService, getFormsRecordsService, notificationService, $ionicSideMenuDelegate, ionicDatePicker, ionicTimePicker, trackRecordService, constantService, SendEmailService, recordDetailsService) {
	var vm = this;
	vm.getFormsNames = getFormsNames;
	vm.getTableRecords = getTableRecords;
	vm.showAllRecord = showAllRecord;
	vm.createNewField = createNewField;
	vm.updateRecord = updateRecord;
	vm.checkColrestrict = checkColrestrict;
	vm.ColumanCondition = ColumanCondition;
	vm.dateConvert = dateConvert;
	vm.getRecordTrack = getRecordTrack;
	vm.cancel = cancel;
	vm.setEditTrue = setEditTrue;
	vm.valuationDatePickerOpen = valuationDatePickerOpen;
	vm.settabledatepacknote = settabledatepacknote;
	vm.SqlDateFormate = SqlDateFormate;
	vm.getDate = getDate;
	vm.getTime = getTime;
	vm.addRow = addRow;
	vm.tableName = [];
	vm.TableRecords = [];
	vm.tableValue = [];
	vm.allrecord = ''
	var guid = 1;
	vm.showInput = true;
	vm.hideButtons = false;
	vm.formFields = [];
	vm.formName = '';
	vm.current_field = {};
	vm.tableRowId = 0;
	vm.tableRowName = '';
	vm.updateDept = '';
	vm.rejectToDept = '';
	vm.setMoveToDept = '';
	vm.formLevelInnumber = '';
	vm.presentRecorIndex = '';
	vm.returnDept = [];
	vm.hideIndex = [];
	vm.allDetails = [];
	vm.hideOperator = false;
	vm.showtable = false;
	vm.flag = true;
	vm.showAdd = false;

	$rootScope.codeNameOfRecord = "";

	vm.serviceMailInformation = [];

	vm.department = $window.localStorage.userDept;
	vm.loggedInUser = $window.localStorage.empName + ' ' + $window.localStorage.empCode;
	vm.operator = $window.localStorage.empCode + ' ' + $window.localStorage.empName;
	vm.loginUserRole = $window.localStorage.userType;

	$scope.$watch(function() {
		return $state.params;
	});
	var FormName = $state.params.getFormName;
	vm.typeRejected = $state.params.type;
	console.log('vm.typeRejected', vm.typeRejected);
	vm.LoginUserTypee = LoginUserTypee;
	LoginUserTypee();

	getFormsNames();
	notificationService.showSpinner();

	function LoginUserTypee() {
		if ($window.localStorage.userDept == '' || $window.localStorage.userDept == undefined)
			$state.go('login');
		var LoginUserType = $window.localStorage.userDept;
		if (LoginUserType == 'Engineering') {
			vm.access = 3;
		} else if (LoginUserType == 'QA') {
			vm.access = 15;
			vm.showhidenform = true;
		} else {
			vm.access = 15;
		}
		if ($state.current.name == 'getFormRecords' && $window.localStorage.userType == 'Operator') {
			vm.hideOperator = true;
		}
	}

	function getDate(x) {
		var ipObj2 = {
			callback: function(val) { //Mandatory
				console.log('Return value from the datepicker popup is : ' + val, new Date(val));
				x.Value = dateConvert(new Date(val));
				x['Edited'] = true;
				//vm.ColumanCondition(col, celll, tablejson);
			},
			from: new Date(2000, 1, 1), //Optional
			to: new Date(2200, 10, 30), //Optional
			inputDate: new Date(), //Optional
			mondayFirst: true, //Optional
			disableWeekdays: [0], //Optional
			closeOnSelect: false, //Optional
			templateType: 'popup' //Optional
		};
		ionicDatePicker.openDatePicker(ipObj2);
	}

	function getTime(cell, col, celll, tablejson) {
		console.log("col ", col)
		console.log("cell ", cell)
		console.log("celll ", celll)
		console.log("tablejson ", tablejson)
		var timmm = ''
		var ipObj1 = {
			callback: function(val) { //Mandatory
				if (typeof(val) === 'undefined') {
					console.log('Time not selected');
				} else {
					var selectedTime = new Date(val * 1000);
					timmm = selectedTime.getUTCHours();
					cell.Value = selectedTime.getUTCHours() + " : " + selectedTime.getUTCMinutes()
					vm.ColumanCondition(col, celll, tablejson, cell);
				}
			},
			inputTime: 12 * 60 * 60, //Optional
			format: 12, //Optional
			step: 1, //Optional
			setLabel: 'Ok' //Optional
		};
		ionicTimePicker.openTimePicker(ipObj1);
	}

	function setEditTrue(value, tableRecord, recorvalue) {
		console.log('entered set edit');
		getValueDynamicaly(value, tableRecord, recorvalue);
		value['Edited'] = true;
		var cavityCount = 0;
		var statIndex = 0;

		if (value.Cavity != "" && value.Type == "dropdown") {
			angular.forEach(vm.tableValue, function(val, key) {
				if (val.Type == 'table' && val.Cavity != '') {
					angular.forEach(val.Value.table[0].columns, function(val1, key1) {
						if (val1.Value == val.Cavity) {
							statIndex = key1;
						}
						if (val1.cavity) {
							cavityCount++;
						}
					})
					var count = value.Value - 1;
					if (cavityCount == 0) {
						angular.forEach(val.Value.table[0].columns, function(val1, key1) {
							if (val1.Value == value.Cavity) {

								for (i = 0; i < count; i++) {
									val.Value.table[0].columns.splice(statIndex + 1 + i, 0, { 'Value': value.Cavity + '_' + (i + 1), 'Value1': val.Value.table[0].columns[statIndex].Value1 + '_' + (i + 1), 'cavity': true });
									if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) + 1;
								}
								angular.forEach(val.Value.table[1].rows, function(val2, key2) {
									for (i = 0; i < count; i++) {
										val2.cells.splice(statIndex + 1 + i, 0, { 'Value': '', 'cavity': true });

									}
								});
							}
						});
					} else if (cavityCount < count) {
						for (i = cavityCount; i < count; i++) {
							val.Value.table[0].columns.splice(statIndex + i + 1, 0, { 'Value': value.Cavity + '_' + (i + 1), 'Value1': val.Value.table[0].columns[statIndex].Value1 + '_' + (i + 1), 'cavity': true });
							if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) + 1;
						}
						angular.forEach(val.Value.table[1].rows, function(val2, key2) {
							for (i = cavityCount; i < count; i++) {
								val2.cells.splice(statIndex + i + 1, 0, { 'Value': '', 'cavity': true });
							}
						});
					} else if (cavityCount > count) {
						for (i = cavityCount; i > count; i--) {
							var tot = statIndex + i
							if (val.Value.table[0].columns[tot].cavity) {
								val.Value.table[0].columns.splice(tot, 1);
								if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) - 1;
							}
						}
						angular.forEach(val.Value.table[1].rows, function(val2, key2) {
							for (i = cavityCount; i > count; i--) {
								val2.cells.splice(tot, 1);
							}
						});
					}
				}
			});
		}
	}

	function getFormsNames() {
		createFormsService.getTableJSON().then(function(data) {
			angular.forEach(data.rows, function(value, key) {
				vm.tableName.push(value);
				if (FormName.toUpperCase().includes("IN-PROCESS")) {
					vm.showAdd = true;
				}
				if (value.tableName == FormName) {
					getTableRecords(value.rowid);
				}
				notificationService.hideLoad();
			});
			notificationService.hideLoad();
		});
	}

	function getTableRecords(id) {
		var deferred = $q.defer();
		vm.hideIndex = [];
		var promiscall = [];
		vm.selectedTable = id;
		notificationService.showSpinner();
		vm.TableRecords = [];
		vm.RejeTableRecords = [];
		var level = '';
		var dept = '';
		var idList = [];
		vm.compare = []
		getTableDetails(vm.selectedTable);

		getFormsRecordsService.getTableRecords(vm.selectedTable).then(function(data) {
			getFormsRecordsService.checkWorkflow(FormName).then(function(res) {
				angular.forEach(res.rows, function(value, key) {
					if (value.department == $window.localStorage.userDept) {
						level = value.lev.replace('level', '').trim();
						vm.formLevelInnumber = level;
					}
				});

				console.log(' $state.params', $state.params);
				Type = $state.params.type;
				recordType = 'approved';
				var rejectQuery = '';
				vm.returnDept = [];
				angular.forEach(res.rows, function(value, key) {
					if ('level ' + (parseInt(level) + 1) == value.lev) {
						vm.moveToDept = value.department;
					}
					if ('level ' + (parseInt(level) - 1) == value.lev) {
						vm.rejectToDept = value.department;
					}
					if (value.department != $window.localStorage.userDept && level > value.lev.replace('level', '').trim()) vm.returnDept.push(value.department);

					if (parseInt(value.lev.replace('level', '').trim()) >= parseInt(level)) {
						rejectQuery = rejectQuery + "'" + value.department + "',";
					}
				});
				if (res.rows.length == 1) {
					vm.hideradioButtons = false;
					vm.setMoveToDept = 'done';
				} else {
					vm.hideradioButtons = true;
				}
				if (dept == '') dept = 'Engineering';
				if (vm.rejectToDept == undefined) vm.moveToDept = 'Engineering';
				vm.updateDept = dept;
				var recordType = $state.params.type;
				dept = $window.localStorage.userDept;
				var timer;

				getFormsRecordsService.checkApproved(vm.selectedTable, dept, recordType, FormName, rejectQuery.slice(0, -1)).then(function(res1) {
					console.log("res1  ", res1);
					console.log('type', Type);
					if (res1.rows.length > 0) {
						angular.forEach(data.rows, function(value, key) {

							console.log('value res1', value)
							if (key == '0') {
								$rootScope.codeNameOfRecord = getCodeNameOfRecord(value.recordName);
							}
							if (value.qainspector == vm.loggedInUser || value.supervisor == vm.loggedInUser || value.supervisorname == vm.loggedInUser ||
								value.operator == vm.operator || value.senioroperator == vm.operator || Type == '' || Type == undefined || vm.department == "Engineering" ||
								value.approvedby == vm.loggedInUser ||
								vm.formName.toUpperCase().includes('LEAK') || vm.formName.toUpperCase().includes('PACK') ||
								vm.formName.toUpperCase().includes('IN-PROCESS') || vm.formName.toUpperCase().includes('DAILY OPERATOR WORK ALLOTMENT') ||
								vm.formName.toUpperCase().includes('CHEMICAL MIXING AT BUSH CASTING') ||
								vm.formName.toUpperCase().includes('SHIFT RELEIVING SHEET') || vm.formName.toUpperCase().includes('INSPECTION REPORT') ||
								vm.formName.toUpperCase().includes('LEAK TESTING CUM GUMMING APPLICATION RECORD') ||
								vm.formName.toUpperCase().includes('MOLD CHANGE CHECK SHEET') ||
								vm.formName.toUpperCase().includes('IN-PROCESS SET UP APPROVAL') || vm.formName.toUpperCase().includes('FLAMMABILITY REPORT')) {

								// console.log("value for res1 ", value);
								// console.log('type after res1', Type);
								var flag = true;
								promiscall.push(
									angular.forEach(res1.rows, function(value1, key1) {
										angular.forEach(value, function(val, keyy) {
											// console.log('val after res1', val);

											// console.log('vm.department', vm.department);
											// console.log('value1.approved', value1.approved);
											// console.log('keyy', keyy);
											// console.log('value1.recordId', value1.recordId);
											if (((vm.department == "Engineering" && vm.operator == value.preparedby) ||(vm.department == "Production" && vm.operator == value.operator) || (vm.department == "QA" && (vm.operator == value.qainspector || vm.operator == value.inspectedby))) && value1.approved == "draft" && value1.recordId == val && keyy == 'rowid' && Type == 'Draft') {
												console.log('draft')
												console.log('draft value',value);
												vm.TableRecords.push(value);
											} else if (Type == 'Reject') {
												if (value1.recordId == val && keyy == 'rowid' && Type.toLowerCase() == value1.approved.toLowerCase()) vm.TableRecords.push(value);
											} else if (Type == 'done') {
												console.log('found here0');
												if (value1.recordId == val && keyy == 'rowid' && Type.toLowerCase() == value1.approved.toLowerCase()) {
													if (vm.compare.indexOf(value.rowid) == -1) {
														vm.TableRecords.push(value);
														vm.compare.push(value.rowid)
													}
												}
											} else if (Type == 'inprocess' && value1.approved != "draft") {

												console.log('inprocess tablerecords');

												if (value1.recordId == val && keyy == 'rowid' && (!vm.formName.toUpperCase().includes('IN-PROCESS SET UP APPROVAL') && !vm.formName.toUpperCase().includes('FLAMMABILITY REPORT')) ||
													value1.recordId == val && (value.supervisorname == vm.loggedInUser || value.senioroperator == vm.operator || value.approvedby == vm.loggedInUser || value.inspectedby == vm.operator || (value.operatorname == vm.operator && vm.loginUserRole == "Operator" && vm.department == "Production")) ||
													(value.qaincharge == $window.localStorage.empName) && keyy == 'rowid') {
												console.log('value.qaincharge == ',value.qaincharge);
												console.log('$window.localStorage.empName',$window.localStorage.empName);

													console.log('value1', value1);
													if (vm.department != value1.deptName || vm.loginUserRole == 'Operator' || (vm.loginUserRole == 'Approver' && vm.department=="QA" && vm.formName.toUpperCase().includes('INSPECTION REPORT') && value.qaincharge == $window.localStorage.empName) || (vm.department == value1.deptName && value1.approved != "approved" && value1.updatedBy == vm.department))
														vm.TableRecords.push(value);
												}
											} else {
												if (value1.recordId == val && keyy == 'rowid' && vm.department == 'QA') {
													console.log("QA tablerecords");
													console.log('vm.loggedInUser', vm.loggedInUser);
													console.log('vm.value.qaincharge', value.qaincharge);
													console.log('vm.loginUserRole', vm.loginUserRole);
													console.log('vm.formName.toUpperCase()', vm.formName.toUpperCase());
													console.log(vm.formName.toUpperCase().includes('INSPECTION REPORT') && vm.loginUserRole == "Approver" && value.qaincharge == vm.loggedInUser);
													if (value.qainspector == vm.loggedInUser || value.qaleaktest == vm.loggedInUser ||
														value.qagumming1 == vm.loggedInUser || value.qabushcasting == vm.loggedInUser ||
														(vm.formName.toUpperCase().includes('INSPECTION REPORT') && vm.loginUserRole == "Approver" && value.qaincharge == vm.loggedInUser) ||
														(vm.formName.toUpperCase().includes('FLAMMABILITY REPORT') && vm.loginUserRole == "Approver")) {
														flag = false;
														console.log('entered qa tablerecords');
														vm.TableRecords.push(value);
													}
												} else if (value1.recordId == val && keyy == 'rowid' && vm.department == 'Production' && value1.approved != "draft") {
													console.log("Production tablerecords");
													console.log('vm.loggedInUser', vm.loggedInUser);
													console.log('value.senioroperator', value.senioroperator);
													if (value.supervisor == vm.loggedInUser || value.operator == vm.operator || value.supervisorname == vm.loggedInUser || value.senioroperator == $window.localStorage.empCode + ' ' + $window.localStorage.empName) {
														flag = false;
														if (value1.approved == 'transfer') vm.moveToDept = value1.updatedBy;
														vm.TableRecords.push(value);
													}
												} else if (value1.recordId == val && keyy == 'rowid' && vm.department == 'Engineering') {
													console.log('found here');
													if (value1.approved == 'transfer') vm.moveToDept = value1.updatedBy;
													flag = false;
													if (value1.recordId == val && keyy == 'rowid' && Type.toLowerCase() == value1.approved.toLowerCase()) {
														vm.TableRecords.push(value);
														// && value.approverby==vm.loggedInUser
													} else if (value1.recordId == val && keyy == 'rowid' && 'approved' == value1.approved.toLowerCase()) {
														vm.TableRecords.push(value);
													}
												} else if (value1.recordId == val && keyy == 'rowid' && vm.department == 'Mold Maintenance') {
													console.log('mold');
													flag = false;
													vm.TableRecords.push(value);
												}
											}
										});
									})
								)
								$q.all(promiscall).then(function(datavalue) {
									if (vm.department == 'Engineering' && flag && Type != 'Reject'  && Type != 'Draft' && Type != 'Transfer' && Type != 'Return' && Type != 'done' && Type != 'inprocess') {
										vm.TableRecords.push(value);
									}
								})
							}
						})
					} else if (vm.department == 'Engineering' && Type != 'Reject'&& Type != 'Draft' && Type != 'Transfer' && Type != 'Return' && Type != 'done' && Type != 'inprocess') {

						console.log('found here 2');
						angular.forEach(data.rows, function(value, key) {
							vm.TableRecords.push(value);
						});
					} else {
						console.log('found here 3');
						console.log('res ', res);
						if (Type == 'Draft') {
							console.log('found here 3 sub 0');
							var query = "SELECT  * FROM  DF_TBL_APR WHERE status='a' AND approved='draft' AND tableName ='" + vm.selectedTable + "' AND updatedBy ='" + $window.localStorage.userDept + "'";
							getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {
								var flag = '';
								angular.forEach(data.rows, function(value, key) {
									flag = false;
									angular.forEach(reqVal.data, function(value1, key1) {
										if (value1.recordId == value.rowid && value1.approved == 'draft') flag = true
									});
									if (flag) {
										vm.TableRecords.push(value);
									}
								});
							})
						} else if (res.rows.length == 1 && Type != 'Reject' && Type != 'Transfer' && Type != 'Return' && Type != 'done' && Type != 'inprocess') {
							console.log('found here 3 sub 1');
							var query = "SELECT  * FROM  DF_TBL_APR WHERE status='a' AND approved='done' AND tableName ='" + vm.selectedTable + "' AND updatedBy ='" + $window.localStorage.userDept + "'";
							getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {
								console.log('reqVal ', reqVal);
								var flag = '';
								angular.forEach(data.rows, function(value, key) {
									flag = true
									angular.forEach(reqVal.data, function(value1, key1) {
										if (value1.recordId == value.rowid) flag = false
									});
									if (flag) {
										vm.TableRecords.push(value);
									}
								});
							})
						} else if (res1.rows.length == 0 && Type == '') {
							console.log('found here 3 sub 2');
							var query = "SELECT  * FROM  DF_TBL_APR WHERE  status='a' AND tableName ='" + vm.selectedTable + "'";
							getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {
								console.log('found here 3 sub 2 reqVal', reqVal);
								var flag = '';

								if (reqVal.data.length > 0) {
									angular.forEach(data.rows, function(value, key) {
										console.log('value1', value);
										flag = false;
										var valueApproved = '',
											reqValDept = '';
										angular.forEach(reqVal.data, function(value1, key1) {
											if (value1.recordId == value.rowid) {
												// console.log('value1',value1);
												// console.log('value1.recordId',value1.recordId);
												// console.log('value.rowid',value.rowid);
												flag = true;
												valueApproved = value1.approved;
												reqValDept = value1.deptName;
											}
										});
										if (flag) {
											console.log('reqVal.deptName ', reqVal.deptName);
											console.log('vm.loginUserRole ', vm.loginUserRole);
											console.log('value.qainspector', value.qainspector);
											console.log('vm.department', vm.department);
											console.log('vm.loggedInUser', vm.loggedInUser);
											console.log('flag true:', flag && reqVal.deptName == "Mold Maintainance" && vm.loginUserRole == "Approver" && value.qainspector == vm.loggedInUser);
										}

										// if (flag && value.supervisorname == vm.loggedInUser && valueApproved != 'done' &&
										// (vm.department == "deptName" && vm.loginUserRole == "Approver" && reqValDept != "QA"))
										if (flag && value.supervisorname == vm.loggedInUser && valueApproved != 'done' &&
											(vm.loginUserRole == "Approver" && reqValDept != "QA"))
											vm.TableRecords.push(value);
										else if (flag && reqValDept == "Mold Maintainance" && vm.loginUserRole == "Approver" && value.qainspector == vm.loggedInUser) {
											vm.TableRecords.push(value);
										}
									});
								} else {
									angular.forEach(data.rows, function(value, key) {
										vm.TableRecords.push(value);
									})
								}
							})
						} else if (res.rows.length > 0) {
							angular.forEach(res.rows, function(resRows) {
								// 
							});
						}
					}
				});

			});
			notificationService.hideLoad();
		});
	}

	function showAllRecord(record, presentRecorIndex) {
		var FormName = $state.params.getFormName;
		if (FormName.toUpperCase().includes("DAILY OPERATOR") && vm.typeRejected.toUpperCase() == "RETURN") {
			$state.go('getForms', { 'getFormName': FormName, 'type': 'return', 'selectedId': record.recordName }, { reload: true });
		} else if (FormName.toUpperCase().includes("LEAK TESTING") && vm.typeRejected.toUpperCase() == "RETURN") {
			$state.go('getForms', { 'getFormName': FormName, 'type': 'return', 'selectedId': record.recordName }, { reload: true });
		}
		/*else if(FormName.toUpperCase().includes("MOLD CHANGE")){
				  $state.go('getForms', {'getFormName': FormName, 'type': 'return', 'selectedId':record.recordName}, {reload:true}); 
				}*/
		if (vm.selectedRecord != record.recordName) {
			vm.showtable = false;
			vm.showRecordDetails = true;
			vm.selectedRecord = record.recordName;
			vm.presentRecorIndex = presentRecorIndex;
			$scope.activeMenu = vm.presentRecorIndex;
			notificationService.showSpinner();
			$('#border_getform').addClass('border-getForm');
			vm.showInput = true;
			vm.hideButtons = true;
			for (var i = 0; i < vm.tableName.length; i++) {
				if (vm.selectedTable == vm.tableName[i].rowid) {
					vm.selectedTableShow = vm.tableName[i].tableName;
				}
			}
			vm.rowData = record;
			vm.showInput = false;
			vm.tableValue = [];
			var subId = 0;
			var recordId = '';
			angular.forEach(vm.formFields, function(value, key) {
				var table = '[]';
				var deferred = $q.defer();
				var tableData = [];
				var checkvalue = '';
				var Restrict = '';
				var Remark = '';
				var colSpan = '';
				var Rquired = '';
				var Name = '';
				var FieldOption = [];
				var Cavity = ''
					//Setting Array 3- for Colam Span
					//Setting Array 4 - for restriction
				var ColCondValue = '';
				var readOnly = '';
				var ColValue1 = '';
				var colCondition = false;
				var Disabled = '';
				angular.forEach(value.Settings, function(value1, key1) {
					if (value1.Name == 'Internal Name') {
						colSpan = value.Settings[3].Value;
						Name = value.Name;
						if (value.Type == 'radio') {
							FieldOption = value.Settings[2].PossibleValue;
							if (value.Settings[4].Restrict != '') Restrict = value.Settings[4].Restrict[vm.department];
							else Restrict = true;
							Rquired = value.Settings[4].Options[0].Value;
							Disabled = value.Settings[4].Options[0].Value;
						} else if (value.Type == 'checkbox') {
							FieldOption = value.Settings[2].PossibleValue;
							if (value.Settings[4].Restrict != '') Restrict = value.Settings[4].Restrict[vm.department];
							else Restrict = true;
							Rquired = value.Settings[4].Options[0].Value;
							Disabled = value.Settings[4].Options[0].Value;
						} else if (value.Type == 'dropdown') {
							/*angular.forEach(value.Settings[2].PossibleValue, function(avalue, aKey){
								console.log("))0000000000000000000", avalue)
								FieldOption.push({"Text": avalue.Text})
							})*/
							FieldOption = value.Settings[2].PossibleValue;
							console.log('FieldOption', FieldOption);
							if (value.Settings[4].Restrict != '') Restrict = value.Settings[4].Restrict[vm.department];
							else Restrict = true;
							Rquired = value.Settings[4].Options[0].Value;
							Disabled = value.Settings[4].Options[1].Value;
							if (value.Settings[4].Cavity != '') Cavity = value.Settings[4].Cavity;

						} else if (value.Type != 'table') {
							if (value.Settings[4].Restrict != '') Restrict = value.Settings[4].Restrict[vm.department];
							else Restrict = true;
							Rquired = value.Settings[4].Options[0].Value;
						} else {

						}
						if (FormName.toUpperCase().includes("LEAK")) {
							angular.forEach(vm.tableValue, function(valLeak, keyLeal) {
								if (valLeak.InternalName == 'process') {
									varProcess = valLeak.Value;
									if ((value.Settings[1].Value).includes((valLeak.Value).toLowerCase().split(" ")[0])) {
										Restrict = true;
									} else {
										Restrict = false;
									}
								} else if (valLeak.Type = 'dropdown') {
									if ((valLeak.InternalName.toUpperCase()).includes('BUSH') || (valLeak.InternalName.toUpperCase()).includes('LEAK') || (valLeak.InternalName.toUpperCase()).includes('GUMM')) {
										var aaa = (varProcess.split(" ")[0]).toLowerCase();
										if ((valLeak.InternalName).includes(aaa)) {
											value.Restrict = true;
										} else {
											value.Restrict = false;
										}
									}

								}
							})
						}


						if (value.Type == 'table') {
							Rquired = value.Settings[5].Options[0].Value;
							var val = (value1.Value).replace(' ', '_');
							if (value.Settings[5].colCondition && value.Settings[5].ColCondValue != undefined && value.Settings[5].ColCondValue != '' && value.Settings[5].ColCondValue.Condtion != undefined) {
								ColCondValue = value.Settings[5].ColCondValue;
								colCondition = value.Settings[5].colCondition;
							}
							if (value.Settings[5].Restrict != '' && value.Settings[5].Restrict != undefined) {
								Restrict = value.Settings[5].Restrict[vm.department] != undefined ? value.Settings[5].Restrict[vm.department] : false;
							} else {
								Restrict = true;
							}

							if (FormName.toUpperCase().includes("LEAK")) {
								var varProcess = ''
								angular.forEach(vm.tableValue, function(valLeak, keyLeal) {
									if (valLeak.InternalName == 'process') {
										varProcess = valLeak.Value;
										if ((value.Settings[1].Value).includes((valLeak.Value).toLowerCase().split(" ")[0])) {
											Restrict = true;
										} else {
											Restrict = false;
										}
									}
								})
							}


							if (record[val] != undefined) {
								recordId = record[val];
								/*vm.tableRowName = val;
								vm.tableRowId = recordId;*/
							} else {
								record[val] = recordId;
							}
							if (value.Settings[5].Remark != '' && value.Settings[5].Remark != undefined) Remark = value.Settings[5].Remark;
							else Remark = '';
							if (value.Settings[5].Cavity != '' && value.Settings[5].Cavity != undefined) Cavity = value.Settings[5].Cavity;
							else Cavity = '';

							var columns = [];
							var addColumn = true;
							var promiscall = [];

							getFormsRecordsService.getInnerTableRecord(vm.selectedTable, subId, recordId, FormName).then(function(data) {
									var rows = [];
									angular.forEach(data.rows, function(value2, key2) {
										var cells = [];
										angular.forEach(value2, function(value3, key3) {
											if (angular.isString(value3) && value3.split("_").length > 1) {
												var vall = value3.split("_");
												angular.forEach(vall, function(val4, key4) {
													var colIndex = key3.replace("column", '') - 1;
													if (key4 == 0) {
														if (addColumn) {
															columns.push({ 'Value': key3, 'Value1': value.Settings[3].PossibleValue[0].columns[colIndex].value1 })
															if (value.Settings[5].Remark > 0) {
																var cavLen = parseInt(Remark) + vall.length;
																Remark = cavLen - 1;
															}
														}
														cells.push({ 'Value': val4 })
													} else if (key4 > 0) {
														if (addColumn) {
															columns.push({ 'Value': key3 + '_' + (key4), 'Value1': value.Settings[3].PossibleValue[0].columns[colIndex].value1 + '_' + (key4), 'cavity': true })
														}
														cells.push({ 'Value': val4, 'cavity': true })
													} else {

													}
												})
											} else {
												if (addColumn) {
													var showTime = false;
													var showDate = false;
													angular.forEach(value.Settings[3].PossibleValue[0].columns, function(rVal, rKey) {
														if (key3 == rVal.value) {
															readOnly = rVal.readonly == true ? true : false;
															ColValue1 = value.Settings[3].PossibleValue[0].columns[rKey].value1;
															showTime = rVal.showTime;
															showDate = rVal.showDate;
														}
													})
													columns.push({ 'Value': key3, 'Value1': ColValue1, 'readOnly': Boolean(readOnly), 'showTime': showTime, "showDate": showDate })
												}
												cells.push({ 'Value': value3 })
											}
										});
										rows.push({ 'cells': cells, 'rowid': value2.rowid });
										addColumn = false;
									});
									tableData.push({ 'columns': columns });
									tableData.push({ 'rows': setColumnInOrder(rows, record) });
									if (vm.tableValue.length >= value.id - 1 && !(FormName.toUpperCase().includes("PACK NOTE"))) {
										vm.tableValue.splice(vm.tableValue.length - 1, 0, { 'Value': { 'table': tableData }, 'InternalName': val, 'Type': 'table', 'Restrict': Restrict, 'ColSpan': colSpan, 'Rquired': Rquired, 'Name': Name, 'ColmRestrict': value.Settings[5].ColmRestrict, 'Remark': Remark, 'Cavity': Cavity, 'ColCondValue': ColCondValue, 'colCondition': colCondition });
										if (FormName.toUpperCase().includes("CHEMICAL")) {
											settabledatechemical(vm.tableValue)
										} else if ((FormName.toUpperCase()).includes("INSPECTION REPORT") || FormName.toUpperCase().includes("SHIFT")) {
											setInspectionRepott(vm.tableValue);
										}
									} else {
										vm.tableValue.push({ 'Value': { 'table': tableData }, 'InternalName': val, 'Type': 'table', 'Restrict': Restrict, 'ColSpan': colSpan, 'Rquired': Rquired, 'Name': Name, 'ColmRestrict': value.Settings[5].ColmRestrict, 'Remark': Remark, 'Cavity': Cavity, 'ColCondValue': ColCondValue, 'colCondition': colCondition })
										if (FormName.toUpperCase().includes("CHEMICAL")) {
											settabledatechemical(vm.tableValue)
										} else if ((FormName.toUpperCase()).includes("INSPECTION REPORT") || FormName.toUpperCase().includes("SHIFT")) {
											setInspectionRepott(vm.tableValue);
										}
										if (FormName.toUpperCase().includes("PACK NOTE")) {
											settabledatepacknote(vm.tableValue)
										}

									}
									notificationService.hideLoad();
								})
								++subId;
						} else if (value.Type == 'checkbox' && record[value1.Value] != undefined) {
							vm.tableValue.push({ 'Value': record[value1.Value].length > 0 ? JSON.parse(record[value1.Value]) : record[value1.Value], 'InternalName': value1.Value, 'Type': value.Type, 'Restrict': Restrict, 'ColSpan': colSpan, 'Rquired': Rquired, 'Name': Name, 'ReadOnly': Disabled, 'FieldOption': FieldOption })
						} else if (record[value1.Value] != undefined) {
							Disabled = value.Settings[4].Options[1].Value;
							if (value.Type == 'text' && value.Settings[4].AutoFill != '' && value.Settings[4].AutoFill != undefined) {
								Disabled = value.Settings[4].Options[2].Value;
								if (value.Settings[4].AutoFill == $window.localStorage.userDept + " " + $window.localStorage.userType) {
									record[value1.Value] = $window.localStorage.empName + " " + $window.localStorage.empCode;
								} else if (value.Settings[4].AutoFill.includes('QA') && ($state.params.getFormName).toUpperCase().includes("IN-PROCESS") && $window.localStorage.userDept == 'QA') {
									record[value1.Value] = $window.localStorage.empName + " " + $window.localStorage.empCode;
								}
							}

							if (value.Type == 'dropdown') {
								var cFlag = false;
								angular.forEach(FieldOption, function(cValue, cKey) {
										if (record[value1.Value] == cValue.Text) cFlag = true;
									})
									/*if(!cFlag){
										FieldOption = [];
										FieldOption.push({"Text": record[value1.Value]})
									}*/
							}
							if (value.Type == 'date') {
								record[value1.Value] = vm.SqlDateFormate(record[value1.Value])
							}
							vm.tableValue.push({ 'Value': record[value1.Value], 'InternalName': value1.Value, 'Type': value.Type, 'Restrict': Restrict, 'ColSpan': colSpan, 'Rquired': Rquired, 'Name': Name, 'FieldOption': FieldOption, 'Cavity': Cavity, 'ReadOnly': Disabled })
						} else {

						}
					}
				});
			});

			if (vm.tableRowId == 0) {
				vm.tableRowId = record.rowid;
				vm.tableRowName = 'rowid';

				notificationService.hideLoad();
			}
			vm.formRecord = vm.formFields;

		}
	}

	function settabledatepacknote(value) {
		var promiscall = [];
		var rows = [];
		var tablename = '';
		if ($window.localStorage.userDept == 'QA' && $window.localStorage.userType == 'Operator' && $state.params.type == '') {
			angular.forEach(value, function(val, key) {
				if (val.InternalName == "partno") {
					var query = "select * from DF_TBL_PGR_4108 where partno='" + val.Value + "'";
					constantService.selectedTableName = "DF_TBL_PGR_4108";
					getFormsService.getTableRecordBasedOnQuery(query).then(function(resul2) {
						if (resul2.data.length > 0) {
							angular.forEach(value, function(vall, keyl) {
								if (vall.Type == 'table' && resul2.data[resul2.data.length - 1][vall.InternalName] != null) {
									var query = "select * from DF_TBL_PGR_TBL_4108_0 where tableid='" + resul2.data[resul2.data.length - 1][vall.InternalName] + "'  order by column1 ASC";
									/*promiscall.push(*/
									getFormsService.getTableRecordBasedOnQuery(query).then(function(resul1) {
											if (resul1.data.length > 0) {
												angular.forEach(resul1.data, function(value1, keyy1) {
													var cells = [];
													angular.forEach(vall.Value.table[1].rows[0].cells, function(val3, key3) {
														if (key3 > 6 && keyy1 == 0) {
															var aa = parseInt(key3) - 2;
															var bb = 'column' + aa;
															//cells[key3].Value = value1[bb];
															cells.push({ "Value": value1[bb], 'Edited': true });
														} else if (key3 > 6 && keyy1 > 0) {
															var aa = parseInt(key3) - 2;
															var bb = 'column' + aa;
															//val3.Value = value1[bb];
															//cells[key3].Value = value1[bb]
															cells.push({ "Value": value1[bb], 'Edited': true, 'NewAdded': true });
														} else {

															if (vall.Value.table[0].columns[key3].Value1 == 'Sl No') {
																val3.Value = parseInt(keyy1) + 1;
																cells.push({ "Value": val3.Value });
															} else {
																cells.push(val3);
															}

														}

														if (key3 == vall.Value.table[1].rows[0].cells.length - 1) {
															rows.push({ "cells": cells });
														}
														if (key3 == vall.Value.table[1].rows[0].cells.length - 1 && keyy1 == resul1.data.length - 1) {
															/*vall.Value.table[1].rows.push({"cells":cells});*/
															vall.Value.table[1].rows = [];
															angular.forEach(rows, function(val, key) {
																vall.Value.table[1].rows.push(val)
															})
														}
													});
												})
											}
										})
										/*)*/
								}
							});

						}

					});

				}
			})
		}
	}

	function setInspectionRepott(value) {
		angular.forEach(value, function(value1, key1) {
			if (value1.Type == 'table') {
				angular.forEach(value1.Value.table[0].columns, function(value2, key2) {
					if (value2.Value == 'column1') {
						value2.showColumn = true;
					} else {
						value2.showColumn = false;
					}
				});
			}
		})
	}

	function settabledatechemical(value) {
		var selectedProcess = ''
		angular.forEach(value, function(val, key) {
			if (val.Name == "Process") {
				selectedProcess = val.Value
			}
		})
		switch (selectedProcess) {
			case 'Die Coat Chemical Mixing':
				angular.forEach(value, function(value1, key1) {
					if (value1.Type == 'table') {
						angular.forEach(value1.Value.table[0].columns, function(value2, key2) {
							if (key2 > 2 && key2 < 6) {
								value2.showColumn = false;
							} else {
								value2.showColumn = true;
							}
						});
					}
				})
				break;
			case 'Gum Mixing':
				angular.forEach(value, function(value1, key1) {
					if (value1.Type == 'table') {
						angular.forEach(value1.Value.table[0].columns, function(value2, key2) {
							if (key2 > 2 && key2 < 6) {
								value2.showColumn = true;
							} else {
								value2.showColumn = false;
							}
						});
					}
				})
				break;

			case 'Cork Mixing':
				angular.forEach(value, function(value1, key1) {
					if (value1.Type == 'table') {
						angular.forEach(value1.Value.table[0].columns, function(value2, key2) {
							if (key2 > 2 && key2 < 6) {
								value2.showColumn = true;
							} else {
								value2.showColumn = false;
							}
						});
					}
				})
				break;
			default:
		}
	}

	function getTableDetails(taleId) {
		notificationService.showSpinner();
		vm.formFields = [];
		createFormsService.getTableByName(taleId).then(function(record) {
			if ($state.current.name == 'getFormRecords') {
				vm.formName = record.rows[0].tableName;
				console.log('is form vm.formName', vm.formName);
			}
			angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
				addElement(value);
			});
			notificationService.hideLoad();
		});
	}

	function setColumnInOrder(rows, record) {
		var flag = false;
		var rowww = [];
		if (record.shift != undefined && rows.length > 6) {
			angular.forEach(constantService[record.shift], function(value, key) {
				angular.forEach(rows, function(value1, key1) {
					console.log(value, (value1.cells[3].Value))
					if (value == value1.cells[3].Value) {
						console.log(value1)
						rowww.push(value1);
					}
				});
				if (key == constantService[record.shift].length - 1 && rowww.length == rows.length) { flag = true; }
			});
		}
		if (flag) {
			return rowww;
		} else {
			return rows;
		}
	}

	function createNewField() {
		return {
			'id': ++guid,
			'Name': '',
			'Settings': [],
			'Active': true,
			'ChangeFieldSetting': function(Value, SettingName) {
				switch (SettingName) {
					case 'Field Label':
						{
							vm.current_field.Name = Value;
							vm.current_field.Settings[0].Value = $scope.current_field.Name;
							break;
						}
					case 'Internal Name':
						{
							vm.current_field.Settings[2].Value = Value.replace(/\s/g, '_');
							break;
						}

					default:
						break;
				}
			},
			'GetFieldSetting': function(settingName) {
				var result = {};
				var settings = this.Settings;
				$.each(settings, function(index, set) {
					if (set.Name == settingName) {
						result = set;
						return;
					}
				});
				if (!Object.keys(result).length) {
					//Continue to search settings in the checkbox zone
					$.each(settings[settings.length - 1].Options, function(index, set) {
						if (set.Name == settingName) {
							result = set;
							return;
						}
					});
				}
				return result;
			}
		};
	}

	function addElement(ele, idx) {
		vm.current_field.Active = false;
		vm.current_field = createNewField();
		//Merge setting from template object
		angular.merge(vm.current_field, ele);

		if (typeof idx == 'undefined') {
			vm.formFields.push(vm.current_field);
		} else {
			vm.formFields.splice(idx, 0, vm.current_field);
			//$('#fieldSettingTab_lnk').tab('show');
		}
	};

	function updateRecord() {
		$window.localStorage.approved = vm.approved;
		console.log('vm.approved', vm.approved);
		var updateString = '';
		if (vm.approved == 'return') vm.moveToDept = vm.returnDept[0];
		if (vm.approved == 'transfer') vm.moveToDept = vm.deptTransform;
		// if (vm.approved == 'approved' && $window.localStorage.userType == 'Approver' && vm.department == "Production" && FormName.toUpperCase().includes("MOLD CHANGE CHECK SHEET"))
		// 	vm.approved = 'done';
		if (vm.setMoveToDept == 'done' && vm.approved == 'approved' && $window.localStorage.userType != 'Operator') {
			vm.moveToDept == 'done'
			vm.approved = 'done';
		}
		if (vm.approved == 'approved' && $window.localStorage.userType == 'Approver' && vm.department == "Production" && FormName.toUpperCase().includes("Daily Operator Work Allotment")) {
			vm.approved = 'done';
		}
		if (vm.moveToDept == undefined && vm.approved == 'return' && vm.department == "Engineering") {
			vm.moveToDept = "Engineering"
		} else if (vm.approved == 'approved' && vm.department == "Engineering" && $window.localStorage.userType == 'Operator') {
			vm.moveToDept = "Engineering"
		} else if (vm.moveToDept == undefined && vm.approved == 'return' && vm.department == "QA") {
			vm.moveToDept = 'QA'
		} else if (vm.moveToDept == undefined && vm.approved == 'return' && vm.department == "Production") {
			vm.moveToDept = 'Production'
		}
		if (vm.department == 'QA' && vm.approved == 'approved') {
			if (FormName.toUpperCase().includes("IN-PROCESS") || FormName.toUpperCase().includes("LEAK") || FormName.toUpperCase().includes("CHEMICAL") || FormName.toUpperCase().includes("MATERIAL PRE") || FormName.toUpperCase().includes("PACK NOTE")) {
				vm.approved = 'done';
			}
		}
		// if (vm.department == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Approver' && FormName.toUpperCase().includes('IN-PROCESS SET UP APPROVAL')) {
		// 	vm.approved = 'done';
		// }
		// if (vm.department == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Approver' && FormName.toUpperCase().includes('LEAK TESTING CUM GUMMING APPLICATION RECORD')) {
		// 	vm.moveToDept = 'QA';
		// }
		if (vm.department == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Operator') {
			if (FormName.toUpperCase().includes("MATERIAL PRE")) {
				vm.moveToDept = 'Production';
			}
		}

		if (vm.department == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Approver') {
			if (FormName.toUpperCase().includes("MOLD CHANGE CHECK SHEET")) {
				vm.moveToDept = 'Mold Maintainance';
				vm.approved = "approved";
				console.log('Mold Maintainance');
			}
		}
		if (vm.department == 'Mold Maintenance' && vm.approved == 'approved') {
			if (FormName.toUpperCase().includes("MOLD")) {
				vm.approved = 'done';
				vm.moveToDept = 'Production';
			}
		}
		if (FormName.toUpperCase().includes("CHEMICAL") && vm.approved == 'approved' && $window.localStorage.userType == 'Operator') {
			vm.moveToDept = 'Production'
		}
		//consider as updating record 
		if (vm.moveToDept == undefined && vm.comment == undefined) {
			vm.comment = 'Updating Record';
			vm.moveToDept = vm.department;
		}
		if (vm.moveToDept == undefined && vm.approved == 'approved' && $window.localStorage.userType == 'Operator') {
			vm.moveToDept = vm.department;
		}

		// if ((vm.approved == 'return'||vm.approved == 'reject') && $window.localStorage.userType == 'Approver' && vm.department == "Production" && FormName.toUpperCase().includes("Daily Operator Work Allotment")) {
		// 	if(vm.approved == 'return'){vm.approved = 'return';}else vm.approved = 'reject';
		// }
		console.log("vm.approved upadted chec", vm.approved);

		if (vm.approved != undefined) {
			notificationService.showSpinner();
			console.log('vm.selectedTable', vm.selectedTable);
			console.log('vm.tableRowId', vm.tableRowId);
			console.log('vm.tableValue', vm.tableValue);
			console.log('vm.approved', vm.approved);
			console.log('vm.selectedTableShow', vm.selectedTableShow);
			getFormsRecordsService.updateRecord(vm.selectedTable, vm.tableRowId, vm.tableRowName, vm.tableValue, vm.approved, vm.selectedTableShow, vm.updateDept, vm.moveToDept, vm.rejectToDept).then(function(record) {

				getFormsRecordsService.updateAprrovalProcess(vm.formLevelInnumber, vm.selectedTable, vm.approved, vm.comment, vm.tableRowId).then(function(data) {

					getFormsRecordsService.getApproverDetails(vm.selectedTable, vm.tableRowId).then(function(data) {

						vm.serviceMailInformation = {
							serviceRecordId: vm.tableRowId,
							serviceFirstPerson: data.firstPerson,
							serviceSecondPerson: data.secondPerson,
							serviceThirdPerson: data.thirdPerson
						};
						recordDetailsService.getStatusInformation(vm.selectedTable, vm.tableRowId).then(function(response) {
							SendEmailService.sendmailNotifiction(vm.serviceMailInformation, response);
						});
					});
					notificationService.hideLoad();
					notificationService.alert('', 'Form Updated Successfully', function() {
						$state.reload();
					});
					if (vm.approved != '' && vm.approved != undefined) {
						vm.TableRecords.splice(vm.presentRecorIndex, 1);
						vm.tableValue = [];
						vm.showRecordDetails = false;
					}
					/*vm.approved = '';*/
				});
			});
		} else {
			notificationService.hideLoad();
			notificationService.alert('', 'Select any one approval');
		}
	}

	function valuationDatePickerOpen($event) {
		if ($event) {
			$event.preventDefault();
			$event.stopPropagation(); // This is the magic
		}
		this.valuationDatePickerIsOpen = true;
	};

	$scope.openMenu = function() {
		$ionicSideMenuDelegate.toggleLeft();
	};

	function checkColrestrict(colm, data, index) {
		var ret = true;
		angular.forEach(data, function(val, key) {
			if (colm == val.column) {
				if (val.depts.length == 0) {
					ret = true;
				} else if (val.depts.indexOf(vm.department) == -1) {
					if (vm.hideIndex.indexOf(index)) {
						vm.hideIndex.push(index)
					}
					ret = false;
				} else {
					ret = true;
				}
			}
		});
		return ret;
	}

	function cancel() {
		notificationService.confirm('Are you sure want to cancel', 'Yes', 'No', function() {
			$state.reload();
		}, function() {})
	}

	function ColumanCondition(col, celll, tablejson, cel) {
		/*console.log('cel1111 ', cel)
	    console.log('col ', col)
	    console.log('celll ', celll)
	    console.log('tablejson ', tablejson)*/


		setEditTrue(cel)
		var colIndex = tablejson.ColCondValue.resultCol
		var flag = false;
		if (tablejson.colCondition || cel.cavity) {
			var aa = tablejson.ColCondValue.Condtion;
			var index = '';
			if (cel.cavity) {
				index = 10;
			} else {
				if (col.includes('_')) col = col.split("_")[0];
				index = aa.indexOf(col);
			}

			if (index >= 0 && aa.length > 3) {
				var a = celll[parseInt(aa[0].replace("column", '')) + 2].Value;
				var b = celll[parseInt(aa[2].replace("column", '')) + 2].Value;
				var c = '';
				if (cel.cavity) {
					c = cel.Value;
				} else {
					c = celll[parseInt(aa[4].replace("column", '')) + 2].Value;
				}
				var TableCavityCount = 1;
				angular.forEach(tablejson.Value.table[0].columns, function(cellvalue, cellKey) {
					if (cellvalue.cavity) TableCavityCount++;
				})
				if (aa[1] == "+/-") {
					if (TableCavityCount == 1) {
						var count = 0;
						celll[parseInt(tablejson.Remark) + 3]['Edited'] = true;
						angular.forEach(a.split("/"), function(value, key) {
							if (b.split("/").length > 1) {
								if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b.split("/")[1])).toFixed(2) && parseFloat(c.split("/")[key]) >= (parseFloat(value) + parseFloat(b.split("/")[0])).toFixed(2)) {
									count++;
								} else {
									celll[parseInt(tablejson.Remark) + 3].Value = false
								}
							} else {
								if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b)).toFixed(2) && parseFloat(c.split("/")[key]) >= (value - b).toFixed(2)) {
									count++;
								} else {
									celll[parseInt(tablejson.Remark) + 3].Value = false
								}
							}
						})
						if (c.split("/").length > a.split("/").length) {
							celll[parseInt(tablejson.Remark) + 3].Value = false;
						} else if (count == a.split("/").length) {
							celll[parseInt(tablejson.Remark) + 3].Value = true
						} else if (b.toUpperCase() == 'REF') {
							celll[parseInt(tablejson.Remark) + 3].Value = true
						} else {
							celll[parseInt(tablejson.Remark) + 3].Value = false
						}
					} else if (TableCavityCount > 1) {
						var cavityStartNumber = 0;
						var flagg = true;
						var count = 0;
						var noofcavityColumn = 0;
						angular.forEach(tablejson.Value.table[0].columns, function(cellvalue, cellKey) {
							if (parseInt(aa[4].replace("column", '')) + 2 == cellKey || cellvalue.cavity) {
								if (flagg) {
									cavityStartNumber = cellvalue.Value.replace("column", '') - 1;
									flagg = false;
								}
								c = celll[parseInt(cellKey)].Value;
								cavityStartNumber++;
								angular.forEach(a.split("/"), function(value, key) {
									if (b.split("/").length > 1) {
										if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b.split("/")[1])).toFixed(2) && parseFloat(c.split("/")[key]) >= (parseFloat(value) + parseFloat(b.split("/")[0])).toFixed(2)) {
											count++;
										} else {
											celll[parseInt(tablejson.Remark) + 3].Value = false
										}
									} else {
										if (parseFloat(c.split("/")[key]) <= parseFloat((parseFloat(value) + parseFloat(b)).toFixed(2)) && parseFloat(c.split("/")[key]) >= parseFloat((value - b).toFixed(2))) {
											count++;
										} else {
											celll[parseInt(tablejson.Remark) + 3].Value = false
										}
									}
								})
								noofcavityColumn++;

							}

							var qq = a.split('/').length
							if (cellKey == tablejson.Value.table[0].columns.length - 1 && count / (a.split('/').length) == noofcavityColumn) {
								celll[parseInt(tablejson.Remark) + 3].Value = true
							} else if (b.toUpperCase() == 'REF') {
								celll[parseInt(tablejson.Remark) + 3].Value = true
							} else {
								celll[parseInt(tablejson.Remark) + 3].Value = false
							}
						})
					}

				} else if (aa[1] == "+") {
					if ((a - b) == c) {
						celll[parseInt(tablejson.Remark) + 3].Value = true
					} else {
						celll[parseInt(tablejson.Remark) + 3].Value = false
					}
				} else if (aa[1] == "-") {
					if ((parseInt(a) + parseInt(b) == c)) celll[tablejson.Remark].Value = true
					else celll[parseInt(tablejson.Remark) + 3].Value = false
				} else {

				}
			} else {
				var a = celll[aa[0].replace("column", '') - 1].value;
				var b = celll[aa[2].replace("column", '') - 1].value;

				var time1 = (a.split(":")[0] * 60 * 60) + (a.split(":")[1] * 60);
				var time2 = (b.split(":")[0] * 60 * 60) + (b.split(":")[1] * 60);
				if (time1 >= 0 && time2 >= 0) {
					var settime = (time2 - time1) / (60 * 60);
					var timemin = (settime.toString().split(".")[1]) != undefined ? (settime.toString().split(".")[1]) * (60) / 100 : '00'
					var finalTime = settime.toString().split(".")[0] + " " + timemin;
					celll[colIndex.replace("column", '') - 1].value = finalTime;
					celll[colIndex.replace("column", '') - 1]['Edited'] = true;
				}
			}
		}
	}

	function getValueDynamicaly(tablecol, tableRecord, recorvalue) {

		console.log('tablecol', tablecol);
		console.log('tableRecord', tableRecord);
		console.log('recorvalue', recorvalue);
		if (FormName.toUpperCase().includes('LEAK TESTING')) {
			angular.forEach(tableRecord, function(value, key) {
				console.log('value getValueDynamicaly', value);
				if (value.Type == 'table') {
					var aaa = value.InternalName;
					var bbb = tablecol.Value;
					if (aaa.includes((bbb.split(" ")[0]).toLowerCase())) {
						value['Restrict'] = true;
					} else {
						value.Restrict = false;
					}
					value['InternalName'] = aaa;
				}
			})
		}
	}

	function dateConvert(istdate) {
		var date = new Date(istdate);
		var newDate = (date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear());
		return newDate;
	}

	function SqlDateFormate(date) {
		var date1 = '';
		if (date.split(' ')[0].split('/').length > 1) {
			var val = date.split(' ')[0].split('/')
			date1 = val[1] + '-' + val[0] + '-' + val[2]
		} else {
			var val = date.split(' ')[0].split('-')
			date1 = val[0] + '-' + val[1] + '-' + val[2]
		}
		return date1;
	}

	function getRecordTrack() {
		if (vm.showtable) {
			vm.showtable = false;
			vm.flag = false;
		} else {
			vm.flag = true;
			vm.showtable = true;
		}
		if (vm.flag) {
			vm.allDetails = []
			trackRecordService.getRecordDetail(vm.selectedTable, vm.rowData.recordName.split('-')[vm.rowData.recordName.split('-').length - 1]).then(function(response) {
				angular.forEach(response.data, function(value, key) {
						vm.allDetails.push(value);
						vm.showtable = true;
					})
					/*if(vm.allDetails.length > 1){
						vm.showtable = true;
						vm.flag = false;
					}else{
						vm.showtable = false;
					}*/
			});
		}
	}

	function addRow(value) {

		var row = {
				cells: []
			},
			colLen = value.Value.table[0].columns.length;
		for (var i = 0; i < colLen; i++) {
			row.cells.push({
				value: ''
			});
		}
		value.Value.table[1].rows.push(row);
	}


	function getCodeNameOfRecord(recordName) {
		console.log("recordName.slice(0,recordName.indexOf('2017-')+('2017-').length)", recordName.slice(0, recordName.indexOf('2017-') + ('2017-').length));
		return recordName.slice(0, recordName.indexOf('2017-') + ('2017-').length);
	}
}
