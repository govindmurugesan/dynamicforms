angular.module('amaraja').controller('mainController', mainController);


mainController.$inject = ['$scope', '$rootScope', '$location', '$http', 'mainService'];

function mainController($scope, $rootScope, $location, $http, mainService) {
  var vm = this;

  vm.addRecord = addRecord;
  vm.getRecord = getRecord;
  vm.createTable = createTable;


  getRecord();

  function getRecord() {
    mainService.getRecord().then(function(data) {
      console.log("haaaaaaaaa ", JSON.parse(data.rows[0].dpt2));
      vm.bootstrapData = JSON.parse(data.rows[0].dpt2).value;
      console.log("vm.bootstrapData ", vm.bootstrapData);
    });
  }

  function addRecord(record) {
    console.log("record ", record);
    mainService.addRecordJSON(record).then(function(data) {
      console.log("added succesfuly ", data.rows)
    })

  }

  /*createTable();*/
  function createTable() {
    var jsondata = {
      'value': [{
        "label": "Do you have a website?",
        "field_type": "text",
        "required": false,
        "field_options": {},
        "cid": "c1"
      }, {
        "label": "Please enter your clearance number",
        "field_type": "text",
        "required": true,
        "field_options": {},
        "cid": "c6"
      }, {
        "label": "Security personnel #82?",
        "field_type": "radio",
        "required": true,
        "field_options": {
          "options": [{
            "label": "Yes",
            "checked": false
          }, {
            "label": "No",
            "checked": false
          }],
          "include_other_option": true
        },
        "cid": "c10"
      }, {
        "label": "Medical history",
        "field_type": "file",
        "required": true,
        "field_options": {},
        "cid": "c14"
      }]
    }
    var tableRow = [];
    console.log("jsondata  ", jsondata.value);
    angular.forEach(jsondata.value, function(value, key) {
      tableRow.push(value.cid);
    });
    mainService.createTable(tableRow);
    /*.then(function(data){
                  console.log("added succesfuly ", data.rows)
                });*/
  }

}