(function() {
	'use strict';

	angular
		.module('amaraja')
		.controller('workflowController', workflowController);

	workflowController.$inject = ['workflowService', '$cordovaSQLite', 'notificationService', '$state', '$scope', '$ionicSideMenuDelegate'];

	function workflowController(workflowService, $cordovaSQLite, notificationService, $state, $scope, $ionicSideMenuDelegate) {
		var vm = this;
		vm.levelsArray = [];
		vm.levelsData = [];
		vm.departments = [];
		vm.forms = [];
		vm.levels = 10;
		vm.levelsChange = levelsChange;
		vm.submit = submit;
		vm.hideLevels = false;
		vm.getWorkflowDataByid = getWorkflowDataByid;
		vm.workName = [];
		vm.showWork = false;
		vm.update = update;
		vm.workflowEdit = workflowEdit;
		var details = "";
		vm.removeLevel = removeLevel;
		var removeElements = [];
		$ionicSideMenuDelegate.canDragContent(false);
		$scope.$watch(function() {
	    return $state.params;
	  });
	  var editWorkFlowName = $state.params.workName;

		for (var i = 1; i <= vm.levels; i++) {
			vm.levelsArray.push(i);
		}

		function levelsChange(count) {
			vm.hideLevels = true;
			vm.levelsData = [];
			for (var i = 1; i <= count; i++) {
				vm.levelsData.push(i);
			}
			if($state.current.name == 'editWorkflow'){
				var dataCount = vm.workflowData.length;
				if(count>dataCount){
					for(var j = 1; j<= (count-dataCount); j++){
						var levelcount = parseInt(dataCount)+(j-1);
						vm.workflowData.push({"department": "",
																"level":"level "+ levelcount,
																"levelform":"",
																"levelname":"",
																"workflowid":details.workflowId,
																"create": true
															})
					}
				}else{
					for(var j = 1; j<= (dataCount-count); j++){
						vm.workflowData.pop();
					}
				}
			}
				
		}

		workflowService.getDepartments().then(function(data) {console.log('data',data);
			for (var i = 0; i < data.rows.length; i++) {
				vm.departments.push(data.rows[i]);
			}
		})

		workflowService.getForms().then(function(data) {
			for (var i = 0; i < data.rows.length; i++) {
				vm.forms.push(data.rows[i]);
			}
		})

		getState();
		function getState(){
			if($state.current.name == 'editWorkflow'){
				getWorkflowDataByid(editWorkFlowName);
			}
		}

	

		function getWorkflowDataByid(workflow){
			vm.workflowData = [];
			details = JSON.parse(workflow);
			vm.workflowname = "";
			vm.levelsCount = details.levels;
			workflowService.getWorkflowData(details.workflowId).then(function(data) {
				vm.workflowname = details.workflowName;
	      angular.forEach(data.rows, function(value, index){
	      	vm.workflowData.push(value);
	      })
	      vm.showWork = true;
				vm.hideLevels = true;

			})
		}	

		function workflowEdit(level){
			level.edited = true;
		}

		function submit() {
			var request = {
				workflowname: vm.workflowname,
				levels: vm.levelsCount,
				departments: vm.deptSelected,
				forms: vm.formSelected,
				formname: vm.formName
			}
			workflowService.submitworkflow(request).then(function(data) {
				vm.workflowname = "";
				vm.levelsCount = "";
				vm.deptSelected = "";
				vm.formSelected = "";
				vm.formName = "";
				vm.levelsData = [];
				vm.hideLevels = false;
				notificationService.alert('', 'Workflow Details Saved successfully', function() {
				});
			})
		}

		function removeLevel(level, index){
			console.log('level',level);
			removeElements.push(level);
			vm.workflowData.splice(index,1);
			angular.forEach(vm.workflowData, function(value, index){
	      vm.workflowData[index].level = "level "+ parseInt(index+1);
	      vm.workflowData[index].edited = true;
      })
		}

		function update(){
			workflowService.updateworkflow(vm.workflowData, removeElements).then(function(data) {
		})
		}

	}

})();