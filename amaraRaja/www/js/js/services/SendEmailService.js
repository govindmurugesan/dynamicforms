angular.module('amaraja').factory('SendEmailService', SendEmailService);

SendEmailService.$inject = ['$q', '$http', '$rootScope', '$window', '$cordovaSQLite', 'constantService', 'recordDetailsService', 'loginService'];

function SendEmailService($q, $http, $rootScope, $window, $cordovaSQLite, constantService, recordDetailsService, loginService) {

  var service = {
    sendEmail: sendEmail,
    sendmailNotifiction: sendmailNotifiction
  }
  return service;

  function sendEmail(user) {
    var deferred = $q.defer();
    console.log("sendmailNotifiction msg", user);
    var request = {
      method: 'POST',
      url: constantService.serverURL + 'SendEmail',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      data: user
    }
    $http(request).then(function(response) {
      deferred.resolve(response);
    }, function(error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  function sendmailNotifiction(recordInformation, statusInformation) {
    console.log("sendmailNotifiction $window ", $window.localStorage);
    console.log("sendmailNotifiction $rootScope.codeNameOfRecord ", $rootScope.codeNameOfRecord);
    console.log("sendmailNotifiction recordInformation.serviceFirstPerson ", recordInformation.serviceFirstPerson);
    console.log("sendmailNotifiction recordInformation.serviceSecondPerson ", recordInformation.serviceSecondPerson);
    console.log("sendmailNotifiction recordInformation.serviceThirdPerson ", recordInformation.serviceThirdPerson);
    console.log("statusInformation.formName.toLowerCase().includes('shift releiving sheet')", statusInformation.formName.toLowerCase().includes('shift releiving sheet'));
    if (!$window.localStorage.empName.toLowerCase().includes('admin') && !statusInformation.formName.toLowerCase().includes('shift releiving sheet')) {
      var codeNameOfRecord = $rootScope.codeNameOfRecord + recordInformation.serviceRecordId;
      var consultPerson = '',
        fromPerson = '',
        status = statusInformation.approved,
        email = '',
        mailingPerson = '';
      if ((status == "done" || status == "reject") && statusInformation.updatedBy == "Production") {
        console.log('entering production done or reject');
        consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
        mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
        fromPerson = $window.localStorage.empName;
      }else if ((status == "done" || status == "reject") && statusInformation.updatedBy == "QA") {
        if (statusInformation.formName.toLowerCase().includes('pack note cum pre delivery inspection report')) {
          consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
          mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
        } else if (statusInformation.formName.toLowerCase() == "inspection report"){
          consultPerson = recordInformation.serviceThirdPerson.replace(/[0-9]/g, '');
          mailingPerson = recordInformation.serviceThirdPerson.replace(/[^0-9]/g, '');
        }else {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
        }
        fromPerson = $window.localStorage.empName;
      } else if (statusInformation.deptName == "Production") {
        console.log('entering production udation');
        if (status == "return" && statusInformation.updatedBy == "Production") {
          consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
          console.log('status return');
          console.log('Consult person',consultPerson);
          console.log('Mailing person',mailingPerson);
        }
        else if (status == "return" && statusInformation.updatedBy == "QA") {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');

        } else {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
        }
      } else if (statusInformation.deptName == "QA") {
        if (status == "return" && statusInformation.formName.toLowerCase() != "inspection report") {
          consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
        } else if (statusInformation.formName.toLowerCase() == "inspection report" && $window.localStorage.userType == "Operator") {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
        } else {
          consultPerson = recordInformation.serviceThirdPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceThirdPerson.replace(/[^0-9]/g, '');
        }
      } else if ($window.localStorage.userDept == "Engineering") {
        if ($window.localStorage.userType == "Operator") {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
          status = "raised";
        } else {
          console.log('eng Process else');
          consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
        }
      }else if (statusInformation.deptName == "Mold Maintainance") {
        if (statusInformation.updatedBy == "Production") {
          consultPerson = recordInformation.serviceThirdPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceThirdPerson.replace(/[^0-9]/g, '');
        } else {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
        }
      }
      loginService.getUserDetail(mailingPerson).then(function(resUserDetail) {
        email = resUserDetail.data[0].email;
        if (email == '' || email == null || email == undefined)
          email = "vck@amararaja.co.in";
        if (($window.localStorage.userDept == "Production" && $window.localStorage.userType == "Operator" && status == "approved") ||
          (status == "approved" && (statusInformation.formName.toLowerCase() == "flammability report" || statusInformation.formName.toLowerCase() == "inspection report") && $window.localStorage.userDept == "QA" && $window.localStorage.userType == "Operator"))
          status = "raised";
        else if (status == "done")
          status = "approved";
        if (consultPerson.trim() == '' || consultPerson == null || consultPerson == undefined)
          consultPerson = "Sir/Madam";
        var msg = "Dear " + consultPerson + " \n\n The document " + codeNameOfRecord + " has been " + status.toUpperCase();
        if (fromPerson.trim() != '' && fromPerson != null && fromPerson != undefined)
          msg += " by " + fromPerson;
        msg += ". Please proceed.\n(This is automatic mail from  Online Process Approval Request ,Don’t Reply to This MailID)\n\n Thanks";
        var sub = "Online Process Request";
        var req = { "email": email, "subj": sub, "msg": msg }
        sendEmail(req).then(function(aa) {});
        console.log('sendmailNotifiction recordInformation', recordInformation);
        console.log('sendmailNotifiction statusInformation', statusInformation);
        console.log('sendmailNotifiction email', email);
        console.log('sendmailNotifiction codeNameOfRecord', codeNameOfRecord);
        console.log('sendmailNotifiction consultPerson', consultPerson);
        console.log('sendmailNotifiction fromPerson', fromPerson);
      });
    }
  }
}
