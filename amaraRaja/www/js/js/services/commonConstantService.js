(function() {
'use strict';

	var constantService = {
				db: window.openDatabase("amaraja.db", "1", "Sqlite", "2000"),
				mainTable: 'DF_DYNTABLES',
				tableName: 'DF_TBL_PGR_',
				selectedTableName: '',
				innerTableName: 'DF_TBL_PGR_TBL_',
				deptTable: 'DF_MSTR_DEPT',
				usersTable: 'DF_MSTR_USERS',
				workFlowMaster: 'DF_WORKFLOW',
				workFlowItems:'DF_WORKFLOW_I',
				trackApproval: 'DF_TBL_FMH',
				loginTable: 'Users',
				approveTable: 'DF_TBL_APR',
				machine: 'machine',
				partNO: 'partno',
				shift: 'shift',
				V0:[10, 50, 50, 'Not Allowed', 'Not Allowed'],
				NEWV0:[10, 50, 50, 'Allowed', 'Not Allowed'],
				V2:[30, 250, 250, 'Allowed', 'Not Allowed'],
				A: ['6 to 7', '7 to 8', '8 to 9', '9 to 10', '10 to 11', '11 to 12', '12 to 1', '1 to 2'],
				B: ['2 to 3', '3 to 4', '4 to 5', '5 to 6', '6 to 7', '7 to 8', '8 to 9', '9 to 10'],
				C: ['10 to 11', '11 to 12', '12 to 1', '1 to 2', '2 to 3', '3 to 4', '4 to 5', '5 to 6'],
				// serverURL: 'http://localhost/dfserver/DFServerAPI.svc/'
				//serverURL: 'http://52.77.96.35/dfserver/DFServerAPI.svc/'
				//serverURL: 'http://eappsdev.argroup.co.in/dfserver/DFServerAPI.svc/'
				serverURL: 'http://arsrva008/dfserver/DFServerAPI.svc/'
	};	

	angular.module('amaraja').constant('constantService', constantService);

	})();
