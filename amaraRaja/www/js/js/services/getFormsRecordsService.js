angular.module('amaraja').factory('getFormsRecordsService', getFormsRecordsService);

getFormsRecordsService.$inject = ['$q', '$http', '$window', '$cordovaSQLite', 'notificationService', 'constantService', '$cordovaNetwork', 'SendEmailService', 'loginService'];

function getFormsRecordsService($q, $http, $window, $cordovaSQLite, notificationService, constantService, $cordovaNetwork, SendEmailService, loginService) {

    var service = {
        getTableRecords: getTableRecords,
        getInnerTableRecord: getInnerTableRecord,
        updateRecord: updateRecord,
        updateActiveRecord: updateActiveRecord,
        checkApproved: checkApproved,
        checkWorkflow: checkWorkflow,
        returnApproveal: returnApproveal,
        transformApproveal: transformApproveal,
        updateAprrovalProcess: updateAprrovalProcess,
        getCompletedRecordinOneLevel: getCompletedRecordinOneLevel,
        getApproverDetails: getApproverDetails
    }
    return service;

    function getTableRecords(tableId) {
        var deferred = $q.defer();
        var tableName = constantService.tableName + tableId;
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        if (1) {
            var query = "SELECT rowid,* FROM  " + tableName;
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': tableName,
                    'type': 'SELECT'
                }
            }
            console.log("query  ", query)
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "SELECT rowid,* FROM  " + tableName).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function getInnerTableRecord(TableNameId, subId, TableRowId, formName) {
        var deferred = $q.defer();
        var tableName = constantService.innerTableName + TableNameId + "_" + subId;
        var query = '';
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        //var query = "SELECT  rowid,* FROM  " + tableName + " WHERE tableid=" + TableRowId+"  order by column1 * 1 ASC";
        console.log("formName sada", formName)
        if (formName.toUpperCase().includes("IN-PROCESS") || formName.toUpperCase().includes("PROCESS SPECIFICATION") || formName.toUpperCase().includes("SHIFT")) {
            query = "select rowid,*  from " + tableName + " where tableid = '" + TableRowId + "'  order by column1  * 1 ASC";
        } else {
            query = "select rowid,*  from " + tableName + " where tableid = '" + TableRowId + "'  order by column1 ASC";
        }
        console.log("inner table ", query)
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': tableName,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }

        return deferred.promise;
    }

    function checkWorkflow(formName) {
        var deferred = $q.defer();
        if (1) {
            var query = "SELECT  * FROM  " + constantService.workFlowItems + " WHERE levelform='" + formName + "'";
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.workFlowItems,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, 'SELECT  * FROM  ' + constantService.workFlowItems + " WHERE levelform='" + formName + "'").then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function checkApproved(tableid, dept, type, formName, rejectQuery) {
        var deferred = $q.defer();
        var query1 = '';
        console.log("type  ", type)
        if (type != 'approved' && type != '') {
            console.log('checkApproved')
            if (type == 'inprocess') {
                // if ($window.localStorage.userDept == 'QA')
                query1 = "updatedBy='" + dept + "' AND status='a' AND approved!='done' AND formName ='" + formName + "'";
                // else
                //     query1 = " formName='" + formName + "' and approved !='done' and approved !='reject' and tableName in(select DISTINCT formName from DF_TBL_FMH where  status != 'reject'  and role in('" + dept + "'))"
            } else if (type == 'done') {
                query1 = "approved='done' and formName='" + formName + "'";
            } else if (type == 'Reject') {
                query1 = "updatedBy in(" + rejectQuery + ") AND status='a' AND approved='" + type + "' AND formName ='" + formName + "'";
            } else {
                query1 = "deptName='" + dept + "' AND status='a' AND approved='" + type + "' AND formName ='" + formName + "'";
            }
        } else {
            console.log('Else checkApproved')
            if ($window.localStorage.userDept == 'Engineering') {
                query1 = "status='a'  AND tableName='" + tableid + "' AND formName ='" + formName + "'";
            }
            //  else if ($window.localStorage.userDept == 'QA') {
            //     query1 = "updatedBy='" + dept + "' AND status='a' AND approved!='done' AND formName ='" + formName + "'";
            // }
            else {
                query1 = "deptName='" + dept + "' AND status='a' AND approved='approved' AND formName ='" + formName + "'";
            }
        }

        var query = "SELECT  * FROM  " + constantService.approveTable + " WHERE " + query1;
        console.log("complee  ", query)
        if (1) {
            console.log('query----- ', query);
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, query, null).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function updateInnerTableRecord(tableNameId, subId, tablRowId, tableRowName, tableRec) {
        var tableName = constantService.innerTableName + tableNameId + "_" + subId;
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        if (1) {
            var query = "UPDATE " + tableName + " SET " + tableRowName + "= '" + tableRec + "' WHERE rowid=" + tablRowId;
            console.log("query    table =====  ", query)
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': tableName,
                    'type': 'UPDATE'
                }
            }
            $http(dynareq).then(function(response) {}, function(error) {
                console.log('error###', error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "UPDATE " + tableName + " SET " + tableRowName + "=" + JSON.stringify(tableRec) + " WHERE rowid=" + tablRowId);
        }
    }

    function updateRecord(tableNameId, tablRowId, tableRowName, tableRec, approved, formName, updateDept, moveToDept, rejctDept) {
        console.log('rrr tableNameId', tableNameId);
        console.log('rrr tablRowId', tablRowId);
        console.log('rrr tableRowName', tableRowName);
        console.log('rrr tableRec', tableRec);
        console.log('rrr approved', approved);
        console.log('rrr formName', formName);
        console.log('rrr updateDept', updateDept);
        console.log('rrr moveToDept', moveToDept);
        console.log('rrr rejctDept', rejctDept);

        var approved1 = approved;
        var deferred = $q.defer();
        var subId = 0;
        var cavityColum = '';
        var tableName = constantService.tableName + tableNameId;
        var promisecall = [];
        var flag = true;
        promisecall.push(
            angular.forEach(tableRec, function(value, key) {
                if (value.Type == 'table') {
                    var col = [];
                    angular.forEach(value.Value.table, function(value1, key1) {
                        if (value1.columns != '') {
                            angular.forEach(value1.columns, function(value2, key2) {
                                if (value2.cavity) cavityColum = value2.Value;
                                else col.push(value2.Value);
                            });
                        }
                        if (value1.rows != '') {
                            angular.forEach(value1.rows, function(value2, key2) {
                                var rowId = 0;
                                var setRowId = true;
                                var cavityValue = '';
                                var cavityExist = false;
                                angular.forEach(value2.cells, function(value3, key3) {
                                    if (setRowId) {
                                        rowId = value3.Value;
                                        setRowId = false;
                                    }
                                    if (col[key3] == cavityColum.split('_')[0] || value3.cavity == true) {
                                        cavityValue = cavityValue + "_" + value3.Value;
                                        cavityExist = true;
                                    } else {
                                        if (value3.Edited && flag) {
                                            updateInnerTableRecord(tableNameId, subId, rowId, col[key3], value3.Value);
                                        }
                                    }
                                    if (key3 == value2.cells.length - 1 && cavityExist && flag) {
                                        updateInnerTableRecord(tableNameId, subId, rowId, cavityColum.split('_')[0], cavityValue.trim().slice(1));
                                    }

                                    if (value3.NewAdded && flag) {
                                        addInnerTable(tableNameId, subId, rowId, value.Value.table)
                                        flag = false;
                                    }
                                });
                            });
                        }
                    });
                    ++subId;
                } else if (value.Type == 'checkbox' && value.Edited == true) {
                    var query = '';
                    if (value.Type == 'checkbox') query = "UPDATE " + tableName + " SET " + value.InternalName + "= '" + JSON.stringify(value.Value) + "' WHERE " + tableRowName + "=" + tablRowId;
                    else query = "UPDATE " + tableName + " SET " + value.InternalName + "='" + value.Value + "' WHERE " + tableRowName + "=" + tablRowId;

                    if (1) {
                        var dynareq = {
                            method: 'POST',
                            url: constantService.serverURL + 'CreateDynamicTable',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            data: {
                                'query': query,
                                'table_name': tableName,
                                'type': 'UPDATE'
                            }
                        }
                        $http(dynareq).then(function(response) {
                            console.log('response', response);
                        }, function(error) {
                            console.log('error###', error);
                        });
                    } else {
                        $cordovaSQLite.execute(constantService.db, query);
                    }
                } else if (value.Edited == true) {
                    var query = "UPDATE " + tableName + " SET " + value.InternalName + "='" + value.Value + "' WHERE " + tableRowName + "=" + tablRowId;
                    console.log("query update ", query)
                    if (1) {
                        var dynareq = {
                            method: 'POST',
                            url: constantService.serverURL + 'CreateDynamicTable',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            data: {
                                'query': query,
                                'table_name': tableName,
                                'type': 'UPDATE'
                            }
                        }
                        $http(dynareq).then(function(response) {
                            console.log('response after upadting 1', response);
                            console.log('constantService.approveTable', tableName);
                            // console.log('query', query);
                        }, function(error) {
                            console.log('error###', error);
                        });
                    } else {
                        $cordovaSQLite.execute(constantService.db, query);
                    }
                } else {}
            }))


        $q.all(promisecall).then(function(data) {
            if (approved == 'return' || approved == 'transfer') {
                if (1) {
                    var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                    console.log('query (0000)  ', query)
                    var dynareq = {
                        method: 'POST',
                        url: constantService.serverURL + 'CreateDynamicTable',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            'query': query,
                            'table_name': constantService.approveTable,
                            'type': 'UPDATE'
                        }
                    }
                    $http(dynareq).then(function(response) {
                        console.log('response after upadting 2', response);
                        console.log('constantService.approveTable', constantService.approveTable);
                    }, function(error) {
                        console.log('error###', error);
                    });
                } else {
                    $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='reject', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                }
            } else {
                if (approved != undefined) {
                    if (1) {
                        var query = "SELECT * FROM " + constantService.approveTable + " WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                        console.log("query @@@@@ ", query)
                        var dynareq = {
                            method: 'POST',
                            url: constantService.serverURL + 'SelectDynamicTable',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            data: {
                                'query': query,
                                'table_name': constantService.approveTable,
                                'type': 'SELECT'
                            }
                        }
                        $http(dynareq).then(function(response) {
                            var data = [];
                            data.rows = response.data;
                            deferred.resolve(data);
                            approved = approved1;

                            if (data.rows != '' && data.rows.length > 0) {
                                var rejectedBy = '';
                                if (approved != 'approved') {
                                    if (1) {
                                        var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                                        console.log("query ", query);
                                        var dynareq = {
                                            method: 'POST',
                                            url: constantService.serverURL + 'CreateDynamicTable',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            data: {
                                                'query': query,
                                                'table_name': constantService.approveTable,
                                                'type': 'UPDATE'
                                            }
                                        }
                                        $http(dynareq).then(function(response) {
                                            console.log('response after upadting 3', response);
                                            console.log('constantService.approveTable', constantService.approveTable);
                                        }, function(error) {
                                            console.log('error###', error);
                                        });
                                    } else {
                                        $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='reject',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                                    }
                                } else {
                                    var approved = 'approved';
                                    if (moveToDept == undefined) {
                                        moveToDept = 'done';
                                        approved = 'done';
                                    }
                                    if (1) {
                                        var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                                        console.log("query 1 ", query);
                                        var dynareq = {
                                            method: 'POST',
                                            url: constantService.serverURL + 'CreateDynamicTable',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            data: {
                                                'query': query,
                                                'table_name': constantService.approveTable,
                                                'type': 'UPDATE'
                                            }
                                        }
                                        $http(dynareq).then(function(response) {}, function(error) {
                                            console.log('error###', error);
                                        });
                                    } else {
                                        $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='approved', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                                    }
                                }
                            } else {
                                updateApproveTable(approved, tableNameId, tablRowId, formName, moveToDept);
                            }
                        }, function(error) {
                            console.log('error###', error);
                        });
                    }
                    //LOCAL STORAGE COED COMMENTED BY CHETHA 
                    /*else {
                       console.log("2222222222222222222222222222222222222222222222222222222222222222222222222222222222");
                        $cordovaSQLite.execute(constantService.db, "SELECT * FROM " + constantService.approveTable + " WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'").then(function(aaa) {
                            if (aaa.rows.length > 0) {
                                var rejectedBy = '';
                                if (approved != 'approved') {
                                    if (1) {
                                        var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                                        var dynareq = {
                                            method: 'POST',
                                            url: constantService.serverURL + 'CreateDynamicTable',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            data: {
                                                'query': query,
                                                'table_name': constantService.approveTable,
                                                'type': 'UPDATE'
                                            }
                                        }
                                        $http(dynareq).then(function(response) {
                                            console.log('response', response);
                                        }, function(error) {
                                            console.log('error###', error);
                                        });
                                    } else {
                                        console.log("UPDATE " + constantService.approveTable + " SET approved='" + approved + "',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'")
                                        $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='reject',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                                    }
                                } else {
                                    var approved = 'approved';
                                    if (moveToDept == undefined) {
                                        moveToDept = 'done';
                                        approved = 'done';
                                    }
                                    if (1) {
                                        var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                                        var dynareq = {
                                            method: 'POST',
                                            url: constantService.serverURL + 'CreateDynamicTable',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            data: {
                                                'query': query,
                                                'table_name': constantService.approveTable,
                                                'type': 'UPDATE'
                                            }
                                        }
                                        $http(dynareq).then(function(response) {
                                            console.log('response', response);
                                        }, function(error) {
                                            console.log('error###', error);
                                        });
                                    } else {
                                        console.log("UPDATE " + constantService.approveTable + " SET approved='" + approved + "', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'")
                                        $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='approved', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                                    }
                                }
                            } else {
                                console.log("updateApproveTable 22222222222222222222 approved", approved)
                                updateApproveTable(approved, tableNameId, tablRowId, formName, moveToDept);
                            }
                        });
                    }*/
                }
            }
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function returnApproveal(tableNameId, tablRowId, formName) {
        var deferred = $q.defer();
        var rejectedBy = '';
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        if (1) {
            var query = "UPDATE " + constantService.approveTable + " SET approved='approved', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'UPDATE'
                }
            }
            $http(dynareq).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET   approved='reject' WHERE tableName='" + tableNameId + "' AND formName ='" + formName + "' AND status='A'").then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function transformApproveal(tableNameId, tablRowId, formName, transDept) {
        var deferred = $q.defer();
        var rejectedBy = '';
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        if (1) {
            var query = "UPDATE " + constantService.approveTable + " SET   approved='reject' WHERE tableName='" + tableNameId + "' AND tablRowId ='" + tablRowId + "' AND status='A' AND deptName'" + transDept + "'";
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'UPDATE'
                }
            }
            $http(dynareq).then(function(response) {
                console.log('response', response);
                deferred.resolve(response);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET   approved='reject' WHERE tableName='" + tableNameId + "' AND tablRowId ='" + tablRowId + "' AND status='A' AND deptName'" + transDept + "'").then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function updateActiveRecord(tableId) {
        var deferred = $q.defer();
        var tableName = constantService.tableName + tableId;
        if (1) {
            var query = "SELECT rowid,* FROM  " + tableName;
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': tableName,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "SELECT rowid,* FROM  " + tableName).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function updateApproveTable(approved, tableName, rowid, formName, moveToDept) {
        console.log("approved in updateApproveTable ", approved)
        var deferred = $q.defer();
        var rejectedBy = '';
        rejectedBy = $window.localStorage.userDept;
        if (moveToDept == undefined) moveToDept = $window.localStorage.userDept;;
        if (1) {
            var query = "INSERT INTO " + constantService.approveTable + " (approved , tableName , recordId , deptName , formName, updatedBy, status) VALUES ('" + approved + "','" + tableName + "','" + rowid + "','" + moveToDept + "','" + formName + "','" + rejectedBy + "','a')";
            console.log('query', query);
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'INSERT'
                }
            }
            $http(dynareq).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, 'INSERT INTO ' + constantService.approveTable + ' (approved , tableName , recordId , deptName , formName, updatedBy, status) VALUES ("' + approved + '","' + tableName + '","' + rowid + '","' + moveToDept + '","' + formName + '","' + rejectedBy + '","a")').then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function updateAprrovalProcess(formlevel, formName, status, comment, recordId) {
        var deferred = $q.defer();
        var query = "INSER INTO " + constantService.trackApproval + " (userName , userId , role , formLevel , formName, status, comment ,recordId) VALUES ('" + $window.localStorage.empName + "','" + $window.localStorage.empCode + "','" + $window.localStorage.userDept + "','" + formlevel + "','" + formName + "','" + status + "','" + comment + "','" + recordId + "')";
        console.log('query insert track approval ', query);
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'INSERT'
                }
            }
            $http(dynareq).then(function(response) {
                console.log('constantService.approveTable updateAprrovalProcess', constantService.approveTable);
                console.log('constantService.approveTable trackApproval', constantService.trackApproval);
                console.log('constantService.db updateAprrovalProcess', constantService.db);
                // SendEmailService.sendmailNotifiction(recordId, );
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
        } else {

            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function getCompletedRecordinOneLevel(query) {
        var deferred = $q.defer();
        console.log('query insert track approval ', query);
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {

            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function addInnerTable(tableNameId, subId, rowId, tablerecord) {
        var tableName = constantService.innerTableName + tableNameId + "_" + subId;
        var query = "delete from " + tableName + " where  tableid in (select tableid from " + tableName + " where rowid='" + rowId + "')";
        var dynareq = {
            method: 'POST',
            url: constantService.serverURL + 'CreateDynamicTable',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'query': query,
                'table_name': tableName,
                'type': 'SELECT'
            }
        }
        $http(dynareq).then(function(response) {

            var inTblecolm = "";
            var inTbleValues = "";
            angular.forEach(tablerecord[0].columns, function(value, key) {
                if (!value.cavity && !value.Value.includes('rowid')) {
                    inTblecolm = inTblecolm + value.Value + ",";
                }
            })
            angular.forEach(tablerecord[1].rows, function(value, key) {
                angular.forEach(value.cells, function(value1, key1) {
                    var aa = tablerecord[0].columns[key1].Value;
                    var bb = value1.Value == undefined ? '' : value1.Value;
                    if (!tablerecord[0].columns[key1].cavity && !aa.includes('rowid')) {
                        inTbleValues = inTbleValues + "'" + bb + "',";
                    } else if (!aa.includes('rowid')) {
                        inTbleValues = inTbleValues.trim().slice(0, -2) + "_" + bb + "',";
                    }
                })
                var query = "INSERT INTO " + tableName + " ( " + inTblecolm.trim().slice(0, -1) + ") VALUES (" + inTbleValues.trim().slice(0, -1) + ")";
                var dynareq = {
                    method: 'POST',
                    url: constantService.serverURL + 'CreateDynamicTable',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        'query': query,
                        'table_name': tableName,
                        'type': 'INSERT'
                    }
                }
                $http(dynareq).then(function(response) {}, function(error) {});

                inTbleValues = "";
            })

        }, function(error) {});
    }

    function getApproverDetails(tableId, rowId) {

        var deferred = $q.defer();
        var queryForGetApproverDetails = "select * from " + constantService.tableName + tableId + " where rowid=" + rowId + "";
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': queryForGetApproverDetails,
                    'table_name': constantService.tableName + tableId,
                    'type': 'SELECT'
                }
            }
            console.log("queryForGetApproverDetails  ", queryForGetApproverDetails)
            $http(dynareq).then(function(response) {
                    console.log('queryForGetApproverDetails response', response);
                    console.log('getDataPersonDetails(response)', getDataPersonDetails(response));
                    deferred.resolve(getDataPersonDetails(response));
                },
                function(error) {
                    console.log('error###', error);
                    deferred.reject(error);
                });
        } else {
            $cordovaSQLite.execute(constantService.db, queryForGetApproverDetails).then(function(data) {
                deferred.resolve(getDataPersonDetails(response));
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function getDataPersonDetails(response) {
        if (response.data.length > 0) {
            var result = response.data[0];
            var data = {},
                email = {};
            var qainspector = '',
                operator = '',
                supervisor = '',
                operatorEmail = '',
                supervisorEmail = '',
                qainspectorEmail = '';
            if (result.operator || result.senioroperator || result.operatorname || result.preparedby)
                operator = result.operator || result.operatorname || result.senioroperator || result.preparedby;
            if (result.supervisor || result.supervisorname || result.inspectedby|| result.qaincharge)
                supervisor = result.supervisor || result.supervisorname || result.inspectedby|| result.qaincharge;
            if (result.qainspector || result.qaleaktest || result.qagumming1 || result.qabushcasting || result.approvedby )
                qainspector = result.qainspector || result.qaleaktest || result.qagumming1 || result.qabushcasting || result.approvedby ;
            if(result.approverby && result.recordName.includes('EPSfI'))
                supervisor=result.approverby;
            return data = {
                    firstPerson: operator,
                    secondPerson: supervisor,
                    thirdPerson: qainspector
                }
                // angular.forEach(data,function(val,keyyy){
                //     loginService.getUserDetail(val.replace(/[^0-9]/g, '')).then(function (resUserDetail) {
                //         if(keyyy=="firstPerson")
                //             operatorEmail=resUserDetail.data[0].email;
                //         else if(keyyy=="secondPerson")
                //             supervisorEmail=resUserDetail.data[0].email;
                //         else if(keyyy=="thirdPerson")
                //             qainspectorEmail=resUserDetail.data[0].email;
                //     });
                // });
                // return data = {
                //     firstPerson: operator,
                //     firstPersonEmail: operatorEmail,
                //     secondPerson: supervisor,
                //     secondPersonEmail: supervisorEmail,
                //     thirdPerson: qainspector,
                //     thirdPersonEmail: qainspectorEmail
                // }
        }
    }

    function getStatusInformation(tableId, recordId) {
        var deferred = $q.defer();
        var query = "SELECT  * FROM  " + constantService.approveTable + " WHERE tableName='" + tableId + "' and recordId='" + recordId + "'";

        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }
}
