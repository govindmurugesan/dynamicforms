angular.module('amaraja').factory('recordDetailsService', recordDetailsService);

recordDetailsService.$inject = ['$q', '$http', '$window', '$cordovaSQLite', 'constantService', '$cordovaNetwork'];

function recordDetailsService($q, $http, $window, $cordovaSQLite, constantService, $cordovaNetwork) {

    var service = {
        getStatusInformation: getStatusInformation

    }
    return service;

    function getStatusInformation(tableId, recordId) {
        var deferred = $q.defer();
        var query = "SELECT  * FROM  " + constantService.approveTable + " WHERE tableName='" + tableId + "' and recordId='" + recordId + "'";
        console.log('getStatusInformation query', query);
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                deferred.resolve(response.data[0]);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    // function getApprovalInformation(tableId, recordId) {
    //     var deferred = $q.defer();
    //     var query = "SELECT  * FROM  " + constantService.approveTable + " WHERE tableName='" + tableId + "' and recordId='" + recordId + "'";
    //     console.log('getStatusInformation query', query);
    //     if (1) {
    //         var dynareq = {
    //             method: 'POST',
    //             url: constantService.serverURL + 'SelectDynamicTable',
    //             headers: {
    //                 'Content-Type': 'application/json'
    //             },
    //             data: {
    //                 'query': query,
    //                 'table_name': constantService.approveTable,
    //                 'type': 'SELECT'
    //             }
    //         }
    //         $http(dynareq).then(function(response) {
    //             var data = [];
    //             data.rows = response.data;
    //             deferred.resolve(data);
    //         }, function(error) {
    //             console.log('error###', error);
    //             deferred.reject(error);
    //         });
    //     } else {
    //         $cordovaSQLite.execute(constantService.db, query).then(function(data) {
    //             deferred.resolve(data);
    //         }, function(error) {
    //             deferred.reject(error);
    //         });
    //     }
    //     return deferred.promise;
    // }
}
