angular.module('amaraja').controller('createFormsController', createFormsController);

createFormsController.$inject = ['$scope', '$rootScope', '$location', '$http', '$state', '$window', 'createFormsService', 'notificationService', '$ionicSideMenuDelegate','constantService', 'workflowService', '$ionicPopup', '$parse'];

function createFormsController($scope, $rootScope, $location, $http, $state, $window, createFormsService, notificationService, $ionicSideMenuDelegate, constantService, workflowService, $ionicPopup, $parse) {
	var vm = this;
	var guid = 1;
	vm.getBase64 = getBase64;
	vm.changeSelectedMaster = changeSelectedMaster;
	vm.changeSelectedColumn = changeSelectedColumn;
	vm.selectionChange = selectionChange;
	vm.setDynamicTable = setDynamicTable;
	vm.getDynamicTableColuman = getDynamicTableColuman;
	vm.setColCondition = setColCondition;
	vm.setCondition = setCondition;
	vm.setTableColumnRef = setTableColumnRef;
	vm.valuesTables = [];
	var colSpans = ['2', '3', '4', '6', '8', '9', '10', '12'];
	var tableid = "";
	vm.populate = populate;
	vm.masterCheckChange = masterCheckChange;
	var uniqueInternalName = [];
	vm.getDepartments = getDepartments;
	var previousForm = "";
	vm.selectCondTable = selectCondTable;
	var selecttableName = "";
	vm.selectCondColumn = selectCondColumn;
	vm.addCondition = addCondition;
	vm.addSelectCondition = addSelectCondition;
	vm.getAllColumnList= getAllColumnList;
	vm.addColDept = addColDept;
	vm.showCondition = false;
	vm.cancel = cancel;
	vm.changingColumanName = changingColumanName;
	vm.addColTimer = addColTimer;
	vm.addColDate = addColDate;
	vm.dynamicTableList = [];
	vm.checkLogin = checkLogin;

  checkLogin()
	function checkLogin(){
		if($window.localStorage.userDept == '' || $window.localStorage.userDept == undefined  )
      $state.go('login');
	}

	vm.OperatoList = ['Engineering Operator','Engineering Approver','Production Operator', 'Production Approver', 'QA Operator', 'QA Approver']
	
	$scope.$watch(function() {
    return $state.params;
  });
  var editFormName = $state.params.formName;
  $ionicSideMenuDelegate.canDragContent(false);
	$scope.dragElements = [{
		'Name': "Radio",
		'Type': "radio",
		'Image': 'ion-android-radio-button-on',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text',
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Choice',
				'Type': 'dropdown_increment',
				'PossibleValue': [{
					'Text': 'Choice 1',
				}, {
					'Text': 'Choice 2'
				}]
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""
			}

		]
	}, {
		'Name': "Checkbox",
		'Type': "checkbox",
		'Image': 'fa fa-check-square-o',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Choice',
				'Type': 'dropdown_increment',
				'PossibleValue': [{
					'Text': 'Choice 1',
					'Value': 'check1'
				}, {
					'Text': 'Choice 2',
					'Value': 'check2'
				}]
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""

			}

		]
	}, {
		'Name': "Input",
		'Type': "text",
		'Image': 'fa fa-font',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Max Input Length',
				'Value': '50',
				'Type': 'text'
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				}, {
					'Name': 'Unique',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""
			}

		]
	}, {
		'Name': "Date",
		'Type': "date",
		'Image': 'fa fa-calendar',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Type',
				'Value': '',
				'Type': 'radio',
				'PossibleValue': [{
					'Text': 'Date',
					'Checked': true
				}, {
					'Text': 'Current Date',
					'Checked': false
				}]
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""
			}

		]
	}, {
		'Name': "Select Box",
		"Type": "dropdown",
		'Image': 'fa fa-caret-down',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Choice',
				'Type': 'select_drop_increment',
				'MasterData':[{
					'TableName': '',
					'ColumnName': ''
				}],
				'PossibleValue': [{
					'Text': 'Choice 1'
				}, {
					'Text': 'Choice 2'
				}],
				'MasterRef': false,
				'ConditionData':[{
					'condColumn':'',
					'condTableName': '',
					'condColumnName': '',
					'condValue': '',
					'condChange': '',
					'condEquals': 'IN'
				}],	
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': "",
				'Cavity':""
			}

		]
	}, {
		'Name': "Text Area",
		"Type": "textarea",
		'Image': 'ion-compose',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
          'Name': 'Row Size',
          'Value': 4,
          'Type': 'number'
      }, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': ""
			}

		]
	}/*, {
		'Name': "Image",
		"Type": "button",
		'Image': 'fa fa-picture-o',
		'Settings': [{
				'Name': 'Field Type',
				'Value': 'Image',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': 'Internal Name',
				'Type': 'text'
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'Base64',
				'Value': '',

			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				}]
			}

		]
	}*/, {
		'Name': "Table",
		"Type": "table",
		'Image': 'fa fa-table',
		'Settings': [{
				'Name': 'Field Label',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Internal Name',
				'Value': '',
				'Type': 'text'
			}, {
				'Name': 'Column Span',
				'Value': '12',
				'Type': 'dropdown',
				'PossibleValue': colSpans
			}, {
				'Name': 'Table',
				'Type': 'table',
				'PossibleValue': [{
					'columns': [{
						'value': 'column1',
						'value1':'column1',
						'readonly' : false,
						'showTime': false,
						'colRef':{
							 'referMaster':false,
							 'dropdown':false,
							 'PossibleValue':[]
						}
					}, {
						'value': 'column2',
						'value1':'column2',
						'readonly' : false,
						'colRef':{			
							'referMaster':false,	
							'dropdown':false,			 
							'PossibleValue':[]
						}
					}, {
						'value': 'column3',
						'value1':'column3',
						'readonly' : false,
						'colRef':{
							 'referMaster':false,	
							 'dropdown':false,
							 'PossibleValue':[]
						}
					}],
					'rows': [{
						'cells': [{
							'value': 'row01'
						}, {
							'value': 'row02'
						}, {
							'value': 'row03'
						}]
					}]
				}]
			},{
				'Name': 'Choice',
				'Type': 'select_table_drop_increment',
				'MasterData':[{
					'TableName': '',
					'ColumnName': '',
					'column': ''
				}],	
				'PossibleValue': [{
					'Text': 'Choice 1'
				}, {
					'Text': 'Choice 2'
				}],
				'MasterRef': false,
				'ConditionData':[{
					'condColumn':'',
					'condTableName': '',
					'condColumnName': '',
					'condValue': '',
					'condChange': '',
					'condEquals': 'IN'
				}],	
				'DynamicTableRef': false,
				'DynaicRef':{ 
					'TableId':'',
					'RefCol':'',
					'FilterCol':''
				}
			}, {
				'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				},
				{
					'Name': 'Readonly',
					'Value': false
				}],
				'Restrict': "",
				'ColmRestrict': [],				
				'ColCondValue': {"Condtion":[]},
			}
		]
	}];

	$scope.formFields = [];

	$scope.current_field = {};

	var createNewField = function() {
		return {
			'id': ++guid,
			'Name': '',
			'Settings': [],
			'Active': true,
			'ChangeFieldSetting': function(Value, SettingName) {
				switch (SettingName) {
					case 'Field Label':
						{
							$scope.current_field.Name = Value;
							$scope.current_field.Settings[0].Value = $scope.current_field.Name;
							break;
						}					
					case 'Internal Name':
						{
							$scope.current_field.Settings[1].Value = Value.replace(/\s/g, '_');
							break;
						}

					default:
						break;
				}
			},
			'GetFieldSetting': function(settingName) {
				var result = {};
				var settings = this.Settings;
				$.each(settings, function(index, set) {
					if (set.Name == settingName) {
						result = set;
						return;
					}
				});
				if (!Object.keys(result).length) {
					//Continue to search settings in the checkbox zone
					$.each(settings[settings.length - 1].Options, function(index, set) {
						if (set.Name == settingName) {
							result = set;
							return;
						}
					});
				}
				return result;
			}
		};
	}

/*	$scope.changeFieldName = function(Value) {
		$scope.current_field.Name = Value;
		$scope.current_field.Settings[0].Value = $scope.current_field.Name;
		$scope.current_field.Settings[1].Value = $scope.current_field.Name;
		$scope.current_field.Settings[2].Value = 'x' + $scope.current_field.Name.replace(/\s/g, '_');
	}*/

	$scope.removeElement = function(idx) {
		if ($scope.formFields[idx].Active) {
			$('#addFieldTab_lnk').tab('show');
			$scope.current_field = {};

		}
		$scope.formFields.splice(idx, 1);

	};

	$scope.addElement = function(ele, idx) {
    $('#formBuilderContent').animate({scrollTop: $('#formBuilderContent').prop("scrollHeight")}, 200);
		vm.showButtons = true;
		$scope.current_field.Active = false;
		$scope.current_field = createNewField();
		//Merge setting from template object
		angular.merge($scope.current_field, ele);

		if (typeof idx == 'undefined') {
			$scope.formFields.push($scope.current_field);
		} else {
			$scope.formFields.splice(idx, 0, $scope.current_field);
			$('#fieldSettingTab_lnk').tab('show');
		}
	};

	$scope.activeField = function(f) {
		$scope.current_field.Active = false;
		$scope.current_field = f;
		f.Active = true;
		$('#fieldSettingTab_lnk').tab('show');
	};

	$scope.addChoice = function(set) {
		if (set.PossibleValue[set.PossibleValue.length - 1].Text != '') {
			set.PossibleValue.push({
				'Text': ''
			});
		}
	}

	$scope.removeChoice = function(set, index) {
		if (set.PossibleValue.length > 1) {  
			set.PossibleValue.splice(index, 1);
		}
	}
	$scope.formbuilderSortableOpts = {
		'ui-floating': true,
	};
	/*table add and remove items */
	$scope.removeCol = function(set, index) {
		if (index > -1 && set.Settings[3].PossibleValue[0].columns.length > 1) {
			set.Settings[3].PossibleValue[0].columns.splice(index, 1);
			var len = set.Settings[3].PossibleValue[0].rows.length
			for (var i = 0; i < len; i++) {
				set.Settings[3].PossibleValue[0].rows[i].cells.splice(index, 1);
			}
		}
	};

	$scope.removeRow = function($index, set) {
		if ($index > -1 && set.Settings[3].PossibleValue[0].rows.length > 1) {
			set.Settings[3].PossibleValue[0].rows.splice($index, 1);
		}
	};

	$scope.addCol = function(value) {
		var leng = value.PossibleValue[0].columns.length+1;
		value.PossibleValue[0].columns.push({
			'value': 'column'+leng,
			'value1': 'column'+leng,
			'readonly' : false,
			'showTime': false,
			'colRef':{
							 'referMaster':false,	
							 'dropdown':false,
							 'PossibleValue':[]
						}
		})
		var rowLen = value.PossibleValue[0].rows.length;
		for (var i = 0; i < rowLen; i++) {
			value.PossibleValue[0].rows[i].cells.push({
				value: ''
			});
		}
	};

	$scope.addRow = function(value) {
		var row = {
				cells: []
			},
			colLen = value.PossibleValue[0].columns.length;
		for (var i = 0; i < colLen; i++) {
			row.cells.push({
				value: ''
			});
		}
		value.PossibleValue[0].rows.push(row);
	};

	$scope.addTableJSON = function(record) {
		if (record != "" && (vm.formName != undefined && vm.formName != "")) {
			angular.forEach(record, function(value, key) {
				angular.forEach(value.Settings, function(value1, key1) {
					if (value1.Name == "Internal Name") {
						var intName = value1.Value.replace(/\s/g, '_');
						if(value1.Value != '' && value1.Value != undefined && uniqueInternalName.indexOf(intName) == -1){
							uniqueInternalName.push(intName);
						}						
					}
				});
			});

			if(uniqueInternalName.length == record.length){
				uniqueInternalName = [];
				vm.tableColums = [];
				angular.forEach(record, function(value, key) {
					delete value['GetFieldSetting'];
					delete value['ChangeFieldSetting'];
					delete value['$$hashKey'];
					delete value['id'];
					angular.forEach(value.Settings, function(value1, key1) {
						delete value1['$$hashKey'];
						if (value1.Name == "Internal Name") {
	            vm.tableColums.push({'Type':value.Type, 'ColName': value1.Value.replace(/\s/g, '_')});
	          }
					});
				});
				notificationService.showSpinner();
				createFormsService.addTableJSON(vm.formName, record).then(function(req) {
					createFormsService.createTable(req.rowid, vm.tableColums).then(function(requ){
							createFormsService.createInnerTable(req.rowid, requ.rowid, record).then(function(reque){
									notificationService.hideLoad();	
									notificationService.alert('', 'Form Saved Successfully', function() {
										$scope.formFields = [];
										vm.formName = "";
										vm.selectedTable="";
										vm.showButtons = false;
										$scope.current_field = [];
										$('#addFieldTab_lnk').tab('show');
									});
							});
					})
					
				})
			}else{
    		notificationService.alert('', 'Internal Name should be Required && Unique', function() { 
    			uniqueInternalName = [];
    		});
    	}
		}else{
			if(vm.formName == undefined || vm.formName != "" ){
				notificationService.alert('', 'Enter the Form Name', function() {	});
			}else{
				notificationService.alert('', 'Add atleast one filed to the form', function() { });
			}
		}
	}
	$scope.updateTable = function(record){
		if (record != "" && vm.formName != undefined) {
			notificationService.showSpinner();
		  createFormsService.updateTableJSON(editFormName, vm.formName, record).then(function(req) {
		  	if(previousForm != vm.formName){
		  		createFormsService.updateFormName(vm.formName, previousForm).then(function(req) {
		  		})
		  	}

		  	notificationService.hideLoad();
				notificationService.alert('', 'Form Updated Successfully', function() {	
					$scope.formFields = [];
					vm.formName = "";
					vm.selectedTable ="";
					vm.showButtons = false;
					$scope.current_field = [];
					$('#addFieldTab_lnk').tab('show');
				});
			})
		}
	}
	
  getState();
	function getState(){
		if($state.current.name == 'editForms'){
			 //getForms();
			 getTableByName(editFormName);
		}
	}

	/*function getForms() {
		vm.tableName = [];
		createFormsService.getTableJSON().then(function(data) {
			angular.forEach(data.rows, function(value, key) {
				vm.tableName.push(value);
			});
		});
	}*/
	vm.showButtons = false;
	function getTableByName(selectedTable) {
		$scope.formFields = [];
		createFormsService.getTableByName(selectedTable).then(function(record) {
			if($state.current.name == 'editForms'){
				vm.formName = record.rows[0].tableName;
				previousForm = record.rows[0].tableName
			}
			angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
				
				$scope.addElement(value);
			})
		});
	}
	$scope.cancel = function(){
		$scope.formFields = [];
		vm.formName = "";
		vm.selectedTable="";
		vm.showButtons = false;
		$scope.current_field = [];
		$('#addFieldTab_lnk').tab('show');
	}

	function getBase64(field) {
		var logoImage = document.getElementById('logo_image');
		imgData = getBase64Image(logoImage);
		field.Settings[3].Value = imgData;
	}

	function getBase64Image(img) {
		var canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;

		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);

		var dataURL = canvas.toDataURL("image/jpeg");

		return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
	}

	function changeSelectedMaster(value){
		vm.columnNames = [];
		vm.OtherRef = false;
		angular.forEach(vm.masterTableNames, function(data, key) {
			//Modified to dynamic table
			if(data.tableName == value || data.rowid == value){
				console.log("value  ", value, value.substring(0,7))
				if(value == 'Other'){
					vm.OtherRef = true;
					vm.columnNames.push("Auto-increment");
				}else if((value.substring(0,7)) == 'DF_MSTR'){
					createFormsService.getMasterByName(data.tableName).then(function(record) {
						angular.forEach(record.rows, function(value, key) {
							vm.columnNames.push(value.colName);
						})
					});
				}else{
					tableid = data.rowid;
					createFormsService.getTableByName(data.rowid).then(function(record) {
						console.log("record  ---------------", record)
						angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
							for(var i=0;i<value.Settings.length;i++){
								if(value.Settings[i].Name == 'Internal Name'){
									vm.columnNames.push(value.Settings[i].Value);
								}
							}
						})
					});
				}
			}
		});
	}

	function changeSelectedColumn(currentField, masterdata, conditionData){
		console.log('masterdata000',masterdata);
		vm.valuesTables = [];
		var dynamicTableName = "";
		if(tableid != "")
			dynamicTableName = constantService.tableName+tableid;
		else
			dynamicTableName = masterdata.TableName;
		createFormsService.getColumnValuesCondition(masterdata.ColumnName, dynamicTableName, conditionData).then(function(record) {
			if(masterdata.ColumnName == 'empName'){
				angular.forEach(record.rows, function(value, key) {
					vm.valuesTables.push({'Text' : value[masterdata.ColumnName], 'Text1':value['empCode']});
				})
			}else{
				angular.forEach(record.rows, function(value, key) {
					vm.valuesTables.push({'Text' : value[masterdata.ColumnName]});
				})
			}
			$scope.current_field.Settings[2].PossibleValue = vm.valuesTables;
		})
	}

	function selectionChange(set, current_field){
		set.PossibleValue = [];
		set.MasterRef = !set.MasterRef;

		if(!set.MasterRef){
			$ionicPopup.show({
	    templateUrl: 'templates/masterPopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	          set.MasterRef = false;
	          set.MasterData[0].TableName = '';
						set.MasterData[0].ColumnName = '';
			  		vm.tableNames = [];
						set.PossibleValue.push({
							'Text': 'Choice 1'
						}, {
							'Text': 'Choice 2'
						});
	        } 
	      },
	      {
	        text: '<b>Populate</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	          changeSelectedColumn(current_field, set.MasterData[0], set.ConditionData)
	        }
	      }
	    ]
	  });
			set.MasterRef = true;
			vm.masterShow = true;
			vm.masterTableNames = [];
  		createFormsService.getTableJSON().then(function(data) {
				angular.forEach(data.rows, function(value, key) {
					vm.masterTableNames.push(value);
				});
				createFormsService.getTableMaster().then(function(masterData) {
					angular.forEach(masterData.rows, function(mdata, key) {
						vm.masterTableNames.push(mdata);
					});
				})
			});
		}else{
			set.MasterRef = false;
			set.MasterData[0].TableName = '';
			set.MasterData[0].ColumnName = '';
  		vm.tableNames = [];
			set.PossibleValue.push({
				'Text': 'Choice 1'
			}, {
				'Text': 'Choice 2'
			});
		}
	}
	/*	'DynamicTableRef': false,
				'DynaicRef':{ 
					'Table':'',
					'RefCol':'',
					'FilterCol':''
				}*/


	function setDynamicTable(set, current_field){
		$ionicPopup.show({
	    templateUrl: 'templates/dynamicTablePopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	          set.DynamicTableRef = false;
	          set.DynaicRef.TableId = '';
						set.DynaicRef.RefCol = '';
						set.DynaicRef.FilterCol = '';
	        } 
	      },
	      {
	        text: '<b>Set</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	         
	        }
	      }
	    ]
	  });
		vm.dynamicTableList =[];
	  createFormsService.getTableJSON().then(function(data) {
			angular.forEach(data.rows, function(value, key) {
				vm.dynamicTableList.push(value);
			});
			createFormsService.getTableMaster().then(function(masterData) {
				angular.forEach(masterData.rows, function(mdata, key) {
					vm.dynamicTableList.push(mdata);
				});
			})
		});
	}

	vm.dynamicTableColList = [];
	function getDynamicTableColuman(id){
		vm.dynamicTableColList = [];
		createFormsService.getDynamicTableColName(id).then(function(data) {
			angular.forEach(data.data, function(value, key) {
				vm.dynamicTableColList.push(value);
			});
		});
	}

	vm.getDynamicsameTableColuman = getDynamicsameTableColuman;
	vm.dynamicsameTableColList = [];
	function getDynamicsameTableColuman(id){
		vm.dynamicsameTableColList = [];
		createFormsService.getDynamicTableColName(id).then(function(data) {
			angular.forEach(data.data, function(value, key) {
				vm.dynamicsameTableColList.push(value);
			});
		});
	}


/*'colRef':{
							 'colRefence': false,
							 'masterTable':'',
							 'masterCol':'',
							 'masterRefCol':'',
							 'PossibleValue':[]
						}*/
  function setTableColumnRef(set, current_field){
  	console.log("set ",set)
  	console.log("current fielf ", current_field)

	 /* set.PossibleValue = [];
		set.MasterRef = !set.MasterRef;*/
			
			$ionicPopup.show({
	    templateUrl: 'templates/tableColRefPopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {	          
						
	        } 
	      },
	      {
	        text: '<b>Save</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	         
	        }
	      }
	    ]
	  });
			vm.masterTableNames = [];
  		createFormsService.getTableJSON().then(function(data) {
				angular.forEach(data.rows, function(value, key) {
					vm.masterTableNames.push(value);
				});
				createFormsService.getTableMaster().then(function(masterData) {
					angular.forEach(masterData.rows, function(mdata, key) {
						vm.masterTableNames.push(mdata);
					});
				})
			});
  }









	function populate(set){
		var index = "";
		vm.valuesTables = [];
		var dynamicTableName = "";
		if(tableid != "")
			dynamicTableName = constantService.tableName+tableid;
		else
			dynamicTableName = set.Settings[4].MasterData[0].TableName;
		var count = 0;
		angular.forEach(set.Settings[3].PossibleValue[0].columns, function(data, key) {
			if(data.value == set.Settings[4].MasterData[0].column){
				index = key;
				if(vm.OtherRef){
					vm.autoIncrIndex = index;
					count = vm.autoIncrement != undefined?vm.autoIncrement:0;
				}
			}
		})
		for(var z = 0; z < set.Settings[3].PossibleValue[0].rows.length; z++){
			angular.forEach(set.Settings[3].PossibleValue[0].rows[z].cells, function(value , key){
				if(vm.autoIncrIndex == key){
					count = count + 1 ;
					set.Settings[3].PossibleValue[0].rows[z].cells[key].value = count;
				}
			})
		}
		
		/*createFormsService.getColumnValues(set.Settings[4].MasterData[0].ColumnName, dynamicTableName).then(function(record) {*/
		createFormsService.getColumnValuesCondition(set.Settings[4].MasterData[0].ColumnName, dynamicTableName, set.Settings[4].ConditionData).then(function(record) {

		//createFormsService.getColumnValuesCondition(set.Settings[4].MasterData[0].ColumnName, dynamicTableName, set.Settings[4].ConditionData[0].condColumn, set.Settings[4].ConditionData[0].TableName, set.Settings[4].ConditionData[0].ColumnName, set.Settings[4].ConditionData[0].value).then(function(record) {

			var startPoint = 0;
			var existcells = [];
			for(var i = 0; i < set.Settings[3].PossibleValue[0].rows.length; i++ ){
				if(set.Settings[3].PossibleValue[0].rows[i].cells[index].value != ""){
					startPoint = i+1;
				}
			}
			angular.forEach(record.rows, function(value, key) {
				if(set.Settings[3].PossibleValue[0].rows.length > 0){
					if(startPoint == set.Settings[3].PossibleValue[0].rows.length){
						var cells = [];
						for(var j = 0; j < set.Settings[3].PossibleValue[0].rows[0].cells.length; j++ ){
							if(index == j){
								cells.push({'value': value[set.Settings[4].MasterData[0].ColumnName]});
							}else if(vm.autoIncrIndex == j){
								count = count + 1;
								cells.push({'value': count});
							}else{
								cells.push({'value': ''});
							}
						}
						set.Settings[3].PossibleValue[0].rows.push({'cells': cells});
						startPoint++;
					}else{
						var cells = [];
						var m = 0;
						for(var k = 0; k < set.Settings[3].PossibleValue[0].rows[0].cells.length; k++ ){
							if(index == k){
								cells.push({'value': value[set.Settings[4].MasterData[0].ColumnName]});
							}else if(vm.autoIncrIndex == j){
								count = count + 1;
								cells.push({'value': count});
							}else{	
								cells.push({'value': set.Settings[3].PossibleValue[0].rows[startPoint].cells[m].value});
							}
							m++;
						}
						set.Settings[3].PossibleValue[0].rows[startPoint]['cells']= cells;
						startPoint++;
					}
				}else{
					var cells = [];
					for(var n = 0; n < set.Settings[3].PossibleValue[0].rows[0].cells.length; n++ ){
						if(index == n){
							cells.push({'value': value[set.Settings[4].MasterData[0].ColumnName]});
						}else if(vm.autoIncrIndex == j){
								count = count + 1;
								cells.push({'value': count});
						}else{								
							cells.push({'value': ''});
						}
					}
					set.Settings[3].PossibleValue[0].rows.push({'cells': cells});
				}

			})
		})
	}

	function masterCheckChange(set, current_field){
		set.PossibleValue = [];
		set.MasterRef = !set.MasterRef;
		if(!set.MasterRef){
			$ionicPopup.show({
	    templateUrl: 'templates/tablePopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	          set.MasterRef = false;
	        } 
	      },
	      {
	        text: '<b>Populate</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	          populate(current_field);
	        }
	      }
	    ]
	  });
			set.MasterRef = true;
			vm.masterTableNames = [];
  		createFormsService.getTableJSON().then(function(data) {
				angular.forEach(data.rows, function(value, key) {
					vm.masterTableNames.push(value);
				});
				createFormsService.getTableMaster().then(function(masterData) {
					angular.forEach(masterData.rows, function(mdata, key) {
						vm.masterTableNames.push(mdata);
					});
				})
				vm.masterTableNames.push({"rowid":'',tableName:'Other'});
			});
		}else{
			set.MasterRef = false;
			set.MasterData[0].TableName = '';
			set.MasterData[0].ColumnName = '';
		}
	}


	function setColCondition(set, current_field){
		console.log("current_field ", current_field)
		current_field.Settings[5].ColCondValue = ''
		current_field.Settings[5].ColCondValue['resultCol'] = '';
		current_field.Settings[5].ColCondValue.Condtion = [];
		getAllColumnList(current_field);
		console.log("set in table condition ", current_field)				
		$ionicPopup.show({
	    templateUrl: 'templates/tableColConditionPopup.html',
	    title: 'Enter Details',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	          
	        } 
	      },
	      {
	        text: '<b>Ok</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	         
	        }
	      }
	    ]
	  });
			
		vm.masterTableNames = [];
		
		
	}
	
	function setCondition(set, value, index){		
		console.log("set ", set)
		if(set.Condtion == undefined){
			set.Condtion = [];
		}
			
		set.Condtion[index] = value;
		
		
				console.log("setrttt ", set)
	}



/*'Name': 'General Options',
				'Type': 'checkBoxZone',
				'Options': [{
					'Name': 'Required',
					'Value': false
				}],
				'Restrict': "",
				'ColmRestrict': [],
				'ColCondition': false,
				'ColCondValue': {"Condtion":[]},*/






  vm.colList = [];
	function  getAllColumnList(current_field){
		console.log("jo ", current_field)
		angular.forEach($scope.formFields, function(value, key){
			if(value.Type == "table" && current_field.Name == value.Name){			
			console.log("value ", value)			
				angular.forEach(value.Settings[3].PossibleValue[0].columns, function(value1, key1){
					var count = 0;
					angular.forEach(vm.colList, function(val2, key2){
						if(val2.value1 != value1.value1 || val2.value != value1.value){
							count++
						}
					})
					if(count == vm.colList.length){
						value.Settings[5].ColmRestrict.push({column: value1.value1, depts:[]});
						vm.colList.push({'value':value1.value, 'value1':value1.value1});
					}
				});
			}
		})
		getDepartments('true');
	}

	function addColDept(value, colname, set){		
		angular.forEach(set.ColmRestrict, function(val1, key1){
			if(colname == val1.column){
				if(val1.depts.indexOf(value) == -1) {					
		      val1.depts.push(value);
		    }else{
		    	 val1.depts.splice(val1.depts.indexOf(value), 1)
		    }
			}
		})
	}

	function addColTimer(value,  curentField){		
		angular.forEach(curentField.Settings[3].PossibleValue[0].columns, function(val1, key1){
			if(value.value == val1.value){
				if(val1.showTime != undefined){
					val1.showTime = true;
				}else{
					val1['showTime'] = true;
				}
			}
		});
	}

	function addColDate(value,  curentField){		
		angular.forEach(curentField.Settings[3].PossibleValue[0].columns, function(val1, key1){
			if(value.value == val1.value){
				if(val1.showDate != undefined){
					val1.showDate = true;
				}else{
					val1['showDate'] = true;
				}
			}
		});
	}




	function getDepartments(rest){
		if(rest){
			vm.departments = [];
			workflowService.getDepartments().then(function(data) {
				angular.forEach(data.rows, function(value, key){
					vm.departments.push(value);
				})
			});
		}
	}

	function selectCondTable(value, colIndex){
		var the_string = "condColumnNames"+colIndex;
	  var model = $parse(the_string);
	  model.assign($scope, true);
		$scope[the_string] = [];
		selecttableName = value;
		angular.forEach(vm.masterTableNames, function(data, key) {
			//Modified to dynamic table
			if(data.tableName == value){
				if((value.substring(0,7)) == 'DF_MSTR'){
					createFormsService.getMasterByName(data.tableName).then(function(record) {
						angular.forEach(record.rows, function(value, key) {
							$scope[the_string].push(value.colName);
						})
					});
				}else{
					tableid = data.rowid;
					createFormsService.getTableByName(data.rowid).then(function(record) {
						angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
							for(var i=0;i<value.Settings.length;i++){
								if(value.Settings[i].Name == 'Internal Name'){
									$scope[the_string].push(value.Settings[i].Value);
								}
							}
						})
					});
				}
			}
		});
		//colIndex = colIndex+1;
	}

	$scope.getValues = function (index) {
	  return $scope["condColumnNames"+index];
	}
	$scope.getDataValues = function (index) {
	  return $scope["dataValues"+index];
	}
	function changingColumanName(val){


		console.log("asd ", val)
	}
	function selectCondColumn(value, valIndex){
		var dataval = "dataValues"+valIndex;
	  var model = $parse(dataval);
	  model.assign($scope, true);
		$scope[dataval] = [];
		//vm.dataValues =[];
		createFormsService.getColumnValues(value, selecttableName).then(function(record) {
			angular.forEach(record.rows, function(data, key) {
				$scope[dataval].push(data[value]);
			})

		})
		//valIndex = valIndex+1;
	}

	function addCondition(current_field){
		current_field.Settings[4].ConditionData.push({
					'condColumn':'',
					'condTableName': '',
					'condColumnName': '',
					'condValue': '',
					'condChange': '',
					'condEquals': 'IN'
				})
		vm.showCondition = true;
	}

	function addSelectCondition(current_field){
		current_field.Settings[2].ConditionData.push({
					'condColumn':'',
					'condTableName': '',
					'condColumnName': '',
					'condValue': '',
					'condChange': '',
					'condEquals': 'IN'
				})
		vm.showCondition = true;
	}

	function cancel(){
		notificationService.confirm('Are you sure want to cancel', 'Yes', 'No',function(){
      $scope.formFields = [];
			vm.formName = "";
			vm.selectedTable="";
			vm.showButtons = false;
			$scope.current_field = [];
			$('#addFieldTab_lnk').tab('show');
    },function(){
    })
	}

}

$(function() {
	// Code here
	var dh = $(document).height();
	$('#sidebar-tab-content').height(dh - 115);
	$('#main-content').height(dh - 10);
});
(function() {
	'use strict';

	angular
		.module('amaraja')
		.controller('getAllFormsController', getAllFormsController);

	getAllFormsController.$inject = ['workflowService', '$cordovaSQLite', 'notificationService', '$state', '$scope', '$q'];

	function getAllFormsController(workflowService, $cordovaSQLite, notificationService, $state, $scope, $q) {
		var vm = this;
		var workflowNames = [];
		var promiseColl = [];
		var previousValue = "";
		var cond = true;

		workflowService.getAllDetails().then(function(data) {
			vm.workflowNames = [];
			angular.forEach(data.rows, function(value, key) {
				if(key > 0 && cond){
					previousValue = data.rows[key-1].workflowName;
					cond = false;
				}
				if(value.workflowName == previousValue){
					value.workflowName = "";
					value.levels = "";
				}else{
					cond = true;
				}
      	vm.workflowNames.push(value);
      });
     
    })

	}

})();
angular.module('amaraja').controller('getFormsRecordsCtrls', getFormsRecordsCtrls);

getFormsRecordsCtrls.$inject = ['$scope', '$timeout', '$rootScope', '$interval', '$state', '$window', '$q', '$rootScope', '$location', '$http', 'getFormsService', 'createFormsService', 'getFormsRecordsService', 'notificationService', '$ionicSideMenuDelegate', 'ionicDatePicker', 'ionicTimePicker', 'trackRecordService', 'constantService', 'SendEmailService', 'recordDetailsService'];

function getFormsRecordsCtrls($scope, $timeout, $rootScope, $interval, $state, $window, $q, $rootScope, $location, $http, getFormsService, createFormsService, getFormsRecordsService, notificationService, $ionicSideMenuDelegate, ionicDatePicker, ionicTimePicker, trackRecordService, constantService, SendEmailService, recordDetailsService) {
	var vm = this;
	vm.getFormsNames = getFormsNames;
	vm.getTableRecords = getTableRecords;
	vm.showAllRecord = showAllRecord;
	vm.createNewField = createNewField;
	vm.updateRecord = updateRecord;
	vm.checkColrestrict = checkColrestrict;
	vm.ColumanCondition = ColumanCondition;
	vm.dateConvert = dateConvert;
	vm.getRecordTrack = getRecordTrack;
	vm.cancel = cancel;
	vm.setEditTrue = setEditTrue;
	vm.valuationDatePickerOpen = valuationDatePickerOpen;
	vm.settabledatepacknote = settabledatepacknote;
	vm.SqlDateFormate = SqlDateFormate;
	vm.getDate = getDate;
	vm.getTime = getTime;
	vm.addRow = addRow;
	vm.tableName = [];
	vm.TableRecords = [];
	vm.tableValue = [];
	vm.allrecord = ''
	var guid = 1;
	vm.showInput = true;
	vm.hideButtons = false;
	vm.formFields = [];
	vm.formName = '';
	vm.current_field = {};
	vm.tableRowId = 0;
	vm.tableRowName = '';
	vm.updateDept = '';
	vm.rejectToDept = '';
	vm.setMoveToDept = '';
	vm.formLevelInnumber = '';
	vm.presentRecorIndex = '';
	vm.returnDept = [];
	vm.hideIndex = [];
	vm.allDetails = [];
	vm.hideOperator = false;
	vm.showtable = false;
	vm.flag = true;
	vm.showAdd = false;

	$rootScope.codeNameOfRecord = "";

	vm.serviceMailInformation = [];

	vm.department = $window.localStorage.userDept;
	vm.loggedInUser = $window.localStorage.empName + ' ' + $window.localStorage.empCode;
	vm.operator = $window.localStorage.empCode + ' ' + $window.localStorage.empName;
	vm.loginUserRole = $window.localStorage.userType;

	$scope.$watch(function() {
		return $state.params;
	});
	var FormName = $state.params.getFormName;
	vm.typeRejected = $state.params.type;
	console.log('vm.typeRejected', vm.typeRejected);
	vm.LoginUserTypee = LoginUserTypee;
	LoginUserTypee();

	getFormsNames();
	notificationService.showSpinner();

	function LoginUserTypee() {
		if ($window.localStorage.userDept == '' || $window.localStorage.userDept == undefined)
			$state.go('login');
		var LoginUserType = $window.localStorage.userDept;
		if (LoginUserType == 'Engineering') {
			vm.access = 3;
		} else if (LoginUserType == 'QA') {
			vm.access = 15;
			vm.showhidenform = true;
		} else {
			vm.access = 15;
		}
		if ($state.current.name == 'getFormRecords' && $window.localStorage.userType == 'Operator') {
			vm.hideOperator = true;
		}
	}

	function getDate(x) {
		var ipObj2 = {
			callback: function(val) { //Mandatory
				console.log('Return value from the datepicker popup is : ' + val, new Date(val));
				x.Value = dateConvert(new Date(val));
				x['Edited'] = true;
				//vm.ColumanCondition(col, celll, tablejson);
			},
			from: new Date(2000, 1, 1), //Optional
			to: new Date(2200, 10, 30), //Optional
			inputDate: new Date(), //Optional
			mondayFirst: true, //Optional
			disableWeekdays: [0], //Optional
			closeOnSelect: false, //Optional
			templateType: 'popup' //Optional
		};
		ionicDatePicker.openDatePicker(ipObj2);
	}

	function getTime(cell, col, celll, tablejson) {
		console.log("col ", col)
		console.log("cell ", cell)
		console.log("celll ", celll)
		console.log("tablejson ", tablejson)
		var timmm = ''
		var ipObj1 = {
			callback: function(val) { //Mandatory
				if (typeof(val) === 'undefined') {
					console.log('Time not selected');
				} else {
					var selectedTime = new Date(val * 1000);
					timmm = selectedTime.getUTCHours();
					cell.Value = selectedTime.getUTCHours() + " : " + selectedTime.getUTCMinutes()
					vm.ColumanCondition(col, celll, tablejson, cell);
				}
			},
			inputTime: 12 * 60 * 60, //Optional
			format: 12, //Optional
			step: 1, //Optional
			setLabel: 'Ok' //Optional
		};
		ionicTimePicker.openTimePicker(ipObj1);
	}

	function setEditTrue(value, tableRecord, recorvalue) {
		console.log('entered set edit');
		getValueDynamicaly(value, tableRecord, recorvalue);
		value['Edited'] = true;
		var cavityCount = 0;
		var statIndex = 0;

		if (value.Cavity != "" && value.Type == "dropdown") {
			angular.forEach(vm.tableValue, function(val, key) {
				if (val.Type == 'table' && val.Cavity != '') {
					angular.forEach(val.Value.table[0].columns, function(val1, key1) {
						if (val1.Value == val.Cavity) {
							statIndex = key1;
						}
						if (val1.cavity) {
							cavityCount++;
						}
					})
					var count = value.Value - 1;
					if (cavityCount == 0) {
						angular.forEach(val.Value.table[0].columns, function(val1, key1) {
							if (val1.Value == value.Cavity) {

								for (i = 0; i < count; i++) {
									val.Value.table[0].columns.splice(statIndex + 1 + i, 0, { 'Value': value.Cavity + '_' + (i + 1), 'Value1': val.Value.table[0].columns[statIndex].Value1 + '_' + (i + 1), 'cavity': true });
									if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) + 1;
								}
								angular.forEach(val.Value.table[1].rows, function(val2, key2) {
									for (i = 0; i < count; i++) {
										val2.cells.splice(statIndex + 1 + i, 0, { 'Value': '', 'cavity': true });

									}
								});
							}
						});
					} else if (cavityCount < count) {
						for (i = cavityCount; i < count; i++) {
							val.Value.table[0].columns.splice(statIndex + i + 1, 0, { 'Value': value.Cavity + '_' + (i + 1), 'Value1': val.Value.table[0].columns[statIndex].Value1 + '_' + (i + 1), 'cavity': true });
							if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) + 1;
						}
						angular.forEach(val.Value.table[1].rows, function(val2, key2) {
							for (i = cavityCount; i < count; i++) {
								val2.cells.splice(statIndex + i + 1, 0, { 'Value': '', 'cavity': true });
							}
						});
					} else if (cavityCount > count) {
						for (i = cavityCount; i > count; i--) {
							var tot = statIndex + i
							if (val.Value.table[0].columns[tot].cavity) {
								val.Value.table[0].columns.splice(tot, 1);
								if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) - 1;
							}
						}
						angular.forEach(val.Value.table[1].rows, function(val2, key2) {
							for (i = cavityCount; i > count; i--) {
								val2.cells.splice(tot, 1);
							}
						});
					}
				}
			});
		}
	}

	function getFormsNames() {
		createFormsService.getTableJSON().then(function(data) {
			angular.forEach(data.rows, function(value, key) {
				vm.tableName.push(value);
				if (FormName.toUpperCase().includes("IN-PROCESS")) {
					vm.showAdd = true;
				}
				if (value.tableName == FormName) {
					getTableRecords(value.rowid);
				}
				notificationService.hideLoad();
			});
			notificationService.hideLoad();
		});
	}

	function getTableRecords(id) {
		var deferred = $q.defer();
		vm.hideIndex = [];
		var promiscall = [];
		vm.selectedTable = id;
		notificationService.showSpinner();
		vm.TableRecords = [];
		vm.RejeTableRecords = [];
		var level = '';
		var dept = '';
		var idList = [];
		vm.compare = []
		getTableDetails(vm.selectedTable);

		getFormsRecordsService.getTableRecords(vm.selectedTable).then(function(data) {
			getFormsRecordsService.checkWorkflow(FormName).then(function(res) {
				angular.forEach(res.rows, function(value, key) {
					if (value.department == $window.localStorage.userDept) {
						level = value.lev.replace('level', '').trim();
						vm.formLevelInnumber = level;
					}
				});

				console.log(' $state.params', $state.params);
				Type = $state.params.type;
				recordType = 'approved';
				var rejectQuery = '';
				vm.returnDept = [];
				angular.forEach(res.rows, function(value, key) {
					if ('level ' + (parseInt(level) + 1) == value.lev) {
						vm.moveToDept = value.department;
					}
					if ('level ' + (parseInt(level) - 1) == value.lev) {
						vm.rejectToDept = value.department;
					}
					if (value.department != $window.localStorage.userDept && level > value.lev.replace('level', '').trim()) vm.returnDept.push(value.department);

					if (parseInt(value.lev.replace('level', '').trim()) >= parseInt(level)) {
						rejectQuery = rejectQuery + "'" + value.department + "',";
					}
				});
				if (res.rows.length == 1) {
					vm.hideradioButtons = false;
					vm.setMoveToDept = 'done';
				} else {
					vm.hideradioButtons = true;
				}
				if (dept == '') dept = 'Engineering';
				if (vm.rejectToDept == undefined) vm.moveToDept = 'Engineering';
				vm.updateDept = dept;
				var recordType = $state.params.type;
				dept = $window.localStorage.userDept;
				var timer;

				getFormsRecordsService.checkApproved(vm.selectedTable, dept, recordType, FormName, rejectQuery.slice(0, -1)).then(function(res1) {
					console.log("res1  ", res1);
					console.log('type', Type);
					if (res1.rows.length > 0) {
						angular.forEach(data.rows, function(value, key) {

							console.log('value res1', value)
							if (key == '0') {
								$rootScope.codeNameOfRecord = getCodeNameOfRecord(value.recordName);
							}
							if (value.qainspector == vm.loggedInUser || value.supervisor == vm.loggedInUser || value.supervisorname == vm.loggedInUser ||
								value.operator == vm.operator || value.senioroperator == vm.operator || Type == '' || Type == undefined || vm.department == "Engineering" ||
								value.approvedby == vm.loggedInUser ||
								vm.formName.toUpperCase().includes('LEAK') || vm.formName.toUpperCase().includes('PACK') ||
								vm.formName.toUpperCase().includes('IN-PROCESS') || vm.formName.toUpperCase().includes('DAILY OPERATOR WORK ALLOTMENT') ||
								vm.formName.toUpperCase().includes('CHEMICAL MIXING AT BUSH CASTING') ||
								vm.formName.toUpperCase().includes('SHIFT RELEIVING SHEET') || vm.formName.toUpperCase().includes('INSPECTION REPORT') ||
								vm.formName.toUpperCase().includes('LEAK TESTING CUM GUMMING APPLICATION RECORD') ||
								vm.formName.toUpperCase().includes('MOLD CHANGE CHECK SHEET') ||
								vm.formName.toUpperCase().includes('IN-PROCESS SET UP APPROVAL') || vm.formName.toUpperCase().includes('FLAMMABILITY REPORT')) {

								// console.log("value for res1 ", value);
								// console.log('type after res1', Type);
								var flag = true;
								promiscall.push(
									angular.forEach(res1.rows, function(value1, key1) {
										angular.forEach(value, function(val, keyy) {
											// console.log('val after res1', val);

											// console.log('vm.department', vm.department);
											// console.log('value1.approved', value1.approved);
											// console.log('keyy', keyy);
											// console.log('value1.recordId', value1.recordId);
											if (((vm.department == "Engineering" && vm.operator == value.preparedby) ||(vm.department == "Production" && vm.operator == value.operator) || (vm.department == "QA" && (vm.operator == value.qainspector || vm.operator == value.inspectedby))) && value1.approved == "draft" && value1.recordId == val && keyy == 'rowid' && Type == 'Draft') {
												console.log('draft')
												console.log('draft value',value);
												vm.TableRecords.push(value);
											} else if (Type == 'Reject') {
												if (value1.recordId == val && keyy == 'rowid' && Type.toLowerCase() == value1.approved.toLowerCase()) vm.TableRecords.push(value);
											} else if (Type == 'done') {
												console.log('found here0');
												if (value1.recordId == val && keyy == 'rowid' && Type.toLowerCase() == value1.approved.toLowerCase()) {
													if (vm.compare.indexOf(value.rowid) == -1) {
														vm.TableRecords.push(value);
														vm.compare.push(value.rowid)
													}
												}
											} else if (Type == 'inprocess' && value1.approved != "draft") {

												console.log('inprocess tablerecords');

												if (value1.recordId == val && keyy == 'rowid' && (!vm.formName.toUpperCase().includes('IN-PROCESS SET UP APPROVAL') && !vm.formName.toUpperCase().includes('FLAMMABILITY REPORT')) ||
													value1.recordId == val && (value.supervisorname == vm.loggedInUser || value.senioroperator == vm.operator || value.approvedby == vm.loggedInUser || value.inspectedby == vm.operator || (value.operatorname == vm.operator && vm.loginUserRole == "Operator" && vm.department == "Production")) ||
													(value.qaincharge == $window.localStorage.empName) && keyy == 'rowid') {
												console.log('value.qaincharge == ',value.qaincharge);
												console.log('$window.localStorage.empName',$window.localStorage.empName);

													console.log('value1', value1);
													if (vm.department != value1.deptName || vm.loginUserRole == 'Operator' || (vm.loginUserRole == 'Approver' && vm.department=="QA" && vm.formName.toUpperCase().includes('INSPECTION REPORT') && value.qaincharge == $window.localStorage.empName) || (vm.department == value1.deptName && value1.approved != "approved" && value1.updatedBy == vm.department))
														vm.TableRecords.push(value);
												}
											} else {
												if (value1.recordId == val && keyy == 'rowid' && vm.department == 'QA') {
													console.log("QA tablerecords");
													console.log('vm.loggedInUser', vm.loggedInUser);
													console.log('vm.value.qaincharge', value.qaincharge);
													console.log('vm.loginUserRole', vm.loginUserRole);
													console.log('vm.formName.toUpperCase()', vm.formName.toUpperCase());
													console.log(vm.formName.toUpperCase().includes('INSPECTION REPORT') && vm.loginUserRole == "Approver" && value.qaincharge == vm.loggedInUser);
													if (value.qainspector == vm.loggedInUser || value.qaleaktest == vm.loggedInUser ||
														value.qagumming1 == vm.loggedInUser || value.qabushcasting == vm.loggedInUser ||
														(vm.formName.toUpperCase().includes('INSPECTION REPORT') && vm.loginUserRole == "Approver" && value.qaincharge == vm.loggedInUser) ||
														(vm.formName.toUpperCase().includes('FLAMMABILITY REPORT') && vm.loginUserRole == "Approver")) {
														flag = false;
														console.log('entered qa tablerecords');
														vm.TableRecords.push(value);
													}
												} else if (value1.recordId == val && keyy == 'rowid' && vm.department == 'Production' && value1.approved != "draft") {
													console.log("Production tablerecords");
													console.log('vm.loggedInUser', vm.loggedInUser);
													console.log('value.senioroperator', value.senioroperator);
													if (value.supervisor == vm.loggedInUser || value.operator == vm.operator || value.supervisorname == vm.loggedInUser || value.senioroperator == $window.localStorage.empCode + ' ' + $window.localStorage.empName) {
														flag = false;
														if (value1.approved == 'transfer') vm.moveToDept = value1.updatedBy;
														vm.TableRecords.push(value);
													}
												} else if (value1.recordId == val && keyy == 'rowid' && vm.department == 'Engineering') {
													console.log('found here');
													if (value1.approved == 'transfer') vm.moveToDept = value1.updatedBy;
													flag = false;
													if (value1.recordId == val && keyy == 'rowid' && Type.toLowerCase() == value1.approved.toLowerCase()) {
														vm.TableRecords.push(value);
														// && value.approverby==vm.loggedInUser
													} else if (value1.recordId == val && keyy == 'rowid' && 'approved' == value1.approved.toLowerCase()) {
														vm.TableRecords.push(value);
													}
												} else if (value1.recordId == val && keyy == 'rowid' && vm.department == 'Mold Maintenance') {
													console.log('mold');
													flag = false;
													vm.TableRecords.push(value);
												}
											}
										});
									})
								)
								$q.all(promiscall).then(function(datavalue) {
									if (vm.department == 'Engineering' && flag && Type != 'Reject'  && Type != 'Draft' && Type != 'Transfer' && Type != 'Return' && Type != 'done' && Type != 'inprocess') {
										vm.TableRecords.push(value);
									}
								})
							}
						})
					} else if (vm.department == 'Engineering' && Type != 'Reject'&& Type != 'Draft' && Type != 'Transfer' && Type != 'Return' && Type != 'done' && Type != 'inprocess') {

						console.log('found here 2');
						angular.forEach(data.rows, function(value, key) {
							vm.TableRecords.push(value);
						});
					} else {
						console.log('found here 3');
						console.log('res ', res);
						if (Type == 'Draft') {
							console.log('found here 3 sub 0');
							var query = "SELECT  * FROM  DF_TBL_APR WHERE status='a' AND approved='draft' AND tableName ='" + vm.selectedTable + "' AND updatedBy ='" + $window.localStorage.userDept + "'";
							getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {
								var flag = '';
								angular.forEach(data.rows, function(value, key) {
									flag = false;
									angular.forEach(reqVal.data, function(value1, key1) {
										if (value1.recordId == value.rowid && value1.approved == 'draft') flag = true
									});
									if (flag) {
										vm.TableRecords.push(value);
									}
								});
							})
						} else if (res.rows.length == 1 && Type != 'Reject' && Type != 'Transfer' && Type != 'Return' && Type != 'done' && Type != 'inprocess') {
							console.log('found here 3 sub 1');
							var query = "SELECT  * FROM  DF_TBL_APR WHERE status='a' AND approved='done' AND tableName ='" + vm.selectedTable + "' AND updatedBy ='" + $window.localStorage.userDept + "'";
							getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {
								console.log('reqVal ', reqVal);
								var flag = '';
								angular.forEach(data.rows, function(value, key) {
									flag = true
									angular.forEach(reqVal.data, function(value1, key1) {
										if (value1.recordId == value.rowid) flag = false
									});
									if (flag) {
										vm.TableRecords.push(value);
									}
								});
							})
						} else if (res1.rows.length == 0 && Type == '') {
							console.log('found here 3 sub 2');
							var query = "SELECT  * FROM  DF_TBL_APR WHERE  status='a' AND tableName ='" + vm.selectedTable + "'";
							getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {
								console.log('found here 3 sub 2 reqVal', reqVal);
								var flag = '';

								if (reqVal.data.length > 0) {
									angular.forEach(data.rows, function(value, key) {
										console.log('value1', value);
										flag = false;
										var valueApproved = '',
											reqValDept = '';
										angular.forEach(reqVal.data, function(value1, key1) {
											if (value1.recordId == value.rowid) {
												// console.log('value1',value1);
												// console.log('value1.recordId',value1.recordId);
												// console.log('value.rowid',value.rowid);
												flag = true;
												valueApproved = value1.approved;
												reqValDept = value1.deptName;
											}
										});
										if (flag) {
											console.log('reqVal.deptName ', reqVal.deptName);
											console.log('vm.loginUserRole ', vm.loginUserRole);
											console.log('value.qainspector', value.qainspector);
											console.log('vm.department', vm.department);
											console.log('vm.loggedInUser', vm.loggedInUser);
											console.log('flag true:', flag && reqVal.deptName == "Mold Maintainance" && vm.loginUserRole == "Approver" && value.qainspector == vm.loggedInUser);
										}

										// if (flag && value.supervisorname == vm.loggedInUser && valueApproved != 'done' &&
										// (vm.department == "deptName" && vm.loginUserRole == "Approver" && reqValDept != "QA"))
										if (flag && value.supervisorname == vm.loggedInUser && valueApproved != 'done' &&
											(vm.loginUserRole == "Approver" && reqValDept != "QA"))
											vm.TableRecords.push(value);
										else if (flag && reqValDept == "Mold Maintainance" && vm.loginUserRole == "Approver" && value.qainspector == vm.loggedInUser) {
											vm.TableRecords.push(value);
										}
									});
								} else {
									angular.forEach(data.rows, function(value, key) {
										vm.TableRecords.push(value);
									})
								}
							})
						} else if (res.rows.length > 0) {
							angular.forEach(res.rows, function(resRows) {
								// 
							});
						}
					}
				});

			});
			notificationService.hideLoad();
		});
	}

	function showAllRecord(record, presentRecorIndex) {
		var FormName = $state.params.getFormName;
		if (FormName.toUpperCase().includes("DAILY OPERATOR") && vm.typeRejected.toUpperCase() == "RETURN") {
			$state.go('getForms', { 'getFormName': FormName, 'type': 'return', 'selectedId': record.recordName }, { reload: true });
		} else if (FormName.toUpperCase().includes("LEAK TESTING") && vm.typeRejected.toUpperCase() == "RETURN") {
			$state.go('getForms', { 'getFormName': FormName, 'type': 'return', 'selectedId': record.recordName }, { reload: true });
		}
		/*else if(FormName.toUpperCase().includes("MOLD CHANGE")){
				  $state.go('getForms', {'getFormName': FormName, 'type': 'return', 'selectedId':record.recordName}, {reload:true}); 
				}*/
		if (vm.selectedRecord != record.recordName) {
			vm.showtable = false;
			vm.showRecordDetails = true;
			vm.selectedRecord = record.recordName;
			vm.presentRecorIndex = presentRecorIndex;
			$scope.activeMenu = vm.presentRecorIndex;
			notificationService.showSpinner();
			$('#border_getform').addClass('border-getForm');
			vm.showInput = true;
			vm.hideButtons = true;
			for (var i = 0; i < vm.tableName.length; i++) {
				if (vm.selectedTable == vm.tableName[i].rowid) {
					vm.selectedTableShow = vm.tableName[i].tableName;
				}
			}
			vm.rowData = record;
			vm.showInput = false;
			vm.tableValue = [];
			var subId = 0;
			var recordId = '';
			angular.forEach(vm.formFields, function(value, key) {
				var table = '[]';
				var deferred = $q.defer();
				var tableData = [];
				var checkvalue = '';
				var Restrict = '';
				var Remark = '';
				var colSpan = '';
				var Rquired = '';
				var Name = '';
				var FieldOption = [];
				var Cavity = ''
					//Setting Array 3- for Colam Span
					//Setting Array 4 - for restriction
				var ColCondValue = '';
				var readOnly = '';
				var ColValue1 = '';
				var colCondition = false;
				var Disabled = '';
				angular.forEach(value.Settings, function(value1, key1) {
					if (value1.Name == 'Internal Name') {
						colSpan = value.Settings[3].Value;
						Name = value.Name;
						if (value.Type == 'radio') {
							FieldOption = value.Settings[2].PossibleValue;
							if (value.Settings[4].Restrict != '') Restrict = value.Settings[4].Restrict[vm.department];
							else Restrict = true;
							Rquired = value.Settings[4].Options[0].Value;
							Disabled = value.Settings[4].Options[0].Value;
						} else if (value.Type == 'checkbox') {
							FieldOption = value.Settings[2].PossibleValue;
							if (value.Settings[4].Restrict != '') Restrict = value.Settings[4].Restrict[vm.department];
							else Restrict = true;
							Rquired = value.Settings[4].Options[0].Value;
							Disabled = value.Settings[4].Options[0].Value;
						} else if (value.Type == 'dropdown') {
							/*angular.forEach(value.Settings[2].PossibleValue, function(avalue, aKey){
								console.log("))0000000000000000000", avalue)
								FieldOption.push({"Text": avalue.Text})
							})*/
							FieldOption = value.Settings[2].PossibleValue;
							console.log('FieldOption', FieldOption);
							if (value.Settings[4].Restrict != '') Restrict = value.Settings[4].Restrict[vm.department];
							else Restrict = true;
							Rquired = value.Settings[4].Options[0].Value;
							Disabled = value.Settings[4].Options[1].Value;
							if (value.Settings[4].Cavity != '') Cavity = value.Settings[4].Cavity;

						} else if (value.Type != 'table') {
							if (value.Settings[4].Restrict != '') Restrict = value.Settings[4].Restrict[vm.department];
							else Restrict = true;
							Rquired = value.Settings[4].Options[0].Value;
						} else {

						}
						if (FormName.toUpperCase().includes("LEAK")) {
							angular.forEach(vm.tableValue, function(valLeak, keyLeal) {
								if (valLeak.InternalName == 'process') {
									varProcess = valLeak.Value;
									if ((value.Settings[1].Value).includes((valLeak.Value).toLowerCase().split(" ")[0])) {
										Restrict = true;
									} else {
										Restrict = false;
									}
								} else if (valLeak.Type = 'dropdown') {
									if ((valLeak.InternalName.toUpperCase()).includes('BUSH') || (valLeak.InternalName.toUpperCase()).includes('LEAK') || (valLeak.InternalName.toUpperCase()).includes('GUMM')) {
										var aaa = (varProcess.split(" ")[0]).toLowerCase();
										if ((valLeak.InternalName).includes(aaa)) {
											value.Restrict = true;
										} else {
											value.Restrict = false;
										}
									}

								}
							})
						}


						if (value.Type == 'table') {
							Rquired = value.Settings[5].Options[0].Value;
							var val = (value1.Value).replace(' ', '_');
							if (value.Settings[5].colCondition && value.Settings[5].ColCondValue != undefined && value.Settings[5].ColCondValue != '' && value.Settings[5].ColCondValue.Condtion != undefined) {
								ColCondValue = value.Settings[5].ColCondValue;
								colCondition = value.Settings[5].colCondition;
							}
							if (value.Settings[5].Restrict != '' && value.Settings[5].Restrict != undefined) {
								Restrict = value.Settings[5].Restrict[vm.department] != undefined ? value.Settings[5].Restrict[vm.department] : false;
							} else {
								Restrict = true;
							}

							if (FormName.toUpperCase().includes("LEAK")) {
								var varProcess = ''
								angular.forEach(vm.tableValue, function(valLeak, keyLeal) {
									if (valLeak.InternalName == 'process') {
										varProcess = valLeak.Value;
										if ((value.Settings[1].Value).includes((valLeak.Value).toLowerCase().split(" ")[0])) {
											Restrict = true;
										} else {
											Restrict = false;
										}
									}
								})
							}


							if (record[val] != undefined) {
								recordId = record[val];
								/*vm.tableRowName = val;
								vm.tableRowId = recordId;*/
							} else {
								record[val] = recordId;
							}
							if (value.Settings[5].Remark != '' && value.Settings[5].Remark != undefined) Remark = value.Settings[5].Remark;
							else Remark = '';
							if (value.Settings[5].Cavity != '' && value.Settings[5].Cavity != undefined) Cavity = value.Settings[5].Cavity;
							else Cavity = '';

							var columns = [];
							var addColumn = true;
							var promiscall = [];

							getFormsRecordsService.getInnerTableRecord(vm.selectedTable, subId, recordId, FormName).then(function(data) {
									var rows = [];
									angular.forEach(data.rows, function(value2, key2) {
										var cells = [];
										angular.forEach(value2, function(value3, key3) {
											if (angular.isString(value3) && value3.split("_").length > 1) {
												var vall = value3.split("_");
												angular.forEach(vall, function(val4, key4) {
													var colIndex = key3.replace("column", '') - 1;
													if (key4 == 0) {
														if (addColumn) {
															columns.push({ 'Value': key3, 'Value1': value.Settings[3].PossibleValue[0].columns[colIndex].value1 })
															if (value.Settings[5].Remark > 0) {
																var cavLen = parseInt(Remark) + vall.length;
																Remark = cavLen - 1;
															}
														}
														cells.push({ 'Value': val4 })
													} else if (key4 > 0) {
														if (addColumn) {
															columns.push({ 'Value': key3 + '_' + (key4), 'Value1': value.Settings[3].PossibleValue[0].columns[colIndex].value1 + '_' + (key4), 'cavity': true })
														}
														cells.push({ 'Value': val4, 'cavity': true })
													} else {

													}
												})
											} else {
												if (addColumn) {
													var showTime = false;
													var showDate = false;
													angular.forEach(value.Settings[3].PossibleValue[0].columns, function(rVal, rKey) {
														if (key3 == rVal.value) {
															readOnly = rVal.readonly == true ? true : false;
															ColValue1 = value.Settings[3].PossibleValue[0].columns[rKey].value1;
															showTime = rVal.showTime;
															showDate = rVal.showDate;
														}
													})
													columns.push({ 'Value': key3, 'Value1': ColValue1, 'readOnly': Boolean(readOnly), 'showTime': showTime, "showDate": showDate })
												}
												cells.push({ 'Value': value3 })
											}
										});
										rows.push({ 'cells': cells, 'rowid': value2.rowid });
										addColumn = false;
									});
									tableData.push({ 'columns': columns });
									tableData.push({ 'rows': setColumnInOrder(rows, record) });
									if (vm.tableValue.length >= value.id - 1 && !(FormName.toUpperCase().includes("PACK NOTE"))) {
										vm.tableValue.splice(vm.tableValue.length - 1, 0, { 'Value': { 'table': tableData }, 'InternalName': val, 'Type': 'table', 'Restrict': Restrict, 'ColSpan': colSpan, 'Rquired': Rquired, 'Name': Name, 'ColmRestrict': value.Settings[5].ColmRestrict, 'Remark': Remark, 'Cavity': Cavity, 'ColCondValue': ColCondValue, 'colCondition': colCondition });
										if (FormName.toUpperCase().includes("CHEMICAL")) {
											settabledatechemical(vm.tableValue)
										} else if ((FormName.toUpperCase()).includes("INSPECTION REPORT") || FormName.toUpperCase().includes("SHIFT")) {
											setInspectionRepott(vm.tableValue);
										}
									} else {
										vm.tableValue.push({ 'Value': { 'table': tableData }, 'InternalName': val, 'Type': 'table', 'Restrict': Restrict, 'ColSpan': colSpan, 'Rquired': Rquired, 'Name': Name, 'ColmRestrict': value.Settings[5].ColmRestrict, 'Remark': Remark, 'Cavity': Cavity, 'ColCondValue': ColCondValue, 'colCondition': colCondition })
										if (FormName.toUpperCase().includes("CHEMICAL")) {
											settabledatechemical(vm.tableValue)
										} else if ((FormName.toUpperCase()).includes("INSPECTION REPORT") || FormName.toUpperCase().includes("SHIFT")) {
											setInspectionRepott(vm.tableValue);
										}
										if (FormName.toUpperCase().includes("PACK NOTE")) {
											settabledatepacknote(vm.tableValue)
										}

									}
									notificationService.hideLoad();
								})
								++subId;
						} else if (value.Type == 'checkbox' && record[value1.Value] != undefined) {
							vm.tableValue.push({ 'Value': record[value1.Value].length > 0 ? JSON.parse(record[value1.Value]) : record[value1.Value], 'InternalName': value1.Value, 'Type': value.Type, 'Restrict': Restrict, 'ColSpan': colSpan, 'Rquired': Rquired, 'Name': Name, 'ReadOnly': Disabled, 'FieldOption': FieldOption })
						} else if (record[value1.Value] != undefined) {
							Disabled = value.Settings[4].Options[1].Value;
							if (value.Type == 'text' && value.Settings[4].AutoFill != '' && value.Settings[4].AutoFill != undefined) {
								Disabled = value.Settings[4].Options[2].Value;
								if (value.Settings[4].AutoFill == $window.localStorage.userDept + " " + $window.localStorage.userType) {
									record[value1.Value] = $window.localStorage.empName + " " + $window.localStorage.empCode;
								} else if (value.Settings[4].AutoFill.includes('QA') && ($state.params.getFormName).toUpperCase().includes("IN-PROCESS") && $window.localStorage.userDept == 'QA') {
									record[value1.Value] = $window.localStorage.empName + " " + $window.localStorage.empCode;
								}
							}

							if (value.Type == 'dropdown') {
								var cFlag = false;
								angular.forEach(FieldOption, function(cValue, cKey) {
										if (record[value1.Value] == cValue.Text) cFlag = true;
									})
									/*if(!cFlag){
										FieldOption = [];
										FieldOption.push({"Text": record[value1.Value]})
									}*/
							}
							if (value.Type == 'date') {
								record[value1.Value] = vm.SqlDateFormate(record[value1.Value])
							}
							vm.tableValue.push({ 'Value': record[value1.Value], 'InternalName': value1.Value, 'Type': value.Type, 'Restrict': Restrict, 'ColSpan': colSpan, 'Rquired': Rquired, 'Name': Name, 'FieldOption': FieldOption, 'Cavity': Cavity, 'ReadOnly': Disabled })
						} else {

						}
					}
				});
			});

			if (vm.tableRowId == 0) {
				vm.tableRowId = record.rowid;
				vm.tableRowName = 'rowid';

				notificationService.hideLoad();
			}
			vm.formRecord = vm.formFields;

		}
	}

	function settabledatepacknote(value) {
		var promiscall = [];
		var rows = [];
		var tablename = '';
		if ($window.localStorage.userDept == 'QA' && $window.localStorage.userType == 'Operator' && $state.params.type == '') {
			angular.forEach(value, function(val, key) {
				if (val.InternalName == "partno") {
					var query = "select * from DF_TBL_PGR_4108 where partno='" + val.Value + "'";
					constantService.selectedTableName = "DF_TBL_PGR_4108";
					getFormsService.getTableRecordBasedOnQuery(query).then(function(resul2) {
						if (resul2.data.length > 0) {
							angular.forEach(value, function(vall, keyl) {
								if (vall.Type == 'table' && resul2.data[resul2.data.length - 1][vall.InternalName] != null) {
									var query = "select * from DF_TBL_PGR_TBL_4108_0 where tableid='" + resul2.data[resul2.data.length - 1][vall.InternalName] + "'  order by column1 ASC";
									/*promiscall.push(*/
									getFormsService.getTableRecordBasedOnQuery(query).then(function(resul1) {
											if (resul1.data.length > 0) {
												angular.forEach(resul1.data, function(value1, keyy1) {
													var cells = [];
													angular.forEach(vall.Value.table[1].rows[0].cells, function(val3, key3) {
														if (key3 > 6 && keyy1 == 0) {
															var aa = parseInt(key3) - 2;
															var bb = 'column' + aa;
															//cells[key3].Value = value1[bb];
															cells.push({ "Value": value1[bb], 'Edited': true });
														} else if (key3 > 6 && keyy1 > 0) {
															var aa = parseInt(key3) - 2;
															var bb = 'column' + aa;
															//val3.Value = value1[bb];
															//cells[key3].Value = value1[bb]
															cells.push({ "Value": value1[bb], 'Edited': true, 'NewAdded': true });
														} else {

															if (vall.Value.table[0].columns[key3].Value1 == 'Sl No') {
																val3.Value = parseInt(keyy1) + 1;
																cells.push({ "Value": val3.Value });
															} else {
																cells.push(val3);
															}

														}

														if (key3 == vall.Value.table[1].rows[0].cells.length - 1) {
															rows.push({ "cells": cells });
														}
														if (key3 == vall.Value.table[1].rows[0].cells.length - 1 && keyy1 == resul1.data.length - 1) {
															/*vall.Value.table[1].rows.push({"cells":cells});*/
															vall.Value.table[1].rows = [];
															angular.forEach(rows, function(val, key) {
																vall.Value.table[1].rows.push(val)
															})
														}
													});
												})
											}
										})
										/*)*/
								}
							});

						}

					});

				}
			})
		}
	}

	function setInspectionRepott(value) {
		angular.forEach(value, function(value1, key1) {
			if (value1.Type == 'table') {
				angular.forEach(value1.Value.table[0].columns, function(value2, key2) {
					if (value2.Value == 'column1') {
						value2.showColumn = true;
					} else {
						value2.showColumn = false;
					}
				});
			}
		})
	}

	function settabledatechemical(value) {
		var selectedProcess = ''
		angular.forEach(value, function(val, key) {
			if (val.Name == "Process") {
				selectedProcess = val.Value
			}
		})
		switch (selectedProcess) {
			case 'Die Coat Chemical Mixing':
				angular.forEach(value, function(value1, key1) {
					if (value1.Type == 'table') {
						angular.forEach(value1.Value.table[0].columns, function(value2, key2) {
							if (key2 > 2 && key2 < 6) {
								value2.showColumn = false;
							} else {
								value2.showColumn = true;
							}
						});
					}
				})
				break;
			case 'Gum Mixing':
				angular.forEach(value, function(value1, key1) {
					if (value1.Type == 'table') {
						angular.forEach(value1.Value.table[0].columns, function(value2, key2) {
							if (key2 > 2 && key2 < 6) {
								value2.showColumn = true;
							} else {
								value2.showColumn = false;
							}
						});
					}
				})
				break;

			case 'Cork Mixing':
				angular.forEach(value, function(value1, key1) {
					if (value1.Type == 'table') {
						angular.forEach(value1.Value.table[0].columns, function(value2, key2) {
							if (key2 > 2 && key2 < 6) {
								value2.showColumn = true;
							} else {
								value2.showColumn = false;
							}
						});
					}
				})
				break;
			default:
		}
	}

	function getTableDetails(taleId) {
		notificationService.showSpinner();
		vm.formFields = [];
		createFormsService.getTableByName(taleId).then(function(record) {
			if ($state.current.name == 'getFormRecords') {
				vm.formName = record.rows[0].tableName;
				console.log('is form vm.formName', vm.formName);
			}
			angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
				addElement(value);
			});
			notificationService.hideLoad();
		});
	}

	function setColumnInOrder(rows, record) {
		var flag = false;
		var rowww = [];
		if (record.shift != undefined && rows.length > 6) {
			angular.forEach(constantService[record.shift], function(value, key) {
				angular.forEach(rows, function(value1, key1) {
					console.log(value, (value1.cells[3].Value))
					if (value == value1.cells[3].Value) {
						console.log(value1)
						rowww.push(value1);
					}
				});
				if (key == constantService[record.shift].length - 1 && rowww.length == rows.length) { flag = true; }
			});
		}
		if (flag) {
			return rowww;
		} else {
			return rows;
		}
	}

	function createNewField() {
		return {
			'id': ++guid,
			'Name': '',
			'Settings': [],
			'Active': true,
			'ChangeFieldSetting': function(Value, SettingName) {
				switch (SettingName) {
					case 'Field Label':
						{
							vm.current_field.Name = Value;
							vm.current_field.Settings[0].Value = $scope.current_field.Name;
							break;
						}
					case 'Internal Name':
						{
							vm.current_field.Settings[2].Value = Value.replace(/\s/g, '_');
							break;
						}

					default:
						break;
				}
			},
			'GetFieldSetting': function(settingName) {
				var result = {};
				var settings = this.Settings;
				$.each(settings, function(index, set) {
					if (set.Name == settingName) {
						result = set;
						return;
					}
				});
				if (!Object.keys(result).length) {
					//Continue to search settings in the checkbox zone
					$.each(settings[settings.length - 1].Options, function(index, set) {
						if (set.Name == settingName) {
							result = set;
							return;
						}
					});
				}
				return result;
			}
		};
	}

	function addElement(ele, idx) {
		vm.current_field.Active = false;
		vm.current_field = createNewField();
		//Merge setting from template object
		angular.merge(vm.current_field, ele);

		if (typeof idx == 'undefined') {
			vm.formFields.push(vm.current_field);
		} else {
			vm.formFields.splice(idx, 0, vm.current_field);
			//$('#fieldSettingTab_lnk').tab('show');
		}
	};

	function updateRecord() {
		$window.localStorage.approved = vm.approved;
		console.log('vm.approved', vm.approved);
		var updateString = '';
		if (vm.approved == 'return') vm.moveToDept = vm.returnDept[0];
		if (vm.approved == 'transfer') vm.moveToDept = vm.deptTransform;
		// if (vm.approved == 'approved' && $window.localStorage.userType == 'Approver' && vm.department == "Production" && FormName.toUpperCase().includes("MOLD CHANGE CHECK SHEET"))
		// 	vm.approved = 'done';
		if (vm.setMoveToDept == 'done' && vm.approved == 'approved' && $window.localStorage.userType != 'Operator') {
			vm.moveToDept == 'done'
			vm.approved = 'done';
		}
		if (vm.approved == 'approved' && $window.localStorage.userType == 'Approver' && vm.department == "Production" && FormName.toUpperCase().includes("Daily Operator Work Allotment")) {
			vm.approved = 'done';
		}
		if (vm.moveToDept == undefined && vm.approved == 'return' && vm.department == "Engineering") {
			vm.moveToDept = "Engineering"
		} else if (vm.approved == 'approved' && vm.department == "Engineering" && $window.localStorage.userType == 'Operator') {
			vm.moveToDept = "Engineering"
		} else if (vm.moveToDept == undefined && vm.approved == 'return' && vm.department == "QA") {
			vm.moveToDept = 'QA'
		} else if (vm.moveToDept == undefined && vm.approved == 'return' && vm.department == "Production") {
			vm.moveToDept = 'Production'
		}
		if (vm.department == 'QA' && vm.approved == 'approved') {
			if (FormName.toUpperCase().includes("IN-PROCESS") || FormName.toUpperCase().includes("LEAK") || FormName.toUpperCase().includes("CHEMICAL") || FormName.toUpperCase().includes("MATERIAL PRE") || FormName.toUpperCase().includes("PACK NOTE")) {
				vm.approved = 'done';
			}
		}
		// if (vm.department == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Approver' && FormName.toUpperCase().includes('IN-PROCESS SET UP APPROVAL')) {
		// 	vm.approved = 'done';
		// }
		// if (vm.department == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Approver' && FormName.toUpperCase().includes('LEAK TESTING CUM GUMMING APPLICATION RECORD')) {
		// 	vm.moveToDept = 'QA';
		// }
		if (vm.department == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Operator') {
			if (FormName.toUpperCase().includes("MATERIAL PRE")) {
				vm.moveToDept = 'Production';
			}
		}

		if (vm.department == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Approver') {
			if (FormName.toUpperCase().includes("MOLD CHANGE CHECK SHEET")) {
				vm.moveToDept = 'Mold Maintainance';
				vm.approved = "approved";
				console.log('Mold Maintainance');
			}
		}
		if (vm.department == 'Mold Maintenance' && vm.approved == 'approved') {
			if (FormName.toUpperCase().includes("MOLD")) {
				vm.approved = 'done';
				vm.moveToDept = 'Production';
			}
		}
		if (FormName.toUpperCase().includes("CHEMICAL") && vm.approved == 'approved' && $window.localStorage.userType == 'Operator') {
			vm.moveToDept = 'Production'
		}
		//consider as updating record 
		if (vm.moveToDept == undefined && vm.comment == undefined) {
			vm.comment = 'Updating Record';
			vm.moveToDept = vm.department;
		}
		if (vm.moveToDept == undefined && vm.approved == 'approved' && $window.localStorage.userType == 'Operator') {
			vm.moveToDept = vm.department;
		}

		// if ((vm.approved == 'return'||vm.approved == 'reject') && $window.localStorage.userType == 'Approver' && vm.department == "Production" && FormName.toUpperCase().includes("Daily Operator Work Allotment")) {
		// 	if(vm.approved == 'return'){vm.approved = 'return';}else vm.approved = 'reject';
		// }
		console.log("vm.approved upadted chec", vm.approved);

		if (vm.approved != undefined) {
			notificationService.showSpinner();
			console.log('vm.selectedTable', vm.selectedTable);
			console.log('vm.tableRowId', vm.tableRowId);
			console.log('vm.tableValue', vm.tableValue);
			console.log('vm.approved', vm.approved);
			console.log('vm.selectedTableShow', vm.selectedTableShow);
			getFormsRecordsService.updateRecord(vm.selectedTable, vm.tableRowId, vm.tableRowName, vm.tableValue, vm.approved, vm.selectedTableShow, vm.updateDept, vm.moveToDept, vm.rejectToDept).then(function(record) {

				getFormsRecordsService.updateAprrovalProcess(vm.formLevelInnumber, vm.selectedTable, vm.approved, vm.comment, vm.tableRowId).then(function(data) {

					getFormsRecordsService.getApproverDetails(vm.selectedTable, vm.tableRowId).then(function(data) {

						vm.serviceMailInformation = {
							serviceRecordId: vm.tableRowId,
							serviceFirstPerson: data.firstPerson,
							serviceSecondPerson: data.secondPerson,
							serviceThirdPerson: data.thirdPerson
						};
						recordDetailsService.getStatusInformation(vm.selectedTable, vm.tableRowId).then(function(response) {
							SendEmailService.sendmailNotifiction(vm.serviceMailInformation, response);
						});
					});
					notificationService.hideLoad();
					notificationService.alert('', 'Form Updated Successfully', function() {
						$state.reload();
					});
					if (vm.approved != '' && vm.approved != undefined) {
						vm.TableRecords.splice(vm.presentRecorIndex, 1);
						vm.tableValue = [];
						vm.showRecordDetails = false;
					}
					/*vm.approved = '';*/
				});
			});
		} else {
			notificationService.hideLoad();
			notificationService.alert('', 'Select any one approval');
		}
	}

	function valuationDatePickerOpen($event) {
		if ($event) {
			$event.preventDefault();
			$event.stopPropagation(); // This is the magic
		}
		this.valuationDatePickerIsOpen = true;
	};

	$scope.openMenu = function() {
		$ionicSideMenuDelegate.toggleLeft();
	};

	function checkColrestrict(colm, data, index) {
		var ret = true;
		angular.forEach(data, function(val, key) {
			if (colm == val.column) {
				if (val.depts.length == 0) {
					ret = true;
				} else if (val.depts.indexOf(vm.department) == -1) {
					if (vm.hideIndex.indexOf(index)) {
						vm.hideIndex.push(index)
					}
					ret = false;
				} else {
					ret = true;
				}
			}
		});
		return ret;
	}

	function cancel() {
		notificationService.confirm('Are you sure want to cancel', 'Yes', 'No', function() {
			$state.reload();
		}, function() {})
	}

	function ColumanCondition(col, celll, tablejson, cel) {
		/*console.log('cel1111 ', cel)
	    console.log('col ', col)
	    console.log('celll ', celll)
	    console.log('tablejson ', tablejson)*/


		setEditTrue(cel)
		var colIndex = tablejson.ColCondValue.resultCol
		var flag = false;
		if (tablejson.colCondition || cel.cavity) {
			var aa = tablejson.ColCondValue.Condtion;
			var index = '';
			if (cel.cavity) {
				index = 10;
			} else {
				if (col.includes('_')) col = col.split("_")[0];
				index = aa.indexOf(col);
			}

			if (index >= 0 && aa.length > 3) {
				var a = celll[parseInt(aa[0].replace("column", '')) + 2].Value;
				var b = celll[parseInt(aa[2].replace("column", '')) + 2].Value;
				var c = '';
				if (cel.cavity) {
					c = cel.Value;
				} else {
					c = celll[parseInt(aa[4].replace("column", '')) + 2].Value;
				}
				var TableCavityCount = 1;
				angular.forEach(tablejson.Value.table[0].columns, function(cellvalue, cellKey) {
					if (cellvalue.cavity) TableCavityCount++;
				})
				if (aa[1] == "+/-") {
					if (TableCavityCount == 1) {
						var count = 0;
						celll[parseInt(tablejson.Remark) + 3]['Edited'] = true;
						angular.forEach(a.split("/"), function(value, key) {
							if (b.split("/").length > 1) {
								if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b.split("/")[1])).toFixed(2) && parseFloat(c.split("/")[key]) >= (parseFloat(value) + parseFloat(b.split("/")[0])).toFixed(2)) {
									count++;
								} else {
									celll[parseInt(tablejson.Remark) + 3].Value = false
								}
							} else {
								if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b)).toFixed(2) && parseFloat(c.split("/")[key]) >= (value - b).toFixed(2)) {
									count++;
								} else {
									celll[parseInt(tablejson.Remark) + 3].Value = false
								}
							}
						})
						if (c.split("/").length > a.split("/").length) {
							celll[parseInt(tablejson.Remark) + 3].Value = false;
						} else if (count == a.split("/").length) {
							celll[parseInt(tablejson.Remark) + 3].Value = true
						} else if (b.toUpperCase() == 'REF') {
							celll[parseInt(tablejson.Remark) + 3].Value = true
						} else {
							celll[parseInt(tablejson.Remark) + 3].Value = false
						}
					} else if (TableCavityCount > 1) {
						var cavityStartNumber = 0;
						var flagg = true;
						var count = 0;
						var noofcavityColumn = 0;
						angular.forEach(tablejson.Value.table[0].columns, function(cellvalue, cellKey) {
							if (parseInt(aa[4].replace("column", '')) + 2 == cellKey || cellvalue.cavity) {
								if (flagg) {
									cavityStartNumber = cellvalue.Value.replace("column", '') - 1;
									flagg = false;
								}
								c = celll[parseInt(cellKey)].Value;
								cavityStartNumber++;
								angular.forEach(a.split("/"), function(value, key) {
									if (b.split("/").length > 1) {
										if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b.split("/")[1])).toFixed(2) && parseFloat(c.split("/")[key]) >= (parseFloat(value) + parseFloat(b.split("/")[0])).toFixed(2)) {
											count++;
										} else {
											celll[parseInt(tablejson.Remark) + 3].Value = false
										}
									} else {
										if (parseFloat(c.split("/")[key]) <= parseFloat((parseFloat(value) + parseFloat(b)).toFixed(2)) && parseFloat(c.split("/")[key]) >= parseFloat((value - b).toFixed(2))) {
											count++;
										} else {
											celll[parseInt(tablejson.Remark) + 3].Value = false
										}
									}
								})
								noofcavityColumn++;

							}

							var qq = a.split('/').length
							if (cellKey == tablejson.Value.table[0].columns.length - 1 && count / (a.split('/').length) == noofcavityColumn) {
								celll[parseInt(tablejson.Remark) + 3].Value = true
							} else if (b.toUpperCase() == 'REF') {
								celll[parseInt(tablejson.Remark) + 3].Value = true
							} else {
								celll[parseInt(tablejson.Remark) + 3].Value = false
							}
						})
					}

				} else if (aa[1] == "+") {
					if ((a - b) == c) {
						celll[parseInt(tablejson.Remark) + 3].Value = true
					} else {
						celll[parseInt(tablejson.Remark) + 3].Value = false
					}
				} else if (aa[1] == "-") {
					if ((parseInt(a) + parseInt(b) == c)) celll[tablejson.Remark].Value = true
					else celll[parseInt(tablejson.Remark) + 3].Value = false
				} else {

				}
			} else {
				var a = celll[aa[0].replace("column", '') - 1].value;
				var b = celll[aa[2].replace("column", '') - 1].value;

				var time1 = (a.split(":")[0] * 60 * 60) + (a.split(":")[1] * 60);
				var time2 = (b.split(":")[0] * 60 * 60) + (b.split(":")[1] * 60);
				if (time1 >= 0 && time2 >= 0) {
					var settime = (time2 - time1) / (60 * 60);
					var timemin = (settime.toString().split(".")[1]) != undefined ? (settime.toString().split(".")[1]) * (60) / 100 : '00'
					var finalTime = settime.toString().split(".")[0] + " " + timemin;
					celll[colIndex.replace("column", '') - 1].value = finalTime;
					celll[colIndex.replace("column", '') - 1]['Edited'] = true;
				}
			}
		}
	}

	function getValueDynamicaly(tablecol, tableRecord, recorvalue) {

		console.log('tablecol', tablecol);
		console.log('tableRecord', tableRecord);
		console.log('recorvalue', recorvalue);
		if (FormName.toUpperCase().includes('LEAK TESTING')) {
			angular.forEach(tableRecord, function(value, key) {
				console.log('value getValueDynamicaly', value);
				if (value.Type == 'table') {
					var aaa = value.InternalName;
					var bbb = tablecol.Value;
					if (aaa.includes((bbb.split(" ")[0]).toLowerCase())) {
						value['Restrict'] = true;
					} else {
						value.Restrict = false;
					}
					value['InternalName'] = aaa;
				}
			})
		}
	}

	function dateConvert(istdate) {
		var date = new Date(istdate);
		var newDate = (date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear());
		return newDate;
	}

	function SqlDateFormate(date) {
		var date1 = '';
		if (date.split(' ')[0].split('/').length > 1) {
			var val = date.split(' ')[0].split('/')
			date1 = val[1] + '-' + val[0] + '-' + val[2]
		} else {
			var val = date.split(' ')[0].split('-')
			date1 = val[0] + '-' + val[1] + '-' + val[2]
		}
		return date1;
	}

	function getRecordTrack() {
		if (vm.showtable) {
			vm.showtable = false;
			vm.flag = false;
		} else {
			vm.flag = true;
			vm.showtable = true;
		}
		if (vm.flag) {
			vm.allDetails = []
			trackRecordService.getRecordDetail(vm.selectedTable, vm.rowData.recordName.split('-')[vm.rowData.recordName.split('-').length - 1]).then(function(response) {
				angular.forEach(response.data, function(value, key) {
						vm.allDetails.push(value);
						vm.showtable = true;
					})
					/*if(vm.allDetails.length > 1){
						vm.showtable = true;
						vm.flag = false;
					}else{
						vm.showtable = false;
					}*/
			});
		}
	}

	function addRow(value) {

		var row = {
				cells: []
			},
			colLen = value.Value.table[0].columns.length;
		for (var i = 0; i < colLen; i++) {
			row.cells.push({
				value: ''
			});
		}
		value.Value.table[1].rows.push(row);
	}


	function getCodeNameOfRecord(recordName) {
		console.log("recordName.slice(0,recordName.indexOf('2017-')+('2017-').length)", recordName.slice(0, recordName.indexOf('2017-') + ('2017-').length));
		return recordName.slice(0, recordName.indexOf('2017-') + ('2017-').length);
	}
}

angular.module('amaraja').controller('getFormsController', getFormsController);

getFormsController.$inject = ['$scope', '$rootScope', '$location', '$q', '$state', '$http', '$window', 'createFormsService', 'getFormsService', 'notificationService', '$ionicSideMenuDelegate', 'constantService', '$timeout', 'ionicTimePicker', 'ionicDatePicker', 'SendEmailService', 'getFormsRecordsService', 'recordDetailsService'];

function getFormsController($scope, $rootScope, $location, $q, $state, $http, $window, createFormsService, getFormsService, notificationService, $ionicSideMenuDelegate, constantService, $timeout, ionicTimePicker, ionicDatePicker, SendEmailService, getFormsRecordsService, recordDetailsService) {
  var vm = this;
  vm.getTableJSON = getTableJSON;
  vm.getTableByName = getTableByName;
  vm.saveFormsData = saveFormsData;
  vm.valuationDatePickerOpen = valuationDatePickerOpen;
  vm.getValueDynamicaly = getValueDynamicaly;
  vm.dynamicLoad = dynamicLoad;
  vm.ColumanCondition = ColumanCondition;
  vm.getTime = getTime;
  vm.getDate = getDate;
  vm.checkDept = checkDept;
  vm.cancel = cancel;
  vm.dynamicColChange = dynamicColChange;
  // vm.sendmailNotifiction = sendmailNotifiction;
  vm.removeRow = removeRow;
  vm.logoShow = false;
  vm.hideButtons = false;
  vm.tableName = [];
  vm.recordName = ''
  vm.tableId = '';
  vm.tableRecord = '';
  vm.TableJson = ''
  vm.record = '';
  vm.returnSelectedId = '';
  vm.serviceMailInformation = '';
  vm.showError = false;
  vm.deptpartment = $window.localStorage.userDept;
  vm.showComment = false;
  vm.showOption = false;
  vm.SqlDateFormate = SqlDateFormate;
  vm.getRecordBasedOnSelectedId = getRecordBasedOnSelectedId;
  vm.notifid = 0;
  vm.isFlamabityOrInspection = false;

  vm.userDept = $window.localStorage.userDept;
  vm.userRole = $window.localStorage.userType;
  if ($state.params.getFormName == "Flammability Report" || $state.params.getFormName == "Inspection Report"|| $state.params.getFormName == "Engg Process Specification for IMM")
    vm.isFlamabityOrInspection = true;


  function getTime(cell, col, celll, tablejson) {
    var timmm = ''
    var ipObj1 = {
      callback: function(val) { //Mandatory
        if (typeof(val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);

          timmm = selectedTime.getUTCHours();
          console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
          if (tablejson != null) {
            cell.value = selectedTime.getUTCHours() + " : " + selectedTime.getUTCMinutes()
            vm.ColumanCondition(col, celll, tablejson);
          } else {
            vm.record[cell] = selectedTime.getUTCHours() + " : " + selectedTime.getUTCMinutes()
          }
        }
      },
      inputTime: 12 * 60 * 60, //Optional
      format: 12, //Optional
      step: 1, //Optional
      setLabel: 'Ok' //Optional
    };
    ionicTimePicker.openTimePicker(ipObj1);
  }


  function getDate(cell, col, celll, tablejson) {
    var ipObj2 = {
      callback: function(val) { //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        if (celll == 'selectdate') {
          vm.record[cell] = dateConvert(new Date(val));
        } else {
          cell.value = dateConvert(new Date(val));
          vm.ColumanCondition(col, celll, tablejson);
        }
      },
      /*disabledDates: [            //Optional
        new Date(2016, 2, 16),
        new Date(2015, 3, 16),
        new Date(2015, 4, 16),
        new Date(2015, 5, 16),
        new Date('Wednesday, August 12, 2015'),
        new Date("08-16-2016"),
        new Date(1439676000000)
      ],*/
      from: new Date(2000, 1, 1), //Optional
      to: new Date(2200, 10, 30), //Optional
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      disableWeekdays: [0], //Optional
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };
    ionicDatePicker.openDatePicker(ipObj2);
  }


  $scope.$watch(function() {
    return $state.params;
  });
  FormName = $state.params.getFormName;
  getTableJSON();

  function getTableJSON() {
    if ($window.localStorage.userDept == '' || $window.localStorage.userDept == undefined)
      $state.go('login');

    vm.tableName = [];
    createFormsService.getTableJSON().then(function(data) {
      angular.forEach(data.rows, function(value, key) {
        vm.tableName.push(value);
        if (value.tableName == FormName) {
          getTableByName(value.rowid);
        }
      });
    });
  }

  function getTableByName(id) {
    vm.selectedTable = id;
    var promiscall = [];
    var deferred = $q.defer();
    $('#border_getform').addClass('border-getForm');
    vm.logoShow = true;
    vm.showOption = true;
    vm.hideButtons = true;
    for (var i = 0; i < vm.tableName.length; i++) {
      if (vm.selectedTable == vm.tableName[i].rowid) {
        vm.selectedTableShow = vm.tableName[i].tableName;
        var matches = vm.selectedTableShow.match(/\b(\w)/g);
        vm.recordName = matches.join('') + '-2017-';
      }
    }

    createFormsService.getTableByName(vm.selectedTable).then(function(record) {
      if (record.rows[0].tableRecord != '') {
        vm.tableColName = [];
        var tableData = [];
        var PossibleValue = [];
        var promiseColl = [];
        var data = {};
        var ColCondValue = "";
        var autofillUser = ''
        vm.TableJson = JSON.parse(record.rows[0].tableRecord).data;

        promiscall.push(
          angular.forEach(JSON.parse(record.rows[0].tableRecord).data, function(value, key) {
            data.Name = value.Name;
            data.Type = value.Type;
            if (value.setTime) {
              data.setTime = value.setTime;
            }
            angular.forEach(value.Settings, function(value1, key1) {
              if (value1.Name == "Choice") {
                if (value1.DynamicTableRef) {
                  data.DynamicTableRef = value1.DynamicTableRef;
                  data.DynaicRef = value1.DynaicRef;
                  data.FieldOption = [];
                } else {
                  if (value1.MasterData != undefined && value1.MasterData.length > 0 && value1.MasterData[0].TableName == 'DF_MSTR_USERS') {
                    angular.forEach(value1.PossibleValue, function(value2, key2) {
                      PossibleValue.push({
                        "Text": value2.Text,
                        "Text1": value2.Text1
                      });
                    });
                  } else {
                    angular.forEach(value1.PossibleValue, function(value2, key2) {
                      PossibleValue.push({
                        "Text": value2.Text
                      });
                    });
                  }
                  data.FieldOption = PossibleValue;
                }
              } else if (value1.Name == "General Options") {
                if (value1.Remark != '' && value1.Remark != undefined) data.Remark = value1.Remark;
                else value1.Remark = 'false'
                if (value.Type == 'dropdown' && value1.Cavity != '') {
                  data.Cavity = value1.Cavity;
                }

                if (value1.colCondition) {
                  data.colCondition = value1.colCondition;
                  data.ColCondValue = value1.ColCondValue;
                }
                angular.forEach(value1.Options, function(value2, key2) {
                  if (value2.Name == "Required") {
                    data.Required = value2.Value;
                  }
                  if (value2.Name == "Readonly") {
                    data.ReadOnly = value2.Value;
                  }

                });
                data.Restrict = [];
                angular.forEach(value1.Restrict, function(value2, key2) {
                  if (value2) {
                    data.Restrict.push(key2);
                  }
                });
                if (value.Type == 'text' && value1.AutoFill != '' && value1.AutoFill != undefined) {
                  if (value1.AutoFill == $window.localStorage.userDept + " " + $window.localStorage.userType) {
                    data.value = $window.localStorage.empCode + " " + $window.localStorage.empName;
                  } else if (value1.AutoFill.includes('QA') && FormName.toUpperCase().includes("IN-PROCESS") && $window.localStorage.userDept == 'QA') {
                    data.value = $window.localStorage.empCode + " " + $window.localStorage.empName;
                  }
                }

              } else if (value1.Name == "Internal Name") {
                vm.tableColName.push(value1.Value);
                data.InternalName = value1.Value;
                if (value1.Value == 'specno' && value.Type == 'text') {
                  data.value = 'PLD-PRS-';
                } else if (value1.Value == 'applicablespecification') {
                  data.value = 'MPPW-PLS-TSP-004 (UL94)';
                } else if (value1.Value == 'fromcustomer' && value.Type == 'text') {
                  data.value = 'MIL-2 PLD';
                }
              } else if (value1.Name == "Column Span") {
                data.ColSpan = value1.Value;
              } else if (value1.Name == "Base64") {
                data.base64 = value1.Value;
              } else if (value1.Name == "Table") {
                data.Table = value1.PossibleValue[0];
                vm.valuationDate = new Date();
                vm.valuationDatePickerIsOpen = false;
                vm.opens = [];

                $scope.$watch(function() {
                  return vm.valuationDatePickerIsOpen;
                }, function(value) {
                  vm.opens.push("valuationDatePickerIsOpen: " + value + " at: " + new Date());
                });
              }
            })

            tableData.push(data);
            PossibleValue = [];
            data = {};
          })
        )
        $q.all(promiscall).then(function(datavalue) {
          vm.tableRecord = tableData;
          dynamicLoad(vm.tableRecord);
          vm.showComment = true;
          vm.getRecordBasedOnSelectedId($state.params);

        })
      }
    });

  }

  function settableLeakTesting() {
    if (FormName.toUpperCase().includes('LEAK TESTING') && $state.params.type.toUpperCase() == "RETURN") {
      angular.forEach(vm.tableRecord, function(value, key) {
        if (value.InternalName == 'process') {
          getValueDynamicaly(value, vm.tableRecord, vm.record, vm.record['process'])
        } else if (value.Type == 'table') {
          setColumnInOrder(value.Table.rows, vm.record, value.InternalName)
        }

      });
    }
    if (FormName.toUpperCase().includes('DAILY') && $state.params.type.toUpperCase() == "RETURN") {
      angular.forEach(vm.tableRecord, function(value, key) {
        if (value.InternalName == 'machine') {
          getValueDynamicaly(value, vm.tableRecord, vm.record, vm.record['machine'])
        } else if (value.Type == 'table') {

          setColumnInOrder(value.Table.rows, vm.record, value.InternalName)
        }
      });
    }
  }

  function saveFormsData(record) {
    console.log('record saave', record);
    if (record.recordName != undefined) {
      var deptName = vm.deptpartment;
      console.log("===========================", FormName.toUpperCase().includes("LEAK TEST"), $window.localStorage.userDept, vm.approved, $window.localStorage.userType)
      if (FormName.toUpperCase().includes("LEAK TEST") && $window.localStorage.userDept == 'Production' && vm.approved == 'approved' && $window.localStorage.userType == 'Approver') {
        deptName = 'QA';
      }
      //updating record preeating record
      var flag = true;
      var tabcount = 0;
      console.log('enter save formsdata');
      angular.forEach(vm.tableRecord, function(val, key) {
        if (val.Type == 'table') {
          angular.forEach(record[val.InternalName].rows, function(val1, key1) {
            var col = '';
            var insertCol = "'" + vm.subtableId + "',";
            angular.forEach(val1.cells, function(val2, key2) {
              col = col + 'column' + (parseInt(key2) + parseInt(1)) + "='" + val2.value + "',";
              insertCol = insertCol + "'" + val2.value + "',"
              if (key2 == val1.cells.length - 1) {
                var query = "UPDATE " + constantService.innerTableName + vm.selectedTable + '_' + tabcount + " SET " + col.trim().slice(0, -1) + " WHERE tableid='" + vm.subtableId + "' and column1='" + val1.cells[0].value + "'";
                if (val1.cells[0].value != undefined) {
                  if (tabcount == '1') {
                    if (flag) {
                      flag = false;
                      var query4 = "delete from " + constantService.innerTableName + vm.selectedTable + '_1' + " WHERE tableid='" + vm.subtableId + "' and column1=''";
                      getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {});
                    }
                    var query1 = "Select * From " + constantService.innerTableName + vm.selectedTable + '_1' + " WHERE tableid='" + vm.subtableId + "' and column1='" + val1.cells[0].value + "'";
                    getFormsRecordsService.getCompletedRecordinOneLevel(query1).then(function(reqVal1) {
                      if (reqVal1.data.length > 0) {
                        var query3 = "UPDATE " + constantService.innerTableName + vm.selectedTable + '_1' + " SET " + col.trim().slice(0, -1) + " WHERE tableid='" + vm.subtableId + "' and column1='" + val1.cells[0].value + "'";
                        getFormsRecordsService.getCompletedRecordinOneLevel(query3).then(function(reqVal) {});
                      } else {
                        var query2 = "insert into  " + constantService.innerTableName + vm.selectedTable + '_1' + " values (" + insertCol.trim().slice(0, -1) + ")";
                        getFormsRecordsService.getCompletedRecordinOneLevel(query2).then(function(reqVal) {});

                      }
                    });
                  } else {
                    getFormsRecordsService.getCompletedRecordinOneLevel(query).then(function(reqVal) {});
                  }

                  //var query = "insert into DF_TBL_APR (approved, tableName,recordId,deptName, formName,updatedBy,status) values ('"+vm.approved+"', '"+vm.selectedTable+"', '"+''+"', '"+vm.deptpartment+"','"+FormName+"','"+ vm.deptpartment+"', 'a' )";
                  var query = "update DF_TBL_APR set approved='" + vm.approved + "', deptName='" + deptName + "' where formName = '" + FormName + "' and recordId ='" + vm.returnSelectedId + "'";
                  console.log("query ", query)
                  getFormsService.updateApprovalTable(query).then(function(reqData) {})

                  if (vm.comment != '' && vm.comment != undefined) {
                    var query = "INSERT INTO DF_TBL_FMH (userName , userId , role , formLevel , formName, status, comment ,recordId) VALUES ('" + $window.localStorage.empName + "','" + $window.localStorage.empCode + "','" + $window.localStorage.userDept + "','1','" + vm.selectedTable + "','" + vm.approved + "','" + vm.comment + "','" + '' + "')"
                    console.log("query ", query)
                    getFormsService.updateApprovalTable(query).then(function(reqData) {})
                  }
                }
              }
            });
          });


          tabcount++;
        }
      })
      notificationService.hideLoad();
      notificationService.alert('', 'Data insertd Successfully', function() {
        console.log("Notification 1:", record);
        getFormsRecordsService.getApproverDetails(vm.selectedTable, vm.returnSelectedId).then(function(data) {
          vm.serviceMailInformation = {
            serviceRecordId: vm.returnSelectedId,
            serviceFirstPerson: data.firstPerson,
            serviceSecondPerson: data.secondPerson,
            serviceThirdPerson: data.thirdPerson
          };
          recordDetailsService.getStatusInformation(vm.selectedTable, vm.tableRowId).then(function(response) {
            SendEmailService.sendmailNotifiction(vm.serviceMailInformation, response);
          });
        });
        // sendmailNotifiction(record);
        $rootScope.$emit("CallParentMethod", {});
      });

    } else {

      console.log('enter save formsdata 2');
      vm.showError = false;
      angular.forEach(vm.tableRecord, function(valValue, valKey) {
        if (valValue.Name.includes('Supervisor')) {
          console.log("Notification 2:", "DF_TBL_PGR_" + vm.selectedTable);
          constantService.selectedTableName = "DF_TBL_PGR_" + vm.selectedTable;
          vm.notifid = 2;
          // sendmailNotifiction(record[valValue.InternalName])
        }
        if (valValue.Required == true) {
          if (record[valValue.InternalName] == undefined || record[valValue.InternalName] == '') {
            vm.showError = true;
            return false;
          }
        }
      })

      if (!vm.showError) {
        var promiscall = [];
        var deferred = $q.defer();
        var colums = '';
        var tableValues = '';
        var InnertableData = [];
        var tableCount = 0;
        notificationService.showSpinner();
        angular.forEach(vm.tableColName, function(value, key) {
          if (record[value] == undefined) {
            tableValues = tableValues + " " + "''" + ",";
            colums = colums + " " + value + ",";
          } else {
            angular.forEach(vm.tableRecord, function(value1, key1) {
              if (value1.InternalName == value) {
                if (value1.Type == "table") {
                  InnertableData.push(value1);
                  promiscall.push(
                    getFormsService.getTableRecordCount(vm.selectedTable).then(function(rescount) {
                      if (rescount.rows.length > 0) {
                        colums = colums + " " + value + ",";
                        tableValues = tableValues + " " + (parseInt(rescount.rows[0]['dbcount']) + 1) + ",";
                        tableCount = parseInt(rescount.rows[0]['dbcount']) + 1;
                        vm.tableId = parseInt(rescount.rows[0]['dbcount']) + 1;
                      }
                    })
                  )
                } else if (value1.Type == "checkbox") {
                  tableValues = tableValues + " " + "'" + JSON.stringify(record[value]) + "',";
                  colums = colums + " " + value + ",";
                } else if (value1.Type == "date") {
                  tableValues = tableValues + " " + "'" + DBdateConvert(record[value]) + "',";
                  colums = colums + " " + value + ",";
                } else {
                  tableValues = tableValues + " '" + record[value] + "',";
                  colums = colums + " " + value + ",";
                }
              }
            });
          }
        })
        $q.all(promiscall).then(function(datavalue) {
          colums = "recordName, " + colums;
          tableValues = "'" + vm.recordName + tableCount + "'," + tableValues;
          $rootScope.codeNameOfRecord = vm.recordName;
          var recordIdForMailService = '';
          // adding data to respective form tables ex. _2096
          getFormsService.addTableRecord(vm.selectedTable, colums.trim().slice(0, -1), tableValues.trim().slice(0, -1)).then(function(data) {
            recordIdForMailService = data.data.CreateDynamicTableResult[0].rowid;
            console.log('data recordIdForMailService',recordIdForMailService);
            getFormsService.getWorkFlowLevel(vm.selectedTable).then(function(result) {
              if (result.data != '' && (result.data[0].lev.replace('level', '').trim() > 1 || vm.selectedTableShow.toUpperCase().includes("IN-PROCESS"))) {
                var depts = vm.deptpartment;
                if ((vm.selectedTableShow.toUpperCase().includes("PACK NOTE") || vm.selectedTableShow.toUpperCase().includes("IN-PROCESS")) && $window.localStorage.userDept == 'Production' && vm.approved == 'approved') {
                  depts = 'QA';
                  if (vm.selectedTableShow.toUpperCase().includes("IN-PROCESS SET UP APPROVAL"))
                    depts = 'Production';
                  console.log('depts', depts);
                } else if (vm.selectedTableShow.toUpperCase().includes("SHIFT") && $window.localStorage.userDept == 'Production' && vm.approved == 'approved') {
                  vm.approved = 'done'
                }
                var query = "insert into DF_TBL_APR (approved, tableName,recordId,deptName, formName,updatedBy,status) values ('" + vm.approved + "', '" + vm.selectedTable + "', '" + data.data.CreateDynamicTableResult[0].rowid + "', '" + depts + "','" + FormName + "','" + vm.deptpartment + "', 'a' )";
                getFormsService.updateApprovalTable(query).then(function(reqData) {})

                if (vm.comment != '' && vm.comment != undefined) {
                  var query = "INSERT INTO DF_TBL_FMH (userName , userId , role , formLevel , formName, status, comment ,recordId) VALUES ('" + $window.localStorage.empName + "','" + $window.localStorage.empCode + "','" + $window.localStorage.userDept + "','1','" + vm.selectedTable + "','" + vm.approved + "','" + vm.comment + "','" + data.data.CreateDynamicTableResult[0].rowid + "')"
                  getFormsService.updateApprovalTable(query).then(function(reqData) {})
                }
              } else if (result.data != '' && result.data[0].lev.replace('level', '').trim() > 0 && $window.localStorage.userType == 'Operator') {
                console.log('vm.approved yea', vm.approved);
                if (vm.approved == undefined)
                  vm.approved = 'approved';
                if (vm.selectedTableShow.toUpperCase().includes("SHIFT") && $window.localStorage.userDept == 'Production' && vm.approved == 'approved') {
                  if (vm.approved != "Draft")
                    vm.approved = 'done'
                }
                if (vm.selectedTableShow.toUpperCase().includes("PACK NOTE") && $window.localStorage.userDept == 'Production' && vm.approved == 'approved') {
                  vm.deptpartment = 'QA'
                }
                var query = "insert into DF_TBL_APR (approved, tableName,recordId,deptName, formName,updatedBy,status) values ('" + vm.approved + "', '" + vm.selectedTable + "', '" + data.data.CreateDynamicTableResult[0].rowid + "', '" + vm.deptpartment + "','" + FormName + "','" + vm.deptpartment + "', 'a' )";

                getFormsService.updateApprovalTable(query).then(function(reqData) {})

                if (vm.comment != '' && vm.comment != undefined) {

                  var query = "INSERT INTO DF_TBL_FMH (userName , userId , role , formLevel , formName, status, comment ,recordId) VALUES ('" + $window.localStorage.empName + "','" + $window.localStorage.empCode + "','" + $window.localStorage.userDept + "','1','" + vm.selectedTable + "','" + vm.approved + "','" + vm.comment + "','" + data.data.CreateDynamicTableResult[0].rowid + "')"
                  getFormsService.updateApprovalTable(query).then(function(reqData) {})
                }
              }
            })
            var inTbleValues = " ";
            var subid = 0;
            angular.forEach(InnertableData, function(value, key) {
              var inTblecolm = " ";
              angular.forEach(value.Table.columns, function(value1, key1) {
                if (!value1.cavity) {
                  inTblecolm = inTblecolm + value1.value + ",";
                } else {}

              })
              angular.forEach(value.Table.rows, function(value1, key1) {
                  angular.forEach(value1.cells, function(value2, key2) {
                    if (!value.Table.columns[key2].cavity) {
                      inTbleValues = inTbleValues + "'" + value2.value + "',";
                    } else {
                      inTbleValues = inTbleValues.trim().slice(0, -2) + "_" + value2.value + "',";
                    }

                  })
                  getFormsService.addInnerTableRecord(vm.selectedTable, subid, vm.tableId, inTblecolm.trim().slice(0, -1), inTbleValues.trim().slice(0, -1)).then(function(req) {})
                  inTbleValues = "";
                })
                ++subid;
            })
          });
          notificationService.hideLoad();
          notificationService.alert('', 'Data insertd Successfully', function() {
            console.log("Notification 3:", constantService.selectedTableName);


            if (vm.approved != "draft") {
              console.log('data vm.selectedTable',vm.selectedTable);
              console.log('data vm.recordIdForMailService',vm.recordIdForMailService);

              getFormsRecordsService.getApproverDetails(vm.selectedTable, recordIdForMailService).then(function(data) {
                console.log('data getApproverDetails',data);
                vm.serviceMailInformation = {
                  serviceRecordId: recordIdForMailService,
                  serviceFirstPerson: data.firstPerson,
                  serviceSecondPerson: data.secondPerson,
                  serviceThirdPerson: data.thirdPerson
                };
                recordDetailsService.getStatusInformation(vm.selectedTable, recordIdForMailService).then(function(response) {
                  console.log('data getStatusInformation',response);
                  SendEmailService.sendmailNotifiction(vm.serviceMailInformation, response);
                });
              });
            }



            // sendmailNotifiction(record);
            $rootScope.$emit("CallParentMethod", {});
          });

          deferred.resolve(datavalue);
        }, function(error) {
          deferred.reject(error);
        });
        return deferred.promise;
      }

    }
  }

  function valuationDatePickerOpen($event) {
    if ($event) {
      $event.preventDefault();
      $event.stopPropagation(); // This is the magic
    }
    this.valuationDatePickerIsOpen = true;
  }

  function getValueDynamicaly(tablecol, tableRecord, record, recorvalue) {
    var flag = true;
    if (tablecol.InternalName != constantService.partNO && $window.localStorage.userDept != 'Engineering') {
      console.log('1');
      if (tablecol.InternalName == constantService.shift) {
        console.log('1 1');
        var hoursCol = 0;
        angular.forEach(tableRecord, function(value, key) {
          if (value.Type == 'table') {
            var tFlag = false;
            angular.forEach(value.Table.columns, function(value1, key1) {
              if (value1.value1 == 'Hours') {
                tFlag = true;
                hoursCol = key1;
              }
            });
            if (tFlag) {
              for (i = 0; i < 8; i++) {
                if (value.Table.rows[i] != undefined) {
                  value.Table.rows[i].cells[hoursCol].value = constantService[recorvalue][i]
                } else {
                  var cells = [];
                  for (j = 0; j < value.Table.columns.length; j++) {
                    if (hoursCol == j) {
                      cells.push({ 'value': constantService[recorvalue][i] })
                    } else {
                      cells.push({ 'value': '' })
                    }
                  }
                  value.Table.rows.push({ "cells": cells })
                }
              }
            }
          }
        });
      } else if (tablecol.InternalName == 'process' && $window.localStorage.userDept != 'Engineering') {

        console.log('1 2');
        angular.forEach(tableRecord, function(value, key) {
          if (value.DynamicTableRef && value.DynaicRef.RefCol == tablecol.InternalName) {
            var query = "select * From  DF_TBL_PGR_" + value.DynaicRef.TableId + " where " + tablecol.InternalName + " like '" + record[tablecol.InternalName] + "'";
            constantService.selectedTableName = "DF_TBL_PGR_" + value.DynaicRef.TableId;
            getFormsService.getTableRecordBasedOnQuery(query).then(function(dynResponse) {
              if (dynResponse.data.length > 0) {
                var fflag = true;
                var cells = []
                var flag2 = true;
                if (FormName.toUpperCase().includes('MATERIAL PRE')) {
                  angular.forEach(tableRecord, function(value2, key2) {
                    if (value2.Type == 'table') {
                      angular.forEach(dynResponse.data, function(value5, key5) {
                        getFormsService.getInnerTable(tablecol.DynaicRef.TableId, dynResponse.data[key5][value2.InternalName], 0, FormName).then(function(dyntabResponse) {
                          if (fflag) {
                            vm.record[value2.InternalName].rows = [];
                            fflag = false;
                          }
                          angular.forEach(dyntabResponse.data, function(value3, key3) {
                            angular.forEach(vm.record[value2.InternalName].columns, function(value4, key4) {
                              if (flag2) {
                                value4.colRef.PossibleValue = [];
                                flag2 = false;
                              }
                              if (value4.value != 'rowid' && value4.value != 'tableid' && key3 == 0 && key5 == dynResponse.data.length - 1) {
                                value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                                cells.push({ 'value': value3[value4.value] != undefined ? value3[value4.value] : '' })
                              } else {
                                if (value4.colRef.PossibleValue.indexOf(value3[value4.value]) == -1) {
                                  value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                                }
                              }

                            });
                            if (cells.length > 0) vm.record[value2.InternalName].rows.push({ 'cells': cells });
                            cells = [];
                          });
                        });
                      });

                    }
                  });
                } else {
                  angular.forEach(tableRecord, function(value2, key2) {
                    if (value2.Type == 'table') {
                      console.log("dynResponse   ", dynResponse)
                      getFormsService.getInnerTable(tablecol.DynaicRef.TableId, dynResponse.data[dynResponse.data.length - 1][value2.InternalName], 0, FormName).then(function(dyntabResponse) {
                        console.log("dynResponse11   ", dyntabResponse)
                        vm.record[value2.InternalName].rows = [];
                        var cells = []
                        angular.forEach(dyntabResponse.data, function(value3, key3) {
                          angular.forEach(vm.record[value2.InternalName].columns, function(value4, key4) {

                            if (FormName.toUpperCase().includes('MATERIAL PRE')) {

                              if (key3 == 0 && key4 == 0) value4.colRef.PossibleValue = [];
                              if (value4.value != 'rowid' && value4.value != 'tableid' && key3 == 0) {
                                console.log("----aaa query value3[value4.value]  ", value3[value4.value])
                                value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                                cells.push({ 'value': value3[value4.value] != undefined ? value3[value4.value] : '' })
                              } else {
                                value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                              }




                            } else {
                              if (key3 == 0 && key4 == 0) value4.colRef.PossibleValue = [];
                              if (value4.value != 'rowid' && value4.value != 'tableid') {
                                value4.colRef.PossibleValue.push(value3[value4.value] != undefined ? value3[value4.value] : '')
                                cells.push({ 'value': value3[value4.value] != undefined ? value3[value4.value] : '' })
                              }
                            }
                          });
                          if (cells.length > 0) vm.record[value2.InternalName].rows.push({ 'cells': cells });
                          cells = [];
                        });
                      });
                    }
                  });
                }




              }
            });
          } else {
            if (FormName.toUpperCase().includes('LEAK TESTING')) {
              if (value.Type == 'table') {
                if (value.InternalName.includes(((vm.record.process).split(" ")[0]).toLowerCase())) {
                  value.Restrict[0] = "Production";
                } else {
                  value.Restrict[0] = "Engineering";
                }
              } else if (value.Type == 'dropdown' || value.Type == 'text') {
                if ((value.InternalName.toUpperCase()).includes('BUSH') || (value.InternalName.toUpperCase()).includes('LEAK') || (value.InternalName.toUpperCase()).includes('GUMM')) {
                  if (value.InternalName.includes(((vm.record.process).split(" ")[0]).toLowerCase())) {
                    value.Restrict[0] = "Production";
                  } else {
                    value.Restrict[0] = "Engineering";
                  }
                }
              }
            } else if (FormName.toUpperCase().includes('DAILY')) {
              if (value.DynamicTableRef && value.DynaicRef.FilterCol == tablecol.InternalName) {
                var query = "select * from DF_MSTR_PROC where detail = '" + vm.record[tablecol.InternalName] + "';"
                constantService.selectedTableName = "DF_MSTR_PROC";
                getFormsService.getTableRecordBasedOnQuery(query).then(function(dynResponse) {
                  if (dynResponse.data.length > 0) {
                    var query1 = "select * From " + value.DynaicRef.TableId + " where " + tablecol.InternalName + " like '" + dynResponse.data[0].process + "'";
                    constantService.selectedTableName = value.DynaicRef.TableId;
                    getFormsService.getTableRecordBasedOnQuery(query1).then(function(dynResponse1) {
                      value.FieldOption = []
                      angular.forEach(dynResponse1.data, function(valueMac, keyMac) {
                        value.FieldOption.push({ "Text": valueMac.machine })
                      })
                    });
                  }
                });
              }
            }
          }
        });
      } else if (tablecol.InternalName == 'drawingno') {

        console.log('1 3');
        var query = "select * From  DF_MSTR_DRWNO WHERE drawingNo='" + record[tablecol.InternalName] + "'";
        constantService.selectedTableName = "DF_MSTR_DRWNO";
        getFormsService.getTableRecordBasedOnQuery(query).then(function(dynResponse) {
          if (dynResponse.data.length > 0) {
            vm.record['drawingdesc'] = dynResponse.data[0].descr;
          }
        });
      } else {

        console.log('1 4');
        angular.forEach(tableRecord, function(value, key) {
          if (value.DynamicTableRef && value.DynaicRef.FilterCol == tablecol.InternalName) {

            console.log('1 4 1');
            value.FieldOption = [];
            getFormsService.getBasedOnColumnName(value.DynaicRef.TableId, value.DynaicRef.RefCol, value.DynaicRef.FilterCol, record[tablecol.InternalName], '').then(function(dynResponse) {
              angular.forEach(dynResponse.data, function(value2, key2) {

                value.FieldOption.push({
                  "Text": value2[value.DynaicRef.RefCol]
                });
              })
            });
          }

          if (value.Type == 'table') {
            angular.forEach(value.Table.columns, function(value1, key1) {
              if (value1.colRef != undefined && value1.colRef.referMaster && value1.colRef.filterCol == constantService.machine && value1.colRef.filterCol == tablecol.InternalName) {
                if ((value1.colRef.masterTable).split(' ').length > 0) {
                  angular.forEach(vm.tableName, function(value2, key2) {
                    if (value2.tableName == value1.colRef.masterTable) {
                      value1.colRef.masterTable = 'DF_TBL_PGR_' + value2.rowid
                    }
                  })
                }
                getFormsService.getBasedOnColumnName(value1.colRef.masterTable, value1.colRef.masterCol, value1.colRef.filterCol, record[tablecol.InternalName], 'table').then(function(dynResponse) {
                  value1.colRef.PossibleValue = [];
                  if (FormName.toUpperCase().includes('DAILY')) {

                    angular.forEach(dynResponse.data, function(value4, key4) {
                      if (value4.partno != '' && value4.partno != undefined && dynResponse.data.length - 1 == key4) {

                        value1.colRef.PossibleValue.push(value4.partno);
                      }
                    });
                  } else {
                    angular.forEach(dynResponse.data, function(value4, key4) {
                      if (value4.partno != '' && value4.partno != undefined) {
                        value1.colRef.PossibleValue.push(value4.partno);
                      }
                    });
                  }

                });
              }
            });
          } else if (value.Type == 'dropdown' && value.Cavity != undefined && value.Cavity != '' && parseInt(record[value.InternalName]) > 0) {
            console.log('drop do');
            var cavityCount = 0;
            var statIndex = 0;
            angular.forEach(tableRecord, function(val, key) {
              if (val.Type == 'table' && val.Cavity != '') {
                angular.forEach(val.Table.columns, function(val1, key1) {
                  if (val1.value == value.Cavity) {
                    statIndex = key1 + 1;
                  }
                  if (val1.cavity) {
                    cavityCount++;
                  }
                })
                var count = recorvalue - 1;

                if (cavityCount == 0) {
                  angular.forEach(val.Table.columns, function(val1, key1) {
                    if (val1.value == value.Cavity) {
                      /*var count = recorvalue;  */
                      for (i = 0; i < count; i++) {
                        val.Table.columns.splice(statIndex + cavityCount + i, 0, { 'value1': val.Table.columns[statIndex - 1].value1 + '_' + (i + 1), 'value': 'cavity' + (parseInt(statIndex) + parseInt(i)), 'cavity': true });
                        if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) + 1;
                      }
                      angular.forEach(val.Table.rows, function(val2, key2) {
                        for (i = 0; i < count; i++) {
                          val2.cells.splice(statIndex + cavityCount + i, 0, { 'value': '', 'cavity': true });
                        }
                      });
                    }
                  });
                } else if (cavityCount < count) {
                  var countval = 1;
                  for (i = cavityCount; i < count; i++) {
                    val.Table.columns.splice(parseInt(statIndex) + parseInt(countval), 0, { 'value1': val.Table.columns[statIndex - 1].value1 + '_' + (i + 1), 'value': 'cavity' + parseInt(statIndex) + parseInt(countval) - 1, 'cavity': true });
                    if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) + 1;
                    countval++;
                  }
                  angular.forEach(val.Table.rows, function(val2, key2) {
                    for (i = cavityCount; i < count; i++) {
                      val2.cells.splice(parseInt(statIndex) + parseInt(countval), 0, { 'value': '', 'cavity': true });
                    }
                  });
                } else if (cavityCount > count) {
                  var countval = 1;
                  for (i = cavityCount - count; i > 0; i--) {
                    var tot = statIndex + cavityCount - countval;
                    if (val.Table.columns[tot].cavity) {
                      val.Table.columns.splice(tot, 1);
                      if (val.Remark != '' && val.Remark != undefined) val.Remark = parseInt(val.Remark) - 1;
                    }
                    countval++;
                  }
                  angular.forEach(val.Table.rows, function(val2, key2) {
                    for (i = cavityCount - count; i > 0; i--) {
                      val2.cells.splice(tot, 1);
                    }
                  });
                }
              }
            });
          } else if (value.Type == 'dropdown' && FormName.toUpperCase().includes("FLAMMABILITY")) {

            console.log('1 4 2');
            if (recorvalue == 'V0 Material') {
              angular.forEach(record, function(fvalue, fkey) {
                if (fkey.includes('2')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    fvalue1.cells[3].value = constantService.V0[fkey1];
                  });
                } else if (fkey.includes('1')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    if (fkey1 != 6 && fkey1 != 0 && fkey1 != 7) fvalue1.cells[3].value = 'T1+T2 ≤ 50';
                    else fvalue1.cells[3].value = ''
                  });
                }
              })
            } else if (recorvalue == 'New V0 Material') {
              angular.forEach(record, function(fvalue, fkey) {
                if (fkey.includes('2')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    fvalue1.cells[3].value = constantService.NEWV0[fkey1];
                  });
                } else if (fkey.includes('1')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    if (fkey1 != 6 && fkey1 != 0 && fkey1 != 7) fvalue1.cells[3].value = 'T1+T2 ≤ 50';
                    else fvalue1.cells[3].value = ''
                  });
                }
              })
            } else if (recorvalue == 'V2 Material') {
              angular.forEach(record, function(fvalue, fkey) {
                if (fkey.includes('2')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    fvalue1.cells[3].value = constantService.V2[fkey1];
                  });
                } else if (fkey.includes('1')) {
                  angular.forEach(fvalue.rows, function(fvalue1, fkey1) {
                    if (fkey1 != 6 && fkey1 != 0 && fkey1 != 7) fvalue1.cells[3].value = 'T1+T2 ≤ 250';
                    else fvalue1.cells[3].value = ''
                  });
                }
              })
            }
          }
        })
      }
    } else if (tablecol.InternalName == constantService.partNO || tablecol.InternalName == 'customerpartno') {
      vm.showComment = true;
      if ($window.localStorage.userDept == 'Engineering' || tablecol.DynaicRef == undefined) {
        var tablename = '';
        var tablefield = '';
        if (tablecol.InternalName == 'customerpartno') {
          tablename = 'DF_MSTR_PNO_OP'
          tablefield = 'itemCode';
        } else {
          tablename = 'DF_MSTR_PNO_INT';
          tablefield = 'partNo';
        }
        getFormsService.getEngneeRecord(tablename, tablefield, recorvalue).then(function(dynResponse) {
          if (dynResponse.data.length > 0) {
            angular.forEach(tableRecord, function(value, key) {
              if (tablecol.InternalName == 'customerpartno') {
                if (value.InternalName == 'customerpartdesc') {
                  record['customerpartdesc'] = dynResponse.data[dynResponse.data.length - 1]['descr'];
                }
              } else {
                if (value.InternalName == 'partdesc') {
                  record['partdesc'] = dynResponse.data[dynResponse.data.length - 1]['descr'];
                }
              }
            });
          }
        });
      } else {
        getFormsService.getRecordByMachinPatNo(tablecol.DynaicRef.TableId, tablecol.DynaicRef.RefCol, recorvalue, FormName, vm.record['machine']).then(function(dynResponse) {
          var count = -1;
          angular.forEach(tableRecord, function(value, key) {
            angular.forEach(dynResponse.data[dynResponse.data.length - 1], function(value1, key1) {
              if (value.Type != 'table' && value.InternalName == key1 && key1 != 'datee' && key1 != 'approvedby' && key1 != 'preparedby' && key1 != 'machine' && key1 != 'partno' && key1 != 'rowid' && key1 != 'shift' && !key1.toUpperCase().includes('INSPECTOR')) {
                vm.record[key1] = value1;
              } else if (value.Type == 'table' && key1 == value.InternalName && vm.record[key1]) {
                count++;
                getFormsService.getInnerTable(tablecol.DynaicRef.TableId, value1, count, FormName).then(function(dyntabResponse) {
                  vm.record[key1].rows = [];
                  var cells = [];
                  angular.forEach(dyntabResponse.data, function(value2, key2) {
                    angular.forEach(vm.record[key1].columns, function(value3, key3) {
                      if (value3.value != 'rowid' && value3.value != 'tableid') {
                        cells.push({ 'value': value2[value3.value] != undefined ? value2[value3.value] : '' })
                      }
                    });
                    vm.record[key1].rows.push({ 'cells': cells });
                    cells = [];
                  });
                });
              }
            });

            if (value.DynamicTableRef && value.InternalName == "cavitydetails") {
              value.FieldOption = [];
              getFormsService.getBasedOnColumnName(value.DynaicRef.TableId, value.DynaicRef.RefCol, value.DynaicRef.FilterCol, record[tablecol.InternalName], '').then(function(dynResponse) {
                angular.forEach(dynResponse.data, function(value2, key2) {
                  value.FieldOption.push({
                    "Text": value2[value.DynaicRef.RefCol]
                  });
                })
              });
            }
          })
        });
      }
    }

    if (($state.params.getFormName).toUpperCase().includes("CHEMICAL")) {
      if (tablecol.InternalName != constantService.process) {
        switch (recorvalue) {
          case 'Die Coat Chemical Mixing':
            angular.forEach(vm.tableRecord, function(value, key) {
              if (value.Type == 'table') {
                angular.forEach(value.Table.columns, function(value1, key1) {
                  if (key1 < 3) {
                    value1.showColumn = false;
                  } else {
                    value1.showColumn = true;
                  }
                });
              }
            })
            break;
          case 'Gum Mixing':
            angular.forEach(vm.tableRecord, function(value, key) {
              if (value.Type == 'table') {
                angular.forEach(value.Table.columns, function(value1, key1) {
                  if (key1 < 3) {
                    value1.showColumn = true;
                  } else {
                    value1.showColumn = false;
                  }
                });
              }
            })
            break;

          case 'Cork Mixing':
            angular.forEach(vm.tableRecord, function(value, key) {
              if (value.Type == 'table') {
                angular.forEach(value.Table.columns, function(value1, key1) {
                  if (key1 < 3) {
                    value1.showColumn = true;
                  } else {
                    value1.showColumn = false;
                  }
                });
              }
            })
            break;
          default:
        }
      }
    }
  }

  function dynamicLoad(tableRecord) {
    angular.forEach(tableRecord, function(value, key) {
      if (value.DynamicTableRef) {
        getFormsService.getBasedOnColumnName(value.DynaicRef.TableId, value.DynaicRef.RefCol, null, null, null).then(function(dynResponse) {
          angular.forEach(dynResponse.data, function(value2, key2) {
            value.FieldOption.push({
              "Text": value2[value.DynaicRef.RefCol]
            });
          })
        });
      }

      if (value.Type == 'table') {
        if ((FormName.toUpperCase()).includes("SHIFT RELEIVING SHEET")) {

          var query = '';
          value.Table.rows = []
          angular.forEach(value.Table.columns, function(value1, key1) {
            var tablename = '';
            if (value1.colRef != undefined && value1.colRef.referMaster && value1.colRef.filterCol == undefined) {
              tablename = value1.colRef.masterTable;
              angular.forEach(vm.tableName, function(nValue, nKey) {
                if (nValue.tableName == value1.colRef.masterTable) {
                  tablename = nValue.rowid;
                }
              });
              if (isNaN(tablename)) {
                tableeName = tablename;
              } else {
                tableeName = constantService.tableName + tablename;
              }

              query = "select * from " + tableeName + " where rowid in (select max(rowid) from " + tableeName + " group by machine)";
              console.log("query  ", query)
              constantService.selectedTableName = tableeName;
              getFormsService.getTableRecordBasedOnQuery(query).then(function(dynResponse) {
                angular.forEach(dynResponse.data, function(value2, key2) {
                  var cell = [];
                  for (i = 0; i < value.Table.columns.length; i++) {
                    if (i == 0) {
                      cell.push({ 'value': parseInt(key2) + 1 })
                    } else if (i == 1) {
                      cell.push({ 'value': value2.machine })
                      value.Table.columns[i].colRef.PossibleValue.push(value2.machine)
                    } else if (i == 2) {
                      cell.push({ 'value': value2.partno })
                      value.Table.columns[i].colRef.PossibleValue.push(value2.partno)
                    } else if (i == 3) {
                      cell.push({ 'value': value2.partdesc })
                      value.Table.columns[i].colRef.PossibleValue.push(value2.partdesc)
                    } else cell.push({ 'value': '' })

                  }
                  value.Table.rows.push({ 'cells': cell })
                });
              });
            }
          });
        } else {
          angular.forEach(value.Table.columns, function(value1, key1) {
            if (value1.colRef != undefined && value1.colRef.referMaster) {
              getFormsService.getBasedOnColumnName(value1.colRef.masterTable, value1.colRef.masterCol, null, null, 'table').then(function(dynResponse) {
                angular.forEach(dynResponse.data, function(value2, key2) {
                  value1.colRef.PossibleValue.push(value2[value1.colRef.masterCol])
                });
              });
            }
          });

        }

        if ((FormName.toUpperCase()).includes("INSPECTION REPORT") || (FormName.toUpperCase()).includes("SHIFT")) {
          angular.forEach(value.Table.columns, function(value1, key1) {
            if (value1.value == 'column1') {
              value1.showColumn = true;
            } else {
              value1.showColumn = false;
            }
          });
        }
      }
    })
  }

  function ColumanCondition(col, celll, tablejson) {
    var flag = false;
    if (tablejson.colCondition) {
      var colIndex = tablejson.ColCondValue.resultCol;
      var aa = tablejson.ColCondValue.Condtion;

      var index = '';
      if (col.includes('cavity')) {
        index = 10;
      } else {
        index = aa.indexOf(col);
      }
      var TableCavityCount = 1;
      angular.forEach(tablejson.Table.columns, function(cellvalue, cellKey) {
        if (cellvalue.cavity) TableCavityCount++;
      })


      if (index >= 0 && aa.length > 3) {
        var a = celll[aa[0].replace("column", '') - 1].value;
        var b = celll[aa[2].replace("column", '') - 1].value;
        var c = ''
        if (col.includes('cavity')) {
          c = celll[parseInt(col.replace("cavity", ''))].value;
        } else {
          c = celll[aa[4].replace("column", '') - 1].value;
        }
        if (aa[1] == "+/-") {
          if (TableCavityCount == 1) {
            var count = 0;
            angular.forEach(a.split("/"), function(value, key) {
              if (b.split("/").length > 1) {
                if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b.split("/")[1])).toFixed(2) && parseFloat(c.split("/")[key]) >= (parseFloat(value) + parseFloat(b.split("/")[0])).toFixed(2)) {
                  count++;
                } else {
                  celll[tablejson.Remark].value = false
                }
              } else {
                if (parseFloat(c.split("/")[key]) <= parseFloat((parseFloat(value) + parseFloat(b)).toFixed(2)) && parseFloat(c.split("/")[key]) >= parseFloat((value - b).toFixed(2))) {
                  count++;
                } else {
                  celll[tablejson.Remark].value = false
                }
              }
            })
            if (c.split("/").length > a.split("/").length) {
              celll[parseInt(tablejson.Remark) + 3].Value = false;
            } else if (count == a.split("/").length) {
              celll[tablejson.Remark].value = true
            } else if (b.toUpperCase() == 'REF') {
              celll[tablejson.Remark].value = true
            } else {
              celll[tablejson.Remark].value = false
            }
          } else if (TableCavityCount > 1) {
            var cavityStartNumber = 0;
            var flagg = true;
            var count = 0;
            var noofcavityColumn = 0;
            angular.forEach(tablejson.Table.columns, function(cellvalue, cellKey) {
              if (aa[4].replace("column", '') - 1 == cellKey || cellvalue.cavity) {
                if (flagg) {
                  cavityStartNumber = cellvalue.value.replace("column", '') - 1;
                  flagg = false;
                }
                c = celll[cavityStartNumber].value;
                cavityStartNumber++;

                angular.forEach(a.split("/"), function(value, key) {
                  if (b.split("/").length > 1) {
                    if (parseFloat(c.split("/")[key]) <= (parseFloat(value) + parseFloat(b.split("/")[1])).toFixed(2) && parseFloat(c.split("/")[key]) >= (parseFloat(value) + parseFloat(b.split("/")[0])).toFixed(2)) {
                      count++;
                    } else {
                      celll[tablejson.Remark].value = false
                    }
                  } else {
                    if (parseFloat(c.split("/")[key]) <= parseFloat((parseFloat(value) + parseFloat(b)).toFixed(2)) && parseFloat(c.split("/")[key]) >= parseFloat((value - b).toFixed(2))) {
                      count++;
                    } else {
                      celll[tablejson.Remark].value = false
                    }
                  }
                })
                noofcavityColumn++;

              }
              var qq = a.split('/').length
              if (cellKey == tablejson.Table.columns.length - 1 && count / (a.split('/').length) == noofcavityColumn) {
                celll[tablejson.Remark].value = true
              } else if (b.toUpperCase() == 'REF') {
                celll[tablejson.Remark].value = true
              } else {
                celll[tablejson.Remark].value = false
              }
            })
          }





        } else if (aa[1] == "+") {
          if ((a - b) == c) {
            celll[tablejson.Remark].value = true
          } else {
            celll[tablejson.Remark].value = false
          }
        } else if (aa[1] == "-") {
          if ((parseInt(a) + parseInt(b) == c)) celll[colIndex.replace("column", '') - 1].value = true
          else celll[tablejson.Remark].value = false
        } else {}
      } else {
        var a = celll[aa[0].replace("column", '')].value;
        var b = celll[aa[2].replace("column", '')].value;
        var time1 = (a.split(":")[0] * 60 * 60) + (a.split(":")[1] * 60);
        var time2 = (b.split(":")[0] * 60 * 60) + (b.split(":")[1] * 60);
        if (time1 >= 0 && time2 >= 0) {
          var settime = (time2 - time1) / (60 * 60);
          var timemin = (settime.toString().split(".")[1]) != undefined ? (settime.toString().split(".")[1]) * (60) / 100 : '00'
          var finalTime = settime.toString().split(".")[0] + ":" + timemin;

          celll[colIndex.replace("column", '')].value = finalTime;
        }
      }
    } else if ((FormName.toUpperCase()).includes("CHEMICAL MIXING")) {
      var colnumber = col.replace('column', '');
      switch (col) {
        case 'column8':
          var colVal = celll[parseInt(colnumber) - 1].value
          var hours = parseInt(colVal.split(':')[0]) + 12
          if (hours > 24) {
            hours = hours - 24;
          }
          celll[parseInt(colnumber)].value = hours + ":" + colVal.split(':')[1]
          break;
        case 'column12':
          var colVal = celll[parseInt(colnumber) - 1].value
          var hours = parseInt(colVal.split(':')[0]) + 12
          if (hours > 24) {
            hours = hours - 24;
          }
          celll[parseInt(colnumber)].value = hours + ":" + colVal.split(':')[1]
          break;
        default:
      }

      /*if(parseInt(colnumber) > 5 && parseInt(colnumber) < 14){
        if(colnumber%2 == 0){
          var a =  celll[parseInt(colnumber)-1].value;
          var b =  celll[parseInt(colnumber)].value;
          var time1 = (a.split(" ")[0]*60*60) + (a.split(" ")[1]*60);        
          var time2= (b.split(" ")[0]*60*60) + (b.split(" ")[1]*60);
          var totaltime =  (time2 - time1);
          if(time1 >= 0 && time2 >= 0){
            if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('STIRRRING') && totaltime < 2700){
              celll[colnumber-1].value = '';
            }else if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('SOAKING') && totaltime > 14400){
              celll[colnumber-1].value = '';
            }else if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('MIXING') && totaltime < 2700){
              celll[colnumber-1].value = '';
            }else { }
          }
        }else{
          var a =  celll[parseInt(colnumber)-2].value;
          var b =  celll[parseInt(colnumber)-1].value;
          var time1 = (a.split(" ")[0]*60*60) + (a.split(" ")[1]*60);        
          var time2= (b.split(" ")[0]*60*60) + (b.split(" ")[1]*60);
          var totaltime =  (time2 - time1);
          if(time1 >= 0 && time2 >= 0){
            if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('STIRRRING') && totaltime > 2700){
              celll[colnumber-1].value = '';
            }else if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('SOAKING') && totaltime > 14400){
              celll[colnumber-1].value = '';
            }else if((tablejson.Table.columns[parseInt(colnumber-1)].value1).toUpperCase().includes('MIXING') && totaltime > 2700){
              celll[colnumber-1].value = '';
            }else { }
          }
        }
      }*/
    } else if ((FormName.toUpperCase()).includes("FLAMMABILITY")) {
      if ((tablejson.InternalName).includes('2')) {
        if (col == 'column5') {
          if (isNaN(celll[4].value)) {
            if (celll[4].value.toUpperCase() == celll[3].value.toUpperCase()) {
              celll[5].value = true;
            } else {
              celll[5].value = false;
            }
          } else {
            if (celll[4].value <= celll[3].value) {
              celll[5].value = true;
            } else {
              celll[5].value = false;
            }
          }

        }
      } else if ((tablejson.InternalName).includes('1')) {
        if (celll[0].value <= 6) {
          celll[6].value = parseInt(celll[4].value) + parseInt(celll[5].value)
          var cal = '';
          if (vm.record['nameofthesample'] != undefined && vm.record['nameofthesample'].includes('V2')) {
            cal = 250;
          } else {
            cal = 50
          }
          if (celll[6].value <= cal) {
            celll[8].value = true;
          } else {
            celll[8].value = false;
          }
          var a = 0;
          var b = 0;
          var c = 0;
          var d = 0;
          var e = 0;
          var f = 0;
          angular.forEach(tablejson.Table.rows, function(cValue, ckey) {
            if (cValue.cells[0].value > 1 && cValue.cells[0].value <= 6 && cValue.cells[0].value != '' && cValue.cells[0].value != undefined) {
              if (parseInt(cValue.cells[4].value) != NaN) a = a + parseInt(cValue.cells[4].value);
              if (parseInt(cValue.cells[5].value) != NaN) b = b + parseInt(cValue.cells[5].value);
              if (parseInt(cValue.cells[6].value) != NaN) c = c + parseInt(cValue.cells[6].value);
              if (parseInt(cValue.cells[7].value) != NaN) d = d + parseInt(cValue.cells[7].value);
              if (parseInt(cValue.cells[5].value) != NaN && parseInt(cValue.cells[5].value) > e) e = parseInt(cValue.cells[5].value);
              if (parseInt(cValue.cells[7].value) != NaN && parseInt(cValue.cells[7].value) > f) f = parseInt(cValue.cells[7].value);
            } else if (cValue.cells[0].value != 7 && cValue.cells[0].value != 1) {
              cValue.cells[4].value = a;
              cValue.cells[5].value = b;
              cValue.cells[6].value = c;
              cValue.cells[7].value = d;

              var cal = '';
              if (vm.record['nameofthesample'] != undefined && vm.record['nameofthesample'].includes('V2')) {
                cal = 250;
              } else {
                cal = 50
              }
              if (cValue.cells[6].value <= cal) {
                cValue.cells[8].value = true;
              } else {
                cValue.cells[8].value = false;
              }

            } else if (cValue.cells[0].value == 7 && cValue.cells[0].value != 1) {
              //cValue.cells[4].value = a;
              cValue.cells[5].value = e;
              //cValue.cells[6].value = c;
              cValue.cells[7].value = f;

              var cal = '';
              if (vm.record['nameofthesample'] != undefined && vm.record['nameofthesample'].includes('V2')) {
                cal = 60;
              } else {
                cal = 30
              }
              if (parseInt(cValue.cells[5].value) + parseInt(cValue.cells[7].value) <= cal) {
                cValue.cells[8].value = true;
              } else {
                cValue.cells[8].value = false;
              }
            }
          })
        } else if (celll[0].value == 7) {
          angular.forEach(tablejson.Table.rows, function(cValue, ckey) {})
        } else {}
      }
    }
  }

  function dynamicColChange(colm, colmSelected, cellvla, allColumn, row) {
    console.log("-------------11212")
    var flag = false;
    angular.forEach(cellvla.Table.columns, function(val, key) {
      if (colm.referMaster && val.colRef != undefined && colm.masterCol == val.colRef.filterCol) {
        flag = true;
      }
    })
    if (flag) {
      if (FormName.toUpperCase().includes("MATERIAL PRE")) {
        if (colm.masterCol == "process") {
          //material pre heating on change ro get vale , master material preaheating admin anf process column
          var query = "select * from DF_TBL_PGR_TBL_" + colm.masterTable + "_0 where column1='" + colmSelected.value + "'";
          getFormsService.getTableRecordBasedOnQuery(query).then(function(resul1) {
            if (resul1.data.length > 0) {
              angular.forEach(resul1.data, function(value3, key3) {
                if (key3 == resul1.data.length - 1) {
                  angular.forEach(cellvla.Table.rows, function(value4, key4) {
                    angular.forEach(value4.cells, function(value5, key5) {
                      if (value5.value == colmSelected.value && value4.value != 'rowid' && value4.value != 'tableid') {
                        var i = 0;
                        angular.forEach(value3, function(value6, key6) {
                          if (key6 != 'rowid' && key6 != 'tableid') {
                            value4.cells[i].value = value6;
                            i++;
                          }
                        });
                      }
                    });
                  });
                }
              });

              angular.forEach(cellvla.Table.columns, function(val, key) {
                if (val.colRef.masterCol != undefined && val.colRef.masterCol == 'descr') {
                  row[key].value = resul1.data[0].descr;
                }
              });
            }
          })
        }
      } else {
        getFormsService.getEngneeRecords(colm.masterTable, colmSelected.value, colm.masterCol).then(function(resul) {
          angular.forEach(cellvla.Table.columns, function(val, key) {
            if (colm.referMaster && val.colRef != undefined && colm.masterCol == val.colRef.filterCol) {
              row[key].value = resul.data[resul.data.length - 1][val.colRef.masterCol];
              if (colm.masterCol == 'machine' && val.colRef.masterCol == 'partno' && cellvla.Table.columns[parseInt(key) + 1].colRef.masterCol == 'partdesc') {
                row[parseInt(key) + 1].value = resul.data[resul.data.length - 1][cellvla.Table.columns[parseInt(key) + 1].colRef.masterCol];
              }
              if (FormName.toUpperCase().includes("PACK NOTE")) {
                row[parseInt(key)].value = resul.data[resul.data.length - 1]['partdesc'];
              }
            } else if (val.value1.includes("Cycle Time S")) {
              var query = "select * from DF_TBL_PGR_TBL_" + colm.masterTable.split("_")[colm.masterTable.split("_").length - 1] + "_0 where tableid=" + resul.data[resul.data.length - 1].tableeng1 + " and column2 like '%Cycle time%'"
              getFormsService.getTableRecordBasedOnQuery(query).then(function(resul1) {
                if (resul1.data.length > 0) {
                  row[key].value = resul1.data[resul1.data.length - 1].column4;
                  var aaa = ((3600 * row[4].value) / (resul1.data[resul1.data.length - 1].column4)).toFixed(2);
                  if (aaa != NaN && aaa != 'Infinity') row[11].value = aaa;
                }
              });
            } else {}
          });
        })
      }
    }

    if (FormName.toUpperCase().includes("MATERIAL PRE")) {

      if (colm.masterCol == 'itemCode' || colm.masterCol == 'partNo') {
        var query = "select descr from " + colm.masterTable + " where " + colm.masterCol + "='" + (colmSelected.value).trim() + "'";
        console.log("query query  ", query)
        getFormsService.getTableRecordBasedOnQuery(query).then(function(resul1) {
          if (resul1.data.length > 0) {
            angular.forEach(cellvla.Table.columns, function(val, key) {
              if (val.colRef.masterCol != undefined && val.colRef.masterCol == 'descr') {
                row[key].value = resul1.data[0].descr;
              }
            });
          }
        })
      }
    } else {}




    return true;
  }

  vm.subtableId = '';

  function getRecordBasedOnSelectedId(values) {
    var formName = values.getFormName;
    var recordType = values.type;
    var recordId = values.selectedId;

    var query = "select * from   " + constantService.tableName + vm.selectedTable + " where recordName='" + recordId + "'"
    console.log("query  ", query);
    constantService.selectedTableName = constantService.tableName + vm.selectedTable;
    getFormsService.getTableRecordBasedOnQuery(query).then(function(data) {
      var i = 0;
      if (data.data.length > 0) {
        vm.returnSelectedId = data.data[0].rowid;
        angular.forEach(vm.tableRecord, function(value, key) {
          if (value.Type == 'date') {
            vm.record[value.InternalName] = SqlDateFormate(data.data[0][value.InternalName]);
          } else if (value.Type != 'table') {
            vm.record[value.InternalName] = data.data[0][value.InternalName];
            if (value.InternalName == 'machine') {
              var query = "select * from   " + constantService.tableName + value.DynaicRef.TableId + " where " + value.DynaicRef.RefCol + "='" + data.data[0][value.InternalName] + "'";
              constantService.selectedTableName = constantService.tableName + value.DynaicRef.TableId;
              getFormsService.getTableRecordBasedOnQuery(query).then(function(data1) {
                angular.forEach(vm.tableRecord, function(value2, key2) {
                  if (value2.Type == 'table') {
                    angular.forEach(value2.Table.columns, function(value3, key3) {
                      if (value3.colRef.masterCol == "partno") {
                        value3.colRef.PossibleValue = [];
                        angular.forEach(data1.data, function(value1, key1) {
                          value3.colRef.PossibleValue.push(value1.partno)
                        });
                      }
                    });
                  }
                });
              });
            }
            vm.record['recordName'] = recordId;
          } else if (value.Type == 'table') {
            vm.subtableId = data.data[0][value.InternalName];
            value.Table.rows = [];
            var query = "select * from  " + constantService.innerTableName + vm.selectedTable + "_" + i + " where tableid='" + data.data[0][value.InternalName] + "'";
            i++;
            getFormsService.getTableRecordBasedOnQuery(query).then(function(data1) {
              angular.forEach(data1.data, function(value1, key1) {
                var cell = [];
                angular.forEach(value1, function(value2, key2) {
                  if (key2 != 'tableid' && key2 != 'rowid') {
                    cell.push({ "value": value2 })
                  }
                });
                value.Table.rows.push({ "cells": cell })
              });

              if (key == vm.tableRecord.length - 1) { settableLeakTesting(); }

            });
          } else {

          }
        })
      }
    });
  }

  $scope.openMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  function checkDept(depts) {
    // console.log('depts',depts);
    if (depts.length > 0) {
      if (depts.indexOf(vm.deptpartment) >= 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  function dateConvert(istdate) {
    var date = new Date(istdate);
    var newDate = (date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear());
    return newDate;
  }

  function DBdateConvert(date) {
    var date1 = '';
    if (date.split(' ')[0].split('/').length > 1) {
      var val = date.split(' ')[0].split('/')
      date1 = val[1] + '-' + val[0] + '-' + val[2]
    } else {
      var val = date.split(' ')[0].split('-')
      date1 = val[1] + '-' + val[0] + '-' + val[2]
    }
    return date1;
  }


  function SqlDateFormate(date) {
    var date1 = '';
    if (date.split(' ')[0].split('/').length > 1) {
      var val = date.split(' ')[0].split('/')
      date1 = val[1] + '-' + val[0] + '-' + val[2]
    } else {
      var val = date.split(' ')[0].split('-')
      date1 = val[0] + '-' + val[1] + '-' + val[2]
    }
    return date1;
  }

  $scope.addRow = function(value) {
    var flag = false;
    if (FormName.toLowerCase().includes('inspection') && FormName.toLowerCase().includes('admin')) {
      flag = true;
    }

    var row = {
        cells: []
      },
      colLen = value.Table.columns.length;
    for (var i = 0; i < colLen; i++) {
      if (i == 0 && flag) {
        row.cells.push({ value: parseInt(value.Table.rows.length) + 1 });
      } else {
        row.cells.push({ value: '' });
      }
    }
    value.Table.rows.push(row);
  };

  function cancel() {
    notificationService.confirm('Are you sure want to cancel', 'Yes', 'No', function() {
      $state.reload();
    }, function() {})
  }

  // function sendmailNotifiction(name) {
  //   var empCodee = '';
  //   var no = "";
  //   var cou = 0;
  //   var promiseCall = [];
  //   if (name != undefined && name != '') {
  //     var query1 = "select * from " + constantService.selectedTableName + " where rowid = (select max(rowid) from " + constantService.selectedTableName + ")";
  //     if (constantService.selectedTableName != undefined && constantService.selectedTableName != '') {
  //       promiseCall.push(
  //         getFormsService.getTableRecordBasedOnQuery(query1).then(function(datas) {
  //           console.log("Notifid:", vm.notifid);
  //           if (datas.data.length > 0) {
  //             if (vm.notifid === 2) {
  //               var arr = datas.data[0].recordName.split('-');

  //               no = arr[0] + "-" + arr[1] + "-" + (parseInt(arr[2]) + 1);
  //               vm.notifid = 0;
  //             } else {
  //               no = datas.data[0].recordName;
  //             }
  //           }
  //         })
  //       )
  //       $q.all(promiseCall).then(function(datavalue) {
  //         if (name.approverby == undefined) {
  //           if (typeof name === 'string') {
  //             empCodee = name.split(' ')[name.split(' ').length - 1];
  //           }
  //         } else {
  //           empCodee = name.approverby.split(' ')[name.approverby.split(' ').length - 1];
  //         }

  //         var query = "select * from DF_MSTR_USERS where empCode = '" + empCodee + "'";

  //         getFormsService.getTableRecordBasedOnQuery(query).then(function(data) {
  //           if (data.data.length > 0) {
  //             var email = data.data[0].email == '' ? "mazharudeen@saptalabs.com" : data.data[0].email;
  //             var msg = "Dear Sir/Madam \n\n Please Approve the document with Request No: " + no;
  //             var sub = "Online Process Approval Request";
  //             var req = { "email": email, "subj": sub, "msg": msg }
  //             SendEmailService.sendEmail(req).then(function(aa) {})
  //           }
  //         })
  //       });
  //     }
  //   }
  // }

  function setColumnInOrder(rows, record, inter) {
    var rowww = [];

    var flag = false;
    if (record.shift != undefined && rows.length > 6) {

      angular.forEach(constantService[record.shift], function(value, key) {
        angular.forEach(rows, function(value1, key1) {
          if (value == value1.cells[0].value) {
            flag = true;
            rowww.push(value1);
          }

          if (flag && key == constantService[record.shift].length - 1 && rowww.length == rows.length) {
            console.log("record record 00000000000", rowww)
            vm.record[inter].rows = []
            vm.record[inter].rows = rowww;
            console.log("record  vm.record 00000000000", vm.record)
          }
        });
      });
    }
  }




  function removeRow(index, r, allRecord) {
    allRecord.splice(index, 1);

    angular.forEach(allRecord, function(value, key) {
      if (FormName.toUpperCase().includes("ENGG")) value.cells[0].value = key + 1;
    })
  }
}

angular.module('amaraja').controller('loginController', loginController);

loginController.$inject = ['$scope' ,'$state', '$window', 'notificationService', 'loginService', '$http', 'constantService', '$ionicSideMenuDelegate', '$rootScope'];

function loginController($scope, $state, $window, notificationService, loginService, $http, constantService, $ionicSideMenuDelegate, $rootScope) {
	var vm = this;
	vm.login = login;
	vm.cancel = cancel;
  vm.logout = logout;
  var flag = true;
  console.log('loginController 1');

  $ionicSideMenuDelegate.canDragContent(false);
  function login(){
    if(vm.user.userName != undefined && vm.user.password != undefined && flag){
      flag = false;
      console.log('vm.user',vm.user);
      var request = {"empCode":vm.user.userName,"pass":vm.user.password};
      loginService.userLogin(request).then(function(data){
        console.log('data',data);
        /*if(data.rows.length  > 0){*/
          $window.localStorage.userDept = "";
          $window.localStorage.userType = "";
          $window.localStorage.empName = "";
          $window.localStorage.empCode = "";
          $window.localStorage.email = "";
          if(data.IS_SUCCESS == true){

          $window.localStorage.empName = data.empName;
          $window.localStorage.empCode = data.empCode;
          $window.localStorage.userDept = data.deptName;
          $window.localStorage.userType = data.role;
          $window.localStorage.email = data.email;
          $rootScope.$emit("CallParentMethod", {});
          if((data.role).toUpperCase() == 'ADMIN'){
             $state.go('createForm');
          }else if(data.role == 'Approver'){
            if(data.deptName == 'Engineering'){
              $state.go('getFormRecords', {reload: true});
            }else if(data.deptName == 'Production'){
              $state.go('getFormRecords', {reload: true});
            }else{
              $state.go('getFormRecords', {reload: true});
            }           

          }else if(data.role == 'Supervisor'){
            if(data.deptName == 'Engineering'){
               $state.go('getFormRecords', {reload: true});
            }else if(data.deptName == 'Production'){
              $state.go('getFormRecords', {reload: true});
            }else{
              $state.go('getFormRecords', {reload: true});
            }
          }else if(data.role == 'Operator'){
            if(data.deptName == 'Engineering'){
              console.log('OperatorEngineering')
               $state.go('getForms', {reload: true});
            }else if(data.deptName == 'Production'){
              console.log('OperatorEngineering')
              $state.go('getForms', {reload: true});
            }else{
              $state.go('getFormRecords', {reload: true});
            }
          }else{
            
          }
          flag = true;
        }else{
          flag = true;
          notificationService.alert('', 'UserName and Password does not match', function() { });
        }
        
      });
    }
    	
  }

  function cancel(user){
    flag = true;
  	vm.user = "";
  }

  function logout(){console.log('logout');
    $window.localStorage.removeItem("userDept");
    $window.localStorage.removeItem("userType");
    $window.localStorage.removeItem("userName");
    $state.reload();
    $state.go('login', {reload: true});
  }

        
}



angular.module('amaraja').controller('mainController', mainController);


mainController.$inject = ['$scope', '$rootScope', '$location', '$http', 'mainService'];

function mainController($scope, $rootScope, $location, $http, mainService) {
  var vm = this;

  vm.addRecord = addRecord;
  vm.getRecord = getRecord;
  vm.createTable = createTable;


  getRecord();

  function getRecord() {
    mainService.getRecord().then(function(data) {
      console.log("haaaaaaaaa ", JSON.parse(data.rows[0].dpt2));
      vm.bootstrapData = JSON.parse(data.rows[0].dpt2).value;
      console.log("vm.bootstrapData ", vm.bootstrapData);
    });
  }

  function addRecord(record) {
    console.log("record ", record);
    mainService.addRecordJSON(record).then(function(data) {
      console.log("added succesfuly ", data.rows)
    })

  }

  /*createTable();*/
  function createTable() {
    var jsondata = {
      'value': [{
        "label": "Do you have a website?",
        "field_type": "text",
        "required": false,
        "field_options": {},
        "cid": "c1"
      }, {
        "label": "Please enter your clearance number",
        "field_type": "text",
        "required": true,
        "field_options": {},
        "cid": "c6"
      }, {
        "label": "Security personnel #82?",
        "field_type": "radio",
        "required": true,
        "field_options": {
          "options": [{
            "label": "Yes",
            "checked": false
          }, {
            "label": "No",
            "checked": false
          }],
          "include_other_option": true
        },
        "cid": "c10"
      }, {
        "label": "Medical history",
        "field_type": "file",
        "required": true,
        "field_options": {},
        "cid": "c14"
      }]
    }
    var tableRow = [];
    console.log("jsondata  ", jsondata.value);
    angular.forEach(jsondata.value, function(value, key) {
      tableRow.push(value.cid);
    });
    mainService.createTable(tableRow);
    /*.then(function(data){
                  console.log("added succesfuly ", data.rows)
                });*/
  }

}
angular.module('amaraja').controller('reportController', reportController);

reportController.$inject = ['$scope','$state','$window','$q', '$rootScope', '$location', '$http', 'createFormsService','reportService','notificationService','ionicDatePicker', '$ionicPopup', 'constantService','$ionicSideMenuDelegate','$cordovaPrinter'];

function reportController($scope, $state, $window, $q, $rootScope, $location, $http, createFormsService, reportService,notificationService,   ionicDatePicker, $ionicPopup, constantService, $ionicSideMenuDelegate,$cordovaPrinter) {
	var vm = this;
	vm.getTableByName = getTableByName;
	vm.getFormsNames = getFormsNames;
	vm.setColCondition = setColCondition;
	vm.getAllRecord = getAllRecord;
	vm.CheckforTable = CheckforTable;
	vm.getValueDynamicaly = getValueDynamicaly;
	vm.getDate = getDate;
	vm.getPdf = getPdf;
	vm.columns = [];
	vm.rows = [];
	vm.resultJson = [];
	vm.selectedValue = {};
	vm.dateConvert = dateConvert;
	vm.DBdateConvert = DBdateConvert;
	vm.tableName = [];
	notificationService.showSpinner();
	vm.filter = true;

	$scope.openMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };
	
	 function getDate(cell, col, type){
   var ipObj2 = {
      callback: function (val) {  //Mandatory
      		if(col[cell] == undefined){
      			col[cell] = {};
      		}
      		if(type == 'to'){
      			if(col[cell].from == undefined || col[cell].from == ''){
      				col[cell][type] = '';
      			}else{
      				var aa = col[cell].from;
      				var bb = aa.split("-");
      				var d1 = new Date(bb[2], bb[1] - 1, bb[0])
      				if(d1 <= val){
      					col[cell][type] = dateConvert(new Date(val));
      				}
      			}
      		}else{
      			col[cell][type] = dateConvert(new Date(val));
      		}
      },
      from: new Date(2000, 1, 1), //Optional
      to: new Date(2200, 10, 30), //Optional
      inputDate: new Date(),      //Optional
      mondayFirst: true,          //Optional
            //Optional
      closeOnSelect: false,       //Optional
      templateType: 'popup'       //Optional
    }; 
    ionicDatePicker.openDatePicker(ipObj2);
  }

  function dateConvert(istdate){
    var date = new Date(istdate);
    var newDate = ( date.getDate() + '-' +  (date.getMonth() + 1) + '-' +  date.getFullYear());
    return newDate;
  }

  function DBdateConvert(date){
    var date1 = '';
    if(date.split('/').length > 1){
      var val = date.split('/')
      date1 = val[1]+'-'+val[0]+'-'+val[2]
    }else{
      var val = date.split('-')
      date1 = val[1]+'-'+val[0]+'-'+val[2]
    }
    return date1;
  }



	getFormsNames();
	function getFormsNames(){
		vm.selectedValue = {}
		createFormsService.getTableJSON().then(function(data) {
    	angular.forEach(data.rows, function(value, key) {
    		if(!value.tableName.includes('Admin')){
					value['selected'] = true;
				  vm.tableName.push(value);
    		}
        notificationService.hideLoad();
      });
    });
	}

	function getTableByName(){
		vm.columns = [];
		vm.rows = [];
		vm.selectedValue = {}
		notificationService.showSpinner();
		vm.recordData = [];
		vm.tableData = [];
		createFormsService.getTableByName(vm.selectedTable).then(function(data) {
			if(data.rows.length > 0){
				vm.tableJson = JSON.parse(data.rows[0].tableRecord).data;
				angular.forEach(JSON.parse(data.rows[0].tableRecord).data, function(value, key){
					if(value.Type != 'table') {
						value['selected'] = true;
						if(value.Settings[1].Value == 'partno' || value.Settings[1].Value == 'process' || value.Settings[1].Value == 'machine' ){
							reportService.getByRowName(vm.selectedTable,  value.Settings[1].Value).then(function(result){
								value.Settings[2].PossibleValue = [];
								angular.forEach(result.rows, function(vall, keyy){
									value.Settings[2].PossibleValue.push({'Text': vall[value.Settings[1].Value]})
								})
								vm.recordData.push(value);
							})
						}else{
							vm.recordData.push(value);
						}
					}else{
						angular.forEach(value.Settings[3].PossibleValue[0].columns, function(value1, key1){
							value1['selected'] = true;
						});
						vm.recordData.push(value);
					} 
				})
			}
			notificationService.hideLoad();
		});
	}

	function setColCondition(x, type){
		vm.tableJson = vm.recordData;
		vm.type = type;
		$ionicPopup.show({
	    templateUrl: 'templates/selectColumnPopup.html',
	    title: 'Select Column to be displayed',
	    cssClass:'popupWidth',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel',
	      	onTap: function(e) {
	        } 
	      },
	      {
	        text: '<b>Ok</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	        }
	      }
	    ]
	  });
	}

	function getAllRecord(){
		vm.resultJson = [];
		var subId = 0;
		
		var promiseArray=[];
		var subcell = [];
		vm.columns = [];
		vm.rows = [];
		vm.result = {};
		var condition = '';
		console.log("vm.recordData  ", vm.recordData)
		angular.forEach(vm.recordData, function(value, key){
			if(value.Settings[1].Value == 'partno' || value.Settings[1].Value == 'machine' || value.Settings[1].Value == 'process' || value.Settings[1].Value == 'datee'){
				if(vm.selectedValue[value.Settings[1].Value] != undefined){
					if(value.Settings[1].Value == 'datee'){
						var aa1 = vm.selectedValue[value.Settings[1].Value].from
						condition = condition + value.Settings[1].Value+" BETWEEN '"+ DBdateConvert(aa1) +"' and ";
						if(vm.selectedValue[value.Settings[1].Value].to != undefined && vm.selectedValue[value.Settings[1].Value].to != ''){
							var aa = vm.selectedValue[value.Settings[1].Value].to;      				
							condition = condition +"'"+DBdateConvert(aa) +"' and ";
						}else{
							var aa =  new Date();
							var newDate = aa.getDate() + '-' +  (aa.getMonth() + 1) + '-' +  aa.getFullYear();
							condition = condition +"'"+ DBdateConvert(newDate)+"' and ";
						}
					}else{
						if(vm.selectedValue[value.Settings[1].Value] != '' && vm.selectedValue[value.Settings[1].Value] != undefined){
							condition = condition + value.Settings[1].Value+"='"+vm.selectedValue[value.Settings[1].Value]+"' and ";
						}
					}
				}
			}
		});
		reportService.getTableRecords(vm.selectedTable, condition.trim().slice(0,-4)).then(function(data){
				var promiscall1= [];
			angular.forEach(data.rows, function(value, key){
				var i=0;
				var cell = [];
				subId = 0;
				var flag = true;
				promiseArray = [];
				var result = {};
				var promiscall = [];

				var count = 0;
				promiscall.push(
					angular.forEach(vm.tableJson, function(value1, key1){
						if( value1.selected != false)  value1.selected = true
						if(value1.Type != 'table' && value1.selected){
							if(key == 0){
								vm.columns.push(value1.Name);
								subcell.push('');
							}
							var bbb = value1.Settings[1].Value;
							cell.push(value[bbb])	
							var aaaa = value1.Name;
							result[aaaa] = value[bbb]	
						}else if(value1.Type == 'table'){
							var flag = false;
							var bbb = value1.Settings[1].Value;
						
							if(vm.selectedTable == '3097'  ){
							  	var qq = result.Process;
							  	var ww = (qq.split(" ")[0]).toLowerCase()
							  	if(bbb.includes(ww)){
							  		flag = true;
							  	}
							  }else{
							  	flag = true;
							  }
							  if(flag){
						  	  	count++;
						  		  //result['table'+count] = {};

									var retVal = reportService.getInnerTableRecords(vm.selectedTable,i++, value[bbb]).then(function(inData){
										count++;
										/*result['table'+count] = {}
										result['table'+count]['columns'] = [];
										result['table'+count]['rows'] = []*/


										var tablev = {};
										var column = []
										var rows = []

										tablev['rows'] = []; //
										tablev['column'] = [];
										var loopData = '';
										 if(vm.selectedTable == '3097' || vm.selectedTable == '2093' ){
										 		var valuess = [];
										    var flag = false;

										    var ii = Object.keys(inData.rows[0])
										    if(result.Shift != undefined && ii.length > 6){
										      angular.forEach(constantService[result.Shift], function(value, key){
										        angular.forEach(inData.rows, function(value1, key1){
										          if(value == value1.column1){
										            flag = true;
										            valuess.push(value1);
										          }
										          if(flag && key == constantService[result.Shift].length-1) {
										           	console.log(flag, key, valuess.length);
										            inData.rows = []
										            inData.rows = valuess;
										            loopData = inData.rows
										          }
										        });
										      });
										    }else{
										    	loopData = inData.rows;
										    }
										 }else{	
										 	 loopData = inData.rows;
										 }

										angular.forEach(loopData, function(colValue, ColKey){
											rows = []
											angular.forEach(value1.Settings[3].PossibleValue[0].columns, function(resValue, resKey){
												angular.forEach(colValue, function(colValue1, ColKey1){
													if(resValue.selected != false)resValue.selected = true;
													if(resValue.value == ColKey1 && resValue.selected){
														if(ColKey == 0){
															//result['table'+count]['columns'].push(resValue.value1);
															column.push(resValue.value1);//
														}
														rows.push(colValue1);
													}
												})
											});
											tablev['rows'].push(rows);//


											//return result['table'+count]['rows'].push(rows);
											//result['table'+count]['rows'].push(rows);
										})
										tablev['column'] = column;//

										return tablev;//
									})

									result['table'+count] = retVal;
								
									/*var aaaa = Promise.resolve(retVal);
									aaaa.then(function(v) {
										console.log("2131 3   ", v);
									})*/
										
							  }

					  	}
					  })
				  )
			
					$q.all(promiscall).then(function(datavalue){
					 	vm.rows.push(cell);
						vm.resultJson.push(result);
						
						count = 0;
						if(data.rows.length-1 == key){
							aaa(vm.resultJson)
						}
					});
					
			});
		})
	}


	vm.aaa = aaa;
	function aaa(resultJson){	
		angular.forEach(vm.resultJson, function(jValue, jKey){			
			angular.forEach(jValue, function(jValue1, jKey1){
				if(jKey1.includes('table')){					
					var aaaa  = Promise.resolve(jValue1);
					aaaa.then(function(v) {		
						jValue[jKey1]  = '';
						jValue[jKey1] = v;		

						if(jKey == vm.resultJson.length-1 ){
							angular.element('#filter').triggerHandler('click');
						}
					})					
				}
			})
		})
	}


	function getValueDynamicaly(tableid, colname, colValue){
		angular.forEach(vm.recordData, function(value, key){
			if(value.Type != 'table') {
				switch(colname){
					case 'process':
									if(value.Settings[1].Value == 'partno' || value.Settings[1].Value == 'machine'){
										var condition = 
										reportService.getByRowNamewithcondition(vm.selectedTable,  value.Settings[1].Value, colname,  colValue).then(function(result){
											value.Settings[2].PossibleValue = [];
											angular.forEach(result.rows, function(vall, keyy){
												value.Settings[2].PossibleValue.push({'Text': vall[value.Settings[1].Value]})
											})
										
										})
									}
									break;
					case 'machine':
									if(value.Settings[1].Value == 'partno'  ){
										var condition = 
										reportService.getByRowNamewithcondition(vm.selectedTable,  value.Settings[1].Value).then(function(result){
											value.Settings[2].PossibleValue = [];
											angular.forEach(result.rows, function(vall, keyy){
												value.Settings[2].PossibleValue.push({'Text': vall[value.Settings[1].Value]})
											})
											
										})
									}
									break;
					case 'partno':
									
					 				break
					default:
					 	break;
			}
		}
	})
}


	function getPdf(){
		printDiv();
		//window.print()
	}
 vm.printDiv = printDiv;
	function printDiv() { 
				var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
				if ( app ) {					 
					    var printContents = document.getElementById('prinyy').innerHTML;
						  cordova.plugins.printer.print('<html><head><style type="text/css"> @media all {.page-break	{ display: none; }} @media print {.page-break	{ display: block; page-break-before: always; }}</style><link href="css/style.css" rel="stylesheet"></head><body>' + printContents + '</body></html>', 'BB10');
				} else {
					  var printContents = document.getElementById('prinyy').innerHTML; 
				   	var popupWin =  window.open('', '_blank', 'width=300,height=300')
					  popupWin.document.open();
			  	  popupWin.document.write('<html><head><style type="text/css"> @media all {.page-break	{ display: none; }} @media print {.page-break	{ display: block; page-break-before: always; }}</style><link href="css/style.css" rel="stylesheet"></head><body onload="window.print()">' + printContents + '</body></html>');
				    popupWin.document.close();
				} 


				 // popupWin.focus();
				  //popupWin.close();
				} 
				



	function CheckforTable(x){
		if(x.includes('table')){
			return false;
		}else{
			return true;
		}	

	}
	


}
angular.module('amaraja').controller('trackRecordController', trackRecordController);

trackRecordController.$inject = ['$scope', '$ionicSideMenuDelegate', '$state', '$window', 'getFormsService','trackRecordService'];

function trackRecordController($scope, $ionicSideMenuDelegate, $state, $window, getFormsService, trackRecordService) {
	var vm = this;

	vm.getTableRecords = getTableRecords;
	vm.getFormRecords = getFormRecords;
	vm.showRecordDetails = showRecordDetails;

	vm.formList = [];
	vm.formRecords = [];
	vm.approveCount = 0;
	vm.rejectedCount = 0;
	vm.inprocessCount = 0;
	vm.comletedCount = 0;
	vm.completedrecords = 0;
	vm.allDetails = [];
	vm.rejectedList = [];

	getTableRecords();
	function getTableRecords(){
		trackRecordService.getFormNames().then(function(response){			
			trackRecordService.getWorkflow().then(function(resDate){
				angular.forEach(response.data, function(value, key){
					angular.forEach(resDate.data, function(value1, key1){
						if(value.tableName == value1.levelForm){
							vm.formList.push(value)
						}
					})
				})
			})
		});
	}

	function getFormRecords(){
		vm.formRecords = [];
		trackRecordService.getFormRecords(vm.selectedTable).then(function(response){
			angular.forEach(response.data, function(value, key){
				vm.formRecords.push(value)
			})
		});
	}

	function showRecordDetails(record){
		console.log("record --=- ", record);
		vm.allDetails = [];
		vm.showtable = true;
		trackRecordService.getRecordDetail(vm.selectedTable, record.rowid).then(function(response){
			console.log("asd ", response)
			angular.forEach(response.data, function(value, key){
				vm.allDetails.push(value);
				if(value.status == 'done'){
					vm.comletedCount = vm.comletedCount+1;
					vm.completedrecords.push(value);
				}else if(value.status == 'rejected'){
					vm.rejectedCount = vm.rejectedCount + 1;
					vm.rejectedList.push(value);
				}else{

				}
			})
		});

	}
	$scope.openMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };


}
angular.module('amaraja').controller('userController', userController);

userController.$inject = ['$scope', '$ionicSideMenuDelegate', '$state', '$window', 'getFormsService', 'createFormsService', 'workflowService', '$ionicHistory', '$rootScope'];

function userController($scope, $ionicSideMenuDelegate, $state, $window, getFormsService, createFormsService, workflowService, $ionicHistory, $rootScope) {
	var vm = this;
  vm.getWorkFlowTableList = getWorkFlowTableList;
  vm.getFormName = getFormName;
  vm.getTableRecords = getTableRecords;
  vm.Rejected = Rejected;
	$scope.active1 = false;
  $scope.active2 = false;
  $scope.active3 = false;
  vm.createForm = createForm;
  vm.selectedTableName = selectedTableName;
  vm.selectedWorkName = selectedWorkName;
  vm.trackRecordDetails = vtrackRecordDetails;
  vm.logouthead = logouthead;
  vm.getFormNames = getFormNames;
  vm.inProcess = inProcess;
  vm.gotoWorkflow = gotoWorkflow;
  vm.completed1 = completed1;
  vm.logout = logout;
  vm.pdf = pdf;
  vm.record = [];
  vm.transferList = [];
  vm.returnList = [];
  vm.tableName = [];
  vm.workName = [];
  vm.userType = "";
  vm.userDept = "";
  vm.rejectList = [];
  vm.completed = [];
  vm.inFlowList = [];

  $rootScope.$on("CallParentMethod", function(){
     hideMenu();
  });
  //hideMenu();
  function hideMenu(){
    vm.userType = $window.localStorage.userType;
    vm.userDept = $window.localStorage.userDept;
    getWorkFlowTableList();
  }
  console.log('$window.localStorage.userType',vm.userType);

  function getWorkFlowTableList(){
    vm.record = [];
    getFormsService.getRejectedList(vm.userDept).then(function(result){
      vm.rejectList=[];
      console.log('rejectList ',result);
      if(result.rows.length > 0){
        angular.forEach(result.rows, function(value, key){
          getFormsService.getTableName(value.tableName).then(function(result1){
            // console.log('rejectList result1',result1)
            angular.forEach(result1.data,function(val){
            vm.rejectList.push(val);
          });
          })
        })
      }
    });

    getFormsService.getInFLowList(vm.userDept).then(function(result){
      console.log('inFlowList',result);
      vm.inFlowList =[];
      angular.forEach(result.data, function(value, key){
        // if(vm.userDept=='QA')
          queryVal=value.tableName
        // else
        //   queryVal=value.formName;
        getFormsService.getTableName(queryVal).then(function(result1){
      console.log('inFlowList result1',result1);
          if(result1.data.length > 0){
            var FormName = result1.data[0].tableName;
            if(vm.userType != 'Operator' && vm.userDept == 'QA'){
              if((FormName.toUpperCase().includes("INSPECTION") || FormName.toUpperCase().includes("FLAM")) && !FormName.toUpperCase().includes("PACK") && !FormName.toUpperCase().includes("ADMIN")){
                 if(result1.data.length > 0) vm.inFlowList.push(result1.data[0]);
              }
            }else{
              if(result1.data.length > 0) vm.inFlowList.push(result1.data[0]);
            }
          }
        })
      })
    })
    getFormsService.getWorkFlowTableList(vm.userDept).then(function(data){
      getFormsService.getCompletedList().then(function(result){
        console.log('completed list',result)
        vm.completed =[];
        angular.forEach(result.data, function(value, key){   
          angular.forEach(data.rows, function(value1, key1){
            if(value.formName == value1.levelForm ){
               var FormName = value.formName;

              if(vm.userType != 'Operator' && vm.userDept == 'QA'){
                if((FormName.toUpperCase().includes("INSPECTION") || FormName.toUpperCase().includes("FLAM")) && !FormName.toUpperCase().includes("PACK") && !FormName.toUpperCase().includes("ADMIN")){
                  vm.completed.push(value);
                }
              }else{
                vm.completed.push(value);
              }
            }
          })
        })
      })

     
      vm.transferList =[];
      vm.returnList = [];
      if(data.rows.length > 0){
        if(vm.userDept == 'Engineering'){
          angular.forEach(data.rows, function(value, key){
            vm.record.push(value);
             getFormsService.getApproved(value.levelForm).then(function(response){
              if(response.rows.length > 0){
                angular.forEach(response.rows, function(valueRes, keyRes){
                   if(valueRes.approved == "transfer"){
                      vm.transferList.push(value);
                    }else if(valueRes.approved == "return"){
                      vm.returnList.push(value);
                    }else{

                    }
                });

        console.log('transfer List',vm.transferList)
        console.log('return List',vm.returnList)
              }
            });
          })
        }else if(vm.userDept == 'QA' || vm.userDept == 'Production'){
          vm.record = [];
          vm.transferList = [];
          vm.returnList = [];
          angular.forEach(data.rows, function(value, key){
            getFormsService.getApproved(value.levelForm).then(function(response){
              if(response.rows.length > 0){  
               var aaa = value.levelForm != undefined?value.levelForm:value.formName;
              if($window.localStorage.empName == "Production admin" || $window.localStorage.empName == "QA admin"){
                if(aaa.toUpperCase().includes("ADMIN") ) {         
                  vm.record.push(value); 
                }
              }else{
                if(!aaa.toUpperCase().includes("ADMIN")){
                  var FormName = aaa;
                  if(vm.userType != 'Operator' && vm.userDept == 'QA'){
                    if((FormName.toUpperCase().includes("INSPECTION") || FormName.toUpperCase().includes("FLAM")) && !FormName.toUpperCase().includes("PACK")  && !FormName.toUpperCase().includes("ADMIN")){
                      vm.record.push(value);
                    }
                  }else{
                    vm.record.push(value);
                  }
                }
              }


              angular.forEach(response.rows, function(valueRes, keyRes){
                 if(valueRes.approved == "approved"){
                  }else if(valueRes.approved == "transfer"){
                    vm.transferList.push(value);
                  }else if(valueRes.approved == "return"){
                    vm.returnList.push(value);
                  }/*else if(valueRes.approved != "done" && valueRes.approved != "reject"){
                     vm.returnList.push(value);
                  }*/else{

                  }
              });
              
        console.log('transfer List',vm.transferList)
        console.log('return List',vm.returnList)
              }else if($window.localStorage.userType == "Operator"){
                var aaa = value.levelForm != undefined?value.levelForm:value.formName;
                if(($window.localStorage.empName == "Production admin" || $window.localStorage.empName == "QA admin")) {
                  if(aaa.toUpperCase().includes("ADMIN")) {  
                    vm.record.push(value); 
                  }
                }else{
                  if(!aaa.toUpperCase().includes("ADMIN")){
                     var FormName = value.formName;
                      if(vm.userType != 'Operator' && vm.userDept == 'QA'){
                        if((FormName.toUpperCase().includes("INSPECTION") || FormName.toUpperCase().includes("FLAM")) && !FormName.toUpperCase().includes("PACK") && !FormName.toUpperCase().includes("ADMIN")){
                          vm.record.push(value);
                        }
                      }else{
                       vm.record.push(value);
                      }
                  }
                }
              }
            });             
          })
        }else if(vm.userDept == 'Maintenance'){
          angular.forEach(data.rows, function(value, key){
            vm.record.push(value);
          })
        }else if(vm.userDept == 'Mold Maintenance'){
          angular.forEach(data.rows, function(value, key){
            vm.record.push(value);
          })
        }
      }
    });   
  }
  
  function getFormName(x){
    var query = " select lev from DF_WORKFLOW_I where levelForm='"+x+"' and department ='"+vm.userDept+"'"
    getFormsService.getTableRecordBasedOnQuery(query).then(function(data){
      if(data.data.length > 0){
        if(data.data[0].lev.replace("level", '').trim() > 1 ){
          $state.go('getFormRecords', { 'getFormName': x}, {reload:true});
        }else{
          $state.go('getForms', { 'getFormName': x}, {reload:true});
        }
      }
    })
  }

  function getTableRecords(x){
    $state.go('getFormRecords', { 'getFormName': x, 'type': ''},  {reload:true});
  }

  /*user page content*/
  $scope.openMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };

  function createForm(){
  	$state.go('createForm');
  }
  function gotoWorkflow(){
    $state.go('workflow');
  }
  function Rejected(x, type){  
    $state.go('getFormRecords', {  'getFormName': x, 'type': type}, {reload:true});   
  }
  function inProcess(x,type){
    if (type==undefined) {
     $state.go('getFormRecords', {  'getFormName': x, 'type': 'inprocess'}, {reload:true}); 
    }
    else      
     $state.go('getFormRecords', {  'getFormName': x, 'type': type}, {reload:false}); 
  }
  function completed1(x){
    $state.go('getFormRecords', {  'getFormName': x, 'type': 'done'}, {reload:true}); 
  }

  createFormsService.getTableJSON().then(function(data) {
    angular.forEach(data.rows, function(value, key) {
      vm.tableName.push(value);
    });
  });

  workflowService.getWorkflow().then(function(data) {
    angular.forEach(data.rows, function(value, key) {
      vm.workName.push(value);
    });
  })

  function selectedTableName(form){
    $state.go('editForms', { 'formName': form.rowid});
  }

  function selectedWorkName(work){
    $state.go('editWorkflow', { 'workName': JSON.stringify(work)});
  }
  $scope.opts = [
        {text: '1st' },
        {text: '2nd' },
        {text: '1st' },
        {text: '1st' },
        {text: '1st' },
    ];

  function logout(){
    $window.localStorage.removeItem("userDept");
    $window.localStorage.removeItem("userType");
    $window.localStorage.removeItem("userName");
    $window.localStorage.removeItem("email");    
    $ionicSideMenuDelegate.toggleLeft();
    vm.record = [];
    vm.transferList = [];
    vm.returnList = [];
    vm.tableName = [];
    vm.workName = [];
    vm.rejectList = [];
    vm.completed = [];
    vm.inFlowList = [];
    $state.go('login', {reload: true});
  }


  function pdf(){
    $state.go('report', {reload: true});
  }


  function logouthead(){
    $state.go('login', {reload: true});
  }

  function vtrackRecordDetails(){
    $state.go('trackRecord', {reload: true});
  }

  function getFormNames(){
    $state.go('getAllForms');
  }

}
(function() {
	'use strict';

	angular
		.module('amaraja')
		.controller('workflowController', workflowController);

	workflowController.$inject = ['workflowService', '$cordovaSQLite', 'notificationService', '$state', '$scope', '$ionicSideMenuDelegate'];

	function workflowController(workflowService, $cordovaSQLite, notificationService, $state, $scope, $ionicSideMenuDelegate) {
		var vm = this;
		vm.levelsArray = [];
		vm.levelsData = [];
		vm.departments = [];
		vm.forms = [];
		vm.levels = 10;
		vm.levelsChange = levelsChange;
		vm.submit = submit;
		vm.hideLevels = false;
		vm.getWorkflowDataByid = getWorkflowDataByid;
		vm.workName = [];
		vm.showWork = false;
		vm.update = update;
		vm.workflowEdit = workflowEdit;
		var details = "";
		vm.removeLevel = removeLevel;
		var removeElements = [];
		$ionicSideMenuDelegate.canDragContent(false);
		$scope.$watch(function() {
	    return $state.params;
	  });
	  var editWorkFlowName = $state.params.workName;

		for (var i = 1; i <= vm.levels; i++) {
			vm.levelsArray.push(i);
		}

		function levelsChange(count) {
			vm.hideLevels = true;
			vm.levelsData = [];
			for (var i = 1; i <= count; i++) {
				vm.levelsData.push(i);
			}
			if($state.current.name == 'editWorkflow'){
				var dataCount = vm.workflowData.length;
				if(count>dataCount){
					for(var j = 1; j<= (count-dataCount); j++){
						var levelcount = parseInt(dataCount)+(j-1);
						vm.workflowData.push({"department": "",
																"level":"level "+ levelcount,
																"levelform":"",
																"levelname":"",
																"workflowid":details.workflowId,
																"create": true
															})
					}
				}else{
					for(var j = 1; j<= (dataCount-count); j++){
						vm.workflowData.pop();
					}
				}
			}
				
		}

		workflowService.getDepartments().then(function(data) {console.log('data',data);
			for (var i = 0; i < data.rows.length; i++) {
				vm.departments.push(data.rows[i]);
			}
		})

		workflowService.getForms().then(function(data) {
			for (var i = 0; i < data.rows.length; i++) {
				vm.forms.push(data.rows[i]);
			}
		})

		getState();
		function getState(){
			if($state.current.name == 'editWorkflow'){
				getWorkflowDataByid(editWorkFlowName);
			}
		}

	

		function getWorkflowDataByid(workflow){
			vm.workflowData = [];
			details = JSON.parse(workflow);
			vm.workflowname = "";
			vm.levelsCount = details.levels;
			workflowService.getWorkflowData(details.workflowId).then(function(data) {
				vm.workflowname = details.workflowName;
	      angular.forEach(data.rows, function(value, index){
	      	vm.workflowData.push(value);
	      })
	      vm.showWork = true;
				vm.hideLevels = true;

			})
		}	

		function workflowEdit(level){
			level.edited = true;
		}

		function submit() {
			var request = {
				workflowname: vm.workflowname,
				levels: vm.levelsCount,
				departments: vm.deptSelected,
				forms: vm.formSelected,
				formname: vm.formName
			}
			workflowService.submitworkflow(request).then(function(data) {
				vm.workflowname = "";
				vm.levelsCount = "";
				vm.deptSelected = "";
				vm.formSelected = "";
				vm.formName = "";
				vm.levelsData = [];
				vm.hideLevels = false;
				notificationService.alert('', 'Workflow Details Saved successfully', function() {
				});
			})
		}

		function removeLevel(level, index){
			console.log('level',level);
			removeElements.push(level);
			vm.workflowData.splice(index,1);
			angular.forEach(vm.workflowData, function(value, index){
	      vm.workflowData[index].level = "level "+ parseInt(index+1);
	      vm.workflowData[index].edited = true;
      })
		}

		function update(){
			workflowService.updateworkflow(vm.workflowData, removeElements).then(function(data) {
		})
		}

	}

})();
/*!
 * Bootstrap-select v1.12.2 (http://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2017 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define(["jquery"], function (a0) {
      return (factory(a0));
    });
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require("jquery"));
  } else {
    factory(root["jQuery"]);
  }
}(this, function (jQuery) {

(function ($) {
  'use strict';

  //<editor-fold desc="Shims">
  if (!String.prototype.includes) {
    (function () {
      'use strict'; // needed to support `apply`/`call` with `undefined`/`null`
      var toString = {}.toString;
      var defineProperty = (function () {
        // IE 8 only supports `Object.defineProperty` on DOM elements
        try {
          var object = {};
          var $defineProperty = Object.defineProperty;
          var result = $defineProperty(object, object, object) && $defineProperty;
        } catch (error) {
        }
        return result;
      }());
      var indexOf = ''.indexOf;
      var includes = function (search) {
        if (this == null) {
          throw new TypeError();
        }
        var string = String(this);
        if (search && toString.call(search) == '[object RegExp]') {
          throw new TypeError();
        }
        var stringLength = string.length;
        var searchString = String(search);
        var searchLength = searchString.length;
        var position = arguments.length > 1 ? arguments[1] : undefined;
        // `ToInteger`
        var pos = position ? Number(position) : 0;
        if (pos != pos) { // better `isNaN`
          pos = 0;
        }
        var start = Math.min(Math.max(pos, 0), stringLength);
        // Avoid the `indexOf` call if no match is possible
        if (searchLength + start > stringLength) {
          return false;
        }
        return indexOf.call(string, searchString, pos) != -1;
      };
      if (defineProperty) {
        defineProperty(String.prototype, 'includes', {
          'value': includes,
          'configurable': true,
          'writable': true
        });
      } else {
        String.prototype.includes = includes;
      }
    }());
  }

  if (!String.prototype.startsWith) {
    (function () {
      'use strict'; // needed to support `apply`/`call` with `undefined`/`null`
      var defineProperty = (function () {
        // IE 8 only supports `Object.defineProperty` on DOM elements
        try {
          var object = {};
          var $defineProperty = Object.defineProperty;
          var result = $defineProperty(object, object, object) && $defineProperty;
        } catch (error) {
        }
        return result;
      }());
      var toString = {}.toString;
      var startsWith = function (search) {
        if (this == null) {
          throw new TypeError();
        }
        var string = String(this);
        if (search && toString.call(search) == '[object RegExp]') {
          throw new TypeError();
        }
        var stringLength = string.length;
        var searchString = String(search);
        var searchLength = searchString.length;
        var position = arguments.length > 1 ? arguments[1] : undefined;
        // `ToInteger`
        var pos = position ? Number(position) : 0;
        if (pos != pos) { // better `isNaN`
          pos = 0;
        }
        var start = Math.min(Math.max(pos, 0), stringLength);
        // Avoid the `indexOf` call if no match is possible
        if (searchLength + start > stringLength) {
          return false;
        }
        var index = -1;
        while (++index < searchLength) {
          if (string.charCodeAt(start + index) != searchString.charCodeAt(index)) {
            return false;
          }
        }
        return true;
      };
      if (defineProperty) {
        defineProperty(String.prototype, 'startsWith', {
          'value': startsWith,
          'configurable': true,
          'writable': true
        });
      } else {
        String.prototype.startsWith = startsWith;
      }
    }());
  }

  if (!Object.keys) {
    Object.keys = function (
      o, // object
      k, // key
      r  // result array
      ){
      // initialize object and result
      r=[];
      // iterate over object keys
      for (k in o)
          // fill result array with non-prototypical keys
        r.hasOwnProperty.call(o, k) && r.push(k);
      // return result
      return r;
    };
  }

  // set data-selected on select element if the value has been programmatically selected
  // prior to initialization of bootstrap-select
  // * consider removing or replacing an alternative method *
  var valHooks = {
    useDefault: false,
    _set: $.valHooks.select.set
  };

  $.valHooks.select.set = function(elem, value) {
    if (value && !valHooks.useDefault) $(elem).data('selected', true);

    return valHooks._set.apply(this, arguments);
  };

  var changed_arguments = null;
  $.fn.triggerNative = function (eventName) {
    var el = this[0],
        event;

    if (el.dispatchEvent) { // for modern browsers & IE9+
      if (typeof Event === 'function') {
        // For modern browsers
        event = new Event(eventName, {
          bubbles: true
        });
      } else {
        // For IE since it doesn't support Event constructor
        event = document.createEvent('Event');
        event.initEvent(eventName, true, false);
      }

      el.dispatchEvent(event);
    } else if (el.fireEvent) { // for IE8
      event = document.createEventObject();
      event.eventType = eventName;
      el.fireEvent('on' + eventName, event);
    } else {
      // fall back to jQuery.trigger
      this.trigger(eventName);
    }
  };
  //</editor-fold>

  // Case insensitive contains search
  $.expr.pseudos.icontains = function (obj, index, meta) {
    var $obj = $(obj).find('a');
    var haystack = ($obj.data('tokens') || $obj.text()).toString().toUpperCase();
    return haystack.includes(meta[3].toUpperCase());
  };

  // Case insensitive begins search
  $.expr.pseudos.ibegins = function (obj, index, meta) {
    var $obj = $(obj).find('a');
    var haystack = ($obj.data('tokens') || $obj.text()).toString().toUpperCase();
    return haystack.startsWith(meta[3].toUpperCase());
  };

  // Case and accent insensitive contains search
  $.expr.pseudos.aicontains = function (obj, index, meta) {
    var $obj = $(obj).find('a');
    var haystack = ($obj.data('tokens') || $obj.data('normalizedText') || $obj.text()).toString().toUpperCase();
    return haystack.includes(meta[3].toUpperCase());
  };

  // Case and accent insensitive begins search
  $.expr.pseudos.aibegins = function (obj, index, meta) {
    var $obj = $(obj).find('a');
    var haystack = ($obj.data('tokens') || $obj.data('normalizedText') || $obj.text()).toString().toUpperCase();
    return haystack.startsWith(meta[3].toUpperCase());
  };

  /**
   * Remove all diatrics from the given text.
   * @access private
   * @param {String} text
   * @returns {String}
   */
  function normalizeToBase(text) {
    var rExps = [
      {re: /[\xC0-\xC6]/g, ch: "A"},
      {re: /[\xE0-\xE6]/g, ch: "a"},
      {re: /[\xC8-\xCB]/g, ch: "E"},
      {re: /[\xE8-\xEB]/g, ch: "e"},
      {re: /[\xCC-\xCF]/g, ch: "I"},
      {re: /[\xEC-\xEF]/g, ch: "i"},
      {re: /[\xD2-\xD6]/g, ch: "O"},
      {re: /[\xF2-\xF6]/g, ch: "o"},
      {re: /[\xD9-\xDC]/g, ch: "U"},
      {re: /[\xF9-\xFC]/g, ch: "u"},
      {re: /[\xC7-\xE7]/g, ch: "c"},
      {re: /[\xD1]/g, ch: "N"},
      {re: /[\xF1]/g, ch: "n"}
    ];
    $.each(rExps, function () {
      text = text ? text.replace(this.re, this.ch) : '';
    });
    return text;
  }


  // List of HTML entities for escaping.
  var escapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '`': '&#x60;'
  };
  
  var unescapeMap = {
    '&amp;': '&',
    '&lt;': '<',
    '&gt;': '>',
    '&quot;': '"',
    '&#x27;': "'",
    '&#x60;': '`'
  };

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  var createEscaper = function(map) {
    var escaper = function(match) {
      return map[match];
    };
    // Regexes for identifying a key that needs to be escaped.
    var source = '(?:' + Object.keys(map).join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'g');
    return function(string) {
      string = string == null ? '' : '' + string;
      return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
    };
  };

  var htmlEscape = createEscaper(escapeMap);
  var htmlUnescape = createEscaper(unescapeMap);

  var Selectpicker = function (element, options) {
    // bootstrap-select has been initialized - revert valHooks.select.set back to its original function
    if (!valHooks.useDefault) {
      $.valHooks.select.set = valHooks._set;
      valHooks.useDefault = true;
    }

    this.$element = $(element);
    this.$newElement = null;
    this.$button = null;
    this.$menu = null;
    this.$lis = null;
    this.options = options;

    // If we have no title yet, try to pull it from the html title attribute (jQuery doesnt' pick it up as it's not a
    // data-attribute)
    if (this.options.title === null) {
      this.options.title = this.$element.attr('title');
    }

    // Format window padding
    var winPad = this.options.windowPadding;
    if (typeof winPad === 'number') {
      this.options.windowPadding = [winPad, winPad, winPad, winPad];
    }

    //Expose public methods
    this.val = Selectpicker.prototype.val;
    this.render = Selectpicker.prototype.render;
    this.refresh = Selectpicker.prototype.refresh;
    this.setStyle = Selectpicker.prototype.setStyle;
    this.selectAll = Selectpicker.prototype.selectAll;
    this.deselectAll = Selectpicker.prototype.deselectAll;
    this.destroy = Selectpicker.prototype.destroy;
    this.remove = Selectpicker.prototype.remove;
    this.show = Selectpicker.prototype.show;
    this.hide = Selectpicker.prototype.hide;

    this.init();
  };

  Selectpicker.VERSION = '1.12.2';

  // part of this is duplicated in i18n/defaults-en_US.js. Make sure to update both.
  Selectpicker.DEFAULTS = {
    noneSelectedText: 'Nothing selected',
    noneResultsText: 'No results matched {0}',
    countSelectedText: function (numSelected, numTotal) {
      return (numSelected == 1) ? "{0} item selected" : "{0} items selected";
    },
    maxOptionsText: function (numAll, numGroup) {
      return [
        (numAll == 1) ? 'Limit reached ({n} item max)' : 'Limit reached ({n} items max)',
        (numGroup == 1) ? 'Group limit reached ({n} item max)' : 'Group limit reached ({n} items max)'
      ];
    },
    selectAllText: 'Select All',
    deselectAllText: 'Deselect All',
    doneButton: false,
    doneButtonText: 'Close',
    multipleSeparator: ', ',
    styleBase: 'btn',
    style: 'btn-default',
    size: 'auto',
    title: null,
    selectedTextFormat: 'values',
    width: false,
    container: false,
    hideDisabled: false,
    showSubtext: false,
    showIcon: true,
    showContent: true,
    dropupAuto: true,
    header: false,
    liveSearch: false,
    liveSearchPlaceholder: null,
    liveSearchNormalize: false,
    liveSearchStyle: 'contains',
    actionsBox: false,
    iconBase: 'glyphicon',
    tickIcon: 'glyphicon-ok',
    showTick: false,
    template: {
      caret: '<span class="caret"></span>'
    },
    maxOptions: false,
    mobile: false,
    selectOnTab: false,
    dropdownAlignRight: false,
    windowPadding: 0
  };

  Selectpicker.prototype = {

    constructor: Selectpicker,

    init: function () {
      var that = this,
          id = this.$element.attr('id');

      this.$element.addClass('bs-select-hidden');

      // store originalIndex (key) and newIndex (value) in this.liObj for fast accessibility
      // allows us to do this.$lis.eq(that.liObj[index]) instead of this.$lis.filter('[data-original-index="' + index + '"]')
      this.liObj = {};
      this.multiple = this.$element.prop('multiple');
      this.autofocus = this.$element.prop('autofocus');
      this.$newElement = this.createView();
      this.$element
        .after(this.$newElement)
        .appendTo(this.$newElement);
      this.$button = this.$newElement.children('button');
      this.$menu = this.$newElement.children('.dropdown-menu');
      this.$menuInner = this.$menu.children('.inner');
      this.$searchbox = this.$menu.find('input');

      this.$element.removeClass('bs-select-hidden');

      if (this.options.dropdownAlignRight === true) this.$menu.addClass('dropdown-menu-right');

      if (typeof id !== 'undefined') {
        this.$button.attr('data-id', id);
        $('label[for="' + id + '"]').click(function (e) {
          e.preventDefault();
          that.$button.focus();
        });
      }

      this.checkDisabled();
      this.clickListener();
      if (this.options.liveSearch) this.liveSearchListener();
      this.render();
      this.setStyle();
      this.setWidth();
      if (this.options.container) this.selectPosition();
      this.$menu.data('this', this);
      this.$newElement.data('this', this);
      if (this.options.mobile) this.mobile();

      this.$newElement.on({
        'hide.bs.dropdown': function (e) {
          that.$menuInner.attr('aria-expanded', false);
          that.$element.trigger('hide.bs.select', e);
        },
        'hidden.bs.dropdown': function (e) {
          that.$element.trigger('hidden.bs.select', e);
        },
        'show.bs.dropdown': function (e) {
          that.$menuInner.attr('aria-expanded', true);
          that.$element.trigger('show.bs.select', e);
        },
        'shown.bs.dropdown': function (e) {
          that.$element.trigger('shown.bs.select', e);
        }
      });

      if (that.$element[0].hasAttribute('required')) {
        this.$element.on('invalid', function () {
          that.$button
            .addClass('bs-invalid')
            .focus();

          that.$element.on({
            'focus.bs.select': function () {
              that.$button.focus();
              that.$element.off('focus.bs.select');
            },
            'shown.bs.select': function () {
              that.$element
                .val(that.$element.val()) // set the value to hide the validation message in Chrome when menu is opened
                .off('shown.bs.select');
            },
            'rendered.bs.select': function () {
              // if select is no longer invalid, remove the bs-invalid class
              if (this.validity.valid) that.$button.removeClass('bs-invalid');
              that.$element.off('rendered.bs.select');
            }
          });
        });
      }

      setTimeout(function () {
        that.$element.trigger('loaded.bs.select');
      });
    },

    createDropdown: function () {
      // Options
      // If we are multiple or showTick option is set, then add the show-tick class
      var showTick = (this.multiple || this.options.showTick) ? ' show-tick' : '',
          inputGroup = this.$element.parent().hasClass('input-group') ? ' input-group-btn' : '',
          autofocus = this.autofocus ? ' autofocus' : '';
      // Elements
      var header = this.options.header ? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + '</div>' : '';
      var searchbox = this.options.liveSearch ?
      '<div class="bs-searchbox">' +
      '<input type="text" class="form-control" autocomplete="off"' +
      (null === this.options.liveSearchPlaceholder ? '' : ' placeholder="' + htmlEscape(this.options.liveSearchPlaceholder) + '"') + ' role="textbox" aria-label="Search">' +
      '</div>'
          : '';
      var actionsbox = this.multiple && this.options.actionsBox ?
      '<div class="bs-actionsbox">' +
      '<div class="btn-group btn-group-sm btn-block">' +
      '<button type="button" class="actions-btn bs-select-all btn btn-default">' +
      this.options.selectAllText +
      '</button>' +
      '<button type="button" class="actions-btn bs-deselect-all btn btn-default">' +
      this.options.deselectAllText +
      '</button>' +
      '</div>' +
      '</div>'
          : '';
      var donebutton = this.multiple && this.options.doneButton ?
      '<div class="bs-donebutton">' +
      '<div class="btn-group btn-block">' +
      '<button type="button" class="btn btn-sm btn-default">' +
      this.options.doneButtonText +
      '</button>' +
      '</div>' +
      '</div>'
          : '';
      var drop =
          '<div class="btn-group bootstrap-select' + showTick + inputGroup + '">' +
          '<button type="button" class="' + this.options.styleBase + ' dropdown-toggle" data-toggle="dropdown"' + autofocus + ' role="button">' +
          '<span class="filter-option pull-left"></span>&nbsp;' +
          '<span class="bs-caret">' +
          this.options.template.caret +
          '</span>' +
          '</button>' +
          '<div class="dropdown-menu open" role="combobox">' +
          header +
          searchbox +
          actionsbox +
          '<ul class="dropdown-menu inner" role="listbox" aria-expanded="false">' +
          '</ul>' +
          donebutton +
          '</div>' +
          '</div>';

      return $(drop);
    },

    createView: function () {
      var $drop = this.createDropdown(),
          li = this.createLi();

      $drop.find('ul')[0].innerHTML = li;
      return $drop;
    },

    reloadLi: function () {
      // rebuild
      var li = this.createLi();
      this.$menuInner[0].innerHTML = li;
    },

    createLi: function () {
      var that = this,
          _li = [],
          optID = 0,
          titleOption = document.createElement('option'),
          liIndex = -1; // increment liIndex whenever a new <li> element is created to ensure liObj is correct

      // Helper functions
      /**
       * @param content
       * @param [index]
       * @param [classes]
       * @param [optgroup]
       * @returns {string}
       */
      var generateLI = function (content, index, classes, optgroup) {
        return '<li' +
            ((typeof classes !== 'undefined' & '' !== classes) ? ' class="' + classes + '"' : '') +
            ((typeof index !== 'undefined' & null !== index) ? ' data-original-index="' + index + '"' : '') +
            ((typeof optgroup !== 'undefined' & null !== optgroup) ? 'data-optgroup="' + optgroup + '"' : '') +
            '>' + content + '</li>';
      };

      /**
       * @param text
       * @param [classes]
       * @param [inline]
       * @param [tokens]
       * @returns {string}
       */
      var generateA = function (text, classes, inline, tokens) {
        return '<a tabindex="0"' +
            (typeof classes !== 'undefined' ? ' class="' + classes + '"' : '') +
            (inline ? ' style="' + inline + '"' : '') +
            (that.options.liveSearchNormalize ? ' data-normalized-text="' + normalizeToBase(htmlEscape($(text).html())) + '"' : '') +
            (typeof tokens !== 'undefined' || tokens !== null ? ' data-tokens="' + tokens + '"' : '') +
            ' role="option">' + text +
            '<span class="' + that.options.iconBase + ' ' + that.options.tickIcon + ' check-mark"></span>' +
            '</a>';
      };

      if (this.options.title && !this.multiple) {
        // this option doesn't create a new <li> element, but does add a new option, so liIndex is decreased
        // since liObj is recalculated on every refresh, liIndex needs to be decreased even if the titleOption is already appended
        liIndex--;

        if (!this.$element.find('.bs-title-option').length) {
          // Use native JS to prepend option (faster)
          var element = this.$element[0];
          titleOption.className = 'bs-title-option';
          titleOption.innerHTML = this.options.title;
          titleOption.value = '';
          element.insertBefore(titleOption, element.firstChild);
          // Check if selected or data-selected attribute is already set on an option. If not, select the titleOption option.
          // the selected item may have been changed by user or programmatically before the bootstrap select plugin runs,
          // if so, the select will have the data-selected attribute
          var $opt = $(element.options[element.selectedIndex]);
          if ($opt.attr('selected') === undefined && this.$element.data('selected') === undefined) {
            titleOption.selected = true;
          }
        }
      }

      var $selectOptions = this.$element.find('option');

      $selectOptions.each(function (index) {
        var $this = $(this);

        liIndex++;

        if ($this.hasClass('bs-title-option')) return;

        // Get the class and text for the option
        var optionClass = this.className || '',
            inline = htmlEscape(this.style.cssText),
            text = $this.data('content') ? $this.data('content') : $this.html(),
            tokens = $this.data('tokens') ? $this.data('tokens') : null,
            subtext = typeof $this.data('subtext') !== 'undefined' ? '<small class="text-muted">' + $this.data('subtext') + '</small>' : '',
            icon = typeof $this.data('icon') !== 'undefined' ? '<span class="' + that.options.iconBase + ' ' + $this.data('icon') + '"></span> ' : '',
            $parent = $this.parent(),
            isOptgroup = $parent[0].tagName === 'OPTGROUP',
            isOptgroupDisabled = isOptgroup && $parent[0].disabled,
            isDisabled = this.disabled || isOptgroupDisabled,
            prevHiddenIndex;

        if (icon !== '' && isDisabled) {
          icon = '<span>' + icon + '</span>';
        }

        if (that.options.hideDisabled && (isDisabled && !isOptgroup || isOptgroupDisabled)) {
          // set prevHiddenIndex - the index of the first hidden option in a group of hidden options
          // used to determine whether or not a divider should be placed after an optgroup if there are
          // hidden options between the optgroup and the first visible option
          prevHiddenIndex = $this.data('prevHiddenIndex');
          $this.next().data('prevHiddenIndex', (prevHiddenIndex !== undefined ? prevHiddenIndex : index));

          liIndex--;
          return;
        }

        if (!$this.data('content')) {
          // Prepend any icon and append any subtext to the main text.
          text = icon + '<span class="text">' + text + subtext + '</span>';
        }

        if (isOptgroup && $this.data('divider') !== true) {
          if (that.options.hideDisabled && isDisabled) {
            if ($parent.data('allOptionsDisabled') === undefined) {
              var $options = $parent.children();
              $parent.data('allOptionsDisabled', $options.filter(':disabled').length === $options.length);
            }

            if ($parent.data('allOptionsDisabled')) {
              liIndex--;
              return;
            }
          }

          var optGroupClass = ' ' + $parent[0].className || '';

          if ($this.index() === 0) { // Is it the first option of the optgroup?
            optID += 1;

            // Get the opt group label
            var label = $parent[0].label,
                labelSubtext = typeof $parent.data('subtext') !== 'undefined' ? '<small class="text-muted">' + $parent.data('subtext') + '</small>' : '',
                labelIcon = $parent.data('icon') ? '<span class="' + that.options.iconBase + ' ' + $parent.data('icon') + '"></span> ' : '';

            label = labelIcon + '<span class="text">' + htmlEscape(label) + labelSubtext + '</span>';

            if (index !== 0 && _li.length > 0) { // Is it NOT the first option of the select && are there elements in the dropdown?
              liIndex++;
              _li.push(generateLI('', null, 'divider', optID + 'div'));
            }
            liIndex++;
            _li.push(generateLI(label, null, 'dropdown-header' + optGroupClass, optID));
          }

          if (that.options.hideDisabled && isDisabled) {
            liIndex--;
            return;
          }

          _li.push(generateLI(generateA(text, 'opt ' + optionClass + optGroupClass, inline, tokens), index, '', optID));
        } else if ($this.data('divider') === true) {
          _li.push(generateLI('', index, 'divider'));
        } else if ($this.data('hidden') === true) {
          // set prevHiddenIndex - the index of the first hidden option in a group of hidden options
          // used to determine whether or not a divider should be placed after an optgroup if there are
          // hidden options between the optgroup and the first visible option
          prevHiddenIndex = $this.data('prevHiddenIndex');
          $this.next().data('prevHiddenIndex', (prevHiddenIndex !== undefined ? prevHiddenIndex : index));

          _li.push(generateLI(generateA(text, optionClass, inline, tokens), index, 'hidden is-hidden'));
        } else {
          var showDivider = this.previousElementSibling && this.previousElementSibling.tagName === 'OPTGROUP';

          // if previous element is not an optgroup and hideDisabled is true
          if (!showDivider && that.options.hideDisabled) {
            prevHiddenIndex = $this.data('prevHiddenIndex');

            if (prevHiddenIndex !== undefined) {
              // select the element **before** the first hidden element in the group
              var prevHidden = $selectOptions.eq(prevHiddenIndex)[0].previousElementSibling;
              
              if (prevHidden && prevHidden.tagName === 'OPTGROUP' && !prevHidden.disabled) {
                showDivider = true;
              }
            }
          }

          if (showDivider) {
            liIndex++;
            _li.push(generateLI('', null, 'divider', optID + 'div'));
          }
          _li.push(generateLI(generateA(text, optionClass, inline, tokens), index));
        }

        that.liObj[index] = liIndex;
      });

      //If we are not multiple, we don't have a selected item, and we don't have a title, select the first element so something is set in the button
      if (!this.multiple && this.$element.find('option:selected').length === 0 && !this.options.title) {
        this.$element.find('option').eq(0).prop('selected', true).attr('selected', 'selected');
      }

      return _li.join('');
    },

    findLis: function () {
      if (this.$lis == null) this.$lis = this.$menu.find('li');
      return this.$lis;
    },

    /**
     * @param [updateLi] defaults to true
     */
    render: function (updateLi) {
      var that = this,
          notDisabled,
          $selectOptions = this.$element.find('option');

      //Update the LI to match the SELECT
      if (updateLi !== false) {
        $selectOptions.each(function (index) {
          var $lis = that.findLis().eq(that.liObj[index]);

          that.setDisabled(index, this.disabled || this.parentNode.tagName === 'OPTGROUP' && this.parentNode.disabled, $lis);
          that.setSelected(index, this.selected, $lis);
        });
      }

      this.togglePlaceholder();

      this.tabIndex();

      var selectedItems = $selectOptions.map(function () {
        if (this.selected) {
          if (that.options.hideDisabled && (this.disabled || this.parentNode.tagName === 'OPTGROUP' && this.parentNode.disabled)) return;

          var $this = $(this),
              icon = $this.data('icon') && that.options.showIcon ? '<i class="' + that.options.iconBase + ' ' + $this.data('icon') + '"></i> ' : '',
              subtext;

          if (that.options.showSubtext && $this.data('subtext') && !that.multiple) {
            subtext = ' <small class="text-muted">' + $this.data('subtext') + '</small>';
          } else {
            subtext = '';
          }
          if (typeof $this.attr('title') !== 'undefined') {
            return $this.attr('title');
          } else if ($this.data('content') && that.options.showContent) {
            return $this.data('content').toString();
          } else {
            return icon + $this.html() + subtext;
          }
        }
      }).toArray();

      //Fixes issue in IE10 occurring when no default option is selected and at least one option is disabled
      //Convert all the values into a comma delimited string
      var title = !this.multiple ? selectedItems[0] : selectedItems.join(this.options.multipleSeparator);

      //If this is multi select, and the selectText type is count, the show 1 of 2 selected etc..
      if (this.multiple && this.options.selectedTextFormat.indexOf('count') > -1) {
        var max = this.options.selectedTextFormat.split('>');
        if ((max.length > 1 && selectedItems.length > max[1]) || (max.length == 1 && selectedItems.length >= 2)) {
          notDisabled = this.options.hideDisabled ? ', [disabled]' : '';
          var totalCount = $selectOptions.not('[data-divider="true"], [data-hidden="true"]' + notDisabled).length,
              tr8nText = (typeof this.options.countSelectedText === 'function') ? this.options.countSelectedText(selectedItems.length, totalCount) : this.options.countSelectedText;
          title = tr8nText.replace('{0}', selectedItems.length.toString()).replace('{1}', totalCount.toString());
        }
      }

      if (this.options.title == undefined) {
        this.options.title = this.$element.attr('title');
      }

      if (this.options.selectedTextFormat == 'static') {
        title = this.options.title;
      }

      //If we dont have a title, then use the default, or if nothing is set at all, use the not selected text
      if (!title) {
        title = typeof this.options.title !== 'undefined' ? this.options.title : this.options.noneSelectedText;
      }

      //strip all HTML tags and trim the result, then unescape any escaped tags
      this.$button.attr('title', htmlUnescape($.trim(title.replace(/<[^>]*>?/g, ''))));
      this.$button.children('.filter-option').html(title);

      this.$element.trigger('rendered.bs.select');
    },

    /**
     * @param [style]
     * @param [status]
     */
    setStyle: function (style, status) {
      if (this.$element.attr('class')) {
        this.$newElement.addClass(this.$element.attr('class').replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi, ''));
      }

      var buttonClass = style ? style : this.options.style;

      if (status == 'add') {
        this.$button.addClass(buttonClass);
      } else if (status == 'remove') {
        this.$button.removeClass(buttonClass);
      } else {
        this.$button.removeClass(this.options.style);
        this.$button.addClass(buttonClass);
      }
    },

    liHeight: function (refresh) {
      if (!refresh && (this.options.size === false || this.sizeInfo)) return;

      var newElement = document.createElement('div'),
          menu = document.createElement('div'),
          menuInner = document.createElement('ul'),
          divider = document.createElement('li'),
          li = document.createElement('li'),
          a = document.createElement('a'),
          text = document.createElement('span'),
          header = this.options.header && this.$menu.find('.popover-title').length > 0 ? this.$menu.find('.popover-title')[0].cloneNode(true) : null,
          search = this.options.liveSearch ? document.createElement('div') : null,
          actions = this.options.actionsBox && this.multiple && this.$menu.find('.bs-actionsbox').length > 0 ? this.$menu.find('.bs-actionsbox')[0].cloneNode(true) : null,
          doneButton = this.options.doneButton && this.multiple && this.$menu.find('.bs-donebutton').length > 0 ? this.$menu.find('.bs-donebutton')[0].cloneNode(true) : null;

      text.className = 'text';
      newElement.className = this.$menu[0].parentNode.className + ' open';
      menu.className = 'dropdown-menu open';
      menuInner.className = 'dropdown-menu inner';
      divider.className = 'divider';

      text.appendChild(document.createTextNode('Inner text'));
      a.appendChild(text);
      li.appendChild(a);
      menuInner.appendChild(li);
      menuInner.appendChild(divider);
      if (header) menu.appendChild(header);
      if (search) {
        var input = document.createElement('input');
        search.className = 'bs-searchbox';
        input.className = 'form-control';
        search.appendChild(input);
        menu.appendChild(search);
      }
      if (actions) menu.appendChild(actions);
      menu.appendChild(menuInner);
      if (doneButton) menu.appendChild(doneButton);
      newElement.appendChild(menu);

      document.body.appendChild(newElement);

      var liHeight = a.offsetHeight,
          headerHeight = header ? header.offsetHeight : 0,
          searchHeight = search ? search.offsetHeight : 0,
          actionsHeight = actions ? actions.offsetHeight : 0,
          doneButtonHeight = doneButton ? doneButton.offsetHeight : 0,
          dividerHeight = $(divider).outerHeight(true),
          // fall back to jQuery if getComputedStyle is not supported
          menuStyle = typeof getComputedStyle === 'function' ? getComputedStyle(menu) : false,
          $menu = menuStyle ? null : $(menu),
          menuPadding = {
            vert: parseInt(menuStyle ? menuStyle.paddingTop : $menu.css('paddingTop')) +
                  parseInt(menuStyle ? menuStyle.paddingBottom : $menu.css('paddingBottom')) +
                  parseInt(menuStyle ? menuStyle.borderTopWidth : $menu.css('borderTopWidth')) +
                  parseInt(menuStyle ? menuStyle.borderBottomWidth : $menu.css('borderBottomWidth')),
            horiz: parseInt(menuStyle ? menuStyle.paddingLeft : $menu.css('paddingLeft')) +
                  parseInt(menuStyle ? menuStyle.paddingRight : $menu.css('paddingRight')) +
                  parseInt(menuStyle ? menuStyle.borderLeftWidth : $menu.css('borderLeftWidth')) +
                  parseInt(menuStyle ? menuStyle.borderRightWidth : $menu.css('borderRightWidth'))
          },
          menuExtras =  {
            vert: menuPadding.vert +
                  parseInt(menuStyle ? menuStyle.marginTop : $menu.css('marginTop')) +
                  parseInt(menuStyle ? menuStyle.marginBottom : $menu.css('marginBottom')) + 2,
            horiz: menuPadding.horiz +
                  parseInt(menuStyle ? menuStyle.marginLeft : $menu.css('marginLeft')) +
                  parseInt(menuStyle ? menuStyle.marginRight : $menu.css('marginRight')) + 2
          }

      document.body.removeChild(newElement);

      this.sizeInfo = {
        liHeight: liHeight,
        headerHeight: headerHeight,
        searchHeight: searchHeight,
        actionsHeight: actionsHeight,
        doneButtonHeight: doneButtonHeight,
        dividerHeight: dividerHeight,
        menuPadding: menuPadding,
        menuExtras: menuExtras
      };
    },

    setSize: function () {
      this.findLis();
      this.liHeight();

      if (this.options.header) this.$menu.css('padding-top', 0);
      if (this.options.size === false) return;

      var that = this,
          $menu = this.$menu,
          $menuInner = this.$menuInner,
          $window = $(window),
          selectHeight = this.$newElement[0].offsetHeight,
          selectWidth = this.$newElement[0].offsetWidth,
          liHeight = this.sizeInfo['liHeight'],
          headerHeight = this.sizeInfo['headerHeight'],
          searchHeight = this.sizeInfo['searchHeight'],
          actionsHeight = this.sizeInfo['actionsHeight'],
          doneButtonHeight = this.sizeInfo['doneButtonHeight'],
          divHeight = this.sizeInfo['dividerHeight'],
          menuPadding = this.sizeInfo['menuPadding'],
          menuExtras = this.sizeInfo['menuExtras'],
          notDisabled = this.options.hideDisabled ? '.disabled' : '',
          menuHeight,
          menuWidth,
          getHeight,
          getWidth,
          selectOffsetTop,
          selectOffsetBot,
          selectOffsetLeft,
          selectOffsetRight,
          getPos = function() {
            var pos = that.$newElement.offset(),
                $container = $(that.options.container),
                containerPos;

            if (that.options.container && !$container.is('body')) {
              containerPos = $container.offset();
              containerPos.top += parseInt($container.css('borderTopWidth'));
              containerPos.left += parseInt($container.css('borderLeftWidth'));
            } else {
              containerPos = { top: 0, left: 0 };
            }

            var winPad = that.options.windowPadding;
            selectOffsetTop = pos.top - containerPos.top - $window.scrollTop();
            selectOffsetBot = $window.height() - selectOffsetTop - selectHeight - containerPos.top - winPad[2];
            selectOffsetLeft = pos.left - containerPos.left - $window.scrollLeft();
            selectOffsetRight = $window.width() - selectOffsetLeft - selectWidth - containerPos.left - winPad[1];
            selectOffsetTop -= winPad[0];
            selectOffsetLeft -= winPad[3];
          };

      getPos();

      if (this.options.size === 'auto') {
        var getSize = function () {
          var minHeight,
              hasClass = function (className, include) {
                return function (element) {
                    if (include) {
                        return (element.classList ? element.classList.contains(className) : $(element).hasClass(className));
                    } else {
                        return !(element.classList ? element.classList.contains(className) : $(element).hasClass(className));
                    }
                };
              },
              lis = that.$menuInner[0].getElementsByTagName('li'),
              lisVisible = Array.prototype.filter ? Array.prototype.filter.call(lis, hasClass('hidden', false)) : that.$lis.not('.hidden'),
              optGroup = Array.prototype.filter ? Array.prototype.filter.call(lisVisible, hasClass('dropdown-header', true)) : lisVisible.filter('.dropdown-header');

          getPos();
          menuHeight = selectOffsetBot - menuExtras.vert;
          menuWidth = selectOffsetRight - menuExtras.horiz;

          if (that.options.container) {
            if (!$menu.data('height')) $menu.data('height', $menu.height());
            getHeight = $menu.data('height');

            if (!$menu.data('width')) $menu.data('width', $menu.width());
            getWidth = $menu.data('width');
          } else {
            getHeight = $menu.height();
            getWidth = $menu.width();
          }

          if (that.options.dropupAuto) {
            that.$newElement.toggleClass('dropup', selectOffsetTop > selectOffsetBot && (menuHeight - menuExtras.vert) < getHeight);
          }

          if (that.$newElement.hasClass('dropup')) {
            menuHeight = selectOffsetTop - menuExtras.vert;
          }

          if (that.options.dropdownAlignRight === 'auto') {
            $menu.toggleClass('dropdown-menu-right', selectOffsetLeft > selectOffsetRight && (menuWidth - menuExtras.horiz) < (getWidth - selectWidth));
          }

          if ((lisVisible.length + optGroup.length) > 3) {
            minHeight = liHeight * 3 + menuExtras.vert - 2;
          } else {
            minHeight = 0;
          }

          $menu.css({
            'max-height': menuHeight + 'px',
            'overflow': 'hidden',
            'min-height': minHeight + headerHeight + searchHeight + actionsHeight + doneButtonHeight + 'px'
          });
          $menuInner.css({
            'max-height': menuHeight - headerHeight - searchHeight - actionsHeight - doneButtonHeight - menuPadding.vert + 'px',
            'overflow-y': 'auto',
            'min-height': Math.max(minHeight - menuPadding.vert, 0) + 'px'
          });
        };
        getSize();
        this.$searchbox.off('input.getSize propertychange.getSize').on('input.getSize propertychange.getSize', getSize);
        $window.off('resize.getSize scroll.getSize').on('resize.getSize scroll.getSize', getSize);
      } else if (this.options.size && this.options.size != 'auto' && this.$lis.not(notDisabled).length > this.options.size) {
        var optIndex = this.$lis.not('.divider').not(notDisabled).children().slice(0, this.options.size).last().parent().index(),
            divLength = this.$lis.slice(0, optIndex + 1).filter('.divider').length;
        menuHeight = liHeight * this.options.size + divLength * divHeight + menuPadding.vert;

        if (that.options.container) {
          if (!$menu.data('height')) $menu.data('height', $menu.height());
          getHeight = $menu.data('height');
        } else {
          getHeight = $menu.height();
        }

        if (that.options.dropupAuto) {
          //noinspection JSUnusedAssignment
          this.$newElement.toggleClass('dropup', selectOffsetTop > selectOffsetBot && (menuHeight - menuExtras.vert) < getHeight);
        }
        $menu.css({
          'max-height': menuHeight + headerHeight + searchHeight + actionsHeight + doneButtonHeight + 'px',
          'overflow': 'hidden',
          'min-height': ''
        });
        $menuInner.css({
          'max-height': menuHeight - menuPadding.vert + 'px',
          'overflow-y': 'auto',
          'min-height': ''
        });
      }
    },

    setWidth: function () {
      if (this.options.width === 'auto') {
        this.$menu.css('min-width', '0');

        // Get correct width if element is hidden
        var $selectClone = this.$menu.parent().clone().appendTo('body'),
            $selectClone2 = this.options.container ? this.$newElement.clone().appendTo('body') : $selectClone,
            ulWidth = $selectClone.children('.dropdown-menu').outerWidth(),
            btnWidth = $selectClone2.css('width', 'auto').children('button').outerWidth();

        $selectClone.remove();
        $selectClone2.remove();

        // Set width to whatever's larger, button title or longest option
        this.$newElement.css('width', Math.max(ulWidth, btnWidth) + 'px');
      } else if (this.options.width === 'fit') {
        // Remove inline min-width so width can be changed from 'auto'
        this.$menu.css('min-width', '');
        this.$newElement.css('width', '').addClass('fit-width');
      } else if (this.options.width) {
        // Remove inline min-width so width can be changed from 'auto'
        this.$menu.css('min-width', '');
        this.$newElement.css('width', this.options.width);
      } else {
        // Remove inline min-width/width so width can be changed
        this.$menu.css('min-width', '');
        this.$newElement.css('width', '');
      }
      // Remove fit-width class if width is changed programmatically
      if (this.$newElement.hasClass('fit-width') && this.options.width !== 'fit') {
        this.$newElement.removeClass('fit-width');
      }
    },

    selectPosition: function () {
      this.$bsContainer = $('<div class="bs-container" />');

      var that = this,
          $container = $(this.options.container),
          pos,
          containerPos,
          actualHeight,
          getPlacement = function ($element) {
            that.$bsContainer.addClass($element.attr('class').replace(/form-control|fit-width/gi, '')).toggleClass('dropup', $element.hasClass('dropup'));
            pos = $element.offset();

            if (!$container.is('body')) {
              containerPos = $container.offset();
              containerPos.top += parseInt($container.css('borderTopWidth')) - $container.scrollTop();
              containerPos.left += parseInt($container.css('borderLeftWidth')) - $container.scrollLeft();
            } else {
              containerPos = { top: 0, left: 0 };
            }

            actualHeight = $element.hasClass('dropup') ? 0 : $element[0].offsetHeight;

            that.$bsContainer.css({
              'top': pos.top - containerPos.top + actualHeight,
              'left': pos.left - containerPos.left,
              'width': $element[0].offsetWidth
            });
          };

      this.$button.on('click', function () {
        var $this = $(this);

        if (that.isDisabled()) {
          return;
        }

        getPlacement(that.$newElement);

        that.$bsContainer
          .appendTo(that.options.container)
          .toggleClass('open', !$this.hasClass('open'))
          .append(that.$menu);
      });

      $(window).on('resize scroll', function () {
        getPlacement(that.$newElement);
      });

      this.$element.on('hide.bs.select', function () {
        that.$menu.data('height', that.$menu.height());
        that.$bsContainer.detach();
      });
    },

    /**
     * @param {number} index - the index of the option that is being changed
     * @param {boolean} selected - true if the option is being selected, false if being deselected
     * @param {JQuery} $lis - the 'li' element that is being modified
     */
    setSelected: function (index, selected, $lis) {
      if (!$lis) {
        this.togglePlaceholder(); // check if setSelected is being called by changing the value of the select
        $lis = this.findLis().eq(this.liObj[index]);
      }

      $lis.toggleClass('selected', selected).find('a').attr('aria-selected', selected);
    },

    /**
     * @param {number} index - the index of the option that is being disabled
     * @param {boolean} disabled - true if the option is being disabled, false if being enabled
     * @param {JQuery} $lis - the 'li' element that is being modified
     */
    setDisabled: function (index, disabled, $lis) {
      if (!$lis) {
        $lis = this.findLis().eq(this.liObj[index]);
      }

      if (disabled) {
        $lis.addClass('disabled').children('a').attr('href', '#').attr('tabindex', -1).attr('aria-disabled', true);
      } else {
        $lis.removeClass('disabled').children('a').removeAttr('href').attr('tabindex', 0).attr('aria-disabled', false);
      }
    },

    isDisabled: function () {
      return this.$element[0].disabled;
    },

    checkDisabled: function () {
      var that = this;

      if (this.isDisabled()) {
        this.$newElement.addClass('disabled');
        this.$button.addClass('disabled').attr('tabindex', -1).attr('aria-disabled', true);
      } else {
        if (this.$button.hasClass('disabled')) {
          this.$newElement.removeClass('disabled');
          this.$button.removeClass('disabled').attr('aria-disabled', false);
        }

        if (this.$button.attr('tabindex') == -1 && !this.$element.data('tabindex')) {
          this.$button.removeAttr('tabindex');
        }
      }

      this.$button.click(function () {
        return !that.isDisabled();
      });
    },

    togglePlaceholder: function () {
      var value = this.$element.val();
      this.$button.toggleClass('bs-placeholder', value === null || value === '' || (value.constructor === Array && value.length === 0));
    },

    tabIndex: function () {
      if (this.$element.data('tabindex') !== this.$element.attr('tabindex') && 
        (this.$element.attr('tabindex') !== -98 && this.$element.attr('tabindex') !== '-98')) {
        this.$element.data('tabindex', this.$element.attr('tabindex'));
        this.$button.attr('tabindex', this.$element.data('tabindex'));
      }

      this.$element.attr('tabindex', -98);
    },

    clickListener: function () {
      var that = this,
          $document = $(document);

      $document.data('spaceSelect', false);

      this.$button.on('keyup', function (e) {
        if (/(32)/.test(e.keyCode.toString(10)) && $document.data('spaceSelect')) {
            e.preventDefault();
            $document.data('spaceSelect', false);
        }
      });

      this.$button.on('click', function () {
        that.setSize();
      });

      this.$element.on('shown.bs.select', function () {
        if (!that.options.liveSearch && !that.multiple) {
          that.$menuInner.find('.selected a').focus();
        } else if (!that.multiple) {
          var selectedIndex = that.liObj[that.$element[0].selectedIndex];

          if (typeof selectedIndex !== 'number' || that.options.size === false) return;

          // scroll to selected option
          var offset = that.$lis.eq(selectedIndex)[0].offsetTop - that.$menuInner[0].offsetTop;
          offset = offset - that.$menuInner[0].offsetHeight/2 + that.sizeInfo.liHeight/2;
          that.$menuInner[0].scrollTop = offset;
        }
      });

      this.$menuInner.on('click', 'li a', function (e) {
        var $this = $(this),
            clickedIndex = $this.parent().data('originalIndex'),
            prevValue = that.$element.val(),
            prevIndex = that.$element.prop('selectedIndex'),
            triggerChange = true;

        // Don't close on multi choice menu
        if (that.multiple && that.options.maxOptions !== 1) {
          e.stopPropagation();
        }

        e.preventDefault();

        //Don't run if we have been disabled
        if (!that.isDisabled() && !$this.parent().hasClass('disabled')) {
          var $options = that.$element.find('option'),
              $option = $options.eq(clickedIndex),
              state = $option.prop('selected'),
              $optgroup = $option.parent('optgroup'),
              maxOptions = that.options.maxOptions,
              maxOptionsGrp = $optgroup.data('maxOptions') || false;

          if (!that.multiple) { // Deselect all others if not multi select box
            $options.prop('selected', false);
            $option.prop('selected', true);
            that.$menuInner.find('.selected').removeClass('selected').find('a').attr('aria-selected', false);
            that.setSelected(clickedIndex, true);
          } else { // Toggle the one we have chosen if we are multi select.
            $option.prop('selected', !state);
            that.setSelected(clickedIndex, !state);
            $this.blur();

            if (maxOptions !== false || maxOptionsGrp !== false) {
              var maxReached = maxOptions < $options.filter(':selected').length,
                  maxReachedGrp = maxOptionsGrp < $optgroup.find('option:selected').length;

              if ((maxOptions && maxReached) || (maxOptionsGrp && maxReachedGrp)) {
                if (maxOptions && maxOptions == 1) {
                  $options.prop('selected', false);
                  $option.prop('selected', true);
                  that.$menuInner.find('.selected').removeClass('selected');
                  that.setSelected(clickedIndex, true);
                } else if (maxOptionsGrp && maxOptionsGrp == 1) {
                  $optgroup.find('option:selected').prop('selected', false);
                  $option.prop('selected', true);
                  var optgroupID = $this.parent().data('optgroup');
                  that.$menuInner.find('[data-optgroup="' + optgroupID + '"]').removeClass('selected');
                  that.setSelected(clickedIndex, true);
                } else {
                  var maxOptionsText = typeof that.options.maxOptionsText === 'string' ? [that.options.maxOptionsText, that.options.maxOptionsText] : that.options.maxOptionsText,
                      maxOptionsArr = typeof maxOptionsText === 'function' ? maxOptionsText(maxOptions, maxOptionsGrp) : maxOptionsText,
                      maxTxt = maxOptionsArr[0].replace('{n}', maxOptions),
                      maxTxtGrp = maxOptionsArr[1].replace('{n}', maxOptionsGrp),
                      $notify = $('<div class="notify"></div>');
                  // If {var} is set in array, replace it
                  /** @deprecated */
                  if (maxOptionsArr[2]) {
                    maxTxt = maxTxt.replace('{var}', maxOptionsArr[2][maxOptions > 1 ? 0 : 1]);
                    maxTxtGrp = maxTxtGrp.replace('{var}', maxOptionsArr[2][maxOptionsGrp > 1 ? 0 : 1]);
                  }

                  $option.prop('selected', false);

                  that.$menu.append($notify);

                  if (maxOptions && maxReached) {
                    $notify.append($('<div>' + maxTxt + '</div>'));
                    triggerChange = false;
                    that.$element.trigger('maxReached.bs.select');
                  }

                  if (maxOptionsGrp && maxReachedGrp) {
                    $notify.append($('<div>' + maxTxtGrp + '</div>'));
                    triggerChange = false;
                    that.$element.trigger('maxReachedGrp.bs.select');
                  }

                  setTimeout(function () {
                    that.setSelected(clickedIndex, false);
                  }, 10);

                  $notify.delay(750).fadeOut(300, function () {
                    $(this).remove();
                  });
                }
              }
            }
          }

          if (!that.multiple || (that.multiple && that.options.maxOptions === 1)) {
            that.$button.focus();
          } else if (that.options.liveSearch) {
            that.$searchbox.focus();
          }

          // Trigger select 'change'
          if (triggerChange) {
            if ((prevValue != that.$element.val() && that.multiple) || (prevIndex != that.$element.prop('selectedIndex') && !that.multiple)) {
              // $option.prop('selected') is current option state (selected/unselected). state is previous option state.
              changed_arguments = [clickedIndex, $option.prop('selected'), state];
              that.$element
                .triggerNative('change');
            }
          }
        }
      });

      this.$menu.on('click', 'li.disabled a, .popover-title, .popover-title :not(.close)', function (e) {
        if (e.currentTarget == this) {
          e.preventDefault();
          e.stopPropagation();
          if (that.options.liveSearch && !$(e.target).hasClass('close')) {
            that.$searchbox.focus();
          } else {
            that.$button.focus();
          }
        }
      });

      this.$menuInner.on('click', '.divider, .dropdown-header', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (that.options.liveSearch) {
          that.$searchbox.focus();
        } else {
          that.$button.focus();
        }
      });

      this.$menu.on('click', '.popover-title .close', function () {
        that.$button.click();
      });

      this.$searchbox.on('click', function (e) {
        e.stopPropagation();
      });

      this.$menu.on('click', '.actions-btn', function (e) {
        if (that.options.liveSearch) {
          that.$searchbox.focus();
        } else {
          that.$button.focus();
        }

        e.preventDefault();
        e.stopPropagation();

        if ($(this).hasClass('bs-select-all')) {
          that.selectAll();
        } else {
          that.deselectAll();
        }
      });

      this.$element.change(function () {
        that.render(false);
        that.$element.trigger('changed.bs.select', changed_arguments);
        changed_arguments = null;
      });
    },

    liveSearchListener: function () {
      var that = this,
          $no_results = $('<li class="no-results"></li>');

      this.$button.on('click.dropdown.data-api', function () {
        that.$menuInner.find('.active').removeClass('active');
        if (!!that.$searchbox.val()) {
          that.$searchbox.val('');
          that.$lis.not('.is-hidden').removeClass('hidden');
          if (!!$no_results.parent().length) $no_results.remove();
        }
        if (!that.multiple) that.$menuInner.find('.selected').addClass('active');
        setTimeout(function () {
          that.$searchbox.focus();
        }, 10);
      });

      this.$searchbox.on('click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api', function (e) {
        e.stopPropagation();
      });

      this.$searchbox.on('input propertychange', function () {
        that.$lis.not('.is-hidden').removeClass('hidden');
        that.$lis.filter('.active').removeClass('active');
        $no_results.remove();

        if (that.$searchbox.val()) {
          var $searchBase = that.$lis.not('.is-hidden, .divider, .dropdown-header'),
              $hideItems;
          if (that.options.liveSearchNormalize) {
            $hideItems = $searchBase.not(':a' + that._searchStyle() + '("' + normalizeToBase(that.$searchbox.val()) + '")');
          } else {
            $hideItems = $searchBase.not(':' + that._searchStyle() + '("' + that.$searchbox.val() + '")');
          }

          if ($hideItems.length === $searchBase.length) {
            $no_results.html(that.options.noneResultsText.replace('{0}', '"' + htmlEscape(that.$searchbox.val()) + '"'));
            that.$menuInner.append($no_results);
            that.$lis.addClass('hidden');
          } else {
            $hideItems.addClass('hidden');

            var $lisVisible = that.$lis.not('.hidden'),
                $foundDiv;

            // hide divider if first or last visible, or if followed by another divider
            $lisVisible.each(function (index) {
              var $this = $(this);

              if ($this.hasClass('divider')) {
                if ($foundDiv === undefined) {
                  $this.addClass('hidden');
                } else {
                  if ($foundDiv) $foundDiv.addClass('hidden');
                  $foundDiv = $this;
                }
              } else if ($this.hasClass('dropdown-header') && $lisVisible.eq(index + 1).data('optgroup') !== $this.data('optgroup')) {
                $this.addClass('hidden');
              } else {
                $foundDiv = null;
              }
            });
            if ($foundDiv) $foundDiv.addClass('hidden');

            $searchBase.not('.hidden').first().addClass('active');
            that.$menuInner.scrollTop(0);
          }
        }
      });
    },

    _searchStyle: function () {
      var styles = {
        begins: 'ibegins',
        startsWith: 'ibegins'
      };

      return styles[this.options.liveSearchStyle] || 'icontains';
    },

    val: function (value) {
      if (typeof value !== 'undefined') {
        this.$element.val(value);
        this.render();

        return this.$element;
      } else {
        return this.$element.val();
      }
    },

    changeAll: function (status) {
      if (!this.multiple) return;
      if (typeof status === 'undefined') status = true;

      this.findLis();

      var $options = this.$element.find('option'),
          $lisVisible = this.$lis.not('.divider, .dropdown-header, .disabled, .hidden'),
          lisVisLen = $lisVisible.length,
          selectedOptions = [];
          
      if (status) {
        if ($lisVisible.filter('.selected').length === $lisVisible.length) return;
      } else {
        if ($lisVisible.filter('.selected').length === 0) return;
      }
          
      $lisVisible.toggleClass('selected', status);

      for (var i = 0; i < lisVisLen; i++) {
        var origIndex = $lisVisible[i].getAttribute('data-original-index');
        selectedOptions[selectedOptions.length] = $options.eq(origIndex)[0];
      }

      $(selectedOptions).prop('selected', status);

      this.render(false);

      this.togglePlaceholder();

      this.$element
        .triggerNative('change');
    },

    selectAll: function () {
      return this.changeAll(true);
    },

    deselectAll: function () {
      return this.changeAll(false);
    },

    toggle: function (e) {
      e = e || window.event;

      if (e) e.stopPropagation();

      this.$button.trigger('click');
    },

    keydown: function (e) {
      var $this = $(this),
          $parent = $this.is('input') ? $this.parent().parent() : $this.parent(),
          $items,
          that = $parent.data('this'),
          index,
          prevIndex,
          isActive,
          selector = ':not(.disabled, .hidden, .dropdown-header, .divider)',
          keyCodeMap = {
            32: ' ',
            48: '0',
            49: '1',
            50: '2',
            51: '3',
            52: '4',
            53: '5',
            54: '6',
            55: '7',
            56: '8',
            57: '9',
            59: ';',
            65: 'a',
            66: 'b',
            67: 'c',
            68: 'd',
            69: 'e',
            70: 'f',
            71: 'g',
            72: 'h',
            73: 'i',
            74: 'j',
            75: 'k',
            76: 'l',
            77: 'm',
            78: 'n',
            79: 'o',
            80: 'p',
            81: 'q',
            82: 'r',
            83: 's',
            84: 't',
            85: 'u',
            86: 'v',
            87: 'w',
            88: 'x',
            89: 'y',
            90: 'z',
            96: '0',
            97: '1',
            98: '2',
            99: '3',
            100: '4',
            101: '5',
            102: '6',
            103: '7',
            104: '8',
            105: '9'
          };


      isActive = that.$newElement.hasClass('open');

      if (!isActive && (e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode >= 96 && e.keyCode <= 105 || e.keyCode >= 65 && e.keyCode <= 90)) {
        if (!that.options.container) {
          that.setSize();
          that.$menu.parent().addClass('open');
          isActive = true;
        } else {
          that.$button.trigger('click');
        }
        that.$searchbox.focus();
        return;
      }

      if (that.options.liveSearch) {
        if (/(^9$|27)/.test(e.keyCode.toString(10)) && isActive) {
          e.preventDefault();
          e.stopPropagation();
          that.$menuInner.click();
          that.$button.focus();
        }
      }

      if (/(38|40)/.test(e.keyCode.toString(10))) {
        $items = that.$lis.filter(selector);
        if (!$items.length) return;

        if (!that.options.liveSearch) {
          index = $items.index($items.find('a').filter(':focus').parent());
	    } else {
          index = $items.index($items.filter('.active'));
        }

        prevIndex = that.$menuInner.data('prevIndex');

        if (e.keyCode == 38) {
          if ((that.options.liveSearch || index == prevIndex) && index != -1) index--;
          if (index < 0) index += $items.length;
        } else if (e.keyCode == 40) {
          if (that.options.liveSearch || index == prevIndex) index++;
          index = index % $items.length;
        }

        that.$menuInner.data('prevIndex', index);

        if (!that.options.liveSearch) {
          $items.eq(index).children('a').focus();
        } else {
          e.preventDefault();
          if (!$this.hasClass('dropdown-toggle')) {
            $items.removeClass('active').eq(index).addClass('active').children('a').focus();
            $this.focus();
          }
        }

      } else if (!$this.is('input')) {
        var keyIndex = [],
            count,
            prevKey;

        $items = that.$lis.filter(selector);
        $items.each(function (i) {
          if ($.trim($(this).children('a').text().toLowerCase()).substring(0, 1) == keyCodeMap[e.keyCode]) {
            keyIndex.push(i);
          }
        });

        count = $(document).data('keycount');
        count++;
        $(document).data('keycount', count);

        prevKey = $.trim($(':focus').text().toLowerCase()).substring(0, 1);

        if (prevKey != keyCodeMap[e.keyCode]) {
          count = 1;
          $(document).data('keycount', count);
        } else if (count >= keyIndex.length) {
          $(document).data('keycount', 0);
          if (count > keyIndex.length) count = 1;
        }

        $items.eq(keyIndex[count - 1]).children('a').focus();
      }

      // Select focused option if "Enter", "Spacebar" or "Tab" (when selectOnTab is true) are pressed inside the menu.
      if ((/(13|32)/.test(e.keyCode.toString(10)) || (/(^9$)/.test(e.keyCode.toString(10)) && that.options.selectOnTab)) && isActive) {
        if (!/(32)/.test(e.keyCode.toString(10))) e.preventDefault();
        if (!that.options.liveSearch) {
          var elem = $(':focus');
          elem.click();
          // Bring back focus for multiselects
          elem.focus();
          // Prevent screen from scrolling if the user hit the spacebar
          e.preventDefault();
          // Fixes spacebar selection of dropdown items in FF & IE
          $(document).data('spaceSelect', true);
        } else if (!/(32)/.test(e.keyCode.toString(10))) {
          that.$menuInner.find('.active a').click();
          $this.focus();
        }
        $(document).data('keycount', 0);
      }

      if ((/(^9$|27)/.test(e.keyCode.toString(10)) && isActive && (that.multiple || that.options.liveSearch)) || (/(27)/.test(e.keyCode.toString(10)) && !isActive)) {
        that.$menu.parent().removeClass('open');
        if (that.options.container) that.$newElement.removeClass('open');
        that.$button.focus();
      }
    },

    mobile: function () {
      this.$element.addClass('mobile-device');
    },

    refresh: function () {
      this.$lis = null;
      this.liObj = {};
      this.reloadLi();
      this.render();
      this.checkDisabled();
      this.liHeight(true);
      this.setStyle();
      this.setWidth();
      if (this.$lis) this.$searchbox.trigger('propertychange');

      this.$element.trigger('refreshed.bs.select');
    },

    hide: function () {
      this.$newElement.hide();
    },

    show: function () {
      this.$newElement.show();
    },

    remove: function () {
      this.$newElement.remove();
      this.$element.remove();
    },

    destroy: function () {
      this.$newElement.before(this.$element).remove();

      if (this.$bsContainer) {
        this.$bsContainer.remove();
      } else {
        this.$menu.remove();
      }

      this.$element
        .off('.bs.select')
        .removeData('selectpicker')
        .removeClass('bs-select-hidden selectpicker');
    }
  };

  // SELECTPICKER PLUGIN DEFINITION
  // ==============================
  function Plugin(option) {
    // get the args of the outer function..
    var args = arguments;
    // The arguments of the function are explicitly re-defined from the argument list, because the shift causes them
    // to get lost/corrupted in android 2.3 and IE9 #715 #775
    var _option = option;

    [].shift.apply(args);

    var value;
    var chain = this.each(function () {
      var $this = $(this);
      if ($this.is('select')) {
        var data = $this.data('selectpicker'),
            options = typeof _option == 'object' && _option;

        if (!data) {
          var config = $.extend({}, Selectpicker.DEFAULTS, $.fn.selectpicker.defaults || {}, $this.data(), options);
          config.template = $.extend({}, Selectpicker.DEFAULTS.template, ($.fn.selectpicker.defaults ? $.fn.selectpicker.defaults.template : {}), $this.data().template, options.template);
          $this.data('selectpicker', (data = new Selectpicker(this, config)));
        } else if (options) {
          for (var i in options) {
            if (options.hasOwnProperty(i)) {
              data.options[i] = options[i];
            }
          }
        }

        if (typeof _option == 'string') {
          if (data[_option] instanceof Function) {
            value = data[_option].apply(data, args);
          } else {
            value = data.options[_option];
          }
        }
      }
    });

    if (typeof value !== 'undefined') {
      //noinspection JSUnusedAssignment
      return value;
    } else {
      return chain;
    }
  }

  var old = $.fn.selectpicker;
  $.fn.selectpicker = Plugin;
  $.fn.selectpicker.Constructor = Selectpicker;

  // SELECTPICKER NO CONFLICT
  // ========================
  $.fn.selectpicker.noConflict = function () {
    $.fn.selectpicker = old;
    return this;
  };

  $(document)
      .data('keycount', 0)
      .on('keydown.bs.select', '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="listbox"], .bs-searchbox input', Selectpicker.prototype.keydown)
      .on('focusin.modal', '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="listbox"], .bs-searchbox input', function (e) {
        e.stopPropagation();
      });

  // SELECTPICKER DATA-API
  // =====================
  $(window).on('load.bs.select.data-api', function () {
    $('.selectpicker').each(function () {
      var $selectpicker = $(this);
      Plugin.call($selectpicker, $selectpicker.data());
    })
  });
})(jQuery);


}));

/*!
 * Bootstrap v3.3.5 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(a){"use strict";var b=a.fn.jquery.split(" ")[0].split(".");if(b[0]<2&&b[1]<9||1==b[0]&&9==b[1]&&b[2]<1)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")}(jQuery),+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]};return!1}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one("bsTransitionEnd",function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b(),a.support.transition&&(a.event.special.bsTransitionEnd={bindType:a.support.transition.end,delegateType:a.support.transition.end,handle:function(b){return a(b.target).is(this)?b.handleObj.handler.apply(this,arguments):void 0}})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var c=a(this),e=c.data("bs.alert");e||c.data("bs.alert",e=new d(this)),"string"==typeof b&&e[b].call(c)})}var c='[data-dismiss="alert"]',d=function(b){a(b).on("click",c,this.close)};d.VERSION="3.3.5",d.TRANSITION_DURATION=150,d.prototype.close=function(b){function c(){g.detach().trigger("closed.bs.alert").remove()}var e=a(this),f=e.attr("data-target");f||(f=e.attr("href"),f=f&&f.replace(/.*(?=#[^\s]*$)/,""));var g=a(f);b&&b.preventDefault(),g.length||(g=e.closest(".alert")),g.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(g.removeClass("in"),a.support.transition&&g.hasClass("fade")?g.one("bsTransitionEnd",c).emulateTransitionEnd(d.TRANSITION_DURATION):c())};var e=a.fn.alert;a.fn.alert=b,a.fn.alert.Constructor=d,a.fn.alert.noConflict=function(){return a.fn.alert=e,this},a(document).on("click.bs.alert.data-api",c,d.prototype.close)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof b&&b;e||d.data("bs.button",e=new c(this,f)),"toggle"==b?e.toggle():b&&e.setState(b)})}var c=function(b,d){this.$element=a(b),this.options=a.extend({},c.DEFAULTS,d),this.isLoading=!1};c.VERSION="3.3.5",c.DEFAULTS={loadingText:"loading..."},c.prototype.setState=function(b){var c="disabled",d=this.$element,e=d.is("input")?"val":"html",f=d.data();b+="Text",null==f.resetText&&d.data("resetText",d[e]()),setTimeout(a.proxy(function(){d[e](null==f[b]?this.options[b]:f[b]),"loadingText"==b?(this.isLoading=!0,d.addClass(c).attr(c,c)):this.isLoading&&(this.isLoading=!1,d.removeClass(c).removeAttr(c))},this),0)},c.prototype.toggle=function(){var a=!0,b=this.$element.closest('[data-toggle="buttons"]');if(b.length){var c=this.$element.find("input");"radio"==c.prop("type")?(c.prop("checked")&&(a=!1),b.find(".active").removeClass("active"),this.$element.addClass("active")):"checkbox"==c.prop("type")&&(c.prop("checked")!==this.$element.hasClass("active")&&(a=!1),this.$element.toggleClass("active")),c.prop("checked",this.$element.hasClass("active")),a&&c.trigger("change")}else this.$element.attr("aria-pressed",!this.$element.hasClass("active")),this.$element.toggleClass("active")};var d=a.fn.button;a.fn.button=b,a.fn.button.Constructor=c,a.fn.button.noConflict=function(){return a.fn.button=d,this},a(document).on("click.bs.button.data-api",'[data-toggle^="button"]',function(c){var d=a(c.target);d.hasClass("btn")||(d=d.closest(".btn")),b.call(d,"toggle"),a(c.target).is('input[type="radio"]')||a(c.target).is('input[type="checkbox"]')||c.preventDefault()}).on("focus.bs.button.data-api blur.bs.button.data-api",'[data-toggle^="button"]',function(b){a(b.target).closest(".btn").toggleClass("focus",/^focus(in)?$/.test(b.type))})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},c.DEFAULTS,d.data(),"object"==typeof b&&b),g="string"==typeof b?b:f.slide;e||d.data("bs.carousel",e=new c(this,f)),"number"==typeof b?e.to(b):g?e[g]():f.interval&&e.pause().cycle()})}var c=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=null,this.sliding=null,this.interval=null,this.$active=null,this.$items=null,this.options.keyboard&&this.$element.on("keydown.bs.carousel",a.proxy(this.keydown,this)),"hover"==this.options.pause&&!("ontouchstart"in document.documentElement)&&this.$element.on("mouseenter.bs.carousel",a.proxy(this.pause,this)).on("mouseleave.bs.carousel",a.proxy(this.cycle,this))};c.VERSION="3.3.5",c.TRANSITION_DURATION=600,c.DEFAULTS={interval:5e3,pause:"hover",wrap:!0,keyboard:!0},c.prototype.keydown=function(a){if(!/input|textarea/i.test(a.target.tagName)){switch(a.which){case 37:this.prev();break;case 39:this.next();break;default:return}a.preventDefault()}},c.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},c.prototype.getItemIndex=function(a){return this.$items=a.parent().children(".item"),this.$items.index(a||this.$active)},c.prototype.getItemForDirection=function(a,b){var c=this.getItemIndex(b),d="prev"==a&&0===c||"next"==a&&c==this.$items.length-1;if(d&&!this.options.wrap)return b;var e="prev"==a?-1:1,f=(c+e)%this.$items.length;return this.$items.eq(f)},c.prototype.to=function(a){var b=this,c=this.getItemIndex(this.$active=this.$element.find(".item.active"));return a>this.$items.length-1||0>a?void 0:this.sliding?this.$element.one("slid.bs.carousel",function(){b.to(a)}):c==a?this.pause().cycle():this.slide(a>c?"next":"prev",this.$items.eq(a))},c.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},c.prototype.next=function(){return this.sliding?void 0:this.slide("next")},c.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},c.prototype.slide=function(b,d){var e=this.$element.find(".item.active"),f=d||this.getItemForDirection(b,e),g=this.interval,h="next"==b?"left":"right",i=this;if(f.hasClass("active"))return this.sliding=!1;var j=f[0],k=a.Event("slide.bs.carousel",{relatedTarget:j,direction:h});if(this.$element.trigger(k),!k.isDefaultPrevented()){if(this.sliding=!0,g&&this.pause(),this.$indicators.length){this.$indicators.find(".active").removeClass("active");var l=a(this.$indicators.children()[this.getItemIndex(f)]);l&&l.addClass("active")}var m=a.Event("slid.bs.carousel",{relatedTarget:j,direction:h});return a.support.transition&&this.$element.hasClass("slide")?(f.addClass(b),f[0].offsetWidth,e.addClass(h),f.addClass(h),e.one("bsTransitionEnd",function(){f.removeClass([b,h].join(" ")).addClass("active"),e.removeClass(["active",h].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger(m)},0)}).emulateTransitionEnd(c.TRANSITION_DURATION)):(e.removeClass("active"),f.addClass("active"),this.sliding=!1,this.$element.trigger(m)),g&&this.cycle(),this}};var d=a.fn.carousel;a.fn.carousel=b,a.fn.carousel.Constructor=c,a.fn.carousel.noConflict=function(){return a.fn.carousel=d,this};var e=function(c){var d,e=a(this),f=a(e.attr("data-target")||(d=e.attr("href"))&&d.replace(/.*(?=#[^\s]+$)/,""));if(f.hasClass("carousel")){var g=a.extend({},f.data(),e.data()),h=e.attr("data-slide-to");h&&(g.interval=!1),b.call(f,g),h&&f.data("bs.carousel").to(h),c.preventDefault()}};a(document).on("click.bs.carousel.data-api","[data-slide]",e).on("click.bs.carousel.data-api","[data-slide-to]",e),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var c=a(this);b.call(c,c.data())})})}(jQuery),+function(a){"use strict";function b(b){var c,d=b.attr("data-target")||(c=b.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"");return a(d)}function c(b){return this.each(function(){var c=a(this),e=c.data("bs.collapse"),f=a.extend({},d.DEFAULTS,c.data(),"object"==typeof b&&b);!e&&f.toggle&&/show|hide/.test(b)&&(f.toggle=!1),e||c.data("bs.collapse",e=new d(this,f)),"string"==typeof b&&e[b]()})}var d=function(b,c){this.$element=a(b),this.options=a.extend({},d.DEFAULTS,c),this.$trigger=a('[data-toggle="collapse"][href="#'+b.id+'"],[data-toggle="collapse"][data-target="#'+b.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()};d.VERSION="3.3.5",d.TRANSITION_DURATION=350,d.DEFAULTS={toggle:!0},d.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},d.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b,e=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");if(!(e&&e.length&&(b=e.data("bs.collapse"),b&&b.transitioning))){var f=a.Event("show.bs.collapse");if(this.$element.trigger(f),!f.isDefaultPrevented()){e&&e.length&&(c.call(e,"hide"),b||e.data("bs.collapse",null));var g=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;var h=function(){this.$element.removeClass("collapsing").addClass("collapse in")[g](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return h.call(this);var i=a.camelCase(["scroll",g].join("-"));this.$element.one("bsTransitionEnd",a.proxy(h,this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])}}}},d.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;var e=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")};return a.support.transition?void this.$element[c](0).one("bsTransitionEnd",a.proxy(e,this)).emulateTransitionEnd(d.TRANSITION_DURATION):e.call(this)}}},d.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()},d.prototype.getParent=function(){return a(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(a.proxy(function(c,d){var e=a(d);this.addAriaAndCollapsedClass(b(e),e)},this)).end()},d.prototype.addAriaAndCollapsedClass=function(a,b){var c=a.hasClass("in");a.attr("aria-expanded",c),b.toggleClass("collapsed",!c).attr("aria-expanded",c)};var e=a.fn.collapse;a.fn.collapse=c,a.fn.collapse.Constructor=d,a.fn.collapse.noConflict=function(){return a.fn.collapse=e,this},a(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(d){var e=a(this);e.attr("data-target")||d.preventDefault();var f=b(e),g=f.data("bs.collapse"),h=g?"toggle":e.data();c.call(f,h)})}(jQuery),+function(a){"use strict";function b(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#[A-Za-z]/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}function c(c){c&&3===c.which||(a(e).remove(),a(f).each(function(){var d=a(this),e=b(d),f={relatedTarget:this};e.hasClass("open")&&(c&&"click"==c.type&&/input|textarea/i.test(c.target.tagName)&&a.contains(e[0],c.target)||(e.trigger(c=a.Event("hide.bs.dropdown",f)),c.isDefaultPrevented()||(d.attr("aria-expanded","false"),e.removeClass("open").trigger("hidden.bs.dropdown",f))))}))}function d(b){return this.each(function(){var c=a(this),d=c.data("bs.dropdown");d||c.data("bs.dropdown",d=new g(this)),"string"==typeof b&&d[b].call(c)})}var e=".dropdown-backdrop",f='[data-toggle="dropdown"]',g=function(b){a(b).on("click.bs.dropdown",this.toggle)};g.VERSION="3.3.5",g.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=b(e),g=f.hasClass("open");if(c(),!g){"ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click",c);var h={relatedTarget:this};if(f.trigger(d=a.Event("show.bs.dropdown",h)),d.isDefaultPrevented())return;e.trigger("focus").attr("aria-expanded","true"),f.toggleClass("open").trigger("shown.bs.dropdown",h)}return!1}},g.prototype.keydown=function(c){if(/(38|40|27|32)/.test(c.which)&&!/input|textarea/i.test(c.target.tagName)){var d=a(this);if(c.preventDefault(),c.stopPropagation(),!d.is(".disabled, :disabled")){var e=b(d),g=e.hasClass("open");if(!g&&27!=c.which||g&&27==c.which)return 27==c.which&&e.find(f).trigger("focus"),d.trigger("click");var h=" li:not(.disabled):visible a",i=e.find(".dropdown-menu"+h);if(i.length){var j=i.index(c.target);38==c.which&&j>0&&j--,40==c.which&&j<i.length-1&&j++,~j||(j=0),i.eq(j).trigger("focus")}}}};var h=a.fn.dropdown;a.fn.dropdown=d,a.fn.dropdown.Constructor=g,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=h,this},a(document).on("click.bs.dropdown.data-api",c).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",f,g.prototype.toggle).on("keydown.bs.dropdown.data-api",f,g.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",g.prototype.keydown)}(jQuery),+function(a){"use strict";function b(b,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},c.DEFAULTS,e.data(),"object"==typeof b&&b);f||e.data("bs.modal",f=new c(this,g)),"string"==typeof b?f[b](d):g.show&&f.show(d)})}var c=function(b,c){this.options=c,this.$body=a(document.body),this.$element=a(b),this.$dialog=this.$element.find(".modal-dialog"),this.$backdrop=null,this.isShown=null,this.originalBodyPad=null,this.scrollbarWidth=0,this.ignoreBackdropClick=!1,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,a.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))};c.VERSION="3.3.5",c.TRANSITION_DURATION=300,c.BACKDROP_TRANSITION_DURATION=150,c.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},c.prototype.toggle=function(a){return this.isShown?this.hide():this.show(a)},c.prototype.show=function(b){var d=this,e=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(e),this.isShown||e.isDefaultPrevented()||(this.isShown=!0,this.checkScrollbar(),this.setScrollbar(),this.$body.addClass("modal-open"),this.escape(),this.resize(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.$dialog.on("mousedown.dismiss.bs.modal",function(){d.$element.one("mouseup.dismiss.bs.modal",function(b){a(b.target).is(d.$element)&&(d.ignoreBackdropClick=!0)})}),this.backdrop(function(){var e=a.support.transition&&d.$element.hasClass("fade");d.$element.parent().length||d.$element.appendTo(d.$body),d.$element.show().scrollTop(0),d.adjustDialog(),e&&d.$element[0].offsetWidth,d.$element.addClass("in"),d.enforceFocus();var f=a.Event("shown.bs.modal",{relatedTarget:b});e?d.$dialog.one("bsTransitionEnd",function(){d.$element.trigger("focus").trigger(f)}).emulateTransitionEnd(c.TRANSITION_DURATION):d.$element.trigger("focus").trigger(f)}))},c.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),this.resize(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),this.$dialog.off("mousedown.dismiss.bs.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",a.proxy(this.hideModal,this)).emulateTransitionEnd(c.TRANSITION_DURATION):this.hideModal())},c.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.trigger("focus")},this))},c.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keydown.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keydown.dismiss.bs.modal")},c.prototype.resize=function(){this.isShown?a(window).on("resize.bs.modal",a.proxy(this.handleUpdate,this)):a(window).off("resize.bs.modal")},c.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.$body.removeClass("modal-open"),a.resetAdjustments(),a.resetScrollbar(),a.$element.trigger("hidden.bs.modal")})},c.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},c.prototype.backdrop=function(b){var d=this,e=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var f=a.support.transition&&e;if(this.$backdrop=a(document.createElement("div")).addClass("modal-backdrop "+e).appendTo(this.$body),this.$element.on("click.dismiss.bs.modal",a.proxy(function(a){return this.ignoreBackdropClick?void(this.ignoreBackdropClick=!1):void(a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus():this.hide()))},this)),f&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;f?this.$backdrop.one("bsTransitionEnd",b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):b()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");var g=function(){d.removeBackdrop(),b&&b()};a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):g()}else b&&b()},c.prototype.handleUpdate=function(){this.adjustDialog()},c.prototype.adjustDialog=function(){var a=this.$element[0].scrollHeight>document.documentElement.clientHeight;this.$element.css({paddingLeft:!this.bodyIsOverflowing&&a?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!a?this.scrollbarWidth:""})},c.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})},c.prototype.checkScrollbar=function(){var a=window.innerWidth;if(!a){var b=document.documentElement.getBoundingClientRect();a=b.right-Math.abs(b.left)}this.bodyIsOverflowing=document.body.clientWidth<a,this.scrollbarWidth=this.measureScrollbar()},c.prototype.setScrollbar=function(){var a=parseInt(this.$body.css("padding-right")||0,10);this.originalBodyPad=document.body.style.paddingRight||"",this.bodyIsOverflowing&&this.$body.css("padding-right",a+this.scrollbarWidth)},c.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)},c.prototype.measureScrollbar=function(){var a=document.createElement("div");a.className="modal-scrollbar-measure",this.$body.append(a);var b=a.offsetWidth-a.clientWidth;return this.$body[0].removeChild(a),b};var d=a.fn.modal;a.fn.modal=b,a.fn.modal.Constructor=c,a.fn.modal.noConflict=function(){return a.fn.modal=d,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(c){var d=a(this),e=d.attr("href"),f=a(d.attr("data-target")||e&&e.replace(/.*(?=#[^\s]+$)/,"")),g=f.data("bs.modal")?"toggle":a.extend({remote:!/#/.test(e)&&e},f.data(),d.data());d.is("a")&&c.preventDefault(),f.one("show.bs.modal",function(a){a.isDefaultPrevented()||f.one("hidden.bs.modal",function(){d.is(":visible")&&d.trigger("focus")})}),b.call(f,g,this)})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof b&&b;(e||!/destroy|hide/.test(b))&&(e||d.data("bs.tooltip",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.type=null,this.options=null,this.enabled=null,this.timeout=null,this.hoverState=null,this.$element=null,this.inState=null,this.init("tooltip",a,b)};c.VERSION="3.3.5",c.TRANSITION_DURATION=150,c.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}},c.prototype.init=function(b,c,d){if(this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d),this.$viewport=this.options.viewport&&a(a.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):this.options.viewport.selector||this.options.viewport),this.inState={click:!1,hover:!1,focus:!1},this.$element[0]instanceof document.constructor&&!this.options.selector)throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!");for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focusin",i="hover"==g?"mouseleave":"focusout";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},c.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},c.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),b instanceof a.Event&&(c.inState["focusin"==b.type?"focus":"hover"]=!0),c.tip().hasClass("in")||"in"==c.hoverState?void(c.hoverState="in"):(clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?void(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show)):c.show())},c.prototype.isInStateTrue=function(){for(var a in this.inState)if(this.inState[a])return!0;return!1},c.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),b instanceof a.Event&&(c.inState["focusout"==b.type?"focus":"hover"]=!1),c.isInStateTrue()?void 0:(clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?void(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide)):c.hide())},c.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){this.$element.trigger(b);var d=a.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]);if(b.isDefaultPrevented()||!d)return;var e=this,f=this.tip(),g=this.getUID(this.type);this.setContent(),f.attr("id",g),this.$element.attr("aria-describedby",g),this.options.animation&&f.addClass("fade");var h="function"==typeof this.options.placement?this.options.placement.call(this,f[0],this.$element[0]):this.options.placement,i=/\s?auto?\s?/i,j=i.test(h);j&&(h=h.replace(i,"")||"top"),f.detach().css({top:0,left:0,display:"block"}).addClass(h).data("bs."+this.type,this),this.options.container?f.appendTo(this.options.container):f.insertAfter(this.$element),this.$element.trigger("inserted.bs."+this.type);var k=this.getPosition(),l=f[0].offsetWidth,m=f[0].offsetHeight;if(j){var n=h,o=this.getPosition(this.$viewport);h="bottom"==h&&k.bottom+m>o.bottom?"top":"top"==h&&k.top-m<o.top?"bottom":"right"==h&&k.right+l>o.width?"left":"left"==h&&k.left-l<o.left?"right":h,f.removeClass(n).addClass(h)}var p=this.getCalculatedOffset(h,k,l,m);this.applyPlacement(p,h);var q=function(){var a=e.hoverState;e.$element.trigger("shown.bs."+e.type),e.hoverState=null,"out"==a&&e.leave(e)};a.support.transition&&this.$tip.hasClass("fade")?f.one("bsTransitionEnd",q).emulateTransitionEnd(c.TRANSITION_DURATION):q()}},c.prototype.applyPlacement=function(b,c){var d=this.tip(),e=d[0].offsetWidth,f=d[0].offsetHeight,g=parseInt(d.css("margin-top"),10),h=parseInt(d.css("margin-left"),10);isNaN(g)&&(g=0),isNaN(h)&&(h=0),b.top+=g,b.left+=h,a.offset.setOffset(d[0],a.extend({using:function(a){d.css({top:Math.round(a.top),left:Math.round(a.left)})}},b),0),d.addClass("in");var i=d[0].offsetWidth,j=d[0].offsetHeight;"top"==c&&j!=f&&(b.top=b.top+f-j);var k=this.getViewportAdjustedDelta(c,b,i,j);k.left?b.left+=k.left:b.top+=k.top;var l=/top|bottom/.test(c),m=l?2*k.left-e+i:2*k.top-f+j,n=l?"offsetWidth":"offsetHeight";d.offset(b),this.replaceArrow(m,d[0][n],l)},c.prototype.replaceArrow=function(a,b,c){this.arrow().css(c?"left":"top",50*(1-a/b)+"%").css(c?"top":"left","")},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},c.prototype.hide=function(b){function d(){"in"!=e.hoverState&&f.detach(),e.$element.removeAttr("aria-describedby").trigger("hidden.bs."+e.type),b&&b()}var e=this,f=a(this.$tip),g=a.Event("hide.bs."+this.type);return this.$element.trigger(g),g.isDefaultPrevented()?void 0:(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one("bsTransitionEnd",d).emulateTransitionEnd(c.TRANSITION_DURATION):d(),this.hoverState=null,this)},c.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},c.prototype.hasContent=function(){return this.getTitle()},c.prototype.getPosition=function(b){b=b||this.$element;var c=b[0],d="BODY"==c.tagName,e=c.getBoundingClientRect();null==e.width&&(e=a.extend({},e,{width:e.right-e.left,height:e.bottom-e.top}));var f=d?{top:0,left:0}:b.offset(),g={scroll:d?document.documentElement.scrollTop||document.body.scrollTop:b.scrollTop()},h=d?{width:a(window).width(),height:a(window).height()}:null;return a.extend({},e,g,h,f)},c.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},c.prototype.getViewportAdjustedDelta=function(a,b,c,d){var e={top:0,left:0};if(!this.$viewport)return e;var f=this.options.viewport&&this.options.viewport.padding||0,g=this.getPosition(this.$viewport);if(/right|left/.test(a)){var h=b.top-f-g.scroll,i=b.top+f-g.scroll+d;h<g.top?e.top=g.top-h:i>g.top+g.height&&(e.top=g.top+g.height-i)}else{var j=b.left-f,k=b.left+f+c;j<g.left?e.left=g.left-j:k>g.right&&(e.left=g.left+g.width-k)}return e},c.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},c.prototype.getUID=function(a){do a+=~~(1e6*Math.random());while(document.getElementById(a));return a},c.prototype.tip=function(){if(!this.$tip&&(this.$tip=a(this.options.template),1!=this.$tip.length))throw new Error(this.type+" `template` option must consist of exactly 1 top-level element!");return this.$tip},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},c.prototype.enable=function(){this.enabled=!0},c.prototype.disable=function(){this.enabled=!1},c.prototype.toggleEnabled=function(){this.enabled=!this.enabled},c.prototype.toggle=function(b){var c=this;b&&(c=a(b.currentTarget).data("bs."+this.type),c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c))),b?(c.inState.click=!c.inState.click,c.isInStateTrue()?c.enter(c):c.leave(c)):c.tip().hasClass("in")?c.leave(c):c.enter(c)},c.prototype.destroy=function(){var a=this;clearTimeout(this.timeout),this.hide(function(){a.$element.off("."+a.type).removeData("bs."+a.type),a.$tip&&a.$tip.detach(),a.$tip=null,a.$arrow=null,a.$viewport=null})};var d=a.fn.tooltip;a.fn.tooltip=b,a.fn.tooltip.Constructor=c,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=d,this}}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof b&&b;(e||!/destroy|hide/.test(b))&&(e||d.data("bs.popover",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");c.VERSION="3.3.5",c.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),c.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),c.prototype.constructor=c,c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content").children().detach().end()[this.options.html?"string"==typeof c?"html":"append":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},c.prototype.hasContent=function(){return this.getTitle()||this.getContent()},c.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")};var d=a.fn.popover;a.fn.popover=b,a.fn.popover.Constructor=c,a.fn.popover.noConflict=function(){return a.fn.popover=d,this}}(jQuery),+function(a){"use strict";function b(c,d){this.$body=a(document.body),this.$scrollElement=a(a(c).is(document.body)?window:c),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||"")+" .nav li > a",this.offsets=[],this.targets=[],this.activeTarget=null,this.scrollHeight=0,this.$scrollElement.on("scroll.bs.scrollspy",a.proxy(this.process,this)),this.refresh(),this.process()}function c(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})}b.VERSION="3.3.5",b.DEFAULTS={offset:10},b.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)},b.prototype.refresh=function(){var b=this,c="offset",d=0;this.offsets=[],this.targets=[],this.scrollHeight=this.getScrollHeight(),a.isWindow(this.$scrollElement[0])||(c="position",d=this.$scrollElement.scrollTop()),this.$body.find(this.selector).map(function(){var b=a(this),e=b.data("target")||b.attr("href"),f=/^#./.test(e)&&a(e);return f&&f.length&&f.is(":visible")&&[[f[c]().top+d,e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){b.offsets.push(this[0]),b.targets.push(this[1])})},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.getScrollHeight(),d=this.options.offset+c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(this.scrollHeight!=c&&this.refresh(),b>=d)return g!=(a=f[f.length-1])&&this.activate(a);if(g&&b<e[0])return this.activeTarget=null,this.clear();for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(void 0===e[a+1]||b<e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,this.clear();var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),
d.trigger("activate.bs.scrollspy")},b.prototype.clear=function(){a(this.selector).parentsUntil(this.options.target,".active").removeClass("active")};var d=a.fn.scrollspy;a.fn.scrollspy=c,a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=d,this},a(window).on("load.bs.scrollspy.data-api",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);c.call(b,b.data())})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new c(this)),"string"==typeof b&&e[b]()})}var c=function(b){this.element=a(b)};c.VERSION="3.3.5",c.TRANSITION_DURATION=150,c.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.data("target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a"),f=a.Event("hide.bs.tab",{relatedTarget:b[0]}),g=a.Event("show.bs.tab",{relatedTarget:e[0]});if(e.trigger(f),b.trigger(g),!g.isDefaultPrevented()&&!f.isDefaultPrevented()){var h=a(d);this.activate(b.closest("li"),c),this.activate(h,h.parent(),function(){e.trigger({type:"hidden.bs.tab",relatedTarget:b[0]}),b.trigger({type:"shown.bs.tab",relatedTarget:e[0]})})}}},c.prototype.activate=function(b,d,e){function f(){g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),h?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu").length&&b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),e&&e()}var g=d.find("> .active"),h=e&&a.support.transition&&(g.length&&g.hasClass("fade")||!!d.find("> .fade").length);g.length&&h?g.one("bsTransitionEnd",f).emulateTransitionEnd(c.TRANSITION_DURATION):f(),g.removeClass("in")};var d=a.fn.tab;a.fn.tab=b,a.fn.tab.Constructor=c,a.fn.tab.noConflict=function(){return a.fn.tab=d,this};var e=function(c){c.preventDefault(),b.call(a(this),"show")};a(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',e).on("click.bs.tab.data-api",'[data-toggle="pill"]',e)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof b&&b;e||d.data("bs.affix",e=new c(this,f)),"string"==typeof b&&e[b]()})}var c=function(b,d){this.options=a.extend({},c.DEFAULTS,d),this.$target=a(this.options.target).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(b),this.affixed=null,this.unpin=null,this.pinnedOffset=null,this.checkPosition()};c.VERSION="3.3.5",c.RESET="affix affix-top affix-bottom",c.DEFAULTS={offset:0,target:window},c.prototype.getState=function(a,b,c,d){var e=this.$target.scrollTop(),f=this.$element.offset(),g=this.$target.height();if(null!=c&&"top"==this.affixed)return c>e?"top":!1;if("bottom"==this.affixed)return null!=c?e+this.unpin<=f.top?!1:"bottom":a-d>=e+g?!1:"bottom";var h=null==this.affixed,i=h?e:f.top,j=h?g:b;return null!=c&&c>=e?"top":null!=d&&i+j>=a-d?"bottom":!1},c.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(c.RESET).addClass("affix");var a=this.$target.scrollTop(),b=this.$element.offset();return this.pinnedOffset=b.top-a},c.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},c.prototype.checkPosition=function(){if(this.$element.is(":visible")){var b=this.$element.height(),d=this.options.offset,e=d.top,f=d.bottom,g=Math.max(a(document).height(),a(document.body).height());"object"!=typeof d&&(f=e=d),"function"==typeof e&&(e=d.top(this.$element)),"function"==typeof f&&(f=d.bottom(this.$element));var h=this.getState(g,b,e,f);if(this.affixed!=h){null!=this.unpin&&this.$element.css("top","");var i="affix"+(h?"-"+h:""),j=a.Event(i+".bs.affix");if(this.$element.trigger(j),j.isDefaultPrevented())return;this.affixed=h,this.unpin="bottom"==h?this.getPinnedOffset():null,this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix","affixed")+".bs.affix")}"bottom"==h&&this.$element.offset({top:g-b-f})}};var d=a.fn.affix;a.fn.affix=b,a.fn.affix.Constructor=c,a.fn.affix.noConflict=function(){return a.fn.affix=d,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var c=a(this),d=c.data();d.offset=d.offset||{},null!=d.offsetBottom&&(d.offset.bottom=d.offsetBottom),null!=d.offsetTop&&(d.offset.top=d.offsetTop),b.call(c,d)})})}(jQuery);
/**!
 * AngularJS file upload directives and services. Supports: file upload/drop/paste, resume, cancel/abort,
 * progress, resize, thumbnail, preview, validation and CORS
 * FileAPI Flash shim for old browsers not supporting FormData
 * @author  Danial  <danial.farid@gmail.com>
 * @version 12.2.13
 */

(function () {
  /** @namespace FileAPI.noContentTimeout */

  function patchXHR(fnName, newFn) {
    window.XMLHttpRequest.prototype[fnName] = newFn(window.XMLHttpRequest.prototype[fnName]);
  }

  function redefineProp(xhr, prop, fn) {
    try {
      Object.defineProperty(xhr, prop, {get: fn});
    } catch (e) {/*ignore*/
    }
  }

  if (!window.FileAPI) {
    window.FileAPI = {};
  }

  if (!window.XMLHttpRequest) {
    throw 'AJAX is not supported. XMLHttpRequest is not defined.';
  }

  FileAPI.shouldLoad = !window.FormData || FileAPI.forceLoad;
  if (FileAPI.shouldLoad) {
    var initializeUploadListener = function (xhr) {
      if (!xhr.__listeners) {
        if (!xhr.upload) xhr.upload = {};
        xhr.__listeners = [];
        var origAddEventListener = xhr.upload.addEventListener;
        xhr.upload.addEventListener = function (t, fn) {
          xhr.__listeners[t] = fn;
          if (origAddEventListener) origAddEventListener.apply(this, arguments);
        };
      }
    };

    patchXHR('open', function (orig) {
      return function (m, url, b) {
        initializeUploadListener(this);
        this.__url = url;
        try {
          orig.apply(this, [m, url, b]);
        } catch (e) {
          if (e.message.indexOf('Access is denied') > -1) {
            this.__origError = e;
            orig.apply(this, [m, '_fix_for_ie_crossdomain__', b]);
          }
        }
      };
    });

    patchXHR('getResponseHeader', function (orig) {
      return function (h) {
        return this.__fileApiXHR && this.__fileApiXHR.getResponseHeader ? this.__fileApiXHR.getResponseHeader(h) : (orig == null ? null : orig.apply(this, [h]));
      };
    });

    patchXHR('getAllResponseHeaders', function (orig) {
      return function () {
        return this.__fileApiXHR && this.__fileApiXHR.getAllResponseHeaders ? this.__fileApiXHR.getAllResponseHeaders() : (orig == null ? null : orig.apply(this));
      };
    });

    patchXHR('abort', function (orig) {
      return function () {
        return this.__fileApiXHR && this.__fileApiXHR.abort ? this.__fileApiXHR.abort() : (orig == null ? null : orig.apply(this));
      };
    });

    patchXHR('setRequestHeader', function (orig) {
      return function (header, value) {
        if (header === '__setXHR_') {
          initializeUploadListener(this);
          var val = value(this);
          // fix for angular < 1.2.0
          if (val instanceof Function) {
            val(this);
          }
        } else {
          this.__requestHeaders = this.__requestHeaders || {};
          this.__requestHeaders[header] = value;
          orig.apply(this, arguments);
        }
      };
    });

    patchXHR('send', function (orig) {
      return function () {
        var xhr = this;
        if (arguments[0] && arguments[0].__isFileAPIShim) {
          var formData = arguments[0];
          var config = {
            url: xhr.__url,
            jsonp: false, //removes the callback form param
            cache: true, //removes the ?fileapiXXX in the url
            complete: function (err, fileApiXHR) {
              if (err && angular.isString(err) && err.indexOf('#2174') !== -1) {
                // this error seems to be fine the file is being uploaded properly.
                err = null;
              }
              xhr.__completed = true;
              if (!err && xhr.__listeners.load)
                xhr.__listeners.load({
                  type: 'load',
                  loaded: xhr.__loaded,
                  total: xhr.__total,
                  target: xhr,
                  lengthComputable: true
                });
              if (!err && xhr.__listeners.loadend)
                xhr.__listeners.loadend({
                  type: 'loadend',
                  loaded: xhr.__loaded,
                  total: xhr.__total,
                  target: xhr,
                  lengthComputable: true
                });
              if (err === 'abort' && xhr.__listeners.abort)
                xhr.__listeners.abort({
                  type: 'abort',
                  loaded: xhr.__loaded,
                  total: xhr.__total,
                  target: xhr,
                  lengthComputable: true
                });
              if (fileApiXHR.status !== undefined) redefineProp(xhr, 'status', function () {
                return (fileApiXHR.status === 0 && err && err !== 'abort') ? 500 : fileApiXHR.status;
              });
              if (fileApiXHR.statusText !== undefined) redefineProp(xhr, 'statusText', function () {
                return fileApiXHR.statusText;
              });
              redefineProp(xhr, 'readyState', function () {
                return 4;
              });
              if (fileApiXHR.response !== undefined) redefineProp(xhr, 'response', function () {
                return fileApiXHR.response;
              });
              var resp = fileApiXHR.responseText || (err && fileApiXHR.status === 0 && err !== 'abort' ? err : undefined);
              redefineProp(xhr, 'responseText', function () {
                return resp;
              });
              redefineProp(xhr, 'response', function () {
                return resp;
              });
              if (err) redefineProp(xhr, 'err', function () {
                return err;
              });
              xhr.__fileApiXHR = fileApiXHR;
              if (xhr.onreadystatechange) xhr.onreadystatechange();
              if (xhr.onload) xhr.onload();
            },
            progress: function (e) {
              e.target = xhr;
              if (xhr.__listeners.progress) xhr.__listeners.progress(e);
              xhr.__total = e.total;
              xhr.__loaded = e.loaded;
              if (e.total === e.loaded) {
                // fix flash issue that doesn't call complete if there is no response text from the server
                var _this = this;
                setTimeout(function () {
                  if (!xhr.__completed) {
                    xhr.getAllResponseHeaders = function () {
                    };
                    _this.complete(null, {status: 204, statusText: 'No Content'});
                  }
                }, FileAPI.noContentTimeout || 10000);
              }
            },
            headers: xhr.__requestHeaders
          };
          config.data = {};
          config.files = {};
          for (var i = 0; i < formData.data.length; i++) {
            var item = formData.data[i];
            if (item.val != null && item.val.name != null && item.val.size != null && item.val.type != null) {
              config.files[item.key] = item.val;
            } else {
              config.data[item.key] = item.val;
            }
          }

          setTimeout(function () {
            if (!FileAPI.hasFlash) {
              throw 'Adode Flash Player need to be installed. To check ahead use "FileAPI.hasFlash"';
            }
            xhr.__fileApiXHR = FileAPI.upload(config);
          }, 1);
        } else {
          if (this.__origError) {
            throw this.__origError;
          }
          orig.apply(xhr, arguments);
        }
      };
    });
    window.XMLHttpRequest.__isFileAPIShim = true;
    window.FormData = FormData = function () {
      return {
        append: function (key, val, name) {
          if (val.__isFileAPIBlobShim) {
            val = val.data[0];
          }
          this.data.push({
            key: key,
            val: val,
            name: name
          });
        },
        data: [],
        __isFileAPIShim: true
      };
    };

    window.Blob = Blob = function (b) {
      return {
        data: b,
        __isFileAPIBlobShim: true
      };
    };
  }

})();

(function () {
  /** @namespace FileAPI.forceLoad */
  /** @namespace window.FileAPI.jsUrl */
  /** @namespace window.FileAPI.jsPath */

  function isInputTypeFile(elem) {
    return elem[0].tagName.toLowerCase() === 'input' && elem.attr('type') && elem.attr('type').toLowerCase() === 'file';
  }

  function hasFlash() {
    try {
      var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
      if (fo) return true;
    } catch (e) {
      if (navigator.mimeTypes['application/x-shockwave-flash'] !== undefined) return true;
    }
    return false;
  }

  function getOffset(obj) {
    var left = 0, top = 0;

    if (window.jQuery) {
      return jQuery(obj).offset();
    }

    if (obj.offsetParent) {
      do {
        left += (obj.offsetLeft - obj.scrollLeft);
        top += (obj.offsetTop - obj.scrollTop);
        obj = obj.offsetParent;
      } while (obj);
    }
    return {
      left: left,
      top: top
    };
  }

  if (FileAPI.shouldLoad) {
    FileAPI.hasFlash = hasFlash();

    //load FileAPI
    if (FileAPI.forceLoad) {
      FileAPI.html5 = false;
    }

    if (!FileAPI.upload) {
      var jsUrl, basePath, script = document.createElement('script'), allScripts = document.getElementsByTagName('script'), i, index, src;
      if (window.FileAPI.jsUrl) {
        jsUrl = window.FileAPI.jsUrl;
      } else if (window.FileAPI.jsPath) {
        basePath = window.FileAPI.jsPath;
      } else {
        for (i = 0; i < allScripts.length; i++) {
          src = allScripts[i].src;
          index = src.search(/\/ng\-file\-upload[\-a-zA-z0-9\.]*\.js/);
          if (index > -1) {
            basePath = src.substring(0, index + 1);
            break;
          }
        }
      }

      if (FileAPI.staticPath == null) FileAPI.staticPath = basePath;
      script.setAttribute('src', jsUrl || basePath + 'FileAPI.min.js');
      document.getElementsByTagName('head')[0].appendChild(script);
    }

    FileAPI.ngfFixIE = function (elem, fileElem, changeFn) {
      if (!hasFlash()) {
        throw 'Adode Flash Player need to be installed. To check ahead use "FileAPI.hasFlash"';
      }
      var fixInputStyle = function () {
        var label = fileElem.parent();
        if (elem.attr('disabled')) {
          if (label) label.removeClass('js-fileapi-wrapper');
        } else {
          if (!fileElem.attr('__ngf_flash_')) {
            fileElem.unbind('change');
            fileElem.unbind('click');
            fileElem.bind('change', function (evt) {
              fileApiChangeFn.apply(this, [evt]);
              changeFn.apply(this, [evt]);
            });
            fileElem.attr('__ngf_flash_', 'true');
          }
          label.addClass('js-fileapi-wrapper');
          if (!isInputTypeFile(elem)) {
            label.css('position', 'absolute')
              .css('top', getOffset(elem[0]).top + 'px').css('left', getOffset(elem[0]).left + 'px')
              .css('width', elem[0].offsetWidth + 'px').css('height', elem[0].offsetHeight + 'px')
              .css('filter', 'alpha(opacity=0)').css('display', elem.css('display'))
              .css('overflow', 'hidden').css('z-index', '900000')
              .css('visibility', 'visible');
            fileElem.css('width', elem[0].offsetWidth + 'px').css('height', elem[0].offsetHeight + 'px')
              .css('position', 'absolute').css('top', '0px').css('left', '0px');
          }
        }
      };

      elem.bind('mouseenter', fixInputStyle);

      var fileApiChangeFn = function (evt) {
        var files = FileAPI.getFiles(evt);
        //just a double check for #233
        for (var i = 0; i < files.length; i++) {
          if (files[i].size === undefined) files[i].size = 0;
          if (files[i].name === undefined) files[i].name = 'file';
          if (files[i].type === undefined) files[i].type = 'undefined';
        }
        if (!evt.target) {
          evt.target = {};
        }
        evt.target.files = files;
        // if evt.target.files is not writable use helper field
        if (evt.target.files !== files) {
          evt.__files_ = files;
        }
        (evt.__files_ || evt.target.files).item = function (i) {
          return (evt.__files_ || evt.target.files)[i] || null;
        };
      };
    };

    FileAPI.disableFileInput = function (elem, disable) {
      if (disable) {
        elem.removeClass('js-fileapi-wrapper');
      } else {
        elem.addClass('js-fileapi-wrapper');
      }
    };
  }
})();

if (!window.FileReader) {
  window.FileReader = function () {
    var _this = this, loadStarted = false;
    this.listeners = {};
    this.addEventListener = function (type, fn) {
      _this.listeners[type] = _this.listeners[type] || [];
      _this.listeners[type].push(fn);
    };
    this.removeEventListener = function (type, fn) {
      if (_this.listeners[type]) _this.listeners[type].splice(_this.listeners[type].indexOf(fn), 1);
    };
    this.dispatchEvent = function (evt) {
      var list = _this.listeners[evt.type];
      if (list) {
        for (var i = 0; i < list.length; i++) {
          list[i].call(_this, evt);
        }
      }
    };
    this.onabort = this.onerror = this.onload = this.onloadstart = this.onloadend = this.onprogress = null;

    var constructEvent = function (type, evt) {
      var e = {type: type, target: _this, loaded: evt.loaded, total: evt.total, error: evt.error};
      if (evt.result != null) e.target.result = evt.result;
      return e;
    };
    var listener = function (evt) {
      if (!loadStarted) {
        loadStarted = true;
        if (_this.onloadstart) _this.onloadstart(constructEvent('loadstart', evt));
      }
      var e;
      if (evt.type === 'load') {
        if (_this.onloadend) _this.onloadend(constructEvent('loadend', evt));
        e = constructEvent('load', evt);
        if (_this.onload) _this.onload(e);
        _this.dispatchEvent(e);
      } else if (evt.type === 'progress') {
        e = constructEvent('progress', evt);
        if (_this.onprogress) _this.onprogress(e);
        _this.dispatchEvent(e);
      } else {
        e = constructEvent('error', evt);
        if (_this.onerror) _this.onerror(e);
        _this.dispatchEvent(e);
      }
    };
    this.readAsDataURL = function (file) {
      FileAPI.readAsDataURL(file, listener);
    };
    this.readAsText = function (file) {
      FileAPI.readAsText(file, listener);
    };
  };
}
/**!
 * AngularJS file upload directives and services. Supoorts: file upload/drop/paste, resume, cancel/abort,
 * progress, resize, thumbnail, preview, validation and CORS
 * @author  Danial  <danial.farid@gmail.com>
 * @version 12.2.13
 */

if (window.XMLHttpRequest && !(window.FileAPI && FileAPI.shouldLoad)) {
  window.XMLHttpRequest.prototype.setRequestHeader = (function (orig) {
    return function (header, value) {
      if (header === '__setXHR_') {
        var val = value(this);
        // fix for angular < 1.2.0
        if (val instanceof Function) {
          val(this);
        }
      } else {
        orig.apply(this, arguments);
      }
    };
  })(window.XMLHttpRequest.prototype.setRequestHeader);
}

var ngFileUpload = angular.module('ngFileUpload', []);

ngFileUpload.version = '12.2.13';

ngFileUpload.service('UploadBase', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {
  var upload = this;
  upload.promisesCount = 0;

  this.isResumeSupported = function () {
    return window.Blob && window.Blob.prototype.slice;
  };

  var resumeSupported = this.isResumeSupported();

  function sendHttp(config) {
    config.method = config.method || 'POST';
    config.headers = config.headers || {};

    var deferred = config._deferred = config._deferred || $q.defer();
    var promise = deferred.promise;

    function notifyProgress(e) {
      if (deferred.notify) {
        deferred.notify(e);
      }
      if (promise.progressFunc) {
        $timeout(function () {
          promise.progressFunc(e);
        });
      }
    }

    function getNotifyEvent(n) {
      if (config._start != null && resumeSupported) {
        return {
          loaded: n.loaded + config._start,
          total: (config._file && config._file.size) || n.total,
          type: n.type, config: config,
          lengthComputable: true, target: n.target
        };
      } else {
        return n;
      }
    }

    if (!config.disableProgress) {
      config.headers.__setXHR_ = function () {
        return function (xhr) {
          if (!xhr || !xhr.upload || !xhr.upload.addEventListener) return;
          config.__XHR = xhr;
          if (config.xhrFn) config.xhrFn(xhr);
          xhr.upload.addEventListener('progress', function (e) {
            e.config = config;
            notifyProgress(getNotifyEvent(e));
          }, false);
          //fix for firefox not firing upload progress end, also IE8-9
          xhr.upload.addEventListener('load', function (e) {
            if (e.lengthComputable) {
              e.config = config;
              notifyProgress(getNotifyEvent(e));
            }
          }, false);
        };
      };
    }

    function uploadWithAngular() {
      $http(config).then(function (r) {
          if (resumeSupported && config._chunkSize && !config._finished && config._file) {
            var fileSize = config._file && config._file.size || 0;
            notifyProgress({
                loaded: Math.min(config._end, fileSize),
                total: fileSize,
                config: config,
                type: 'progress'
              }
            );
            upload.upload(config, true);
          } else {
            if (config._finished) delete config._finished;
            deferred.resolve(r);
          }
        }, function (e) {
          deferred.reject(e);
        }, function (n) {
          deferred.notify(n);
        }
      );
    }

    if (!resumeSupported) {
      uploadWithAngular();
    } else if (config._chunkSize && config._end && !config._finished) {
      config._start = config._end;
      config._end += config._chunkSize;
      uploadWithAngular();
    } else if (config.resumeSizeUrl) {
      $http.get(config.resumeSizeUrl).then(function (resp) {
        if (config.resumeSizeResponseReader) {
          config._start = config.resumeSizeResponseReader(resp.data);
        } else {
          config._start = parseInt((resp.data.size == null ? resp.data : resp.data.size).toString());
        }
        if (config._chunkSize) {
          config._end = config._start + config._chunkSize;
        }
        uploadWithAngular();
      }, function (e) {
        throw e;
      });
    } else if (config.resumeSize) {
      config.resumeSize().then(function (size) {
        config._start = size;
        if (config._chunkSize) {
          config._end = config._start + config._chunkSize;
        }
        uploadWithAngular();
      }, function (e) {
        throw e;
      });
    } else {
      if (config._chunkSize) {
        config._start = 0;
        config._end = config._start + config._chunkSize;
      }
      uploadWithAngular();
    }


    promise.success = function (fn) {
      promise.then(function (response) {
        fn(response.data, response.status, response.headers, config);
      });
      return promise;
    };

    promise.error = function (fn) {
      promise.then(null, function (response) {
        fn(response.data, response.status, response.headers, config);
      });
      return promise;
    };

    promise.progress = function (fn) {
      promise.progressFunc = fn;
      promise.then(null, null, function (n) {
        fn(n);
      });
      return promise;
    };
    promise.abort = promise.pause = function () {
      if (config.__XHR) {
        $timeout(function () {
          config.__XHR.abort();
        });
      }
      return promise;
    };
    promise.xhr = function (fn) {
      config.xhrFn = (function (origXhrFn) {
        return function () {
          if (origXhrFn) origXhrFn.apply(promise, arguments);
          fn.apply(promise, arguments);
        };
      })(config.xhrFn);
      return promise;
    };

    upload.promisesCount++;
    if (promise['finally'] && promise['finally'] instanceof Function) {
      promise['finally'](function () {
        upload.promisesCount--;
      });
    }
    return promise;
  }

  this.isUploadInProgress = function () {
    return upload.promisesCount > 0;
  };

  this.rename = function (file, name) {
    file.ngfName = name;
    return file;
  };

  this.jsonBlob = function (val) {
    if (val != null && !angular.isString(val)) {
      val = JSON.stringify(val);
    }
    var blob = new window.Blob([val], {type: 'application/json'});
    blob._ngfBlob = true;
    return blob;
  };

  this.json = function (val) {
    return angular.toJson(val);
  };

  function copy(obj) {
    var clone = {};
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        clone[key] = obj[key];
      }
    }
    return clone;
  }

  this.isFile = function (file) {
    return file != null && (file instanceof window.Blob || (file.flashId && file.name && file.size));
  };

  this.upload = function (config, internal) {
    function toResumeFile(file, formData) {
      if (file._ngfBlob) return file;
      config._file = config._file || file;
      if (config._start != null && resumeSupported) {
        if (config._end && config._end >= file.size) {
          config._finished = true;
          config._end = file.size;
        }
        var slice = file.slice(config._start, config._end || file.size);
        slice.name = file.name;
        slice.ngfName = file.ngfName;
        if (config._chunkSize) {
          formData.append('_chunkSize', config._chunkSize);
          formData.append('_currentChunkSize', config._end - config._start);
          formData.append('_chunkNumber', Math.floor(config._start / config._chunkSize));
          formData.append('_totalSize', config._file.size);
        }
        return slice;
      }
      return file;
    }

    function addFieldToFormData(formData, val, key) {
      if (val !== undefined) {
        if (angular.isDate(val)) {
          val = val.toISOString();
        }
        if (angular.isString(val)) {
          formData.append(key, val);
        } else if (upload.isFile(val)) {
          var file = toResumeFile(val, formData);
          var split = key.split(',');
          if (split[1]) {
            file.ngfName = split[1].replace(/^\s+|\s+$/g, '');
            key = split[0];
          }
          config._fileKey = config._fileKey || key;
          formData.append(key, file, file.ngfName || file.name);
        } else {
          if (angular.isObject(val)) {
            if (val.$$ngfCircularDetection) throw 'ngFileUpload: Circular reference in config.data. Make sure specified data for Upload.upload() has no circular reference: ' + key;

            val.$$ngfCircularDetection = true;
            try {
              for (var k in val) {
                if (val.hasOwnProperty(k) && k !== '$$ngfCircularDetection') {
                  var objectKey = config.objectKey == null ? '[i]' : config.objectKey;
                  if (val.length && parseInt(k) > -1) {
                    objectKey = config.arrayKey == null ? objectKey : config.arrayKey;
                  }
                  addFieldToFormData(formData, val[k], key + objectKey.replace(/[ik]/g, k));
                }
              }
            } finally {
              delete val.$$ngfCircularDetection;
            }
          } else {
            formData.append(key, val);
          }
        }
      }
    }

    function digestConfig() {
      config._chunkSize = upload.translateScalars(config.resumeChunkSize);
      config._chunkSize = config._chunkSize ? parseInt(config._chunkSize.toString()) : null;

      config.headers = config.headers || {};
      config.headers['Content-Type'] = undefined;
      config.transformRequest = config.transformRequest ?
        (angular.isArray(config.transformRequest) ?
          config.transformRequest : [config.transformRequest]) : [];
      config.transformRequest.push(function (data) {
        var formData = new window.FormData(), key;
        data = data || config.fields || {};
        if (config.file) {
          data.file = config.file;
        }
        for (key in data) {
          if (data.hasOwnProperty(key)) {
            var val = data[key];
            if (config.formDataAppender) {
              config.formDataAppender(formData, key, val);
            } else {
              addFieldToFormData(formData, val, key);
            }
          }
        }

        return formData;
      });
    }

    if (!internal) config = copy(config);
    if (!config._isDigested) {
      config._isDigested = true;
      digestConfig();
    }

    return sendHttp(config);
  };

  this.http = function (config) {
    config = copy(config);
    config.transformRequest = config.transformRequest || function (data) {
        if ((window.ArrayBuffer && data instanceof window.ArrayBuffer) || data instanceof window.Blob) {
          return data;
        }
        return $http.defaults.transformRequest[0].apply(this, arguments);
      };
    config._chunkSize = upload.translateScalars(config.resumeChunkSize);
    config._chunkSize = config._chunkSize ? parseInt(config._chunkSize.toString()) : null;

    return sendHttp(config);
  };

  this.translateScalars = function (str) {
    if (angular.isString(str)) {
      if (str.search(/kb/i) === str.length - 2) {
        return parseFloat(str.substring(0, str.length - 2) * 1024);
      } else if (str.search(/mb/i) === str.length - 2) {
        return parseFloat(str.substring(0, str.length - 2) * 1048576);
      } else if (str.search(/gb/i) === str.length - 2) {
        return parseFloat(str.substring(0, str.length - 2) * 1073741824);
      } else if (str.search(/b/i) === str.length - 1) {
        return parseFloat(str.substring(0, str.length - 1));
      } else if (str.search(/s/i) === str.length - 1) {
        return parseFloat(str.substring(0, str.length - 1));
      } else if (str.search(/m/i) === str.length - 1) {
        return parseFloat(str.substring(0, str.length - 1) * 60);
      } else if (str.search(/h/i) === str.length - 1) {
        return parseFloat(str.substring(0, str.length - 1) * 3600);
      }
    }
    return str;
  };

  this.urlToBlob = function(url) {
    var defer = $q.defer();
    $http({url: url, method: 'get', responseType: 'arraybuffer'}).then(function (resp) {
      var arrayBufferView = new Uint8Array(resp.data);
      var type = resp.headers('content-type') || 'image/WebP';
      var blob = new window.Blob([arrayBufferView], {type: type});
      var matches = url.match(/.*\/(.+?)(\?.*)?$/);
      if (matches.length > 1) {
        blob.name = matches[1];
      }
      defer.resolve(blob);
    }, function (e) {
      defer.reject(e);
    });
    return defer.promise;
  };

  this.setDefaults = function (defaults) {
    this.defaults = defaults || {};
  };

  this.defaults = {};
  this.version = ngFileUpload.version;
}

]);

ngFileUpload.service('Upload', ['$parse', '$timeout', '$compile', '$q', 'UploadExif', function ($parse, $timeout, $compile, $q, UploadExif) {
  var upload = UploadExif;
  upload.getAttrWithDefaults = function (attr, name) {
    if (attr[name] != null) return attr[name];
    var def = upload.defaults[name];
    return (def == null ? def : (angular.isString(def) ? def : JSON.stringify(def)));
  };

  upload.attrGetter = function (name, attr, scope, params) {
    var attrVal = this.getAttrWithDefaults(attr, name);
    if (scope) {
      try {
        if (params) {
          return $parse(attrVal)(scope, params);
        } else {
          return $parse(attrVal)(scope);
        }
      } catch (e) {
        // hangle string value without single qoute
        if (name.search(/min|max|pattern/i)) {
          return attrVal;
        } else {
          throw e;
        }
      }
    } else {
      return attrVal;
    }
  };

  upload.shouldUpdateOn = function (type, attr, scope) {
    var modelOptions = upload.attrGetter('ngfModelOptions', attr, scope);
    if (modelOptions && modelOptions.updateOn) {
      return modelOptions.updateOn.split(' ').indexOf(type) > -1;
    }
    return true;
  };

  upload.emptyPromise = function () {
    var d = $q.defer();
    var args = arguments;
    $timeout(function () {
      d.resolve.apply(d, args);
    });
    return d.promise;
  };

  upload.rejectPromise = function () {
    var d = $q.defer();
    var args = arguments;
    $timeout(function () {
      d.reject.apply(d, args);
    });
    return d.promise;
  };

  upload.happyPromise = function (promise, data) {
    var d = $q.defer();
    promise.then(function (result) {
      d.resolve(result);
    }, function (error) {
      $timeout(function () {
        throw error;
      });
      d.resolve(data);
    });
    return d.promise;
  };

  function applyExifRotations(files, attr, scope) {
    var promises = [upload.emptyPromise()];
    angular.forEach(files, function (f, i) {
      if (f.type.indexOf('image/jpeg') === 0 && upload.attrGetter('ngfFixOrientation', attr, scope, {$file: f})) {
        promises.push(upload.happyPromise(upload.applyExifRotation(f), f).then(function (fixedFile) {
          files.splice(i, 1, fixedFile);
        }));
      }
    });
    return $q.all(promises);
  }

  function resizeFile(files, attr, scope, ngModel) {
    var resizeVal = upload.attrGetter('ngfResize', attr, scope);
    if (!resizeVal || !upload.isResizeSupported() || !files.length) return upload.emptyPromise();
    if (resizeVal instanceof Function) {
      var defer = $q.defer();
      return resizeVal(files).then(function (p) {
        resizeWithParams(p, files, attr, scope, ngModel).then(function (r) {
          defer.resolve(r);
        }, function (e) {
          defer.reject(e);
        });
      }, function (e) {
        defer.reject(e);
      });
    } else {
      return resizeWithParams(resizeVal, files, attr, scope, ngModel);
    }
  }

  function resizeWithParams(params, files, attr, scope, ngModel) {
    var promises = [upload.emptyPromise()];

    function handleFile(f, i) {
      if (f.type.indexOf('image') === 0) {
        if (params.pattern && !upload.validatePattern(f, params.pattern)) return;
        params.resizeIf = function (width, height) {
          return upload.attrGetter('ngfResizeIf', attr, scope,
            {$width: width, $height: height, $file: f});
        };
        var promise = upload.resize(f, params);
        promises.push(promise);
        promise.then(function (resizedFile) {
          files.splice(i, 1, resizedFile);
        }, function (e) {
          f.$error = 'resize';
          (f.$errorMessages = (f.$errorMessages || {})).resize = true;
          f.$errorParam = (e ? (e.message ? e.message : e) + ': ' : '') + (f && f.name);
          ngModel.$ngfValidations.push({name: 'resize', valid: false});
          upload.applyModelValidation(ngModel, files);
        });
      }
    }

    for (var i = 0; i < files.length; i++) {
      handleFile(files[i], i);
    }
    return $q.all(promises);
  }

  upload.updateModel = function (ngModel, attr, scope, fileChange, files, evt, noDelay) {
    function update(files, invalidFiles, newFiles, dupFiles, isSingleModel) {
      attr.$$ngfPrevValidFiles = files;
      attr.$$ngfPrevInvalidFiles = invalidFiles;
      var file = files && files.length ? files[0] : null;
      var invalidFile = invalidFiles && invalidFiles.length ? invalidFiles[0] : null;

      if (ngModel) {
        upload.applyModelValidation(ngModel, files);
        ngModel.$setViewValue(isSingleModel ? file : files);
      }

      if (fileChange) {
        $parse(fileChange)(scope, {
          $files: files,
          $file: file,
          $newFiles: newFiles,
          $duplicateFiles: dupFiles,
          $invalidFiles: invalidFiles,
          $invalidFile: invalidFile,
          $event: evt
        });
      }

      var invalidModel = upload.attrGetter('ngfModelInvalid', attr);
      if (invalidModel) {
        $timeout(function () {
          $parse(invalidModel).assign(scope, isSingleModel ? invalidFile : invalidFiles);
        });
      }
      $timeout(function () {
        // scope apply changes
      });
    }

    var allNewFiles, dupFiles = [], prevValidFiles, prevInvalidFiles,
      invalids = [], valids = [];

    function removeDuplicates() {
      function equals(f1, f2) {
        return f1.name === f2.name && (f1.$ngfOrigSize || f1.size) === (f2.$ngfOrigSize || f2.size) &&
          f1.type === f2.type;
      }

      function isInPrevFiles(f) {
        var j;
        for (j = 0; j < prevValidFiles.length; j++) {
          if (equals(f, prevValidFiles[j])) {
            return true;
          }
        }
        for (j = 0; j < prevInvalidFiles.length; j++) {
          if (equals(f, prevInvalidFiles[j])) {
            return true;
          }
        }
        return false;
      }

      if (files) {
        allNewFiles = [];
        dupFiles = [];
        for (var i = 0; i < files.length; i++) {
          if (isInPrevFiles(files[i])) {
            dupFiles.push(files[i]);
          } else {
            allNewFiles.push(files[i]);
          }
        }
      }
    }

    function toArray(v) {
      return angular.isArray(v) ? v : [v];
    }

    function resizeAndUpdate() {
      function updateModel() {
        $timeout(function () {
          update(keep ? prevValidFiles.concat(valids) : valids,
            keep ? prevInvalidFiles.concat(invalids) : invalids,
            files, dupFiles, isSingleModel);
        }, options && options.debounce ? options.debounce.change || options.debounce : 0);
      }

      var resizingFiles = validateAfterResize ? allNewFiles : valids;
      resizeFile(resizingFiles, attr, scope, ngModel).then(function () {
        if (validateAfterResize) {
          upload.validate(allNewFiles, keep ? prevValidFiles.length : 0, ngModel, attr, scope)
            .then(function (validationResult) {
              valids = validationResult.validsFiles;
              invalids = validationResult.invalidsFiles;
              updateModel();
            });
        } else {
          updateModel();
        }
      }, function () {
        for (var i = 0; i < resizingFiles.length; i++) {
          var f = resizingFiles[i];
          if (f.$error === 'resize') {
            var index = valids.indexOf(f);
            if (index > -1) {
              valids.splice(index, 1);
              invalids.push(f);
            }
            updateModel();
          }
        }
      });
    }

    prevValidFiles = attr.$$ngfPrevValidFiles || [];
    prevInvalidFiles = attr.$$ngfPrevInvalidFiles || [];
    if (ngModel && ngModel.$modelValue) {
      prevValidFiles = toArray(ngModel.$modelValue);
    }

    var keep = upload.attrGetter('ngfKeep', attr, scope);
    allNewFiles = (files || []).slice(0);
    if (keep === 'distinct' || upload.attrGetter('ngfKeepDistinct', attr, scope) === true) {
      removeDuplicates(attr, scope);
    }

    var isSingleModel = !keep && !upload.attrGetter('ngfMultiple', attr, scope) && !upload.attrGetter('multiple', attr);

    if (keep && !allNewFiles.length) return;

    upload.attrGetter('ngfBeforeModelChange', attr, scope, {
      $files: files,
      $file: files && files.length ? files[0] : null,
      $newFiles: allNewFiles,
      $duplicateFiles: dupFiles,
      $event: evt
    });

    var validateAfterResize = upload.attrGetter('ngfValidateAfterResize', attr, scope);

    var options = upload.attrGetter('ngfModelOptions', attr, scope);
    upload.validate(allNewFiles, keep ? prevValidFiles.length : 0, ngModel, attr, scope)
      .then(function (validationResult) {
      if (noDelay) {
        update(allNewFiles, [], files, dupFiles, isSingleModel);
      } else {
        if ((!options || !options.allowInvalid) && !validateAfterResize) {
          valids = validationResult.validFiles;
          invalids = validationResult.invalidFiles;
        } else {
          valids = allNewFiles;
        }
        if (upload.attrGetter('ngfFixOrientation', attr, scope) && upload.isExifSupported()) {
          applyExifRotations(valids, attr, scope).then(function () {
            resizeAndUpdate();
          });
        } else {
          resizeAndUpdate();
        }
      }
    });
  };

  return upload;
}]);

ngFileUpload.directive('ngfSelect', ['$parse', '$timeout', '$compile', 'Upload', function ($parse, $timeout, $compile, Upload) {
  var generatedElems = [];

  function isDelayedClickSupported(ua) {
    // fix for android native browser < 4.4 and safari windows
    var m = ua.match(/Android[^\d]*(\d+)\.(\d+)/);
    if (m && m.length > 2) {
      var v = Upload.defaults.androidFixMinorVersion || 4;
      return parseInt(m[1]) < 4 || (parseInt(m[1]) === v && parseInt(m[2]) < v);
    }

    // safari on windows
    return ua.indexOf('Chrome') === -1 && /.*Windows.*Safari.*/.test(ua);
  }

  function linkFileSelect(scope, elem, attr, ngModel, $parse, $timeout, $compile, upload) {
    /** @namespace attr.ngfSelect */
    /** @namespace attr.ngfChange */
    /** @namespace attr.ngModel */
    /** @namespace attr.ngfModelOptions */
    /** @namespace attr.ngfMultiple */
    /** @namespace attr.ngfCapture */
    /** @namespace attr.ngfValidate */
    /** @namespace attr.ngfKeep */
    var attrGetter = function (name, scope) {
      return upload.attrGetter(name, attr, scope);
    };

    function isInputTypeFile() {
      return elem[0].tagName.toLowerCase() === 'input' && attr.type && attr.type.toLowerCase() === 'file';
    }

    function fileChangeAttr() {
      return attrGetter('ngfChange') || attrGetter('ngfSelect');
    }

    function changeFn(evt) {
      if (upload.shouldUpdateOn('change', attr, scope)) {
        var fileList = evt.__files_ || (evt.target && evt.target.files), files = [];
        /* Handle duplicate call in  IE11 */
        if (!fileList) return;
        for (var i = 0; i < fileList.length; i++) {
          files.push(fileList[i]);
        }
        upload.updateModel(ngModel, attr, scope, fileChangeAttr(),
          files.length ? files : null, evt);
      }
    }

    upload.registerModelChangeValidator(ngModel, attr, scope);

    var unwatches = [];
    if (attrGetter('ngfMultiple')) {
      unwatches.push(scope.$watch(attrGetter('ngfMultiple'), function () {
        fileElem.attr('multiple', attrGetter('ngfMultiple', scope));
      }));
    }
    if (attrGetter('ngfCapture')) {
      unwatches.push(scope.$watch(attrGetter('ngfCapture'), function () {
        fileElem.attr('capture', attrGetter('ngfCapture', scope));
      }));
    }
    if (attrGetter('ngfAccept')) {
      unwatches.push(scope.$watch(attrGetter('ngfAccept'), function () {
        fileElem.attr('accept', attrGetter('ngfAccept', scope));
      }));
    }
    unwatches.push(attr.$observe('accept', function () {
      fileElem.attr('accept', attrGetter('accept'));
    }));
    function bindAttrToFileInput(fileElem, label) {
      function updateId(val) {
        fileElem.attr('id', 'ngf-' + val);
        label.attr('id', 'ngf-label-' + val);
      }

      for (var i = 0; i < elem[0].attributes.length; i++) {
        var attribute = elem[0].attributes[i];
        if (attribute.name !== 'type' && attribute.name !== 'class' && attribute.name !== 'style') {
          if (attribute.name === 'id') {
            updateId(attribute.value);
            unwatches.push(attr.$observe('id', updateId));
          } else {
            fileElem.attr(attribute.name, (!attribute.value && (attribute.name === 'required' ||
            attribute.name === 'multiple')) ? attribute.name : attribute.value);
          }
        }
      }
    }

    function createFileInput() {
      if (isInputTypeFile()) {
        return elem;
      }

      var fileElem = angular.element('<input type="file">');

      var label = angular.element('<label>upload</label>');
      label.css('visibility', 'hidden').css('position', 'absolute').css('overflow', 'hidden')
        .css('width', '0px').css('height', '0px').css('border', 'none')
        .css('margin', '0px').css('padding', '0px').attr('tabindex', '-1');
      bindAttrToFileInput(fileElem, label);

      generatedElems.push({el: elem, ref: label});

      document.body.appendChild(label.append(fileElem)[0]);

      return fileElem;
    }

    function clickHandler(evt) {
      if (elem.attr('disabled')) return false;
      if (attrGetter('ngfSelectDisabled', scope)) return;

      var r = detectSwipe(evt);
      // prevent the click if it is a swipe
      if (r != null) return r;

      resetModel(evt);

      // fix for md when the element is removed from the DOM and added back #460
      try {
        if (!isInputTypeFile() && !document.body.contains(fileElem[0])) {
          generatedElems.push({el: elem, ref: fileElem.parent()});
          document.body.appendChild(fileElem.parent()[0]);
          fileElem.bind('change', changeFn);
        }
      } catch (e) {/*ignore*/
      }

      if (isDelayedClickSupported(navigator.userAgent)) {
        setTimeout(function () {
          fileElem[0].click();
        }, 0);
      } else {
        fileElem[0].click();
      }

      return false;
    }


    var initialTouchStartY = 0;
    var initialTouchStartX = 0;

    function detectSwipe(evt) {
      var touches = evt.changedTouches || (evt.originalEvent && evt.originalEvent.changedTouches);
      if (touches) {
        if (evt.type === 'touchstart') {
          initialTouchStartX = touches[0].clientX;
          initialTouchStartY = touches[0].clientY;
          return true; // don't block event default
        } else {
          // prevent scroll from triggering event
          if (evt.type === 'touchend') {
            var currentX = touches[0].clientX;
            var currentY = touches[0].clientY;
            if ((Math.abs(currentX - initialTouchStartX) > 20) ||
              (Math.abs(currentY - initialTouchStartY) > 20)) {
              evt.stopPropagation();
              evt.preventDefault();
              return false;
            }
          }
          return true;
        }
      }
    }

    var fileElem = elem;

    function resetModel(evt) {
      if (upload.shouldUpdateOn('click', attr, scope) && fileElem.val()) {
        fileElem.val(null);
        upload.updateModel(ngModel, attr, scope, fileChangeAttr(), null, evt, true);
      }
    }

    if (!isInputTypeFile()) {
      fileElem = createFileInput();
    }
    fileElem.bind('change', changeFn);

    if (!isInputTypeFile()) {
      elem.bind('click touchstart touchend', clickHandler);
    } else {
      elem.bind('click', resetModel);
    }

    function ie10SameFileSelectFix(evt) {
      if (fileElem && !fileElem.attr('__ngf_ie10_Fix_')) {
        if (!fileElem[0].parentNode) {
          fileElem = null;
          return;
        }
        evt.preventDefault();
        evt.stopPropagation();
        fileElem.unbind('click');
        var clone = fileElem.clone();
        fileElem.replaceWith(clone);
        fileElem = clone;
        fileElem.attr('__ngf_ie10_Fix_', 'true');
        fileElem.bind('change', changeFn);
        fileElem.bind('click', ie10SameFileSelectFix);
        fileElem[0].click();
        return false;
      } else {
        fileElem.removeAttr('__ngf_ie10_Fix_');
      }
    }

    if (navigator.appVersion.indexOf('MSIE 10') !== -1) {
      fileElem.bind('click', ie10SameFileSelectFix);
    }

    if (ngModel) ngModel.$formatters.push(function (val) {
      if (val == null || val.length === 0) {
        if (fileElem.val()) {
          fileElem.val(null);
        }
      }
      return val;
    });

    scope.$on('$destroy', function () {
      if (!isInputTypeFile()) fileElem.parent().remove();
      angular.forEach(unwatches, function (unwatch) {
        unwatch();
      });
    });

    $timeout(function () {
      for (var i = 0; i < generatedElems.length; i++) {
        var g = generatedElems[i];
        if (!document.body.contains(g.el[0])) {
          generatedElems.splice(i, 1);
          g.ref.remove();
        }
      }
    });

    if (window.FileAPI && window.FileAPI.ngfFixIE) {
      window.FileAPI.ngfFixIE(elem, fileElem, changeFn);
    }
  }

  return {
    restrict: 'AEC',
    require: '?ngModel',
    link: function (scope, elem, attr, ngModel) {
      linkFileSelect(scope, elem, attr, ngModel, $parse, $timeout, $compile, Upload);
    }
  };
}]);

(function () {

  ngFileUpload.service('UploadDataUrl', ['UploadBase', '$timeout', '$q', function (UploadBase, $timeout, $q) {
    var upload = UploadBase;
    upload.base64DataUrl = function (file) {
      if (angular.isArray(file)) {
        var d = $q.defer(), count = 0;
        angular.forEach(file, function (f) {
          upload.dataUrl(f, true)['finally'](function () {
            count++;
            if (count === file.length) {
              var urls = [];
              angular.forEach(file, function (ff) {
                urls.push(ff.$ngfDataUrl);
              });
              d.resolve(urls, file);
            }
          });
        });
        return d.promise;
      } else {
        return upload.dataUrl(file, true);
      }
    };
    upload.dataUrl = function (file, disallowObjectUrl) {
      if (!file) return upload.emptyPromise(file, file);
      if ((disallowObjectUrl && file.$ngfDataUrl != null) || (!disallowObjectUrl && file.$ngfBlobUrl != null)) {
        return upload.emptyPromise(disallowObjectUrl ? file.$ngfDataUrl : file.$ngfBlobUrl, file);
      }
      var p = disallowObjectUrl ? file.$$ngfDataUrlPromise : file.$$ngfBlobUrlPromise;
      if (p) return p;

      var deferred = $q.defer();
      $timeout(function () {
        if (window.FileReader && file &&
          (!window.FileAPI || navigator.userAgent.indexOf('MSIE 8') === -1 || file.size < 20000) &&
          (!window.FileAPI || navigator.userAgent.indexOf('MSIE 9') === -1 || file.size < 4000000)) {
          //prefer URL.createObjectURL for handling refrences to files of all sizes
          //since it doesn´t build a large string in memory
          var URL = window.URL || window.webkitURL;
          if (URL && URL.createObjectURL && !disallowObjectUrl) {
            var url;
            try {
              url = URL.createObjectURL(file);
            } catch (e) {
              $timeout(function () {
                file.$ngfBlobUrl = '';
                deferred.reject();
              });
              return;
            }
            $timeout(function () {
              file.$ngfBlobUrl = url;
              if (url) {
                deferred.resolve(url, file);
                upload.blobUrls = upload.blobUrls || [];
                upload.blobUrlsTotalSize = upload.blobUrlsTotalSize || 0;
                upload.blobUrls.push({url: url, size: file.size});
                upload.blobUrlsTotalSize += file.size || 0;
                var maxMemory = upload.defaults.blobUrlsMaxMemory || 268435456;
                var maxLength = upload.defaults.blobUrlsMaxQueueSize || 200;
                while ((upload.blobUrlsTotalSize > maxMemory || upload.blobUrls.length > maxLength) && upload.blobUrls.length > 1) {
                  var obj = upload.blobUrls.splice(0, 1)[0];
                  URL.revokeObjectURL(obj.url);
                  upload.blobUrlsTotalSize -= obj.size;
                }
              }
            });
          } else {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
              $timeout(function () {
                file.$ngfDataUrl = e.target.result;
                deferred.resolve(e.target.result, file);
                $timeout(function () {
                  delete file.$ngfDataUrl;
                }, 1000);
              });
            };
            fileReader.onerror = function () {
              $timeout(function () {
                file.$ngfDataUrl = '';
                deferred.reject();
              });
            };
            fileReader.readAsDataURL(file);
          }
        } else {
          $timeout(function () {
            file[disallowObjectUrl ? '$ngfDataUrl' : '$ngfBlobUrl'] = '';
            deferred.reject();
          });
        }
      });

      if (disallowObjectUrl) {
        p = file.$$ngfDataUrlPromise = deferred.promise;
      } else {
        p = file.$$ngfBlobUrlPromise = deferred.promise;
      }
      p['finally'](function () {
        delete file[disallowObjectUrl ? '$$ngfDataUrlPromise' : '$$ngfBlobUrlPromise'];
      });
      return p;
    };
    return upload;
  }]);

  function getTagType(el) {
    if (el.tagName.toLowerCase() === 'img') return 'image';
    if (el.tagName.toLowerCase() === 'audio') return 'audio';
    if (el.tagName.toLowerCase() === 'video') return 'video';
    return /./;
  }

  function linkFileDirective(Upload, $timeout, scope, elem, attr, directiveName, resizeParams, isBackground) {
    function constructDataUrl(file) {
      var disallowObjectUrl = Upload.attrGetter('ngfNoObjectUrl', attr, scope);
      Upload.dataUrl(file, disallowObjectUrl)['finally'](function () {
        $timeout(function () {
          var src = (disallowObjectUrl ? file.$ngfDataUrl : file.$ngfBlobUrl) || file.$ngfDataUrl;
          if (isBackground) {
            elem.css('background-image', 'url(\'' + (src || '') + '\')');
          } else {
            elem.attr('src', src);
          }
          if (src) {
            elem.removeClass('ng-hide');
          } else {
            elem.addClass('ng-hide');
          }
        });
      });
    }

    $timeout(function () {
      var unwatch = scope.$watch(attr[directiveName], function (file) {
        var size = resizeParams;
        if (directiveName === 'ngfThumbnail') {
          if (!size) {
            size = {
              width: elem[0].naturalWidth || elem[0].clientWidth,
              height: elem[0].naturalHeight || elem[0].clientHeight
            };
          }
          if (size.width === 0 && window.getComputedStyle) {
            var style = getComputedStyle(elem[0]);
            if (style.width && style.width.indexOf('px') > -1 && style.height && style.height.indexOf('px') > -1) {
              size = {
                width: parseInt(style.width.slice(0, -2)),
                height: parseInt(style.height.slice(0, -2))
              };
            }
          }
        }

        if (angular.isString(file)) {
          elem.removeClass('ng-hide');
          if (isBackground) {
            return elem.css('background-image', 'url(\'' + file + '\')');
          } else {
            return elem.attr('src', file);
          }
        }
        if (file && file.type && file.type.search(getTagType(elem[0])) === 0 &&
          (!isBackground || file.type.indexOf('image') === 0)) {
          if (size && Upload.isResizeSupported()) {
            size.resizeIf = function (width, height) {
              return Upload.attrGetter('ngfResizeIf', attr, scope,
                {$width: width, $height: height, $file: file});
            };
            Upload.resize(file, size).then(
              function (f) {
                constructDataUrl(f);
              }, function (e) {
                throw e;
              }
            );
          } else {
            constructDataUrl(file);
          }
        } else {
          elem.addClass('ng-hide');
        }
      });

      scope.$on('$destroy', function () {
        unwatch();
      });
    });
  }


  /** @namespace attr.ngfSrc */
  /** @namespace attr.ngfNoObjectUrl */
  ngFileUpload.directive('ngfSrc', ['Upload', '$timeout', function (Upload, $timeout) {
    return {
      restrict: 'AE',
      link: function (scope, elem, attr) {
        linkFileDirective(Upload, $timeout, scope, elem, attr, 'ngfSrc',
          Upload.attrGetter('ngfResize', attr, scope), false);
      }
    };
  }]);

  /** @namespace attr.ngfBackground */
  /** @namespace attr.ngfNoObjectUrl */
  ngFileUpload.directive('ngfBackground', ['Upload', '$timeout', function (Upload, $timeout) {
    return {
      restrict: 'AE',
      link: function (scope, elem, attr) {
        linkFileDirective(Upload, $timeout, scope, elem, attr, 'ngfBackground',
          Upload.attrGetter('ngfResize', attr, scope), true);
      }
    };
  }]);

  /** @namespace attr.ngfThumbnail */
  /** @namespace attr.ngfAsBackground */
  /** @namespace attr.ngfSize */
  /** @namespace attr.ngfNoObjectUrl */
  ngFileUpload.directive('ngfThumbnail', ['Upload', '$timeout', function (Upload, $timeout) {
    return {
      restrict: 'AE',
      link: function (scope, elem, attr) {
        var size = Upload.attrGetter('ngfSize', attr, scope);
        linkFileDirective(Upload, $timeout, scope, elem, attr, 'ngfThumbnail', size,
          Upload.attrGetter('ngfAsBackground', attr, scope));
      }
    };
  }]);

  ngFileUpload.config(['$compileProvider', function ($compileProvider) {
    if ($compileProvider.imgSrcSanitizationWhitelist) $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|webcal|local|file|data|blob):/);
    if ($compileProvider.aHrefSanitizationWhitelist) $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|webcal|local|file|data|blob):/);
  }]);

  ngFileUpload.filter('ngfDataUrl', ['UploadDataUrl', '$sce', function (UploadDataUrl, $sce) {
    return function (file, disallowObjectUrl, trustedUrl) {
      if (angular.isString(file)) {
        return $sce.trustAsResourceUrl(file);
      }
      var src = file && ((disallowObjectUrl ? file.$ngfDataUrl : file.$ngfBlobUrl) || file.$ngfDataUrl);
      if (file && !src) {
        if (!file.$ngfDataUrlFilterInProgress && angular.isObject(file)) {
          file.$ngfDataUrlFilterInProgress = true;
          UploadDataUrl.dataUrl(file, disallowObjectUrl);
        }
        return '';
      }
      if (file) delete file.$ngfDataUrlFilterInProgress;
      return (file && src ? (trustedUrl ? $sce.trustAsResourceUrl(src) : src) : file) || '';
    };
  }]);

})();

ngFileUpload.service('UploadValidate', ['UploadDataUrl', '$q', '$timeout', function (UploadDataUrl, $q, $timeout) {
  var upload = UploadDataUrl;

  function globStringToRegex(str) {
    var regexp = '', excludes = [];
    if (str.length > 2 && str[0] === '/' && str[str.length - 1] === '/') {
      regexp = str.substring(1, str.length - 1);
    } else {
      var split = str.split(',');
      if (split.length > 1) {
        for (var i = 0; i < split.length; i++) {
          var r = globStringToRegex(split[i]);
          if (r.regexp) {
            regexp += '(' + r.regexp + ')';
            if (i < split.length - 1) {
              regexp += '|';
            }
          } else {
            excludes = excludes.concat(r.excludes);
          }
        }
      } else {
        if (str.indexOf('!') === 0) {
          excludes.push('^((?!' + globStringToRegex(str.substring(1)).regexp + ').)*$');
        } else {
          if (str.indexOf('.') === 0) {
            str = '*' + str;
          }
          regexp = '^' + str.replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\-]', 'g'), '\\$&') + '$';
          regexp = regexp.replace(/\\\*/g, '.*').replace(/\\\?/g, '.');
        }
      }
    }
    return {regexp: regexp, excludes: excludes};
  }

  upload.validatePattern = function (file, val) {
    if (!val) {
      return true;
    }
    var pattern = globStringToRegex(val), valid = true;
    if (pattern.regexp && pattern.regexp.length) {
      var regexp = new RegExp(pattern.regexp, 'i');
      valid = (file.type != null && regexp.test(file.type)) ||
        (file.name != null && regexp.test(file.name));
    }
    var len = pattern.excludes.length;
    while (len--) {
      var exclude = new RegExp(pattern.excludes[len], 'i');
      valid = valid && (file.type == null || exclude.test(file.type)) &&
        (file.name == null || exclude.test(file.name));
    }
    return valid;
  };

  upload.ratioToFloat = function (val) {
    var r = val.toString(), xIndex = r.search(/[x:]/i);
    if (xIndex > -1) {
      r = parseFloat(r.substring(0, xIndex)) / parseFloat(r.substring(xIndex + 1));
    } else {
      r = parseFloat(r);
    }
    return r;
  };

  upload.registerModelChangeValidator = function (ngModel, attr, scope) {
    if (ngModel) {
      ngModel.$formatters.push(function (files) {
        if (ngModel.$dirty) {
          var filesArray = files;
          if (files && !angular.isArray(files)) {
            filesArray = [files];
          }
          upload.validate(filesArray, 0, ngModel, attr, scope).then(function () {
            upload.applyModelValidation(ngModel, filesArray);
          });
        }
        return files;
      });
    }
  };

  function markModelAsDirty(ngModel, files) {
    if (files != null && !ngModel.$dirty) {
      if (ngModel.$setDirty) {
        ngModel.$setDirty();
      } else {
        ngModel.$dirty = true;
      }
    }
  }

  upload.applyModelValidation = function (ngModel, files) {
    markModelAsDirty(ngModel, files);
    angular.forEach(ngModel.$ngfValidations, function (validation) {
      ngModel.$setValidity(validation.name, validation.valid);
    });
  };

  upload.getValidationAttr = function (attr, scope, name, validationName, file) {
    var dName = 'ngf' + name[0].toUpperCase() + name.substr(1);
    var val = upload.attrGetter(dName, attr, scope, {$file: file});
    if (val == null) {
      val = upload.attrGetter('ngfValidate', attr, scope, {$file: file});
      if (val) {
        var split = (validationName || name).split('.');
        val = val[split[0]];
        if (split.length > 1) {
          val = val && val[split[1]];
        }
      }
    }
    return val;
  };

  upload.validate = function (files, prevLength, ngModel, attr, scope) {
    ngModel = ngModel || {};
    ngModel.$ngfValidations = ngModel.$ngfValidations || [];

    angular.forEach(ngModel.$ngfValidations, function (v) {
      v.valid = true;
    });

    var attrGetter = function (name, params) {
      return upload.attrGetter(name, attr, scope, params);
    };

    var ignoredErrors = (upload.attrGetter('ngfIgnoreInvalid', attr, scope) || '').split(' ');
    var runAllValidation = upload.attrGetter('ngfRunAllValidations', attr, scope);

    if (files == null || files.length === 0) {
      return upload.emptyPromise({'validFiles': files, 'invalidFiles': []});
    }

    files = files.length === undefined ? [files] : files.slice(0);
    var invalidFiles = [];

    function validateSync(name, validationName, fn) {
      if (files) {
        var i = files.length, valid = null;
        while (i--) {
          var file = files[i];
          if (file) {
            var val = upload.getValidationAttr(attr, scope, name, validationName, file);
            if (val != null) {
              if (!fn(file, val, i)) {
                if (ignoredErrors.indexOf(name) === -1) {
                  file.$error = name;
                  (file.$errorMessages = (file.$errorMessages || {}))[name] = true;
                  file.$errorParam = val;
                  if (invalidFiles.indexOf(file) === -1) {
                    invalidFiles.push(file);
                  }
                  if (!runAllValidation) {
                    files.splice(i, 1);
                  }
                  valid = false;
                } else {
                  files.splice(i, 1);
                }
              }
            }
          }
        }
        if (valid !== null) {
          ngModel.$ngfValidations.push({name: name, valid: valid});
        }
      }
    }

    validateSync('pattern', null, upload.validatePattern);
    validateSync('minSize', 'size.min', function (file, val) {
      return file.size + 0.1 >= upload.translateScalars(val);
    });
    validateSync('maxSize', 'size.max', function (file, val) {
      return file.size - 0.1 <= upload.translateScalars(val);
    });
    var totalSize = 0;
    validateSync('maxTotalSize', null, function (file, val) {
      totalSize += file.size;
      if (totalSize > upload.translateScalars(val)) {
        files.splice(0, files.length);
        return false;
      }
      return true;
    });

    validateSync('validateFn', null, function (file, r) {
      return r === true || r === null || r === '';
    });

    if (!files.length) {
      return upload.emptyPromise({'validFiles': [], 'invalidFiles': invalidFiles});
    }

    function validateAsync(name, validationName, type, asyncFn, fn) {
      function resolveResult(defer, file, val) {
        function resolveInternal(fn) {
          if (fn()) {
            if (ignoredErrors.indexOf(name) === -1) {
              file.$error = name;
              (file.$errorMessages = (file.$errorMessages || {}))[name] = true;
              file.$errorParam = val;
              if (invalidFiles.indexOf(file) === -1) {
                invalidFiles.push(file);
              }
              if (!runAllValidation) {
                var i = files.indexOf(file);
                if (i > -1) files.splice(i, 1);
              }
              defer.resolve(false);
            } else {
              var j = files.indexOf(file);
              if (j > -1) files.splice(j, 1);
              defer.resolve(true);
            }
          } else {
            defer.resolve(true);
          }
        }

        if (val != null) {
          asyncFn(file, val).then(function (d) {
            resolveInternal(function () {
              return !fn(d, val);
            });
          }, function () {
            resolveInternal(function () {
              return attrGetter('ngfValidateForce', {$file: file});
            });
          });
        } else {
          defer.resolve(true);
        }
      }

      var promises = [upload.emptyPromise(true)];
      if (files) {
        files = files.length === undefined ? [files] : files;
        angular.forEach(files, function (file) {
          var defer = $q.defer();
          promises.push(defer.promise);
          if (type && (file.type == null || file.type.search(type) !== 0)) {
            defer.resolve(true);
            return;
          }
          if (name === 'dimensions' && upload.attrGetter('ngfDimensions', attr) != null) {
            upload.imageDimensions(file).then(function (d) {
              resolveResult(defer, file,
                attrGetter('ngfDimensions', {$file: file, $width: d.width, $height: d.height}));
            }, function () {
              defer.resolve(false);
            });
          } else if (name === 'duration' && upload.attrGetter('ngfDuration', attr) != null) {
            upload.mediaDuration(file).then(function (d) {
              resolveResult(defer, file,
                attrGetter('ngfDuration', {$file: file, $duration: d}));
            }, function () {
              defer.resolve(false);
            });
          } else {
            resolveResult(defer, file,
              upload.getValidationAttr(attr, scope, name, validationName, file));
          }
        });
      }
      var deffer = $q.defer();
      $q.all(promises).then(function (values) {
        var isValid = true;
        for (var i = 0; i < values.length; i++) {
          if (!values[i]) {
            isValid = false;
            break;
          }
        }
        ngModel.$ngfValidations.push({name: name, valid: isValid});
        deffer.resolve(isValid);
      });
      return deffer.promise;
    }

    var deffer = $q.defer();
    var promises = [];

    promises.push(validateAsync('maxHeight', 'height.max', /image/,
      this.imageDimensions, function (d, val) {
        return d.height <= val;
      }));
    promises.push(validateAsync('minHeight', 'height.min', /image/,
      this.imageDimensions, function (d, val) {
        return d.height >= val;
      }));
    promises.push(validateAsync('maxWidth', 'width.max', /image/,
      this.imageDimensions, function (d, val) {
        return d.width <= val;
      }));
    promises.push(validateAsync('minWidth', 'width.min', /image/,
      this.imageDimensions, function (d, val) {
        return d.width >= val;
      }));
    promises.push(validateAsync('dimensions', null, /image/,
      function (file, val) {
        return upload.emptyPromise(val);
      }, function (r) {
        return r;
      }));
    promises.push(validateAsync('ratio', null, /image/,
      this.imageDimensions, function (d, val) {
        var split = val.toString().split(','), valid = false;
        for (var i = 0; i < split.length; i++) {
          if (Math.abs((d.width / d.height) - upload.ratioToFloat(split[i])) < 0.01) {
            valid = true;
          }
        }
        return valid;
      }));
    promises.push(validateAsync('maxRatio', 'ratio.max', /image/,
      this.imageDimensions, function (d, val) {
        return (d.width / d.height) - upload.ratioToFloat(val) < 0.0001;
      }));
    promises.push(validateAsync('minRatio', 'ratio.min', /image/,
      this.imageDimensions, function (d, val) {
        return (d.width / d.height) - upload.ratioToFloat(val) > -0.0001;
      }));
    promises.push(validateAsync('maxDuration', 'duration.max', /audio|video/,
      this.mediaDuration, function (d, val) {
        return d <= upload.translateScalars(val);
      }));
    promises.push(validateAsync('minDuration', 'duration.min', /audio|video/,
      this.mediaDuration, function (d, val) {
        return d >= upload.translateScalars(val);
      }));
    promises.push(validateAsync('duration', null, /audio|video/,
      function (file, val) {
        return upload.emptyPromise(val);
      }, function (r) {
        return r;
      }));

    promises.push(validateAsync('validateAsyncFn', null, null,
      function (file, val) {
        return val;
      }, function (r) {
        return r === true || r === null || r === '';
      }));

    $q.all(promises).then(function () {

      if (runAllValidation) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          if (file.$error) {
            files.splice(i--, 1);
          }
        }
      }

      runAllValidation = false;
      validateSync('maxFiles', null, function (file, val, i) {
        return prevLength + i < val;
      });

      deffer.resolve({'validFiles': files, 'invalidFiles': invalidFiles});
    });
    return deffer.promise;
  };

  upload.imageDimensions = function (file) {
    if (file.$ngfWidth && file.$ngfHeight) {
      var d = $q.defer();
      $timeout(function () {
        d.resolve({width: file.$ngfWidth, height: file.$ngfHeight});
      });
      return d.promise;
    }
    if (file.$ngfDimensionPromise) return file.$ngfDimensionPromise;

    var deferred = $q.defer();
    $timeout(function () {
      if (file.type.indexOf('image') !== 0) {
        deferred.reject('not image');
        return;
      }
      upload.dataUrl(file).then(function (dataUrl) {
        var img = angular.element('<img>').attr('src', dataUrl)
          .css('visibility', 'hidden').css('position', 'fixed')
          .css('max-width', 'none !important').css('max-height', 'none !important');

        function success() {
          var width = img[0].naturalWidth || img[0].clientWidth;
          var height = img[0].naturalHeight || img[0].clientHeight;
          img.remove();
          file.$ngfWidth = width;
          file.$ngfHeight = height;
          deferred.resolve({width: width, height: height});
        }

        function error() {
          img.remove();
          deferred.reject('load error');
        }

        img.on('load', success);
        img.on('error', error);

        var secondsCounter = 0;
        function checkLoadErrorInCaseOfNoCallback() {
          $timeout(function () {
            if (img[0].parentNode) {
              if (img[0].clientWidth) {
                success();
              } else if (secondsCounter++ > 10) {
                error();
              } else {
                checkLoadErrorInCaseOfNoCallback();
              }
            }
          }, 1000);
        }

        checkLoadErrorInCaseOfNoCallback();

        angular.element(document.getElementsByTagName('body')[0]).append(img);
      }, function () {
        deferred.reject('load error');
      });
    });

    file.$ngfDimensionPromise = deferred.promise;
    file.$ngfDimensionPromise['finally'](function () {
      delete file.$ngfDimensionPromise;
    });
    return file.$ngfDimensionPromise;
  };

  upload.mediaDuration = function (file) {
    if (file.$ngfDuration) {
      var d = $q.defer();
      $timeout(function () {
        d.resolve(file.$ngfDuration);
      });
      return d.promise;
    }
    if (file.$ngfDurationPromise) return file.$ngfDurationPromise;

    var deferred = $q.defer();
    $timeout(function () {
      if (file.type.indexOf('audio') !== 0 && file.type.indexOf('video') !== 0) {
        deferred.reject('not media');
        return;
      }
      upload.dataUrl(file).then(function (dataUrl) {
        var el = angular.element(file.type.indexOf('audio') === 0 ? '<audio>' : '<video>')
          .attr('src', dataUrl).css('visibility', 'none').css('position', 'fixed');

        function success() {
          var duration = el[0].duration;
          file.$ngfDuration = duration;
          el.remove();
          deferred.resolve(duration);
        }

        function error() {
          el.remove();
          deferred.reject('load error');
        }

        el.on('loadedmetadata', success);
        el.on('error', error);
        var count = 0;

        function checkLoadError() {
          $timeout(function () {
            if (el[0].parentNode) {
              if (el[0].duration) {
                success();
              } else if (count > 10) {
                error();
              } else {
                checkLoadError();
              }
            }
          }, 1000);
        }

        checkLoadError();

        angular.element(document.body).append(el);
      }, function () {
        deferred.reject('load error');
      });
    });

    file.$ngfDurationPromise = deferred.promise;
    file.$ngfDurationPromise['finally'](function () {
      delete file.$ngfDurationPromise;
    });
    return file.$ngfDurationPromise;
  };
  return upload;
}
]);

ngFileUpload.service('UploadResize', ['UploadValidate', '$q', function (UploadValidate, $q) {
  var upload = UploadValidate;

  /**
   * Conserve aspect ratio of the original region. Useful when shrinking/enlarging
   * images to fit into a certain area.
   * Source:  http://stackoverflow.com/a/14731922
   *
   * @param {Number} srcWidth Source area width
   * @param {Number} srcHeight Source area height
   * @param {Number} maxWidth Nestable area maximum available width
   * @param {Number} maxHeight Nestable area maximum available height
   * @return {Object} { width, height }
   */
  var calculateAspectRatioFit = function (srcWidth, srcHeight, maxWidth, maxHeight, centerCrop) {
    var ratio = centerCrop ? Math.max(maxWidth / srcWidth, maxHeight / srcHeight) :
      Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return {
      width: srcWidth * ratio, height: srcHeight * ratio,
      marginX: srcWidth * ratio - maxWidth, marginY: srcHeight * ratio - maxHeight
    };
  };

  // Extracted from https://github.com/romelgomez/angular-firebase-image-upload/blob/master/app/scripts/fileUpload.js#L89
  var resize = function (imagen, width, height, quality, type, ratio, centerCrop, resizeIf) {
    var deferred = $q.defer();
    var canvasElement = document.createElement('canvas');
    var imageElement = document.createElement('img');
    imageElement.setAttribute('style', 'visibility:hidden;position:fixed;z-index:-100000');
    document.body.appendChild(imageElement);

    imageElement.onload = function () {
      var imgWidth = imageElement.width, imgHeight = imageElement.height;
      imageElement.parentNode.removeChild(imageElement);
      if (resizeIf != null && resizeIf(imgWidth, imgHeight) === false) {
        deferred.reject('resizeIf');
        return;
      }
      try {
        if (ratio) {
          var ratioFloat = upload.ratioToFloat(ratio);
          var imgRatio = imgWidth / imgHeight;
          if (imgRatio < ratioFloat) {
            width = imgWidth;
            height = width / ratioFloat;
          } else {
            height = imgHeight;
            width = height * ratioFloat;
          }
        }
        if (!width) {
          width = imgWidth;
        }
        if (!height) {
          height = imgHeight;
        }
        var dimensions = calculateAspectRatioFit(imgWidth, imgHeight, width, height, centerCrop);
        canvasElement.width = Math.min(dimensions.width, width);
        canvasElement.height = Math.min(dimensions.height, height);
        var context = canvasElement.getContext('2d');
        context.drawImage(imageElement,
          Math.min(0, -dimensions.marginX / 2), Math.min(0, -dimensions.marginY / 2),
          dimensions.width, dimensions.height);
        deferred.resolve(canvasElement.toDataURL(type || 'image/WebP', quality || 0.934));
      } catch (e) {
        deferred.reject(e);
      }
    };
    imageElement.onerror = function () {
      imageElement.parentNode.removeChild(imageElement);
      deferred.reject();
    };
    imageElement.src = imagen;
    return deferred.promise;
  };

  upload.dataUrltoBlob = function (dataurl, name, origSize) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    var blob = new window.Blob([u8arr], {type: mime});
    blob.name = name;
    blob.$ngfOrigSize = origSize;
    return blob;
  };

  upload.isResizeSupported = function () {
    var elem = document.createElement('canvas');
    return window.atob && elem.getContext && elem.getContext('2d') && window.Blob;
  };

  if (upload.isResizeSupported()) {
    // add name getter to the blob constructor prototype
    Object.defineProperty(window.Blob.prototype, 'name', {
      get: function () {
        return this.$ngfName;
      },
      set: function (v) {
        this.$ngfName = v;
      },
      configurable: true
    });
  }

  upload.resize = function (file, options) {
    if (file.type.indexOf('image') !== 0) return upload.emptyPromise(file);

    var deferred = $q.defer();
    upload.dataUrl(file, true).then(function (url) {
      resize(url, options.width, options.height, options.quality, options.type || file.type,
        options.ratio, options.centerCrop, options.resizeIf)
        .then(function (dataUrl) {
          if (file.type === 'image/jpeg' && options.restoreExif !== false) {
            try {
              dataUrl = upload.restoreExif(url, dataUrl);
            } catch (e) {
              setTimeout(function () {throw e;}, 1);
            }
          }
          try {
            var blob = upload.dataUrltoBlob(dataUrl, file.name, file.size);
            deferred.resolve(blob);
          } catch (e) {
            deferred.reject(e);
          }
        }, function (r) {
          if (r === 'resizeIf') {
            deferred.resolve(file);
          }
          deferred.reject(r);
        });
    }, function (e) {
      deferred.reject(e);
    });
    return deferred.promise;
  };

  return upload;
}]);

(function () {
  ngFileUpload.directive('ngfDrop', ['$parse', '$timeout', '$window', 'Upload', '$http', '$q',
    function ($parse, $timeout, $window, Upload, $http, $q) {
      return {
        restrict: 'AEC',
        require: '?ngModel',
        link: function (scope, elem, attr, ngModel) {
          linkDrop(scope, elem, attr, ngModel, $parse, $timeout, $window, Upload, $http, $q);
        }
      };
    }]);

  ngFileUpload.directive('ngfNoFileDrop', function () {
    return function (scope, elem) {
      if (dropAvailable()) elem.css('display', 'none');
    };
  });

  ngFileUpload.directive('ngfDropAvailable', ['$parse', '$timeout', 'Upload', function ($parse, $timeout, Upload) {
    return function (scope, elem, attr) {
      if (dropAvailable()) {
        var model = $parse(Upload.attrGetter('ngfDropAvailable', attr));
        $timeout(function () {
          model(scope);
          if (model.assign) {
            model.assign(scope, true);
          }
        });
      }
    };
  }]);

  function linkDrop(scope, elem, attr, ngModel, $parse, $timeout, $window, upload, $http, $q) {
    var available = dropAvailable();

    var attrGetter = function (name, scope, params) {
      return upload.attrGetter(name, attr, scope, params);
    };

    if (attrGetter('dropAvailable')) {
      $timeout(function () {
        if (scope[attrGetter('dropAvailable')]) {
          scope[attrGetter('dropAvailable')].value = available;
        } else {
          scope[attrGetter('dropAvailable')] = available;
        }
      });
    }
    if (!available) {
      if (attrGetter('ngfHideOnDropNotAvailable', scope) === true) {
        elem.css('display', 'none');
      }
      return;
    }

    function isDisabled() {
      return elem.attr('disabled') || attrGetter('ngfDropDisabled', scope);
    }

    if (attrGetter('ngfSelect') == null) {
      upload.registerModelChangeValidator(ngModel, attr, scope);
    }

    var leaveTimeout = null;
    var stopPropagation = $parse(attrGetter('ngfStopPropagation'));
    var dragOverDelay = 1;
    var actualDragOverClass;

    elem[0].addEventListener('dragover', function (evt) {
      if (isDisabled() || !upload.shouldUpdateOn('drop', attr, scope)) return;
      evt.preventDefault();
      if (stopPropagation(scope)) evt.stopPropagation();
      // handling dragover events from the Chrome download bar
      if (navigator.userAgent.indexOf('Chrome') > -1) {
        var b = evt.dataTransfer.effectAllowed;
        evt.dataTransfer.dropEffect = ('move' === b || 'linkMove' === b) ? 'move' : 'copy';
      }
      $timeout.cancel(leaveTimeout);
      if (!actualDragOverClass) {
        actualDragOverClass = 'C';
        calculateDragOverClass(scope, attr, evt, function (clazz) {
          actualDragOverClass = clazz;
          elem.addClass(actualDragOverClass);
          attrGetter('ngfDrag', scope, {$isDragging: true, $class: actualDragOverClass, $event: evt});
        });
      }
    }, false);
    elem[0].addEventListener('dragenter', function (evt) {
      if (isDisabled() || !upload.shouldUpdateOn('drop', attr, scope)) return;
      evt.preventDefault();
      if (stopPropagation(scope)) evt.stopPropagation();
    }, false);
    elem[0].addEventListener('dragleave', function (evt) {
      if (isDisabled() || !upload.shouldUpdateOn('drop', attr, scope)) return;
      evt.preventDefault();
      if (stopPropagation(scope)) evt.stopPropagation();
      leaveTimeout = $timeout(function () {
        if (actualDragOverClass) elem.removeClass(actualDragOverClass);
        actualDragOverClass = null;
        attrGetter('ngfDrag', scope, {$isDragging: false, $event: evt});
      }, dragOverDelay || 100);
    }, false);
    elem[0].addEventListener('drop', function (evt) {
      if (isDisabled() || !upload.shouldUpdateOn('drop', attr, scope)) return;
      evt.preventDefault();
      if (stopPropagation(scope)) evt.stopPropagation();
      if (actualDragOverClass) elem.removeClass(actualDragOverClass);
      actualDragOverClass = null;
      extractFilesAndUpdateModel(evt.dataTransfer, evt, 'dropUrl');
    }, false);
    elem[0].addEventListener('paste', function (evt) {
      if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1 &&
        attrGetter('ngfEnableFirefoxPaste', scope)) {
        evt.preventDefault();
      }
      if (isDisabled() || !upload.shouldUpdateOn('paste', attr, scope)) return;
      extractFilesAndUpdateModel(evt.clipboardData || evt.originalEvent.clipboardData, evt, 'pasteUrl');
    }, false);

    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1 &&
      attrGetter('ngfEnableFirefoxPaste', scope)) {
      elem.attr('contenteditable', true);
      elem.on('keypress', function (e) {
        if (!e.metaKey && !e.ctrlKey) {
          e.preventDefault();
        }
      });
    }

    function extractFilesAndUpdateModel(source, evt, updateOnType) {
      if (!source) return;
      // html needs to be calculated on the same process otherwise the data will be wiped
      // after promise resolve or setTimeout.
      var html;
      try {
        html = source && source.getData && source.getData('text/html');
      } catch (e) {/* Fix IE11 that throw error calling getData */
      }
      extractFiles(source.items, source.files, attrGetter('ngfAllowDir', scope) !== false,
        attrGetter('multiple') || attrGetter('ngfMultiple', scope)).then(function (files) {
        if (files.length) {
          updateModel(files, evt);
        } else {
          extractFilesFromHtml(updateOnType, html).then(function (files) {
            updateModel(files, evt);
          });
        }
      });
    }

    function updateModel(files, evt) {
      upload.updateModel(ngModel, attr, scope, attrGetter('ngfChange') || attrGetter('ngfDrop'), files, evt);
    }

    function extractFilesFromHtml(updateOn, html) {
      if (!upload.shouldUpdateOn(updateOn, attr, scope) || typeof html !== 'string') return upload.rejectPromise([]);
      var urls = [];
      html.replace(/<(img src|img [^>]* src) *=\"([^\"]*)\"/gi, function (m, n, src) {
        urls.push(src);
      });
      var promises = [], files = [];
      if (urls.length) {
        angular.forEach(urls, function (url) {
          promises.push(upload.urlToBlob(url).then(function (blob) {
            files.push(blob);
          }));
        });
        var defer = $q.defer();
        $q.all(promises).then(function () {
          defer.resolve(files);
        }, function (e) {
          defer.reject(e);
        });
        return defer.promise;
      }
      return upload.emptyPromise();
    }

    function calculateDragOverClass(scope, attr, evt, callback) {
      var obj = attrGetter('ngfDragOverClass', scope, {$event: evt}), dClass = 'dragover';
      if (angular.isString(obj)) {
        dClass = obj;
      } else if (obj) {
        if (obj.delay) dragOverDelay = obj.delay;
        if (obj.accept || obj.reject) {
          var items = evt.dataTransfer.items;
          if (items == null || !items.length) {
            dClass = obj.accept;
          } else {
            var pattern = obj.pattern || attrGetter('ngfPattern', scope, {$event: evt});
            var len = items.length;
            while (len--) {
              if (!upload.validatePattern(items[len], pattern)) {
                dClass = obj.reject;
                break;
              } else {
                dClass = obj.accept;
              }
            }
          }
        }
      }
      callback(dClass);
    }

    function extractFiles(items, fileList, allowDir, multiple) {
      var maxFiles = upload.getValidationAttr(attr, scope, 'maxFiles');
      if (maxFiles == null) {
        maxFiles = Number.MAX_VALUE;
      }
      var maxTotalSize = upload.getValidationAttr(attr, scope, 'maxTotalSize');
      if (maxTotalSize == null) {
        maxTotalSize = Number.MAX_VALUE;
      }
      var includeDir = attrGetter('ngfIncludeDir', scope);
      var files = [], totalSize = 0;

      function traverseFileTree(entry, path) {
        var defer = $q.defer();
        if (entry != null) {
          if (entry.isDirectory) {
            var promises = [upload.emptyPromise()];
            if (includeDir) {
              var file = {type: 'directory'};
              file.name = file.path = (path || '') + entry.name;
              files.push(file);
            }
            var dirReader = entry.createReader();
            var entries = [];
            var readEntries = function () {
              dirReader.readEntries(function (results) {
                try {
                  if (!results.length) {
                    angular.forEach(entries.slice(0), function (e) {
                      if (files.length <= maxFiles && totalSize <= maxTotalSize) {
                        promises.push(traverseFileTree(e, (path ? path : '') + entry.name + '/'));
                      }
                    });
                    $q.all(promises).then(function () {
                      defer.resolve();
                    }, function (e) {
                      defer.reject(e);
                    });
                  } else {
                    entries = entries.concat(Array.prototype.slice.call(results || [], 0));
                    readEntries();
                  }
                } catch (e) {
                  defer.reject(e);
                }
              }, function (e) {
                defer.reject(e);
              });
            };
            readEntries();
          } else {
            entry.file(function (file) {
              try {
                file.path = (path ? path : '') + file.name;
                if (includeDir) {
                  file = upload.rename(file, file.path);
                }
                files.push(file);
                totalSize += file.size;
                defer.resolve();
              } catch (e) {
                defer.reject(e);
              }
            }, function (e) {
              defer.reject(e);
            });
          }
        }
        return defer.promise;
      }

      var promises = [upload.emptyPromise()];

      if (items && items.length > 0 && $window.location.protocol !== 'file:') {
        for (var i = 0; i < items.length; i++) {
          if (items[i].webkitGetAsEntry && items[i].webkitGetAsEntry() && items[i].webkitGetAsEntry().isDirectory) {
            var entry = items[i].webkitGetAsEntry();
            if (entry.isDirectory && !allowDir) {
              continue;
            }
            if (entry != null) {
              promises.push(traverseFileTree(entry));
            }
          } else {
            var f = items[i].getAsFile();
            if (f != null) {
              files.push(f);
              totalSize += f.size;
            }
          }
          if (files.length > maxFiles || totalSize > maxTotalSize ||
            (!multiple && files.length > 0)) break;
        }
      } else {
        if (fileList != null) {
          for (var j = 0; j < fileList.length; j++) {
            var file = fileList.item(j);
            if (file.type || file.size > 0) {
              files.push(file);
              totalSize += file.size;
            }
            if (files.length > maxFiles || totalSize > maxTotalSize ||
              (!multiple && files.length > 0)) break;
          }
        }
      }

      var defer = $q.defer();
      $q.all(promises).then(function () {
        if (!multiple && !includeDir && files.length) {
          var i = 0;
          while (files[i] && files[i].type === 'directory') i++;
          defer.resolve([files[i]]);
        } else {
          defer.resolve(files);
        }
      }, function (e) {
        defer.reject(e);
      });

      return defer.promise;
    }
  }

  function dropAvailable() {
    var div = document.createElement('div');
    return ('draggable' in div) && ('ondrop' in div) && !/Edge\/12./i.test(navigator.userAgent);
  }

})();

// customized version of https://github.com/exif-js/exif-js
ngFileUpload.service('UploadExif', ['UploadResize', '$q', function (UploadResize, $q) {
  var upload = UploadResize;

  upload.isExifSupported = function () {
    return window.FileReader && new FileReader().readAsArrayBuffer && upload.isResizeSupported();
  };

  function applyTransform(ctx, orientation, width, height) {
    switch (orientation) {
      case 2:
        return ctx.transform(-1, 0, 0, 1, width, 0);
      case 3:
        return ctx.transform(-1, 0, 0, -1, width, height);
      case 4:
        return ctx.transform(1, 0, 0, -1, 0, height);
      case 5:
        return ctx.transform(0, 1, 1, 0, 0, 0);
      case 6:
        return ctx.transform(0, 1, -1, 0, height, 0);
      case 7:
        return ctx.transform(0, -1, -1, 0, height, width);
      case 8:
        return ctx.transform(0, -1, 1, 0, 0, width);
    }
  }

  upload.readOrientation = function (file) {
    var defer = $q.defer();
    var reader = new FileReader();
    var slicedFile = file.slice ? file.slice(0, 64 * 1024) : file;
    reader.readAsArrayBuffer(slicedFile);
    reader.onerror = function (e) {
      return defer.reject(e);
    };
    reader.onload = function (e) {
      var result = {orientation: 1};
      var view = new DataView(this.result);
      if (view.getUint16(0, false) !== 0xFFD8) return defer.resolve(result);

      var length = view.byteLength,
        offset = 2;
      while (offset < length) {
        var marker = view.getUint16(offset, false);
        offset += 2;
        if (marker === 0xFFE1) {
          if (view.getUint32(offset += 2, false) !== 0x45786966) return defer.resolve(result);

          var little = view.getUint16(offset += 6, false) === 0x4949;
          offset += view.getUint32(offset + 4, little);
          var tags = view.getUint16(offset, little);
          offset += 2;
          for (var i = 0; i < tags; i++)
            if (view.getUint16(offset + (i * 12), little) === 0x0112) {
              var orientation = view.getUint16(offset + (i * 12) + 8, little);
              if (orientation >= 2 && orientation <= 8) {
                view.setUint16(offset + (i * 12) + 8, 1, little);
                result.fixedArrayBuffer = e.target.result;
              }
              result.orientation = orientation;
              return defer.resolve(result);
            }
        } else if ((marker & 0xFF00) !== 0xFF00) break;
        else offset += view.getUint16(offset, false);
      }
      return defer.resolve(result);
    };
    return defer.promise;
  };

  function arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  upload.applyExifRotation = function (file) {
    if (file.type.indexOf('image/jpeg') !== 0) {
      return upload.emptyPromise(file);
    }

    var deferred = $q.defer();
    upload.readOrientation(file).then(function (result) {
      if (result.orientation < 2 || result.orientation > 8) {
        return deferred.resolve(file);
      }
      upload.dataUrl(file, true).then(function (url) {
        var canvas = document.createElement('canvas');
        var img = document.createElement('img');

        img.onload = function () {
          try {
            canvas.width = result.orientation > 4 ? img.height : img.width;
            canvas.height = result.orientation > 4 ? img.width : img.height;
            var ctx = canvas.getContext('2d');
            applyTransform(ctx, result.orientation, img.width, img.height);
            ctx.drawImage(img, 0, 0);
            var dataUrl = canvas.toDataURL(file.type || 'image/WebP', 0.934);
            dataUrl = upload.restoreExif(arrayBufferToBase64(result.fixedArrayBuffer), dataUrl);
            var blob = upload.dataUrltoBlob(dataUrl, file.name);
            deferred.resolve(blob);
          } catch (e) {
            return deferred.reject(e);
          }
        };
        img.onerror = function () {
          deferred.reject();
        };
        img.src = url;
      }, function (e) {
        deferred.reject(e);
      });
    }, function (e) {
      deferred.reject(e);
    });
    return deferred.promise;
  };

  upload.restoreExif = function (orig, resized) {
    var ExifRestorer = {};

    ExifRestorer.KEY_STR = 'ABCDEFGHIJKLMNOP' +
      'QRSTUVWXYZabcdef' +
      'ghijklmnopqrstuv' +
      'wxyz0123456789+/' +
      '=';

    ExifRestorer.encode64 = function (input) {
      var output = '',
        chr1, chr2, chr3 = '',
        enc1, enc2, enc3, enc4 = '',
        i = 0;

      do {
        chr1 = input[i++];
        chr2 = input[i++];
        chr3 = input[i++];

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
          enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
          enc4 = 64;
        }

        output = output +
          this.KEY_STR.charAt(enc1) +
          this.KEY_STR.charAt(enc2) +
          this.KEY_STR.charAt(enc3) +
          this.KEY_STR.charAt(enc4);
        chr1 = chr2 = chr3 = '';
        enc1 = enc2 = enc3 = enc4 = '';
      } while (i < input.length);

      return output;
    };

    ExifRestorer.restore = function (origFileBase64, resizedFileBase64) {
      if (origFileBase64.match('data:image/jpeg;base64,')) {
        origFileBase64 = origFileBase64.replace('data:image/jpeg;base64,', '');
      }

      var rawImage = this.decode64(origFileBase64);
      var segments = this.slice2Segments(rawImage);

      var image = this.exifManipulation(resizedFileBase64, segments);

      return 'data:image/jpeg;base64,' + this.encode64(image);
    };


    ExifRestorer.exifManipulation = function (resizedFileBase64, segments) {
      var exifArray = this.getExifArray(segments),
        newImageArray = this.insertExif(resizedFileBase64, exifArray);
      return new Uint8Array(newImageArray);
    };


    ExifRestorer.getExifArray = function (segments) {
      var seg;
      for (var x = 0; x < segments.length; x++) {
        seg = segments[x];
        if (seg[0] === 255 & seg[1] === 225) //(ff e1)
        {
          return seg;
        }
      }
      return [];
    };


    ExifRestorer.insertExif = function (resizedFileBase64, exifArray) {
      var imageData = resizedFileBase64.replace('data:image/jpeg;base64,', ''),
        buf = this.decode64(imageData),
        separatePoint = buf.indexOf(255, 3),
        mae = buf.slice(0, separatePoint),
        ato = buf.slice(separatePoint),
        array = mae;

      array = array.concat(exifArray);
      array = array.concat(ato);
      return array;
    };


    ExifRestorer.slice2Segments = function (rawImageArray) {
      var head = 0,
        segments = [];

      while (1) {
        if (rawImageArray[head] === 255 & rawImageArray[head + 1] === 218) {
          break;
        }
        if (rawImageArray[head] === 255 & rawImageArray[head + 1] === 216) {
          head += 2;
        }
        else {
          var length = rawImageArray[head + 2] * 256 + rawImageArray[head + 3],
            endPoint = head + length + 2,
            seg = rawImageArray.slice(head, endPoint);
          segments.push(seg);
          head = endPoint;
        }
        if (head > rawImageArray.length) {
          break;
        }
      }

      return segments;
    };


    ExifRestorer.decode64 = function (input) {
      var chr1, chr2, chr3 = '',
        enc1, enc2, enc3, enc4 = '',
        i = 0,
        buf = [];

      // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
      var base64test = /[^A-Za-z0-9\+\/\=]/g;
      if (base64test.exec(input)) {
        console.log('There were invalid base64 characters in the input text.\n' +
          'Valid base64 characters are A-Z, a-z, 0-9, ' + ', ' / ',and "="\n' +
          'Expect errors in decoding.');
      }
      input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');

      do {
        enc1 = this.KEY_STR.indexOf(input.charAt(i++));
        enc2 = this.KEY_STR.indexOf(input.charAt(i++));
        enc3 = this.KEY_STR.indexOf(input.charAt(i++));
        enc4 = this.KEY_STR.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        buf.push(chr1);

        if (enc3 !== 64) {
          buf.push(chr2);
        }
        if (enc4 !== 64) {
          buf.push(chr3);
        }

        chr1 = chr2 = chr3 = '';
        enc1 = enc2 = enc3 = enc4 = '';

      } while (i < input.length);

      return buf;
    };

    return ExifRestorer.restore(orig, resized);  //<= EXIF
  };

  return upload;
}]);

/*
 jQuery UI Sortable plugin wrapper

 @param [ui-sortable] {object} Options to pass to $.fn.sortable() merged onto ui.config
 */
angular.module('ui.sortable', [])
  .value('uiSortableConfig',{})
  .directive('uiSortable', [
    'uiSortableConfig', '$timeout', '$log',
    function(uiSortableConfig, $timeout, $log) {
      return {
        require: '?ngModel',
        scope: {
          ngModel: '=',
          uiSortable: '='
        },
        link: function(scope, element, attrs, ngModel) {
          var savedNodes;

          function combineCallbacks(first,second){
            if(second && (typeof second === 'function')) {
              return function() {
                first.apply(this, arguments);
                second.apply(this, arguments);
              };
            }
            return first;
          }

          function getSortableWidgetInstance(element) {
            // this is a fix to support jquery-ui prior to v1.11.x
            // otherwise we should be using `element.sortable('instance')`
            var data = element.data('ui-sortable');
            if (data && typeof data === 'object' && data.widgetFullName === 'ui-sortable') {
              return data;
            }
            return null;
          }

          function hasSortingHelper (element, ui) {
            var helperOption = element.sortable('option','helper');
            return helperOption === 'clone' || (typeof helperOption === 'function' && ui.item.sortable.isCustomHelperUsed());
          }

          // thanks jquery-ui
          function isFloating (item) {
            return (/left|right/).test(item.css('float')) || (/inline|table-cell/).test(item.css('display'));
          }

          function getElementScope(elementScopes, element) {
            var result = null;
            for (var i = 0; i < elementScopes.length; i++) {
              var x = elementScopes[i];
              if (x.element[0] === element[0]) {
                result = x.scope;
                break;
              }
            }
            return result;
          }

          function afterStop(e, ui) {
            ui.item.sortable._destroy();
          }

          var opts = {};

          // directive specific options
          var directiveOpts = {
            'ui-floating': undefined
          };

          var callbacks = {
            receive: null,
            remove:null,
            start:null,
            stop:null,
            update:null
          };

          var wrappers = {
            helper: null
          };

          angular.extend(opts, directiveOpts, uiSortableConfig, scope.uiSortable);

          if (!angular.element.fn || !angular.element.fn.jquery) {
            $log.error('ui.sortable: jQuery should be included before AngularJS!');
            return;
          }

          if (ngModel) {

            // When we add or remove elements, we need the sortable to 'refresh'
            // so it can find the new/removed elements.
            scope.$watch('ngModel.length', function() {
              // Timeout to let ng-repeat modify the DOM
              $timeout(function() {
                // ensure that the jquery-ui-sortable widget instance
                // is still bound to the directive's element
                if (!!getSortableWidgetInstance(element)) {
                  element.sortable('refresh');
                }
              }, 0, false);
            });

            callbacks.start = function(e, ui) {
              if (opts['ui-floating'] === 'auto') {
                // since the drag has started, the element will be
                // absolutely positioned, so we check its siblings
                var siblings = ui.item.siblings();
                var sortableWidgetInstance = getSortableWidgetInstance(angular.element(e.target));
                sortableWidgetInstance.floating = isFloating(siblings);
              }

              // Save the starting position of dragged item
              ui.item.sortable = {
                model: ngModel.$modelValue[ui.item.index()],
                index: ui.item.index(),
                source: ui.item.parent(),
                sourceModel: ngModel.$modelValue,
                cancel: function () {
                  ui.item.sortable._isCanceled = true;
                },
                isCanceled: function () {
                  return ui.item.sortable._isCanceled;
                },
                isCustomHelperUsed: function () {
                  return !!ui.item.sortable._isCustomHelperUsed;
                },
                _isCanceled: false,
                _isCustomHelperUsed: ui.item.sortable._isCustomHelperUsed,
                _destroy: function () {
                  angular.forEach(ui.item.sortable, function(value, key) {
                    ui.item.sortable[key] = undefined;
                  });
                }
              };
            };

            callbacks.activate = function(e, ui) {
              // We need to make a copy of the current element's contents so
              // we can restore it after sortable has messed it up.
              // This is inside activate (instead of start) in order to save
              // both lists when dragging between connected lists.
              savedNodes = element.contents();

              // If this list has a placeholder (the connected lists won't),
              // don't inlcude it in saved nodes.
              var placeholder = element.sortable('option','placeholder');

              // placeholder.element will be a function if the placeholder, has
              // been created (placeholder will be an object).  If it hasn't
              // been created, either placeholder will be false if no
              // placeholder class was given or placeholder.element will be
              // undefined if a class was given (placeholder will be a string)
              if (placeholder && placeholder.element && typeof placeholder.element === 'function') {
                var phElement = placeholder.element();
                // workaround for jquery ui 1.9.x,
                // not returning jquery collection
                phElement = angular.element(phElement);

                // exact match with the placeholder's class attribute to handle
                // the case that multiple connected sortables exist and
                // the placehoilder option equals the class of sortable items
                var excludes = element.find('[class="' + phElement.attr('class') + '"]:not([ng-repeat], [data-ng-repeat])');

                savedNodes = savedNodes.not(excludes);
              }

              // save the directive's scope so that it is accessible from ui.item.sortable
              var connectedSortables = ui.item.sortable._connectedSortables || [];

              connectedSortables.push({
                element: element,
                scope: scope
              });

              ui.item.sortable._connectedSortables = connectedSortables;
            };

            callbacks.update = function(e, ui) {
              // Save current drop position but only if this is not a second
              // update that happens when moving between lists because then
              // the value will be overwritten with the old value
              if(!ui.item.sortable.received) {
                ui.item.sortable.dropindex = ui.item.index();
                var droptarget = ui.item.parent();
                ui.item.sortable.droptarget = droptarget;

                var droptargetScope = getElementScope(ui.item.sortable._connectedSortables, droptarget);
                ui.item.sortable.droptargetModel = droptargetScope.ngModel;

                // Cancel the sort (let ng-repeat do the sort for us)
                // Don't cancel if this is the received list because it has
                // already been canceled in the other list, and trying to cancel
                // here will mess up the DOM.
                element.sortable('cancel');
              }

              // Put the nodes back exactly the way they started (this is very
              // important because ng-repeat uses comment elements to delineate
              // the start and stop of repeat sections and sortable doesn't
              // respect their order (even if we cancel, the order of the
              // comments are still messed up).
              if (hasSortingHelper(element, ui) && !ui.item.sortable.received &&
                  element.sortable( 'option', 'appendTo' ) === 'parent') {
                // restore all the savedNodes except .ui-sortable-helper element
                // (which is placed last). That way it will be garbage collected.
                savedNodes = savedNodes.not(savedNodes.last());
              }
              savedNodes.appendTo(element);

              // If this is the target connected list then
              // it's safe to clear the restored nodes since:
              // update is currently running and
              // stop is not called for the target list.
              if(ui.item.sortable.received) {
                savedNodes = null;
              }

              // If received is true (an item was dropped in from another list)
              // then we add the new item to this list otherwise wait until the
              // stop event where we will know if it was a sort or item was
              // moved here from another list
              if(ui.item.sortable.received && !ui.item.sortable.isCanceled()) {
                scope.$apply(function () {
                  ngModel.$modelValue.splice(ui.item.sortable.dropindex, 0,
                                             ui.item.sortable.moved);
                });
              }
            };

            callbacks.stop = function(e, ui) {
              // If the received flag hasn't be set on the item, this is a
              // normal sort, if dropindex is set, the item was moved, so move
              // the items in the list.
              if(!ui.item.sortable.received &&
                 ('dropindex' in ui.item.sortable) &&
                 !ui.item.sortable.isCanceled()) {

                scope.$apply(function () {
                  ngModel.$modelValue.splice(
                    ui.item.sortable.dropindex, 0,
                    ngModel.$modelValue.splice(ui.item.sortable.index, 1)[0]);
                });
              } else {
                // if the item was not moved, then restore the elements
                // so that the ngRepeat's comment are correct.
                if ((!('dropindex' in ui.item.sortable) || ui.item.sortable.isCanceled()) &&
                    !hasSortingHelper(element, ui)) {
                  savedNodes.appendTo(element);
                }
              }

              // It's now safe to clear the savedNodes
              // since stop is the last callback.
              savedNodes = null;
            };

            callbacks.receive = function(e, ui) {
              // An item was dropped here from another list, set a flag on the
              // item.
              ui.item.sortable.received = true;
            };

            callbacks.remove = function(e, ui) {
              // Workaround for a problem observed in nested connected lists.
              // There should be an 'update' event before 'remove' when moving
              // elements. If the event did not fire, cancel sorting.
              if (!('dropindex' in ui.item.sortable)) {
                element.sortable('cancel');
                ui.item.sortable.cancel();
              }

              // Remove the item from this list's model and copy data into item,
              // so the next list can retrive it
              if (!ui.item.sortable.isCanceled()) {
                scope.$apply(function () {
                  ui.item.sortable.moved = ngModel.$modelValue.splice(
                    ui.item.sortable.index, 1)[0];
                });
              }
            };

            wrappers.helper = function (inner) {
              if (inner && typeof inner === 'function') {
                return function (e, item) {
                  var innerResult = inner.apply(this, arguments);
                  item.sortable._isCustomHelperUsed = item !== innerResult;
                  return innerResult;
                };
              }
              return inner;
            };

            scope.$watch('uiSortable', function(newVal /*, oldVal*/) {
              // ensure that the jquery-ui-sortable widget instance
              // is still bound to the directive's element
              var sortableWidgetInstance = getSortableWidgetInstance(element);
              if (!!sortableWidgetInstance) {
                angular.forEach(newVal, function(value, key) {
                  // if it's a custom option of the directive,
                  // handle it approprietly
                  if (key in directiveOpts) {
                    if (key === 'ui-floating' && (value === false || value === true)) {
                      sortableWidgetInstance.floating = value;
                    }

                    opts[key] = value;
                    return;
                  }

                  if (callbacks[key]) {
                    if( key === 'stop' ){
                      // call apply after stop
                      value = combineCallbacks(
                        value, function() { scope.$apply(); });

                      value = combineCallbacks(value, afterStop);
                    }
                    // wrap the callback
                    value = combineCallbacks(callbacks[key], value);
                  } else if (wrappers[key]) {
                    value = wrappers[key](value);
                  }

                  opts[key] = value;
                  element.sortable('option', key, value);
                });
              }
            }, true);

            angular.forEach(callbacks, function(value, key) {
              opts[key] = combineCallbacks(value, opts[key]);
              if( key === 'stop' ){
                opts[key] = combineCallbacks(opts[key], afterStop);
              }
            });

          } else {
            $log.info('ui.sortable: ngModel not provided!', element);
          }

          // Create sortable
          element.sortable(opts);
        }
      };
    }
  ]);

/*
 * angular-ui-bootstrap
 * http://angular-ui.github.io/bootstrap/

 * Version: 0.13.0 - 2015-05-02
 * License: MIT
 */
angular.module("ui.bootstrap",["ui.bootstrap.tpls","ui.bootstrap.collapse","ui.bootstrap.accordion","ui.bootstrap.alert","ui.bootstrap.bindHtml","ui.bootstrap.buttons","ui.bootstrap.carousel","ui.bootstrap.dateparser","ui.bootstrap.position","ui.bootstrap.datepicker","ui.bootstrap.dropdown","ui.bootstrap.modal","ui.bootstrap.pagination","ui.bootstrap.tooltip","ui.bootstrap.popover","ui.bootstrap.progressbar","ui.bootstrap.rating","ui.bootstrap.tabs","ui.bootstrap.timepicker","ui.bootstrap.transition","ui.bootstrap.typeahead"]),angular.module("ui.bootstrap.tpls",["template/accordion/accordion-group.html","template/accordion/accordion.html","template/alert/alert.html","template/carousel/carousel.html","template/carousel/slide.html","template/datepicker/datepicker.html","template/datepicker/day.html","template/datepicker/month.html","template/datepicker/popup.html","template/datepicker/year.html","template/modal/backdrop.html","template/modal/window.html","template/pagination/pager.html","template/pagination/pagination.html","template/tooltip/tooltip-html-popup.html","template/tooltip/tooltip-html-unsafe-popup.html","template/tooltip/tooltip-popup.html","template/tooltip/tooltip-template-popup.html","template/popover/popover-template.html","template/popover/popover.html","template/progressbar/bar.html","template/progressbar/progress.html","template/progressbar/progressbar.html","template/rating/rating.html","template/tabs/tab.html","template/tabs/tabset.html","template/timepicker/timepicker.html","template/typeahead/typeahead-match.html","template/typeahead/typeahead-popup.html"]),angular.module("ui.bootstrap.collapse",[]).directive("collapse",["$animate",function(a){return{link:function(b,c,d){function e(){c.removeClass("collapse").addClass("collapsing"),a.addClass(c,"in",{to:{height:c[0].scrollHeight+"px"}}).then(f)}function f(){c.removeClass("collapsing"),c.css({height:"auto"})}function g(){c.css({height:c[0].scrollHeight+"px"}).removeClass("collapse").addClass("collapsing"),a.removeClass(c,"in",{to:{height:"0"}}).then(h)}function h(){c.css({height:"0"}),c.removeClass("collapsing"),c.addClass("collapse")}b.$watch(d.collapse,function(a){a?g():e()})}}}]),angular.module("ui.bootstrap.accordion",["ui.bootstrap.collapse"]).constant("accordionConfig",{closeOthers:!0}).controller("AccordionController",["$scope","$attrs","accordionConfig",function(a,b,c){this.groups=[],this.closeOthers=function(d){var e=angular.isDefined(b.closeOthers)?a.$eval(b.closeOthers):c.closeOthers;e&&angular.forEach(this.groups,function(a){a!==d&&(a.isOpen=!1)})},this.addGroup=function(a){var b=this;this.groups.push(a),a.$on("$destroy",function(){b.removeGroup(a)})},this.removeGroup=function(a){var b=this.groups.indexOf(a);-1!==b&&this.groups.splice(b,1)}}]).directive("accordion",function(){return{restrict:"EA",controller:"AccordionController",transclude:!0,replace:!1,templateUrl:"template/accordion/accordion.html"}}).directive("accordionGroup",function(){return{require:"^accordion",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/accordion/accordion-group.html",scope:{heading:"@",isOpen:"=?",isDisabled:"=?"},controller:function(){this.setHeading=function(a){this.heading=a}},link:function(a,b,c,d){d.addGroup(a),a.$watch("isOpen",function(b){b&&d.closeOthers(a)}),a.toggleOpen=function(){a.isDisabled||(a.isOpen=!a.isOpen)}}}}).directive("accordionHeading",function(){return{restrict:"EA",transclude:!0,template:"",replace:!0,require:"^accordionGroup",link:function(a,b,c,d,e){d.setHeading(e(a,angular.noop))}}}).directive("accordionTransclude",function(){return{require:"^accordionGroup",link:function(a,b,c,d){a.$watch(function(){return d[c.accordionTransclude]},function(a){a&&(b.html(""),b.append(a))})}}}),angular.module("ui.bootstrap.alert",[]).controller("AlertController",["$scope","$attrs",function(a,b){a.closeable="close"in b,this.close=a.close}]).directive("alert",function(){return{restrict:"EA",controller:"AlertController",templateUrl:"template/alert/alert.html",transclude:!0,replace:!0,scope:{type:"@",close:"&"}}}).directive("dismissOnTimeout",["$timeout",function(a){return{require:"alert",link:function(b,c,d,e){a(function(){e.close()},parseInt(d.dismissOnTimeout,10))}}}]),angular.module("ui.bootstrap.bindHtml",[]).directive("bindHtmlUnsafe",function(){return function(a,b,c){b.addClass("ng-binding").data("$binding",c.bindHtmlUnsafe),a.$watch(c.bindHtmlUnsafe,function(a){b.html(a||"")})}}),angular.module("ui.bootstrap.buttons",[]).constant("buttonConfig",{activeClass:"active",toggleEvent:"click"}).controller("ButtonsController",["buttonConfig",function(a){this.activeClass=a.activeClass||"active",this.toggleEvent=a.toggleEvent||"click"}]).directive("btnRadio",function(){return{require:["btnRadio","ngModel"],controller:"ButtonsController",link:function(a,b,c,d){var e=d[0],f=d[1];f.$render=function(){b.toggleClass(e.activeClass,angular.equals(f.$modelValue,a.$eval(c.btnRadio)))},b.bind(e.toggleEvent,function(){var d=b.hasClass(e.activeClass);(!d||angular.isDefined(c.uncheckable))&&a.$apply(function(){f.$setViewValue(d?null:a.$eval(c.btnRadio)),f.$render()})})}}}).directive("btnCheckbox",function(){return{require:["btnCheckbox","ngModel"],controller:"ButtonsController",link:function(a,b,c,d){function e(){return g(c.btnCheckboxTrue,!0)}function f(){return g(c.btnCheckboxFalse,!1)}function g(b,c){var d=a.$eval(b);return angular.isDefined(d)?d:c}var h=d[0],i=d[1];i.$render=function(){b.toggleClass(h.activeClass,angular.equals(i.$modelValue,e()))},b.bind(h.toggleEvent,function(){a.$apply(function(){i.$setViewValue(b.hasClass(h.activeClass)?f():e()),i.$render()})})}}}),angular.module("ui.bootstrap.carousel",[]).controller("CarouselController",["$scope","$interval","$animate",function(a,b,c){function d(a){if(angular.isUndefined(k[a].index))return k[a];{var b;k.length}for(b=0;b<k.length;++b)if(k[b].index==a)return k[b]}function e(){f();var c=+a.interval;!isNaN(c)&&c>0&&(h=b(g,c))}function f(){h&&(b.cancel(h),h=null)}function g(){var b=+a.interval;i&&!isNaN(b)&&b>0?a.next():a.pause()}var h,i,j=this,k=j.slides=a.slides=[],l=-1;j.currentSlide=null;var m=!1;j.select=a.select=function(b,d){function f(){m||(angular.extend(b,{direction:d,active:!0}),angular.extend(j.currentSlide||{},{direction:d,active:!1}),c.enabled()&&!a.noTransition&&b.$element&&(a.$currentTransition=!0,b.$element.one("$animate:close",function(){a.$currentTransition=null})),j.currentSlide=b,l=g,e())}var g=j.indexOfSlide(b);void 0===d&&(d=g>j.getCurrentIndex()?"next":"prev"),b&&b!==j.currentSlide&&f()},a.$on("$destroy",function(){m=!0}),j.getCurrentIndex=function(){return j.currentSlide&&angular.isDefined(j.currentSlide.index)?+j.currentSlide.index:l},j.indexOfSlide=function(a){return angular.isDefined(a.index)?+a.index:k.indexOf(a)},a.next=function(){var b=(j.getCurrentIndex()+1)%k.length;return a.$currentTransition?void 0:j.select(d(b),"next")},a.prev=function(){var b=j.getCurrentIndex()-1<0?k.length-1:j.getCurrentIndex()-1;return a.$currentTransition?void 0:j.select(d(b),"prev")},a.isActive=function(a){return j.currentSlide===a},a.$watch("interval",e),a.$on("$destroy",f),a.play=function(){i||(i=!0,e())},a.pause=function(){a.noPause||(i=!1,f())},j.addSlide=function(b,c){b.$element=c,k.push(b),1===k.length||b.active?(j.select(k[k.length-1]),1==k.length&&a.play()):b.active=!1},j.removeSlide=function(a){angular.isDefined(a.index)&&k.sort(function(a,b){return+a.index>+b.index});var b=k.indexOf(a);k.splice(b,1),k.length>0&&a.active?j.select(b>=k.length?k[b-1]:k[b]):l>b&&l--}}]).directive("carousel",[function(){return{restrict:"EA",transclude:!0,replace:!0,controller:"CarouselController",require:"carousel",templateUrl:"template/carousel/carousel.html",scope:{interval:"=",noTransition:"=",noPause:"="}}}]).directive("slide",function(){return{require:"^carousel",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/carousel/slide.html",scope:{active:"=?",index:"=?"},link:function(a,b,c,d){d.addSlide(a,b),a.$on("$destroy",function(){d.removeSlide(a)}),a.$watch("active",function(b){b&&d.select(a)})}}}).animation(".item",["$animate",function(a){return{beforeAddClass:function(b,c,d){if("active"==c&&b.parent()&&!b.parent().scope().noTransition){var e=!1,f=b.isolateScope().direction,g="next"==f?"left":"right";return b.addClass(f),a.addClass(b,g).then(function(){e||b.removeClass(g+" "+f),d()}),function(){e=!0}}d()},beforeRemoveClass:function(b,c,d){if("active"==c&&b.parent()&&!b.parent().scope().noTransition){var e=!1,f=b.isolateScope().direction,g="next"==f?"left":"right";return a.addClass(b,g).then(function(){e||b.removeClass(g),d()}),function(){e=!0}}d()}}}]),angular.module("ui.bootstrap.dateparser",[]).service("dateParser",["$locale","orderByFilter",function(a,b){function c(a){var c=[],d=a.split("");return angular.forEach(f,function(b,e){var f=a.indexOf(e);if(f>-1){a=a.split(""),d[f]="("+b.regex+")",a[f]="$";for(var g=f+1,h=f+e.length;h>g;g++)d[g]="",a[g]="$";a=a.join(""),c.push({index:f,apply:b.apply})}}),{regex:new RegExp("^"+d.join("")+"$"),map:b(c,"index")}}function d(a,b,c){return 1>c?!1:1===b&&c>28?29===c&&(a%4===0&&a%100!==0||a%400===0):3===b||5===b||8===b||10===b?31>c:!0}var e=/[\\\^\$\*\+\?\|\[\]\(\)\.\{\}]/g;this.parsers={};var f={yyyy:{regex:"\\d{4}",apply:function(a){this.year=+a}},yy:{regex:"\\d{2}",apply:function(a){this.year=+a+2e3}},y:{regex:"\\d{1,4}",apply:function(a){this.year=+a}},MMMM:{regex:a.DATETIME_FORMATS.MONTH.join("|"),apply:function(b){this.month=a.DATETIME_FORMATS.MONTH.indexOf(b)}},MMM:{regex:a.DATETIME_FORMATS.SHORTMONTH.join("|"),apply:function(b){this.month=a.DATETIME_FORMATS.SHORTMONTH.indexOf(b)}},MM:{regex:"0[1-9]|1[0-2]",apply:function(a){this.month=a-1}},M:{regex:"[1-9]|1[0-2]",apply:function(a){this.month=a-1}},dd:{regex:"[0-2][0-9]{1}|3[0-1]{1}",apply:function(a){this.date=+a}},d:{regex:"[1-2]?[0-9]{1}|3[0-1]{1}",apply:function(a){this.date=+a}},EEEE:{regex:a.DATETIME_FORMATS.DAY.join("|")},EEE:{regex:a.DATETIME_FORMATS.SHORTDAY.join("|")},HH:{regex:"(?:0|1)[0-9]|2[0-3]",apply:function(a){this.hours=+a}},H:{regex:"1?[0-9]|2[0-3]",apply:function(a){this.hours=+a}},mm:{regex:"[0-5][0-9]",apply:function(a){this.minutes=+a}},m:{regex:"[0-9]|[1-5][0-9]",apply:function(a){this.minutes=+a}},sss:{regex:"[0-9][0-9][0-9]",apply:function(a){this.milliseconds=+a}},ss:{regex:"[0-5][0-9]",apply:function(a){this.seconds=+a}},s:{regex:"[0-9]|[1-5][0-9]",apply:function(a){this.seconds=+a}}};this.parse=function(b,f,g){if(!angular.isString(b)||!f)return b;f=a.DATETIME_FORMATS[f]||f,f=f.replace(e,"\\$&"),this.parsers[f]||(this.parsers[f]=c(f));var h=this.parsers[f],i=h.regex,j=h.map,k=b.match(i);if(k&&k.length){var l,m;l=g?{year:g.getFullYear(),month:g.getMonth(),date:g.getDate(),hours:g.getHours(),minutes:g.getMinutes(),seconds:g.getSeconds(),milliseconds:g.getMilliseconds()}:{year:1900,month:0,date:1,hours:0,minutes:0,seconds:0,milliseconds:0};for(var n=1,o=k.length;o>n;n++){var p=j[n-1];p.apply&&p.apply.call(l,k[n])}return d(l.year,l.month,l.date)&&(m=new Date(l.year,l.month,l.date,l.hours,l.minutes,l.seconds,l.milliseconds||0)),m}}}]),angular.module("ui.bootstrap.position",[]).factory("$position",["$document","$window",function(a,b){function c(a,c){return a.currentStyle?a.currentStyle[c]:b.getComputedStyle?b.getComputedStyle(a)[c]:a.style[c]}function d(a){return"static"===(c(a,"position")||"static")}var e=function(b){for(var c=a[0],e=b.offsetParent||c;e&&e!==c&&d(e);)e=e.offsetParent;return e||c};return{position:function(b){var c=this.offset(b),d={top:0,left:0},f=e(b[0]);f!=a[0]&&(d=this.offset(angular.element(f)),d.top+=f.clientTop-f.scrollTop,d.left+=f.clientLeft-f.scrollLeft);var g=b[0].getBoundingClientRect();return{width:g.width||b.prop("offsetWidth"),height:g.height||b.prop("offsetHeight"),top:c.top-d.top,left:c.left-d.left}},offset:function(c){var d=c[0].getBoundingClientRect();return{width:d.width||c.prop("offsetWidth"),height:d.height||c.prop("offsetHeight"),top:d.top+(b.pageYOffset||a[0].documentElement.scrollTop),left:d.left+(b.pageXOffset||a[0].documentElement.scrollLeft)}},positionElements:function(a,b,c,d){var e,f,g,h,i=c.split("-"),j=i[0],k=i[1]||"center";e=d?this.offset(a):this.position(a),f=b.prop("offsetWidth"),g=b.prop("offsetHeight");var l={center:function(){return e.left+e.width/2-f/2},left:function(){return e.left},right:function(){return e.left+e.width}},m={center:function(){return e.top+e.height/2-g/2},top:function(){return e.top},bottom:function(){return e.top+e.height}};switch(j){case"right":h={top:m[k](),left:l[j]()};break;case"left":h={top:m[k](),left:e.left-f};break;case"bottom":h={top:m[j](),left:l[k]()};break;default:h={top:e.top-g,left:l[k]()}}return h}}}]),angular.module("ui.bootstrap.datepicker",["ui.bootstrap.dateparser","ui.bootstrap.position"]).constant("datepickerConfig",{formatDay:"dd",formatMonth:"MMMM",formatYear:"yyyy",formatDayHeader:"EEE",formatDayTitle:"MMMM yyyy",formatMonthTitle:"yyyy",datepickerMode:"day",minMode:"day",maxMode:"year",showWeeks:!0,startingDay:0,yearRange:20,minDate:null,maxDate:null,shortcutPropagation:!1}).controller("DatepickerController",["$scope","$attrs","$parse","$interpolate","$timeout","$log","dateFilter","datepickerConfig",function(a,b,c,d,e,f,g,h){var i=this,j={$setViewValue:angular.noop};this.modes=["day","month","year"],angular.forEach(["formatDay","formatMonth","formatYear","formatDayHeader","formatDayTitle","formatMonthTitle","minMode","maxMode","showWeeks","startingDay","yearRange","shortcutPropagation"],function(c,e){i[c]=angular.isDefined(b[c])?8>e?d(b[c])(a.$parent):a.$parent.$eval(b[c]):h[c]}),angular.forEach(["minDate","maxDate"],function(d){b[d]?a.$parent.$watch(c(b[d]),function(a){i[d]=a?new Date(a):null,i.refreshView()}):i[d]=h[d]?new Date(h[d]):null}),a.datepickerMode=a.datepickerMode||h.datepickerMode,a.maxMode=i.maxMode,a.uniqueId="datepicker-"+a.$id+"-"+Math.floor(1e4*Math.random()),angular.isDefined(b.initDate)?(this.activeDate=a.$parent.$eval(b.initDate)||new Date,a.$parent.$watch(b.initDate,function(a){a&&(j.$isEmpty(j.$modelValue)||j.$invalid)&&(i.activeDate=a,i.refreshView())})):this.activeDate=new Date,a.isActive=function(b){return 0===i.compare(b.date,i.activeDate)?(a.activeDateId=b.uid,!0):!1},this.init=function(a){j=a,j.$render=function(){i.render()}},this.render=function(){if(j.$viewValue){var a=new Date(j.$viewValue),b=!isNaN(a);b?this.activeDate=a:f.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.'),j.$setValidity("date",b)}this.refreshView()},this.refreshView=function(){if(this.element){this._refreshView();var a=j.$viewValue?new Date(j.$viewValue):null;j.$setValidity("date-disabled",!a||this.element&&!this.isDisabled(a))}},this.createDateObject=function(a,b){var c=j.$viewValue?new Date(j.$viewValue):null;return{date:a,label:g(a,b),selected:c&&0===this.compare(a,c),disabled:this.isDisabled(a),current:0===this.compare(a,new Date),customClass:this.customClass(a)}},this.isDisabled=function(c){return this.minDate&&this.compare(c,this.minDate)<0||this.maxDate&&this.compare(c,this.maxDate)>0||b.dateDisabled&&a.dateDisabled({date:c,mode:a.datepickerMode})},this.customClass=function(b){return a.customClass({date:b,mode:a.datepickerMode})},this.split=function(a,b){for(var c=[];a.length>0;)c.push(a.splice(0,b));return c},a.select=function(b){if(a.datepickerMode===i.minMode){var c=j.$viewValue?new Date(j.$viewValue):new Date(0,0,0,0,0,0,0);c.setFullYear(b.getFullYear(),b.getMonth(),b.getDate()),j.$setViewValue(c),j.$render()}else i.activeDate=b,a.datepickerMode=i.modes[i.modes.indexOf(a.datepickerMode)-1]},a.move=function(a){var b=i.activeDate.getFullYear()+a*(i.step.years||0),c=i.activeDate.getMonth()+a*(i.step.months||0);i.activeDate.setFullYear(b,c,1),i.refreshView()},a.toggleMode=function(b){b=b||1,a.datepickerMode===i.maxMode&&1===b||a.datepickerMode===i.minMode&&-1===b||(a.datepickerMode=i.modes[i.modes.indexOf(a.datepickerMode)+b])},a.keys={13:"enter",32:"space",33:"pageup",34:"pagedown",35:"end",36:"home",37:"left",38:"up",39:"right",40:"down"};var k=function(){e(function(){i.element[0].focus()},0,!1)};a.$on("datepicker.focus",k),a.keydown=function(b){var c=a.keys[b.which];if(c&&!b.shiftKey&&!b.altKey)if(b.preventDefault(),i.shortcutPropagation||b.stopPropagation(),"enter"===c||"space"===c){if(i.isDisabled(i.activeDate))return;a.select(i.activeDate),k()}else!b.ctrlKey||"up"!==c&&"down"!==c?(i.handleKeyDown(c,b),i.refreshView()):(a.toggleMode("up"===c?1:-1),k())}}]).directive("datepicker",function(){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/datepicker.html",scope:{datepickerMode:"=?",dateDisabled:"&",customClass:"&",shortcutPropagation:"&?"},require:["datepicker","?^ngModel"],controller:"DatepickerController",link:function(a,b,c,d){var e=d[0],f=d[1];f&&e.init(f)}}}).directive("daypicker",["dateFilter",function(a){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/day.html",require:"^datepicker",link:function(b,c,d,e){function f(a,b){return 1!==b||a%4!==0||a%100===0&&a%400!==0?i[b]:29}function g(a,b){var c=new Array(b),d=new Date(a),e=0;for(d.setHours(12);b>e;)c[e++]=new Date(d),d.setDate(d.getDate()+1);return c}function h(a){var b=new Date(a);b.setDate(b.getDate()+4-(b.getDay()||7));var c=b.getTime();return b.setMonth(0),b.setDate(1),Math.floor(Math.round((c-b)/864e5)/7)+1}b.showWeeks=e.showWeeks,e.step={months:1},e.element=c;var i=[31,28,31,30,31,30,31,31,30,31,30,31];e._refreshView=function(){var c=e.activeDate.getFullYear(),d=e.activeDate.getMonth(),f=new Date(c,d,1),i=e.startingDay-f.getDay(),j=i>0?7-i:-i,k=new Date(f);j>0&&k.setDate(-j+1);for(var l=g(k,42),m=0;42>m;m++)l[m]=angular.extend(e.createDateObject(l[m],e.formatDay),{secondary:l[m].getMonth()!==d,uid:b.uniqueId+"-"+m});b.labels=new Array(7);for(var n=0;7>n;n++)b.labels[n]={abbr:a(l[n].date,e.formatDayHeader),full:a(l[n].date,"EEEE")};if(b.title=a(e.activeDate,e.formatDayTitle),b.rows=e.split(l,7),b.showWeeks){b.weekNumbers=[];for(var o=(11-e.startingDay)%7,p=b.rows.length,q=0;p>q;q++)b.weekNumbers.push(h(b.rows[q][o].date))}},e.compare=function(a,b){return new Date(a.getFullYear(),a.getMonth(),a.getDate())-new Date(b.getFullYear(),b.getMonth(),b.getDate())},e.handleKeyDown=function(a){var b=e.activeDate.getDate();if("left"===a)b-=1;else if("up"===a)b-=7;else if("right"===a)b+=1;else if("down"===a)b+=7;else if("pageup"===a||"pagedown"===a){var c=e.activeDate.getMonth()+("pageup"===a?-1:1);e.activeDate.setMonth(c,1),b=Math.min(f(e.activeDate.getFullYear(),e.activeDate.getMonth()),b)}else"home"===a?b=1:"end"===a&&(b=f(e.activeDate.getFullYear(),e.activeDate.getMonth()));e.activeDate.setDate(b)},e.refreshView()}}}]).directive("monthpicker",["dateFilter",function(a){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/month.html",require:"^datepicker",link:function(b,c,d,e){e.step={years:1},e.element=c,e._refreshView=function(){for(var c=new Array(12),d=e.activeDate.getFullYear(),f=0;12>f;f++)c[f]=angular.extend(e.createDateObject(new Date(d,f,1),e.formatMonth),{uid:b.uniqueId+"-"+f});b.title=a(e.activeDate,e.formatMonthTitle),b.rows=e.split(c,3)},e.compare=function(a,b){return new Date(a.getFullYear(),a.getMonth())-new Date(b.getFullYear(),b.getMonth())},e.handleKeyDown=function(a){var b=e.activeDate.getMonth();if("left"===a)b-=1;else if("up"===a)b-=3;else if("right"===a)b+=1;else if("down"===a)b+=3;else if("pageup"===a||"pagedown"===a){var c=e.activeDate.getFullYear()+("pageup"===a?-1:1);e.activeDate.setFullYear(c)}else"home"===a?b=0:"end"===a&&(b=11);e.activeDate.setMonth(b)},e.refreshView()}}}]).directive("yearpicker",["dateFilter",function(){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/year.html",require:"^datepicker",link:function(a,b,c,d){function e(a){return parseInt((a-1)/f,10)*f+1}var f=d.yearRange;d.step={years:f},d.element=b,d._refreshView=function(){for(var b=new Array(f),c=0,g=e(d.activeDate.getFullYear());f>c;c++)b[c]=angular.extend(d.createDateObject(new Date(g+c,0,1),d.formatYear),{uid:a.uniqueId+"-"+c});a.title=[b[0].label,b[f-1].label].join(" - "),a.rows=d.split(b,5)},d.compare=function(a,b){return a.getFullYear()-b.getFullYear()},d.handleKeyDown=function(a){var b=d.activeDate.getFullYear();"left"===a?b-=1:"up"===a?b-=5:"right"===a?b+=1:"down"===a?b+=5:"pageup"===a||"pagedown"===a?b+=("pageup"===a?-1:1)*d.step.years:"home"===a?b=e(d.activeDate.getFullYear()):"end"===a&&(b=e(d.activeDate.getFullYear())+f-1),d.activeDate.setFullYear(b)},d.refreshView()}}}]).constant("datepickerPopupConfig",{datepickerPopup:"yyyy-MM-dd",html5Types:{date:"yyyy-MM-dd","datetime-local":"yyyy-MM-ddTHH:mm:ss.sss",month:"yyyy-MM"},currentText:"Today",clearText:"Clear",closeText:"Done",closeOnDateSelection:!0,appendToBody:!1,showButtonBar:!0}).directive("datepickerPopup",["$compile","$parse","$document","$position","dateFilter","dateParser","datepickerPopupConfig",function(a,b,c,d,e,f,g){return{restrict:"EA",require:"ngModel",scope:{isOpen:"=?",currentText:"@",clearText:"@",closeText:"@",dateDisabled:"&",customClass:"&"},link:function(h,i,j,k){function l(a){return a.replace(/([A-Z])/g,function(a){return"-"+a.toLowerCase()})}function m(a){if(angular.isNumber(a)&&(a=new Date(a)),a){if(angular.isDate(a)&&!isNaN(a))return a;if(angular.isString(a)){var b=f.parse(a,o,h.date)||new Date(a);return isNaN(b)?void 0:b}return void 0}return null}function n(a,b){var c=a||b;if(angular.isNumber(c)&&(c=new Date(c)),c){if(angular.isDate(c)&&!isNaN(c))return!0;if(angular.isString(c)){var d=f.parse(c,o)||new Date(c);return!isNaN(d)}return!1}return!0}var o,p=angular.isDefined(j.closeOnDateSelection)?h.$parent.$eval(j.closeOnDateSelection):g.closeOnDateSelection,q=angular.isDefined(j.datepickerAppendToBody)?h.$parent.$eval(j.datepickerAppendToBody):g.appendToBody;h.showButtonBar=angular.isDefined(j.showButtonBar)?h.$parent.$eval(j.showButtonBar):g.showButtonBar,h.getText=function(a){return h[a+"Text"]||g[a+"Text"]};var r=!1;if(g.html5Types[j.type]?(o=g.html5Types[j.type],r=!0):(o=j.datepickerPopup||g.datepickerPopup,j.$observe("datepickerPopup",function(a){var b=a||g.datepickerPopup;if(b!==o&&(o=b,k.$modelValue=null,!o))throw new Error("datepickerPopup must have a date format specified.")})),!o)throw new Error("datepickerPopup must have a date format specified.");if(r&&j.datepickerPopup)throw new Error("HTML5 date input types do not support custom formats.");var s=angular.element("<div datepicker-popup-wrap><div datepicker></div></div>");s.attr({"ng-model":"date","ng-change":"dateSelection()"});var t=angular.element(s.children()[0]);if(r&&"month"==j.type&&(t.attr("datepicker-mode",'"month"'),t.attr("min-mode","month")),j.datepickerOptions){var u=h.$parent.$eval(j.datepickerOptions);u.initDate&&(h.initDate=u.initDate,t.attr("init-date","initDate"),delete u.initDate),angular.forEach(u,function(a,b){t.attr(l(b),a)})}h.watchData={},angular.forEach(["minDate","maxDate","datepickerMode","initDate","shortcutPropagation"],function(a){if(j[a]){var c=b(j[a]);if(h.$parent.$watch(c,function(b){h.watchData[a]=b}),t.attr(l(a),"watchData."+a),"datepickerMode"===a){var d=c.assign;h.$watch("watchData."+a,function(a,b){a!==b&&d(h.$parent,a)})}}}),j.dateDisabled&&t.attr("date-disabled","dateDisabled({ date: date, mode: mode })"),j.showWeeks&&t.attr("show-weeks",j.showWeeks),j.customClass&&t.attr("custom-class","customClass({ date: date, mode: mode })"),r?k.$formatters.push(function(a){return h.date=a,a}):(k.$$parserName="date",k.$validators.date=n,k.$parsers.unshift(m),k.$formatters.push(function(a){return h.date=a,k.$isEmpty(a)?a:e(a,o)})),h.dateSelection=function(a){angular.isDefined(a)&&(h.date=a);var b=h.date?e(h.date,o):"";i.val(b),k.$setViewValue(b),p&&(h.isOpen=!1,i[0].focus())},k.$viewChangeListeners.push(function(){h.date=f.parse(k.$viewValue,o,h.date)||new Date(k.$viewValue)});var v=function(a){h.isOpen&&a.target!==i[0]&&h.$apply(function(){h.isOpen=!1})},w=function(a){h.keydown(a)};i.bind("keydown",w),h.keydown=function(a){27===a.which?(a.preventDefault(),h.isOpen&&a.stopPropagation(),h.close()):40!==a.which||h.isOpen||(h.isOpen=!0)},h.$watch("isOpen",function(a){a?(h.$broadcast("datepicker.focus"),h.position=q?d.offset(i):d.position(i),h.position.top=h.position.top+i.prop("offsetHeight"),c.bind("click",v)):c.unbind("click",v)}),h.select=function(a){if("today"===a){var b=new Date;angular.isDate(h.date)?(a=new Date(h.date),a.setFullYear(b.getFullYear(),b.getMonth(),b.getDate())):a=new Date(b.setHours(0,0,0,0))}h.dateSelection(a)},h.close=function(){h.isOpen=!1,i[0].focus()};var x=a(s)(h);s.remove(),q?c.find("body").append(x):i.after(x),h.$on("$destroy",function(){x.remove(),i.unbind("keydown",w),c.unbind("click",v)})}}}]).directive("datepickerPopupWrap",function(){return{restrict:"EA",replace:!0,transclude:!0,templateUrl:"template/datepicker/popup.html",link:function(a,b){b.bind("click",function(a){a.preventDefault(),a.stopPropagation()})}}}),angular.module("ui.bootstrap.dropdown",["ui.bootstrap.position"]).constant("dropdownConfig",{openClass:"open"}).service("dropdownService",["$document","$rootScope",function(a,b){var c=null;this.open=function(b){c||(a.bind("click",d),a.bind("keydown",e)),c&&c!==b&&(c.isOpen=!1),c=b},this.close=function(b){c===b&&(c=null,a.unbind("click",d),a.unbind("keydown",e))};var d=function(a){if(c&&(!a||"disabled"!==c.getAutoClose())){var d=c.getToggleElement();if(!(a&&d&&d[0].contains(a.target))){var e=c.getElement();a&&"outsideClick"===c.getAutoClose()&&e&&e[0].contains(a.target)||(c.isOpen=!1,b.$$phase||c.$apply())}}},e=function(a){27===a.which&&(c.focusToggleElement(),d())}}]).controller("DropdownController",["$scope","$attrs","$parse","dropdownConfig","dropdownService","$animate","$position","$document",function(a,b,c,d,e,f,g,h){var i,j=this,k=a.$new(),l=d.openClass,m=angular.noop,n=b.onToggle?c(b.onToggle):angular.noop,o=!1;this.init=function(d){j.$element=d,b.isOpen&&(i=c(b.isOpen),m=i.assign,a.$watch(i,function(a){k.isOpen=!!a})),o=angular.isDefined(b.dropdownAppendToBody),o&&j.dropdownMenu&&(h.find("body").append(j.dropdownMenu),d.on("$destroy",function(){j.dropdownMenu.remove()}))},this.toggle=function(a){return k.isOpen=arguments.length?!!a:!k.isOpen},this.isOpen=function(){return k.isOpen},k.getToggleElement=function(){return j.toggleElement},k.getAutoClose=function(){return b.autoClose||"always"},k.getElement=function(){return j.$element},k.focusToggleElement=function(){j.toggleElement&&j.toggleElement[0].focus()},k.$watch("isOpen",function(b,c){if(o&&j.dropdownMenu){var d=g.positionElements(j.$element,j.dropdownMenu,"bottom-left",!0);j.dropdownMenu.css({top:d.top+"px",left:d.left+"px",display:b?"block":"none"})}f[b?"addClass":"removeClass"](j.$element,l),b?(k.focusToggleElement(),e.open(k)):e.close(k),m(a,b),angular.isDefined(b)&&b!==c&&n(a,{open:!!b})}),a.$on("$locationChangeSuccess",function(){k.isOpen=!1}),a.$on("$destroy",function(){k.$destroy()})}]).directive("dropdown",function(){return{controller:"DropdownController",link:function(a,b,c,d){d.init(b)}}}).directive("dropdownMenu",function(){return{restrict:"AC",require:"?^dropdown",link:function(a,b,c,d){d&&(d.dropdownMenu=b)}}}).directive("dropdownToggle",function(){return{require:"?^dropdown",link:function(a,b,c,d){if(d){d.toggleElement=b;var e=function(e){e.preventDefault(),b.hasClass("disabled")||c.disabled||a.$apply(function(){d.toggle()})};b.bind("click",e),b.attr({"aria-haspopup":!0,"aria-expanded":!1}),a.$watch(d.isOpen,function(a){b.attr("aria-expanded",!!a)}),a.$on("$destroy",function(){b.unbind("click",e)})}}}}),angular.module("ui.bootstrap.modal",[]).factory("$$stackedMap",function(){return{createNew:function(){var a=[];return{add:function(b,c){a.push({key:b,value:c})},get:function(b){for(var c=0;c<a.length;c++)if(b==a[c].key)return a[c]},keys:function(){for(var b=[],c=0;c<a.length;c++)b.push(a[c].key);return b},top:function(){return a[a.length-1]},remove:function(b){for(var c=-1,d=0;d<a.length;d++)if(b==a[d].key){c=d;break}return a.splice(c,1)[0]},removeTop:function(){return a.splice(a.length-1,1)[0]},length:function(){return a.length}}}}}).directive("modalBackdrop",["$timeout",function(a){function b(b){b.animate=!1,a(function(){b.animate=!0})}return{restrict:"EA",replace:!0,templateUrl:"template/modal/backdrop.html",compile:function(a,c){return a.addClass(c.backdropClass),b}}}]).directive("modalWindow",["$modalStack","$q",function(a,b){return{restrict:"EA",scope:{index:"@",animate:"="},replace:!0,transclude:!0,templateUrl:function(a,b){return b.templateUrl||"template/modal/window.html"},link:function(c,d,e){d.addClass(e.windowClass||""),c.size=e.size,c.close=function(b){var c=a.getTop();c&&c.value.backdrop&&"static"!=c.value.backdrop&&b.target===b.currentTarget&&(b.preventDefault(),b.stopPropagation(),a.dismiss(c.key,"backdrop click"))},c.$isRendered=!0;var f=b.defer();e.$observe("modalRender",function(a){"true"==a&&f.resolve()}),f.promise.then(function(){c.animate=!0;var b=d[0].querySelectorAll("[autofocus]");b.length?b[0].focus():d[0].focus();var e=a.getTop();e&&a.modalRendered(e.key)})}}}]).directive("modalAnimationClass",[function(){return{compile:function(a,b){b.modalAnimation&&a.addClass(b.modalAnimationClass)}}}]).directive("modalTransclude",function(){return{link:function(a,b,c,d,e){e(a.$parent,function(a){b.empty(),b.append(a)})}}}).factory("$modalStack",["$animate","$timeout","$document","$compile","$rootScope","$$stackedMap",function(a,b,c,d,e,f){function g(){for(var a=-1,b=o.keys(),c=0;c<b.length;c++)o.get(b[c]).value.backdrop&&(a=c);return a}function h(a){var b=c.find("body").eq(0),d=o.get(a).value;o.remove(a),j(d.modalDomEl,d.modalScope,function(){b.toggleClass(n,o.length()>0),i()})}function i(){if(l&&-1==g()){var a=m;j(l,m,function(){a=null}),l=void 0,m=void 0}}function j(c,d,f){function g(){g.done||(g.done=!0,c.remove(),d.$destroy(),f&&f())}d.animate=!1,c.attr("modal-animation")&&a.enabled()?c.one("$animate:close",function(){e.$evalAsync(g)}):b(g)}function k(a,b,c){return!a.value.modalScope.$broadcast("modal.closing",b,c).defaultPrevented}var l,m,n="modal-open",o=f.createNew(),p={};return e.$watch(g,function(a){m&&(m.index=a)}),c.bind("keydown",function(a){var b;27===a.which&&(b=o.top(),b&&b.value.keyboard&&(a.preventDefault(),e.$apply(function(){p.dismiss(b.key,"escape key press")})))}),p.open=function(a,b){var f=c[0].activeElement;o.add(a,{deferred:b.deferred,renderDeferred:b.renderDeferred,modalScope:b.scope,backdrop:b.backdrop,keyboard:b.keyboard});var h=c.find("body").eq(0),i=g();if(i>=0&&!l){m=e.$new(!0),m.index=i;var j=angular.element('<div modal-backdrop="modal-backdrop"></div>');j.attr("backdrop-class",b.backdropClass),b.animation&&j.attr("modal-animation","true"),l=d(j)(m),h.append(l)}var k=angular.element('<div modal-window="modal-window"></div>');k.attr({"template-url":b.windowTemplateUrl,"window-class":b.windowClass,size:b.size,index:o.length()-1,animate:"animate"}).html(b.content),b.animation&&k.attr("modal-animation","true");var p=d(k)(b.scope);o.top().value.modalDomEl=p,o.top().value.modalOpener=f,h.append(p),h.addClass(n)},p.close=function(a,b){var c=o.get(a);return c&&k(c,b,!0)?(c.value.deferred.resolve(b),h(a),c.value.modalOpener.focus(),!0):!c},p.dismiss=function(a,b){var c=o.get(a);return c&&k(c,b,!1)?(c.value.deferred.reject(b),h(a),c.value.modalOpener.focus(),!0):!c},p.dismissAll=function(a){for(var b=this.getTop();b&&this.dismiss(b.key,a);)b=this.getTop()},p.getTop=function(){return o.top()},p.modalRendered=function(a){var b=o.get(a);b&&b.value.renderDeferred.resolve()},p}]).provider("$modal",function(){var a={options:{animation:!0,backdrop:!0,keyboard:!0},$get:["$injector","$rootScope","$q","$templateRequest","$controller","$modalStack",function(b,c,d,e,f,g){function h(a){return a.template?d.when(a.template):e(angular.isFunction(a.templateUrl)?a.templateUrl():a.templateUrl)}function i(a){var c=[];return angular.forEach(a,function(a){(angular.isFunction(a)||angular.isArray(a))&&c.push(d.when(b.invoke(a)))
}),c}var j={};return j.open=function(b){var e=d.defer(),j=d.defer(),k=d.defer(),l={result:e.promise,opened:j.promise,rendered:k.promise,close:function(a){return g.close(l,a)},dismiss:function(a){return g.dismiss(l,a)}};if(b=angular.extend({},a.options,b),b.resolve=b.resolve||{},!b.template&&!b.templateUrl)throw new Error("One of template or templateUrl options is required.");var m=d.all([h(b)].concat(i(b.resolve)));return m.then(function(a){var d=(b.scope||c).$new();d.$close=l.close,d.$dismiss=l.dismiss;var h,i={},j=1;b.controller&&(i.$scope=d,i.$modalInstance=l,angular.forEach(b.resolve,function(b,c){i[c]=a[j++]}),h=f(b.controller,i),b.controllerAs&&(d[b.controllerAs]=h)),g.open(l,{scope:d,deferred:e,renderDeferred:k,content:a[0],animation:b.animation,backdrop:b.backdrop,keyboard:b.keyboard,backdropClass:b.backdropClass,windowClass:b.windowClass,windowTemplateUrl:b.windowTemplateUrl,size:b.size})},function(a){e.reject(a)}),m.then(function(){j.resolve(!0)},function(a){j.reject(a)}),l},j}]};return a}),angular.module("ui.bootstrap.pagination",[]).controller("PaginationController",["$scope","$attrs","$parse",function(a,b,c){var d=this,e={$setViewValue:angular.noop},f=b.numPages?c(b.numPages).assign:angular.noop;this.init=function(g,h){e=g,this.config=h,e.$render=function(){d.render()},b.itemsPerPage?a.$parent.$watch(c(b.itemsPerPage),function(b){d.itemsPerPage=parseInt(b,10),a.totalPages=d.calculateTotalPages()}):this.itemsPerPage=h.itemsPerPage,a.$watch("totalItems",function(){a.totalPages=d.calculateTotalPages()}),a.$watch("totalPages",function(b){f(a.$parent,b),a.page>b?a.selectPage(b):e.$render()})},this.calculateTotalPages=function(){var b=this.itemsPerPage<1?1:Math.ceil(a.totalItems/this.itemsPerPage);return Math.max(b||0,1)},this.render=function(){a.page=parseInt(e.$viewValue,10)||1},a.selectPage=function(b,c){a.page!==b&&b>0&&b<=a.totalPages&&(c&&c.target&&c.target.blur(),e.$setViewValue(b),e.$render())},a.getText=function(b){return a[b+"Text"]||d.config[b+"Text"]},a.noPrevious=function(){return 1===a.page},a.noNext=function(){return a.page===a.totalPages}}]).constant("paginationConfig",{itemsPerPage:10,boundaryLinks:!1,directionLinks:!0,firstText:"First",previousText:"Previous",nextText:"Next",lastText:"Last",rotate:!0}).directive("pagination",["$parse","paginationConfig",function(a,b){return{restrict:"EA",scope:{totalItems:"=",firstText:"@",previousText:"@",nextText:"@",lastText:"@"},require:["pagination","?ngModel"],controller:"PaginationController",templateUrl:"template/pagination/pagination.html",replace:!0,link:function(c,d,e,f){function g(a,b,c){return{number:a,text:b,active:c}}function h(a,b){var c=[],d=1,e=b,f=angular.isDefined(k)&&b>k;f&&(l?(d=Math.max(a-Math.floor(k/2),1),e=d+k-1,e>b&&(e=b,d=e-k+1)):(d=(Math.ceil(a/k)-1)*k+1,e=Math.min(d+k-1,b)));for(var h=d;e>=h;h++){var i=g(h,h,h===a);c.push(i)}if(f&&!l){if(d>1){var j=g(d-1,"...",!1);c.unshift(j)}if(b>e){var m=g(e+1,"...",!1);c.push(m)}}return c}var i=f[0],j=f[1];if(j){var k=angular.isDefined(e.maxSize)?c.$parent.$eval(e.maxSize):b.maxSize,l=angular.isDefined(e.rotate)?c.$parent.$eval(e.rotate):b.rotate;c.boundaryLinks=angular.isDefined(e.boundaryLinks)?c.$parent.$eval(e.boundaryLinks):b.boundaryLinks,c.directionLinks=angular.isDefined(e.directionLinks)?c.$parent.$eval(e.directionLinks):b.directionLinks,i.init(j,b),e.maxSize&&c.$parent.$watch(a(e.maxSize),function(a){k=parseInt(a,10),i.render()});var m=i.render;i.render=function(){m(),c.page>0&&c.page<=c.totalPages&&(c.pages=h(c.page,c.totalPages))}}}}}]).constant("pagerConfig",{itemsPerPage:10,previousText:"« Previous",nextText:"Next »",align:!0}).directive("pager",["pagerConfig",function(a){return{restrict:"EA",scope:{totalItems:"=",previousText:"@",nextText:"@"},require:["pager","?ngModel"],controller:"PaginationController",templateUrl:"template/pagination/pager.html",replace:!0,link:function(b,c,d,e){var f=e[0],g=e[1];g&&(b.align=angular.isDefined(d.align)?b.$parent.$eval(d.align):a.align,f.init(g,a))}}}]),angular.module("ui.bootstrap.tooltip",["ui.bootstrap.position","ui.bootstrap.bindHtml"]).provider("$tooltip",function(){function a(a){var b=/[A-Z]/g,c="-";return a.replace(b,function(a,b){return(b?c:"")+a.toLowerCase()})}var b={placement:"top",animation:!0,popupDelay:0,useContentExp:!1},c={mouseenter:"mouseleave",click:"click",focus:"blur"},d={};this.options=function(a){angular.extend(d,a)},this.setTriggers=function(a){angular.extend(c,a)},this.$get=["$window","$compile","$timeout","$document","$position","$interpolate",function(e,f,g,h,i,j){return function(e,k,l,m){function n(a){var b=a||m.trigger||l,d=c[b]||b;return{show:b,hide:d}}m=angular.extend({},b,d,m);var o=a(e),p=j.startSymbol(),q=j.endSymbol(),r="<div "+o+'-popup title="'+p+"title"+q+'" '+(m.useContentExp?'content-exp="contentExp()" ':'content="'+p+"content"+q+'" ')+'placement="'+p+"placement"+q+'" popup-class="'+p+"popupClass"+q+'" animation="animation" is-open="isOpen"origin-scope="origScope" ></div>';return{restrict:"EA",compile:function(){var a=f(r);return function(b,c,d){function f(){E.isOpen?l():j()}function j(){(!D||b.$eval(d[k+"Enable"]))&&(s(),E.popupDelay?A||(A=g(o,E.popupDelay,!1),A.then(function(a){a()})):o()())}function l(){b.$apply(function(){p()})}function o(){return A=null,z&&(g.cancel(z),z=null),(m.useContentExp?E.contentExp():E.content)?(q(),x.css({top:0,left:0,display:"block"}),E.$digest(),F(),E.isOpen=!0,E.$apply(),F):angular.noop}function p(){E.isOpen=!1,g.cancel(A),A=null,E.animation?z||(z=g(r,500)):r()}function q(){x&&r(),y=E.$new(),x=a(y,function(a){B?h.find("body").append(a):c.after(a)}),y.$watch(function(){g(F,0,!1)}),m.useContentExp&&y.$watch("contentExp()",function(a){!a&&E.isOpen&&p()})}function r(){z=null,x&&(x.remove(),x=null),y&&(y.$destroy(),y=null)}function s(){t(),u(),v()}function t(){E.popupClass=d[k+"Class"]}function u(){var a=d[k+"Placement"];E.placement=angular.isDefined(a)?a:m.placement}function v(){var a=d[k+"PopupDelay"],b=parseInt(a,10);E.popupDelay=isNaN(b)?m.popupDelay:b}function w(){var a=d[k+"Trigger"];G(),C=n(a),C.show===C.hide?c.bind(C.show,f):(c.bind(C.show,j),c.bind(C.hide,l))}var x,y,z,A,B=angular.isDefined(m.appendToBody)?m.appendToBody:!1,C=n(void 0),D=angular.isDefined(d[k+"Enable"]),E=b.$new(!0),F=function(){if(x){var a=i.positionElements(c,x,E.placement,B);a.top+="px",a.left+="px",x.css(a)}};E.origScope=b,E.isOpen=!1,E.contentExp=function(){return b.$eval(d[e])},m.useContentExp||d.$observe(e,function(a){E.content=a,!a&&E.isOpen&&p()}),d.$observe("disabled",function(a){a&&E.isOpen&&p()}),d.$observe(k+"Title",function(a){E.title=a});var G=function(){c.unbind(C.show,j),c.unbind(C.hide,l)};w();var H=b.$eval(d[k+"Animation"]);E.animation=angular.isDefined(H)?!!H:m.animation;var I=b.$eval(d[k+"AppendToBody"]);B=angular.isDefined(I)?I:B,B&&b.$on("$locationChangeSuccess",function(){E.isOpen&&p()}),b.$on("$destroy",function(){g.cancel(z),g.cancel(A),G(),r(),E=null})}}}}}]}).directive("tooltipTemplateTransclude",["$animate","$sce","$compile","$templateRequest",function(a,b,c,d){return{link:function(e,f,g){var h,i,j,k=e.$eval(g.tooltipTemplateTranscludeScope),l=0,m=function(){i&&(i.remove(),i=null),h&&(h.$destroy(),h=null),j&&(a.leave(j).then(function(){i=null}),i=j,j=null)};e.$watch(b.parseAsResourceUrl(g.tooltipTemplateTransclude),function(b){var g=++l;b?(d(b,!0).then(function(d){if(g===l){var e=k.$new(),i=d,n=c(i)(e,function(b){m(),a.enter(b,f)});h=e,j=n,h.$emit("$includeContentLoaded",b)}},function(){g===l&&(m(),e.$emit("$includeContentError",b))}),e.$emit("$includeContentRequested",b)):m()}),e.$on("$destroy",m)}}}]).directive("tooltipClasses",function(){return{restrict:"A",link:function(a,b,c){a.placement&&b.addClass(a.placement),a.popupClass&&b.addClass(a.popupClass),a.animation()&&b.addClass(c.tooltipAnimationClass)}}}).directive("tooltipPopup",function(){return{restrict:"EA",replace:!0,scope:{content:"@",placement:"@",popupClass:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-popup.html"}}).directive("tooltip",["$tooltip",function(a){return a("tooltip","tooltip","mouseenter")}]).directive("tooltipTemplatePopup",function(){return{restrict:"EA",replace:!0,scope:{contentExp:"&",placement:"@",popupClass:"@",animation:"&",isOpen:"&",originScope:"&"},templateUrl:"template/tooltip/tooltip-template-popup.html"}}).directive("tooltipTemplate",["$tooltip",function(a){return a("tooltipTemplate","tooltip","mouseenter",{useContentExp:!0})}]).directive("tooltipHtmlPopup",function(){return{restrict:"EA",replace:!0,scope:{contentExp:"&",placement:"@",popupClass:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-popup.html"}}).directive("tooltipHtml",["$tooltip",function(a){return a("tooltipHtml","tooltip","mouseenter",{useContentExp:!0})}]).directive("tooltipHtmlUnsafePopup",function(){return{restrict:"EA",replace:!0,scope:{content:"@",placement:"@",popupClass:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-unsafe-popup.html"}}).value("tooltipHtmlUnsafeSuppressDeprecated",!1).directive("tooltipHtmlUnsafe",["$tooltip","tooltipHtmlUnsafeSuppressDeprecated","$log",function(a,b,c){return b||c.warn("tooltip-html-unsafe is now deprecated. Use tooltip-html or tooltip-template instead."),a("tooltipHtmlUnsafe","tooltip","mouseenter")}]),angular.module("ui.bootstrap.popover",["ui.bootstrap.tooltip"]).directive("popoverTemplatePopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",contentExp:"&",placement:"@",popupClass:"@",animation:"&",isOpen:"&",originScope:"&"},templateUrl:"template/popover/popover-template.html"}}).directive("popoverTemplate",["$tooltip",function(a){return a("popoverTemplate","popover","click",{useContentExp:!0})}]).directive("popoverPopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",popupClass:"@",animation:"&",isOpen:"&"},templateUrl:"template/popover/popover.html"}}).directive("popover",["$tooltip",function(a){return a("popover","popover","click")}]),angular.module("ui.bootstrap.progressbar",[]).constant("progressConfig",{animate:!0,max:100}).controller("ProgressController",["$scope","$attrs","progressConfig",function(a,b,c){var d=this,e=angular.isDefined(b.animate)?a.$parent.$eval(b.animate):c.animate;this.bars=[],a.max=angular.isDefined(a.max)?a.max:c.max,this.addBar=function(b,c){e||c.css({transition:"none"}),this.bars.push(b),b.$watch("value",function(c){b.percent=+(100*c/a.max).toFixed(2)}),b.$on("$destroy",function(){c=null,d.removeBar(b)})},this.removeBar=function(a){this.bars.splice(this.bars.indexOf(a),1)}}]).directive("progress",function(){return{restrict:"EA",replace:!0,transclude:!0,controller:"ProgressController",require:"progress",scope:{},templateUrl:"template/progressbar/progress.html"}}).directive("bar",function(){return{restrict:"EA",replace:!0,transclude:!0,require:"^progress",scope:{value:"=",max:"=?",type:"@"},templateUrl:"template/progressbar/bar.html",link:function(a,b,c,d){d.addBar(a,b)}}}).directive("progressbar",function(){return{restrict:"EA",replace:!0,transclude:!0,controller:"ProgressController",scope:{value:"=",max:"=?",type:"@"},templateUrl:"template/progressbar/progressbar.html",link:function(a,b,c,d){d.addBar(a,angular.element(b.children()[0]))}}}),angular.module("ui.bootstrap.rating",[]).constant("ratingConfig",{max:5,stateOn:null,stateOff:null}).controller("RatingController",["$scope","$attrs","ratingConfig",function(a,b,c){var d={$setViewValue:angular.noop};this.init=function(e){d=e,d.$render=this.render,d.$formatters.push(function(a){return angular.isNumber(a)&&a<<0!==a&&(a=Math.round(a)),a}),this.stateOn=angular.isDefined(b.stateOn)?a.$parent.$eval(b.stateOn):c.stateOn,this.stateOff=angular.isDefined(b.stateOff)?a.$parent.$eval(b.stateOff):c.stateOff;var f=angular.isDefined(b.ratingStates)?a.$parent.$eval(b.ratingStates):new Array(angular.isDefined(b.max)?a.$parent.$eval(b.max):c.max);a.range=this.buildTemplateObjects(f)},this.buildTemplateObjects=function(a){for(var b=0,c=a.length;c>b;b++)a[b]=angular.extend({index:b},{stateOn:this.stateOn,stateOff:this.stateOff},a[b]);return a},a.rate=function(b){!a.readonly&&b>=0&&b<=a.range.length&&(d.$setViewValue(b),d.$render())},a.enter=function(b){a.readonly||(a.value=b),a.onHover({value:b})},a.reset=function(){a.value=d.$viewValue,a.onLeave()},a.onKeydown=function(b){/(37|38|39|40)/.test(b.which)&&(b.preventDefault(),b.stopPropagation(),a.rate(a.value+(38===b.which||39===b.which?1:-1)))},this.render=function(){a.value=d.$viewValue}}]).directive("rating",function(){return{restrict:"EA",require:["rating","ngModel"],scope:{readonly:"=?",onHover:"&",onLeave:"&"},controller:"RatingController",templateUrl:"template/rating/rating.html",replace:!0,link:function(a,b,c,d){var e=d[0],f=d[1];e.init(f)}}}),angular.module("ui.bootstrap.tabs",[]).controller("TabsetController",["$scope",function(a){var b=this,c=b.tabs=a.tabs=[];b.select=function(a){angular.forEach(c,function(b){b.active&&b!==a&&(b.active=!1,b.onDeselect())}),a.active=!0,a.onSelect()},b.addTab=function(a){c.push(a),1===c.length&&a.active!==!1?a.active=!0:a.active?b.select(a):a.active=!1},b.removeTab=function(a){var e=c.indexOf(a);if(a.active&&c.length>1&&!d){var f=e==c.length-1?e-1:e+1;b.select(c[f])}c.splice(e,1)};var d;a.$on("$destroy",function(){d=!0})}]).directive("tabset",function(){return{restrict:"EA",transclude:!0,replace:!0,scope:{type:"@"},controller:"TabsetController",templateUrl:"template/tabs/tabset.html",link:function(a,b,c){a.vertical=angular.isDefined(c.vertical)?a.$parent.$eval(c.vertical):!1,a.justified=angular.isDefined(c.justified)?a.$parent.$eval(c.justified):!1}}}).directive("tab",["$parse","$log",function(a,b){return{require:"^tabset",restrict:"EA",replace:!0,templateUrl:"template/tabs/tab.html",transclude:!0,scope:{active:"=?",heading:"@",onSelect:"&select",onDeselect:"&deselect"},controller:function(){},compile:function(c,d,e){return function(c,d,f,g){c.$watch("active",function(a){a&&g.select(c)}),c.disabled=!1,f.disable&&c.$parent.$watch(a(f.disable),function(a){c.disabled=!!a}),f.disabled&&(b.warn('Use of "disabled" attribute has been deprecated, please use "disable"'),c.$parent.$watch(a(f.disabled),function(a){c.disabled=!!a})),c.select=function(){c.disabled||(c.active=!0)},g.addTab(c),c.$on("$destroy",function(){g.removeTab(c)}),c.$transcludeFn=e}}}}]).directive("tabHeadingTransclude",[function(){return{restrict:"A",require:"^tab",link:function(a,b){a.$watch("headingElement",function(a){a&&(b.html(""),b.append(a))})}}}]).directive("tabContentTransclude",function(){function a(a){return a.tagName&&(a.hasAttribute("tab-heading")||a.hasAttribute("data-tab-heading")||"tab-heading"===a.tagName.toLowerCase()||"data-tab-heading"===a.tagName.toLowerCase())}return{restrict:"A",require:"^tabset",link:function(b,c,d){var e=b.$eval(d.tabContentTransclude);e.$transcludeFn(e.$parent,function(b){angular.forEach(b,function(b){a(b)?e.headingElement=b:c.append(b)})})}}}),angular.module("ui.bootstrap.timepicker",[]).constant("timepickerConfig",{hourStep:1,minuteStep:1,showMeridian:!0,meridians:null,readonlyInput:!1,mousewheel:!0,arrowkeys:!0}).controller("TimepickerController",["$scope","$attrs","$parse","$log","$locale","timepickerConfig",function(a,b,c,d,e,f){function g(){var b=parseInt(a.hours,10),c=a.showMeridian?b>0&&13>b:b>=0&&24>b;return c?(a.showMeridian&&(12===b&&(b=0),a.meridian===p[1]&&(b+=12)),b):void 0}function h(){var b=parseInt(a.minutes,10);return b>=0&&60>b?b:void 0}function i(a){return angular.isDefined(a)&&a.toString().length<2?"0"+a:a.toString()}function j(a){k(),o.$setViewValue(new Date(n)),l(a)}function k(){o.$setValidity("time",!0),a.invalidHours=!1,a.invalidMinutes=!1}function l(b){var c=n.getHours(),d=n.getMinutes();a.showMeridian&&(c=0===c||12===c?12:c%12),a.hours="h"===b?c:i(c),"m"!==b&&(a.minutes=i(d)),a.meridian=n.getHours()<12?p[0]:p[1]}function m(a){var b=new Date(n.getTime()+6e4*a);n.setHours(b.getHours(),b.getMinutes()),j()}var n=new Date,o={$setViewValue:angular.noop},p=angular.isDefined(b.meridians)?a.$parent.$eval(b.meridians):f.meridians||e.DATETIME_FORMATS.AMPMS;this.init=function(c,d){o=c,o.$render=this.render,o.$formatters.unshift(function(a){return a?new Date(a):null});var e=d.eq(0),g=d.eq(1),h=angular.isDefined(b.mousewheel)?a.$parent.$eval(b.mousewheel):f.mousewheel;h&&this.setupMousewheelEvents(e,g);var i=angular.isDefined(b.arrowkeys)?a.$parent.$eval(b.arrowkeys):f.arrowkeys;i&&this.setupArrowkeyEvents(e,g),a.readonlyInput=angular.isDefined(b.readonlyInput)?a.$parent.$eval(b.readonlyInput):f.readonlyInput,this.setupInputEvents(e,g)};var q=f.hourStep;b.hourStep&&a.$parent.$watch(c(b.hourStep),function(a){q=parseInt(a,10)});var r=f.minuteStep;b.minuteStep&&a.$parent.$watch(c(b.minuteStep),function(a){r=parseInt(a,10)}),a.showMeridian=f.showMeridian,b.showMeridian&&a.$parent.$watch(c(b.showMeridian),function(b){if(a.showMeridian=!!b,o.$error.time){var c=g(),d=h();angular.isDefined(c)&&angular.isDefined(d)&&(n.setHours(c),j())}else l()}),this.setupMousewheelEvents=function(b,c){var d=function(a){a.originalEvent&&(a=a.originalEvent);var b=a.wheelDelta?a.wheelDelta:-a.deltaY;return a.detail||b>0};b.bind("mousewheel wheel",function(b){a.$apply(d(b)?a.incrementHours():a.decrementHours()),b.preventDefault()}),c.bind("mousewheel wheel",function(b){a.$apply(d(b)?a.incrementMinutes():a.decrementMinutes()),b.preventDefault()})},this.setupArrowkeyEvents=function(b,c){b.bind("keydown",function(b){38===b.which?(b.preventDefault(),a.incrementHours(),a.$apply()):40===b.which&&(b.preventDefault(),a.decrementHours(),a.$apply())}),c.bind("keydown",function(b){38===b.which?(b.preventDefault(),a.incrementMinutes(),a.$apply()):40===b.which&&(b.preventDefault(),a.decrementMinutes(),a.$apply())})},this.setupInputEvents=function(b,c){if(a.readonlyInput)return a.updateHours=angular.noop,void(a.updateMinutes=angular.noop);var d=function(b,c){o.$setViewValue(null),o.$setValidity("time",!1),angular.isDefined(b)&&(a.invalidHours=b),angular.isDefined(c)&&(a.invalidMinutes=c)};a.updateHours=function(){var a=g();angular.isDefined(a)?(n.setHours(a),j("h")):d(!0)},b.bind("blur",function(){!a.invalidHours&&a.hours<10&&a.$apply(function(){a.hours=i(a.hours)})}),a.updateMinutes=function(){var a=h();angular.isDefined(a)?(n.setMinutes(a),j("m")):d(void 0,!0)},c.bind("blur",function(){!a.invalidMinutes&&a.minutes<10&&a.$apply(function(){a.minutes=i(a.minutes)})})},this.render=function(){var a=o.$viewValue;isNaN(a)?(o.$setValidity("time",!1),d.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')):(a&&(n=a),k(),l())},a.incrementHours=function(){m(60*q)},a.decrementHours=function(){m(60*-q)},a.incrementMinutes=function(){m(r)},a.decrementMinutes=function(){m(-r)},a.toggleMeridian=function(){m(720*(n.getHours()<12?1:-1))}}]).directive("timepicker",function(){return{restrict:"EA",require:["timepicker","?^ngModel"],controller:"TimepickerController",replace:!0,scope:{},templateUrl:"template/timepicker/timepicker.html",link:function(a,b,c,d){var e=d[0],f=d[1];f&&e.init(f,b.find("input"))}}}),angular.module("ui.bootstrap.transition",[]).value("$transitionSuppressDeprecated",!1).factory("$transition",["$q","$timeout","$rootScope","$log","$transitionSuppressDeprecated",function(a,b,c,d,e){function f(a){for(var b in a)if(void 0!==h.style[b])return a[b]}e||d.warn("$transition is now deprecated. Use $animate from ngAnimate instead.");var g=function(d,e,f){f=f||{};var h=a.defer(),i=g[f.animation?"animationEndEventName":"transitionEndEventName"],j=function(){c.$apply(function(){d.unbind(i,j),h.resolve(d)})};return i&&d.bind(i,j),b(function(){angular.isString(e)?d.addClass(e):angular.isFunction(e)?e(d):angular.isObject(e)&&d.css(e),i||h.resolve(d)}),h.promise.cancel=function(){i&&d.unbind(i,j),h.reject("Transition cancelled")},h.promise},h=document.createElement("trans"),i={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"},j={WebkitTransition:"webkitAnimationEnd",MozTransition:"animationend",OTransition:"oAnimationEnd",transition:"animationend"};return g.transitionEndEventName=f(i),g.animationEndEventName=f(j),g}]),angular.module("ui.bootstrap.typeahead",["ui.bootstrap.position","ui.bootstrap.bindHtml"]).factory("typeaheadParser",["$parse",function(a){var b=/^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+([\s\S]+?)$/;return{parse:function(c){var d=c.match(b);if(!d)throw new Error('Expected typeahead specification in form of "_modelValue_ (as _label_)? for _item_ in _collection_" but got "'+c+'".');return{itemName:d[3],source:a(d[4]),viewMapper:a(d[2]||d[1]),modelMapper:a(d[1])}}}}]).directive("typeahead",["$compile","$parse","$q","$timeout","$document","$position","typeaheadParser",function(a,b,c,d,e,f,g){var h=[9,13,27,38,40];return{require:"ngModel",link:function(i,j,k,l){var m,n=i.$eval(k.typeaheadMinLength)||1,o=i.$eval(k.typeaheadWaitMs)||0,p=i.$eval(k.typeaheadEditable)!==!1,q=b(k.typeaheadLoading).assign||angular.noop,r=b(k.typeaheadOnSelect),s=k.typeaheadInputFormatter?b(k.typeaheadInputFormatter):void 0,t=k.typeaheadAppendToBody?i.$eval(k.typeaheadAppendToBody):!1,u=i.$eval(k.typeaheadFocusFirst)!==!1,v=b(k.ngModel).assign,w=g.parse(k.typeahead),x=i.$new();i.$on("$destroy",function(){x.$destroy()});var y="typeahead-"+x.$id+"-"+Math.floor(1e4*Math.random());j.attr({"aria-autocomplete":"list","aria-expanded":!1,"aria-owns":y});var z=angular.element("<div typeahead-popup></div>");z.attr({id:y,matches:"matches",active:"activeIdx",select:"select(activeIdx)",query:"query",position:"position"}),angular.isDefined(k.typeaheadTemplateUrl)&&z.attr("template-url",k.typeaheadTemplateUrl);var A=function(){x.matches=[],x.activeIdx=-1,j.attr("aria-expanded",!1)},B=function(a){return y+"-option-"+a};x.$watch("activeIdx",function(a){0>a?j.removeAttr("aria-activedescendant"):j.attr("aria-activedescendant",B(a))});var C=function(a){var b={$viewValue:a};q(i,!0),c.when(w.source(i,b)).then(function(c){var d=a===l.$viewValue;if(d&&m)if(c&&c.length>0){x.activeIdx=u?0:-1,x.matches.length=0;for(var e=0;e<c.length;e++)b[w.itemName]=c[e],x.matches.push({id:B(e),label:w.viewMapper(x,b),model:c[e]});x.query=a,x.position=t?f.offset(j):f.position(j),x.position.top=x.position.top+j.prop("offsetHeight"),j.attr("aria-expanded",!0)}else A();d&&q(i,!1)},function(){A(),q(i,!1)})};A(),x.query=void 0;var D,E=function(a){D=d(function(){C(a)},o)},F=function(){D&&d.cancel(D)};l.$parsers.unshift(function(a){return m=!0,a&&a.length>=n?o>0?(F(),E(a)):C(a):(q(i,!1),F(),A()),p?a:a?void l.$setValidity("editable",!1):(l.$setValidity("editable",!0),a)}),l.$formatters.push(function(a){var b,c,d={};return p||l.$setValidity("editable",!0),s?(d.$model=a,s(i,d)):(d[w.itemName]=a,b=w.viewMapper(i,d),d[w.itemName]=void 0,c=w.viewMapper(i,d),b!==c?b:a)}),x.select=function(a){var b,c,e={};e[w.itemName]=c=x.matches[a].model,b=w.modelMapper(i,e),v(i,b),l.$setValidity("editable",!0),l.$setValidity("parse",!0),r(i,{$item:c,$model:b,$label:w.viewMapper(i,e)}),A(),d(function(){j[0].focus()},0,!1)},j.bind("keydown",function(a){0!==x.matches.length&&-1!==h.indexOf(a.which)&&(-1!=x.activeIdx||13!==a.which&&9!==a.which)&&(a.preventDefault(),40===a.which?(x.activeIdx=(x.activeIdx+1)%x.matches.length,x.$digest()):38===a.which?(x.activeIdx=(x.activeIdx>0?x.activeIdx:x.matches.length)-1,x.$digest()):13===a.which||9===a.which?x.$apply(function(){x.select(x.activeIdx)}):27===a.which&&(a.stopPropagation(),A(),x.$digest()))}),j.bind("blur",function(){m=!1});var G=function(a){j[0]!==a.target&&(A(),x.$digest())};e.bind("click",G),i.$on("$destroy",function(){e.unbind("click",G),t&&H.remove(),z.remove()});var H=a(z)(x);t?e.find("body").append(H):j.after(H)}}}]).directive("typeaheadPopup",function(){return{restrict:"EA",scope:{matches:"=",query:"=",active:"=",position:"=",select:"&"},replace:!0,templateUrl:"template/typeahead/typeahead-popup.html",link:function(a,b,c){a.templateUrl=c.templateUrl,a.isOpen=function(){return a.matches.length>0},a.isActive=function(b){return a.active==b},a.selectActive=function(b){a.active=b},a.selectMatch=function(b){a.select({activeIdx:b})}}}}).directive("typeaheadMatch",["$templateRequest","$compile","$parse",function(a,b,c){return{restrict:"EA",scope:{index:"=",match:"=",query:"="},link:function(d,e,f){var g=c(f.templateUrl)(d.$parent)||"template/typeahead/typeahead-match.html";a(g).then(function(a){b(a.trim())(d,function(a){e.replaceWith(a)})})}}}]).filter("typeaheadHighlight",function(){function a(a){return a.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(b,c){return c?(""+b).replace(new RegExp(a(c),"gi"),"<strong>$&</strong>"):b}}),angular.module("template/accordion/accordion-group.html",[]).run(["$templateCache",function(a){a.put("template/accordion/accordion-group.html",'<div class="panel panel-default">\n  <div class="panel-heading">\n    <h4 class="panel-title">\n      <a href="javascript:void(0)" tabindex="0" class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading"><span ng-class="{\'text-muted\': isDisabled}">{{heading}}</span></a>\n    </h4>\n  </div>\n  <div class="panel-collapse collapse" collapse="!isOpen">\n	  <div class="panel-body" ng-transclude></div>\n  </div>\n</div>\n')}]),angular.module("template/accordion/accordion.html",[]).run(["$templateCache",function(a){a.put("template/accordion/accordion.html",'<div class="panel-group" ng-transclude></div>')}]),angular.module("template/alert/alert.html",[]).run(["$templateCache",function(a){a.put("template/alert/alert.html",'<div class="alert" ng-class="[\'alert-\' + (type || \'warning\'), closeable ? \'alert-dismissable\' : null]" role="alert">\n    <button ng-show="closeable" type="button" class="close" ng-click="close()">\n        <span aria-hidden="true">&times;</span>\n        <span class="sr-only">Close</span>\n    </button>\n    <div ng-transclude></div>\n</div>\n')}]),angular.module("template/carousel/carousel.html",[]).run(["$templateCache",function(a){a.put("template/carousel/carousel.html",'<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel" ng-swipe-right="prev()" ng-swipe-left="next()">\n    <ol class="carousel-indicators" ng-show="slides.length > 1">\n        <li ng-repeat="slide in slides | orderBy:\'index\' track by $index" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a class="left carousel-control" ng-click="prev()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-left"></span></a>\n    <a class="right carousel-control" ng-click="next()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-right"></span></a>\n</div>\n')}]),angular.module("template/carousel/slide.html",[]).run(["$templateCache",function(a){a.put("template/carousel/slide.html",'<div ng-class="{\n    \'active\': active\n  }" class="item text-center" ng-transclude></div>\n')}]),angular.module("template/datepicker/datepicker.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/datepicker.html",'<div ng-switch="datepickerMode" role="application" ng-keydown="keydown($event)">\n  <daypicker ng-switch-when="day" tabindex="0"></daypicker>\n  <monthpicker ng-switch-when="month" tabindex="0"></monthpicker>\n  <yearpicker ng-switch-when="year" tabindex="0"></yearpicker>\n</div>')}]),angular.module("template/datepicker/day.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/day.html",'<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="{{5 + showWeeks}}"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" ng-disabled="datepickerMode === maxMode" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n    <tr>\n      <th ng-show="showWeeks" class="text-center"></th>\n      <th ng-repeat="label in labels track by $index" class="text-center"><small aria-label="{{label.full}}">{{label.abbr}}</small></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-show="showWeeks" class="text-center h6"><em>{{ weekNumbers[$index] }}</em></td>\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}" ng-class="dt.customClass">\n        <button type="button" style="width:100%;" class="btn btn-default btn-sm" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-muted\': dt.secondary, \'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')}]),angular.module("template/datepicker/month.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/month.html",'<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" ng-disabled="datepickerMode === maxMode" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')}]),angular.module("template/datepicker/popup.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/popup.html",'<ul class="dropdown-menu" ng-style="{display: (isOpen && \'block\') || \'none\', top: position.top+\'px\', left: position.left+\'px\'}" ng-keydown="keydown($event)">\n	<li ng-transclude></li>\n	<li ng-if="showButtonBar" style="padding:10px 9px 2px">\n		<span class="btn-group pull-left">\n			<button type="button" class="btn btn-sm btn-info" ng-click="select(\'today\')">{{ getText(\'current\') }}</button>\n			<button type="button" class="btn btn-sm btn-danger" ng-click="select(null)">{{ getText(\'clear\') }}</button>\n		</span>\n		<button type="button" class="btn btn-sm btn-success pull-right" ng-click="close()">{{ getText(\'close\') }}</button>\n	</li>\n</ul>\n')}]),angular.module("template/datepicker/year.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/year.html",'<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="3"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" ng-disabled="datepickerMode === maxMode" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')
}]),angular.module("template/modal/backdrop.html",[]).run(["$templateCache",function(a){a.put("template/modal/backdrop.html",'<div class="modal-backdrop"\n     modal-animation-class="fade"\n     ng-class="{in: animate}"\n     ng-style="{\'z-index\': 1040 + (index && 1 || 0) + index*10}"\n></div>\n')}]),angular.module("template/modal/window.html",[]).run(["$templateCache",function(a){a.put("template/modal/window.html",'<div modal-render="{{$isRendered}}" tabindex="-1" role="dialog" class="modal"\n    modal-animation-class="fade"\n	ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)">\n    <div class="modal-dialog" ng-class="size ? \'modal-\' + size : \'\'"><div class="modal-content" modal-transclude></div></div>\n</div>\n')}]),angular.module("template/pagination/pager.html",[]).run(["$templateCache",function(a){a.put("template/pagination/pager.html",'<ul class="pager">\n  <li ng-class="{disabled: noPrevious(), previous: align}"><a href ng-click="selectPage(page - 1, $event)">{{getText(\'previous\')}}</a></li>\n  <li ng-class="{disabled: noNext(), next: align}"><a href ng-click="selectPage(page + 1, $event)">{{getText(\'next\')}}</a></li>\n</ul>')}]),angular.module("template/pagination/pagination.html",[]).run(["$templateCache",function(a){a.put("template/pagination/pagination.html",'<ul class="pagination">\n  <li ng-if="boundaryLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(1, $event)">{{getText(\'first\')}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(page - 1, $event)">{{getText(\'previous\')}}</a></li>\n  <li ng-repeat="page in pages track by $index" ng-class="{active: page.active}"><a href ng-click="selectPage(page.number, $event)">{{page.text}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(page + 1, $event)">{{getText(\'next\')}}</a></li>\n  <li ng-if="boundaryLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(totalPages, $event)">{{getText(\'last\')}}</a></li>\n</ul>')}]),angular.module("template/tooltip/tooltip-html-popup.html",[]).run(["$templateCache",function(a){a.put("template/tooltip/tooltip-html-popup.html",'<div class="tooltip"\n  tooltip-animation-class="fade"\n  tooltip-classes\n  ng-class="{ in: isOpen() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind-html="contentExp()"></div>\n</div>\n')}]),angular.module("template/tooltip/tooltip-html-unsafe-popup.html",[]).run(["$templateCache",function(a){a.put("template/tooltip/tooltip-html-unsafe-popup.html",'<div class="tooltip"\n  tooltip-animation-class="fade"\n  tooltip-classes\n  ng-class="{ in: isOpen() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" bind-html-unsafe="content"></div>\n</div>\n')}]),angular.module("template/tooltip/tooltip-popup.html",[]).run(["$templateCache",function(a){a.put("template/tooltip/tooltip-popup.html",'<div class="tooltip"\n  tooltip-animation-class="fade"\n  tooltip-classes\n  ng-class="{ in: isOpen() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n')}]),angular.module("template/tooltip/tooltip-template-popup.html",[]).run(["$templateCache",function(a){a.put("template/tooltip/tooltip-template-popup.html",'<div class="tooltip"\n  tooltip-animation-class="fade"\n  tooltip-classes\n  ng-class="{ in: isOpen() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner"\n    tooltip-template-transclude="contentExp()"\n    tooltip-template-transclude-scope="originScope()"></div>\n</div>\n')}]),angular.module("template/popover/popover-template.html",[]).run(["$templateCache",function(a){a.put("template/popover/popover-template.html",'<div class="popover"\n  tooltip-animation-class="fade"\n  tooltip-classes\n  ng-class="{ in: isOpen() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-if="title"></h3>\n      <div class="popover-content"\n        tooltip-template-transclude="contentExp()"\n        tooltip-template-transclude-scope="originScope()"></div>\n  </div>\n</div>\n')}]),angular.module("template/popover/popover-window.html",[]).run(["$templateCache",function(a){a.put("template/popover/popover-window.html",'<div class="popover {{placement}}" ng-class="{ in: isOpen, fade: animation }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" tooltip-template-transclude></div>\n  </div>\n</div>\n')}]),angular.module("template/popover/popover.html",[]).run(["$templateCache",function(a){a.put("template/popover/popover.html",'<div class="popover"\n  tooltip-animation-class="fade"\n  tooltip-classes\n  ng-class="{ in: isOpen() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-if="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n')}]),angular.module("template/progressbar/bar.html",[]).run(["$templateCache",function(a){a.put("template/progressbar/bar.html",'<div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: (percent < 100 ? percent : 100) + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>\n')}]),angular.module("template/progressbar/progress.html",[]).run(["$templateCache",function(a){a.put("template/progressbar/progress.html",'<div class="progress" ng-transclude></div>')}]),angular.module("template/progressbar/progressbar.html",[]).run(["$templateCache",function(a){a.put("template/progressbar/progressbar.html",'<div class="progress">\n  <div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: (percent < 100 ? percent : 100) + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>\n</div>\n')}]),angular.module("template/rating/rating.html",[]).run(["$templateCache",function(a){a.put("template/rating/rating.html",'<span ng-mouseleave="reset()" ng-keydown="onKeydown($event)" tabindex="0" role="slider" aria-valuemin="0" aria-valuemax="{{range.length}}" aria-valuenow="{{value}}">\n    <i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon" ng-class="$index < value && (r.stateOn || \'glyphicon-star\') || (r.stateOff || \'glyphicon-star-empty\')">\n        <span class="sr-only">({{ $index < value ? \'*\' : \' \' }})</span>\n    </i>\n</span>')}]),angular.module("template/tabs/tab.html",[]).run(["$templateCache",function(a){a.put("template/tabs/tab.html",'<li ng-class="{active: active, disabled: disabled}">\n  <a href ng-click="select()" tab-heading-transclude>{{heading}}</a>\n</li>\n')}]),angular.module("template/tabs/tabset.html",[]).run(["$templateCache",function(a){a.put("template/tabs/tabset.html",'<div>\n  <ul class="nav nav-{{type || \'tabs\'}}" ng-class="{\'nav-stacked\': vertical, \'nav-justified\': justified}" ng-transclude></ul>\n  <div class="tab-content">\n    <div class="tab-pane" \n         ng-repeat="tab in tabs" \n         ng-class="{active: tab.active}"\n         tab-content-transclude="tab">\n    </div>\n  </div>\n</div>\n')}]),angular.module("template/timepicker/timepicker.html",[]).run(["$templateCache",function(a){a.put("template/timepicker/timepicker.html",'<table>\n	<tbody>\n		<tr class="text-center">\n			<td><a ng-click="incrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="incrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n		<tr>\n			<td class="form-group" ng-class="{\'has-error\': invalidHours}">\n				<input style="width:50px;" type="text" ng-model="hours" ng-change="updateHours()" class="form-control text-center" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td>:</td>\n			<td class="form-group" ng-class="{\'has-error\': invalidMinutes}">\n				<input style="width:50px;" type="text" ng-model="minutes" ng-change="updateMinutes()" class="form-control text-center" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td ng-show="showMeridian"><button type="button" class="btn btn-default text-center" ng-click="toggleMeridian()">{{meridian}}</button></td>\n		</tr>\n		<tr class="text-center">\n			<td><a ng-click="decrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="decrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n	</tbody>\n</table>\n')}]),angular.module("template/typeahead/typeahead-match.html",[]).run(["$templateCache",function(a){a.put("template/typeahead/typeahead-match.html",'<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')}]),angular.module("template/typeahead/typeahead-popup.html",[]).run(["$templateCache",function(a){a.put("template/typeahead/typeahead-popup.html",'<ul class="dropdown-menu" ng-show="isOpen()" ng-style="{top: position.top+\'px\', left: position.left+\'px\'}" style="display: block;" role="listbox" aria-hidden="{{!isOpen()}}">\n    <li ng-repeat="match in matches track by $index" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)" role="option" id="{{match.id}}">\n        <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n    </li>\n</ul>\n')}]),!angular.$$csp()&&angular.element(document).find("head").prepend('<style type="text/css">.ng-animate.item:not(.left):not(.right){-webkit-transition:0s ease-in-out left;transition:0s ease-in-out left}</style>');
(function() {
'use strict';

	var constantService = {
				db: window.openDatabase("amaraja.db", "1", "Sqlite", "2000"),
				mainTable: 'DF_DYNTABLES',
				tableName: 'DF_TBL_PGR_',
				selectedTableName: '',
				innerTableName: 'DF_TBL_PGR_TBL_',
				deptTable: 'DF_MSTR_DEPT',
				usersTable: 'DF_MSTR_USERS',
				workFlowMaster: 'DF_WORKFLOW',
				workFlowItems:'DF_WORKFLOW_I',
				trackApproval: 'DF_TBL_FMH',
				loginTable: 'Users',
				approveTable: 'DF_TBL_APR',
				machine: 'machine',
				partNO: 'partno',
				shift: 'shift',
				V0:[10, 50, 50, 'Not Allowed', 'Not Allowed'],
				NEWV0:[10, 50, 50, 'Allowed', 'Not Allowed'],
				V2:[30, 250, 250, 'Allowed', 'Not Allowed'],
				A: ['6 to 7', '7 to 8', '8 to 9', '9 to 10', '10 to 11', '11 to 12', '12 to 1', '1 to 2'],
				B: ['2 to 3', '3 to 4', '4 to 5', '5 to 6', '6 to 7', '7 to 8', '8 to 9', '9 to 10'],
				C: ['10 to 11', '11 to 12', '12 to 1', '1 to 2', '2 to 3', '3 to 4', '4 to 5', '5 to 6'],
				// serverURL: 'http://localhost/dfserver/DFServerAPI.svc/'
				//serverURL: 'http://52.77.96.35/dfserver/DFServerAPI.svc/'
				//serverURL: 'http://eappsdev.argroup.co.in/dfserver/DFServerAPI.svc/'
				serverURL: 'http://arsrva008/dfserver/DFServerAPI.svc/'
	};	

	angular.module('amaraja').constant('constantService', constantService);

	})();

angular.module('amaraja').factory('createFormsService', createFormsService);

createFormsService.$inject = ['$q', '$http', '$cordovaSQLite', 'constantService'];

function createFormsService($q, $http, $cordovaSQLite, constantService) {

  var service = {
    addTableJSON: addTableJSON,
    getTableJSON: getTableJSON,
    getTableByName: getTableByName,
    updateTableJSON: updateTableJSON,
    getColumnValues: getColumnValues,
    createTable: createTable,
    createInnerTable: createInnerTable,
    getTableMaster: getTableMaster,
    getMasterByName: getMasterByName,
    updateFormName: updateFormName,
    getColumnValuesCondition: getColumnValuesCondition,
    getDynamicTableColName: getDynamicTableColName
  }
  return service;

  function addTableJSON(formName, record) {
    var deferred = $q.defer();
    var query = "INSERT INTO " + constantService.mainTable + " (tableName, tableRecord) VALUES ('" + formName + "','" + JSON.stringify({ 'data': record }) + "')";
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'CreateDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.mainTable,
        'type': 'INSERT'
      }
    }
    $http(dynareq).then(function(response) {
      console.log('response', response);
      var result = response.data.CreateDynamicTableResult[0];
      deferred.resolve(result);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'INSERT INTO Tables (tableName, tableRecord) VALUES (?,?)', [formName, JSON.stringify({
      'data': record
    })]).then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;*/
  }

  function createTable(tableId, tableColun) {
    var deferred = $q.defer();
    var col = "";
    var tableName = constantService.tableName + tableId;
    //var flag = true;
    angular.forEach(tableColun, function(value, key) {
      if (value.Type == 'table') {
        col = ' ' + col + value.ColName + " " + 'INTEGER,';
        //flag = false;
      } else if (value.Type == 'date') {
        col = ' ' + col + value.ColName + " " + 'DATETIME,';
      } else if (value.Type == 'checkbox') {
        col = ' ' + col + value.ColName + " " + 'VARCHAR(50),';
      } else if (value.Type == 'textarea') {
        col = ' ' + col + value.ColName + " " + 'VARCHAR(MAX),';
      } else {
        col = ' ' + col + value.ColName + " " + 'VARCHAR(50),';
      }
    });
    /*$cordovaSQLite.execute(constantService.db, 'CREATE TABLE IF NOT EXISTS '+tableName+' ( ' + col.slice(0, -1) + ')').then(function(data) {
       deferred.resolve(data);
     }, function(error) {
       deferred.reject(error);
     });
     return deferred.promise;*/

    var query = "CREATE TABLE " + tableName + " ( rowid INTEGER PRIMARY KEY IDENTITY(1,1), recordName VARCHAR(50), " + col.slice(0, -1).trim() + ")";
    console.log(" query create table ", query)
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'CreateDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': tableName,
        'type': 'CREATE'
      }
    }
    $http(dynareq).then(function(response) {
      //console.log('response',response);
      var result = response.data.CreateDynamicTableResult[0];
      deferred.resolve(result);
    }, function(error) {
      //console.log('error###',error);
      deferred.reject(error);
    });
    return deferred.promise;
  }

  function createInnerTable(pTableID, tableID, record) {
    var deferred = $q.defer();
    var col = "";
    var index = 0;
    angular.forEach(record, function(value, key) {
      if (value.Type == 'table') {
        var tableName = constantService.innerTableName + pTableID + "_" + index;
        angular.forEach(value.Settings, function(value1, key1) {
          if (value1.Type == 'table') {
            angular.forEach(value1.PossibleValue[0].columns, function(value2, key2) {
              col = ' ' + col + value2.value + " " + 'VARCHAR(50),';
              //tableCol = tableCol +" " + value2.value+",";
            })
            var query = "CREATE TABLE " + tableName + " (rowid INTEGER PRIMARY KEY IDENTITY(1,1), tableid INTEGER , " + col.slice(0, -1) + ")";
            var dynareq = {
              method: 'POST',
              url: constantService.serverURL + 'CreateDynamicTable',
              headers: {
                'Content-Type': 'application/json'
              },
              data: {
                'query': query,
                'table_name': tableName,
                'type': 'CREATE'
              }
            }
            $http(dynareq).then(function(response) {
              var tableCol = ''
              angular.forEach(value1.PossibleValue[0].columns, function(value2, key2) {
                tableCol = tableCol + " " + value2.value + ",";
              })
              angular.forEach(value1.PossibleValue[0].rows, function(value2, key2) {
                var colvalues = "";
                angular.forEach(value2.cells, function(value3, key3) {
                  colvalues = colvalues + " '" + value3.value + "',";

                });
                console.log("tableCol ", tableCol)
                var insertquery = "INSERT INTO " + tableName + " (tableid," + tableCol.slice(0, -1) + ") VALUES (0," + colvalues.slice(0, -1) + ")";
                console.log("insert inner query ", insertquery)
                var dynainsreq = {
                  method: 'POST',
                  url: constantService.serverURL + 'CreateDynamicTable',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  data: {
                    'query': insertquery,
                    'table_name': tableName,
                    'type': 'INSERT'
                  }
                }
                $http(dynainsreq).then(function(response) {

                  deferred.resolve(response);
                }, function(error) {
                  console.log('error###', error);
                });
              });
            }, function(error) {
              console.log('error###', error);
            });
          }
        });
        ++index;
        col = '';
      }
    })
    return deferred.promise;
  }

  function updateTableJSON(id, formName, record) {

    var deferred = $q.defer();

    getTableRecord(id).then(function(tableRecord) {
      console.log('tableRecord', tableRecord)
      var updatequery = "UPDATE " + constantService.mainTable + " SET tableName = '" + formName + "', tableRecord = '" + JSON.stringify({ 'data': record }) + "' WHERE rowid = " + id;
      console.log('updatequery', updatequery);
      var dynaupreq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': updatequery,
          'table_name': constantService.mainTable,
          'type': 'UPDATE'
        }
      }
      $http(dynaupreq).then(function(response) {
        console.log("response update form ", response);

        getTableColName(id).then(function(colName) {
          var firstLoop = '',
            secondLoop = '';

          if (colName.data.length <= record.length + 2) {
            firstLoop = record;
            secondLoop = colName.data;
            console.log('firstLoop : Record, secondLoop : colName')
            console.log('record', record)
            console.log('colName', secondLoop)
          } else
          if (colName.data.length > record.length + 2) {
            secondLoop = record;
            firstLoop = colName.data;
            console.log('firstLoop : colName, secondLoop : Record')
            console.log('record', record)
            console.log('colName', firstLoop)
          }

          for (j = 0; j < firstLoop.length; j++) {
            console.log('tableRecordValue', tableRecordValue)
            var flag = true;
            var colNameAsInTable = '',
              tableRecordValue = '',
              isFound = '',
              checkingVal = '',
              checkingObj = '';
            // console.log('colNameAsInTable',colNameAsInTable);
            for (i = 0; i < secondLoop.length; i++) {

              if (colName.data.length <= record.length + 2) {
                colNameAsInTable = secondLoop[i];
                tableRecordValue = firstLoop[j];
                checkingObj = colName;
              } else {
                tableRecordValue = secondLoop[i];
                colNameAsInTable = firstLoop[j];
                checkingObj = record;
              }

              if (colNameAsInTable.COLUMN_NAME == tableRecordValue.Settings[1].Value) {
                flag = false;
                break;
              }
            }

            if (colName.data.length <= record.length + 2) {
              checkingVal = tableRecordValue.Settings[1].Value;
            } else {
              if (colNameAsInTable.COLUMN_NAME != 'rowid' && colNameAsInTable.COLUMN_NAME != 'recordName') {
                checkingVal = colNameAsInTable.COLUMN_NAME;
              }
            }
                isFound = _isContains(checkingObj, checkingVal);

            console.log('contains', isFound, checkingVal);

            if (flag && !isFound) {
              var col = '',
                alterquery = '';

              if (colName.data.length <= record.length + 2) {
                if (tableRecordValue.Type == 'table') {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'INTEGER';
                } else if (tableRecordValue.Type == 'date') {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'DATETIME';
                } else if (tableRecordValue.Type == 'checkbox') {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(50)';
                } else if (tableRecordValue.Type == 'textarea') {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(MAX)';
                } else {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(50)';
                }
                console.log('add col alter table')
                alterquery = "ALTER TABLE " + constantService.tableName + id + " add " + col;
              } else {
                console.log('drop col alter table')
                alterquery = "ALTER TABLE " + constantService.tableName + id + " drop COLUMN " + checkingVal;
              }
              if (alterquery != '') {
                var alterQueryReq = {
                  method: 'POST',
                  url: constantService.serverURL + 'CreateDynamicTable',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  data: {
                    'query': alterquery,
                    'table_name': constantService.tableName + id,
                    'type': 'Update'
                  }
                }

                $http(alterQueryReq).then(function(response) {
                  console.log('alterQueryReq response', response)
                });
              }
            }
          }

          // angular.forEach(colName.data, function(colNameAsInTable) {
          //   var flag = true;
          //   var tableRecordValue = '';
          //   // console.log('colNameAsInTable',colNameAsInTable);
          //   for (i = 0; i < JSON.parse(tableRecord.rows[0].tableRecord).data.length; i++) {

          //     tableRecordValue = JSON.parse(tableRecord.rows[0].tableRecord).data[i];
          //     if (colNameAsInTable.COLUMN_NAME == tableRecordValue.Settings[1].Value) {
          //       flag = false;
          //       break;
          //     }
          //   }
          //   // if (tableRecordValue.Settings[1].Value == "rowid" || tableRecordValue.Settings[1].Value == "recordName" || colNameAsInTable.COLUMN_NAME == "rowid" || colNameAsInTable.COLUMN_NAME == "recordName")
          //   //   flag = false;
          //   if (flag) {
          //     console.log('flag', flag);
          //     console.log('val.Settings[1].Value', tableRecordValue.Settings[1].Value);
          //     console.log('colNameAsInTable.COLUMN_NAME', colNameAsInTable.COLUMN_NAME);
          //     var col = '';
          //     if (tableRecordValue.Type == 'table') {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'INTEGER';
          //     } else if (tableRecordValue.Type == 'date') {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'DATETIME';
          //     } else if (tableRecordValue.Type == 'checkbox') {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(50)';
          //     } else if (tableRecordValue.Type == 'textarea') {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(MAX)';
          //     } else {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(50)';
          //     }

          //     var alterquery = "ALTER TABLE " + constantService.tableName + id + " add " + col;
          //     var alterQueryReq = {
          //       method: 'POST',
          //       url: constantService.serverURL + 'CreateDynamicTable',
          //       headers: {
          //         'Content-Type': 'application/json'
          //       },
          //       data: {
          //         'query': alterquery,
          //         'table_name': constantService.tableName + id,
          //         'type': 'Update'
          //       }
          //     }

          //     $http(alterQueryReq).then(function(response) {
          //       console.log('alterQueryReq response', response)
          //     });
          //   }
          // });
        });


        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });

      /*$cordovaSQLite.execute(constantService.db, 'UPDATE Tables SET tableName = ?, tableRecord = ? WHERE rowid = ?',[formName, JSON.stringify({'data': record}), id]).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });*/
    });
    return deferred.promise;
  }

  function getTableJSON() {
    var deferred = $q.defer();

    var query = "SELECT  rowid,tableName from " + constantService.mainTable;
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.mainTable,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select  rowId,tableName from Tables').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function getTableMaster() {
    var deferred = $q.defer();
    var query = "select  DISTINCT tableName from DF_MSTRS";
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': 'DF_MSTRS',
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select  DISTINCT tableName from DF_MSTRS').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function getTableByName(recordId) {
    var deferred = $q.defer();

    var query = "SELECT * from " + constantService.mainTable + " where rowid =" + recordId;
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.mainTable,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select  * from Tables where rowid =' + '"' + recordId + '"').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function getMasterByName(masterName) {
    var deferred = $q.defer();

    var query = "SELECT colName from DF_MSTRS where tableName =" + "'" + masterName + "'";
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': 'DF_MSTRS',
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select colName from DF_MSTRS where tableName =' + '"' + masterName + '"').then(function(data) {console.log('data----',data);
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function getColumnValues(column, table) {
    var deferred = $q.defer();

    var query = "SELECT DISTINCT " + column + " from " + table;
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': table,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /* $cordovaSQLite.execute(constantService.db, 'SELECT DISTINCT '+ column + ' from '+ table).then(function(data) {
       deferred.resolve(data);
     }, function(error) {
       deferred.reject(error);
     })
     return deferred.promise;*/
  }

  function getColumnValuesCondition(column, table, ConditionData) {
    var deferred = $q.defer();
    var subQuery = "(";
    angular.forEach(ConditionData, function(value, index) {
      subQuery += value.condColumn + " " + value.condEquals + " " + "(SELECT DISTINCT " + value.condColumnName + " FROM " + value.condTableName + " WHERE " + value.condColumnName + " = '" + value.condValue + "') " + value.condChange + " ";
    })
    subQuery += ")"
      //console.log('SELECT '+ column + ' from '+ table + ' WHERE ' + condColumn + ' IN (SELECT ' + condColumn + ' FROM '+ condTable +' WHERE '+ condColumn +' = "'+ condValue + '")');
    if (ConditionData[0].condColumn != "") {

      var query = '';
      if (column.includes('empName')) {
        query = "SELECT " + column + ",empCode from " + table + " WHERE " + subQuery;
      } else {
        query = "SELECT " + column + " from " + table + " WHERE " + subQuery;
      }
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': table,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        console.log('responssssssssss', response);
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        console.log('error###', error);
        deferred.reject(error);
      });

      /*$cordovaSQLite.execute(constantService.db, 'SELECT '+ column + ' from '+ table + ' WHERE ' + subQuery).then(function(data) {
        console.log('data',data);
        deferred.resolve(data);
      }, function(error) {
        console.log('err',error);
        deferred.reject(error);
      })*/
    } else {

      var query = '';
      if (column.includes('empName')) {
        query = "SELECT " + column + ",empCode  from " + table;
      } else {
        query = "SELECT " + column + " from " + table;
      }

      // var query = "SELECT "+ column + " from "+ table;
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': table,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        console.log('error###', error);
        deferred.reject(error);
      });
      /* $cordovaSQLite.execute(constantService.db, 'SELECT '+ column + ' from '+ table).then(function(data) {
         console.log('data',data);
         deferred.resolve(data);
       }, function(error) {
         console.log('err',error);
         deferred.reject(error);
       })*/
    }

    return deferred.promise;
  }

  function updateFormName(formName, previousName) {
    var deferred = $q.defer();

    var query = "UPDATE " + constantService.workFlowItems + " SET levelform = '" + formName + "' WHERE levelform = '" + previousName + "'";
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'CreateDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.workFlowItems,
        'type': 'UPDATE'
      }
    }
    $http(dynareq).then(function(response) {
      var updquery = "UPDATE " + constantService.approveTable + " SET formName = '" + formName + "' WHERE formName = '" + previousName + "'";
      var dynaupdreq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': updquery,
          'table_name': constantService.approveTable,
          'type': 'UPDATE'
        }
      }
      $http(dynaupdreq).then(function(response) {}, function(error) {
        console.log('error###', error);
      });
      deferred.resolve(response);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    /*$cordovaSQLite.execute(constantService.db, 'UPDATE '+constantService.workFlowItems+' SET levelform = "'+ formName +'" WHERE levelform = "'+ previousName + '"').then(function(data) {
      $cordovaSQLite.execute(constantService.db, 'UPDATE '+constantService.approveTable+' SET formName = "'+ formName +'" WHERE formName = "'+ previousName + '"');
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    });*/
    return deferred.promise;
  }

  function getDynamicTableColName(rowid) {
    var deferred = $q.defer();
    var query = '';
    console.log("rowid ", rowid, isNaN(rowid))
    if (isNaN(parseInt(rowid))) {
      query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + rowid + "'";
    } else {
      query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + constantService.tableName + rowid + "'";
    }
    console.log("dynamiv query ", query)
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.workFlowItems,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      deferred.resolve(response);
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  function getTableColName(tableId) {
    var deferred = $q.defer();
    var query = '';
    query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + constantService.tableName + tableId + "'";
    console.log("getTableColName query ", query)
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.tableName + tableId,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      console.log('getTableColName response', response);
      deferred.resolve(response);
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  function getTableRecord(id) {
    var deferred = $q.defer();
    var query = "SELECT tableRecord from " + constantService.mainTable + " where rowid=" + id;
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.mainTable,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      console.log('response getTableRecord', response);
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select  rowId,tableName from Tables').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function _isContains(json, value) {
    let contains = false;
    Object.keys(json).some(key => {
      contains = typeof json[key] === 'object' ? _isContains(json[key], value) : json[key] === value;
      return contains;
    });
    return contains;
  }
}

angular.module('amaraja').factory('getFormsRecordsService', getFormsRecordsService);

getFormsRecordsService.$inject = ['$q', '$http', '$window', '$cordovaSQLite', 'notificationService', 'constantService', '$cordovaNetwork', 'SendEmailService', 'loginService'];

function getFormsRecordsService($q, $http, $window, $cordovaSQLite, notificationService, constantService, $cordovaNetwork, SendEmailService, loginService) {

    var service = {
        getTableRecords: getTableRecords,
        getInnerTableRecord: getInnerTableRecord,
        updateRecord: updateRecord,
        updateActiveRecord: updateActiveRecord,
        checkApproved: checkApproved,
        checkWorkflow: checkWorkflow,
        returnApproveal: returnApproveal,
        transformApproveal: transformApproveal,
        updateAprrovalProcess: updateAprrovalProcess,
        getCompletedRecordinOneLevel: getCompletedRecordinOneLevel,
        getApproverDetails: getApproverDetails
    }
    return service;

    function getTableRecords(tableId) {
        var deferred = $q.defer();
        var tableName = constantService.tableName + tableId;
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        if (1) {
            var query = "SELECT rowid,* FROM  " + tableName;
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': tableName,
                    'type': 'SELECT'
                }
            }
            console.log("query  ", query)
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "SELECT rowid,* FROM  " + tableName).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function getInnerTableRecord(TableNameId, subId, TableRowId, formName) {
        var deferred = $q.defer();
        var tableName = constantService.innerTableName + TableNameId + "_" + subId;
        var query = '';
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        //var query = "SELECT  rowid,* FROM  " + tableName + " WHERE tableid=" + TableRowId+"  order by column1 * 1 ASC";
        console.log("formName sada", formName)
        if (formName.toUpperCase().includes("IN-PROCESS") || formName.toUpperCase().includes("PROCESS SPECIFICATION") || formName.toUpperCase().includes("SHIFT")) {
            query = "select rowid,*  from " + tableName + " where tableid = '" + TableRowId + "'  order by column1  * 1 ASC";
        } else {
            query = "select rowid,*  from " + tableName + " where tableid = '" + TableRowId + "'  order by column1 ASC";
        }
        console.log("inner table ", query)
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': tableName,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }

        return deferred.promise;
    }

    function checkWorkflow(formName) {
        var deferred = $q.defer();
        if (1) {
            var query = "SELECT  * FROM  " + constantService.workFlowItems + " WHERE levelform='" + formName + "'";
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.workFlowItems,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, 'SELECT  * FROM  ' + constantService.workFlowItems + " WHERE levelform='" + formName + "'").then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function checkApproved(tableid, dept, type, formName, rejectQuery) {
        var deferred = $q.defer();
        var query1 = '';
        console.log("type  ", type)
        if (type != 'approved' && type != '') {
            console.log('checkApproved')
            if (type == 'inprocess') {
                // if ($window.localStorage.userDept == 'QA')
                query1 = "updatedBy='" + dept + "' AND status='a' AND approved!='done' AND formName ='" + formName + "'";
                // else
                //     query1 = " formName='" + formName + "' and approved !='done' and approved !='reject' and tableName in(select DISTINCT formName from DF_TBL_FMH where  status != 'reject'  and role in('" + dept + "'))"
            } else if (type == 'done') {
                query1 = "approved='done' and formName='" + formName + "'";
            } else if (type == 'Reject') {
                query1 = "updatedBy in(" + rejectQuery + ") AND status='a' AND approved='" + type + "' AND formName ='" + formName + "'";
            } else {
                query1 = "deptName='" + dept + "' AND status='a' AND approved='" + type + "' AND formName ='" + formName + "'";
            }
        } else {
            console.log('Else checkApproved')
            if ($window.localStorage.userDept == 'Engineering') {
                query1 = "status='a'  AND tableName='" + tableid + "' AND formName ='" + formName + "'";
            }
            //  else if ($window.localStorage.userDept == 'QA') {
            //     query1 = "updatedBy='" + dept + "' AND status='a' AND approved!='done' AND formName ='" + formName + "'";
            // }
            else {
                query1 = "deptName='" + dept + "' AND status='a' AND approved='approved' AND formName ='" + formName + "'";
            }
        }

        var query = "SELECT  * FROM  " + constantService.approveTable + " WHERE " + query1;
        console.log("complee  ", query)
        if (1) {
            console.log('query----- ', query);
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, query, null).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function updateInnerTableRecord(tableNameId, subId, tablRowId, tableRowName, tableRec) {
        var tableName = constantService.innerTableName + tableNameId + "_" + subId;
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        if (1) {
            var query = "UPDATE " + tableName + " SET " + tableRowName + "= '" + tableRec + "' WHERE rowid=" + tablRowId;
            console.log("query    table =====  ", query)
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': tableName,
                    'type': 'UPDATE'
                }
            }
            $http(dynareq).then(function(response) {}, function(error) {
                console.log('error###', error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "UPDATE " + tableName + " SET " + tableRowName + "=" + JSON.stringify(tableRec) + " WHERE rowid=" + tablRowId);
        }
    }

    function updateRecord(tableNameId, tablRowId, tableRowName, tableRec, approved, formName, updateDept, moveToDept, rejctDept) {
        console.log('rrr tableNameId', tableNameId);
        console.log('rrr tablRowId', tablRowId);
        console.log('rrr tableRowName', tableRowName);
        console.log('rrr tableRec', tableRec);
        console.log('rrr approved', approved);
        console.log('rrr formName', formName);
        console.log('rrr updateDept', updateDept);
        console.log('rrr moveToDept', moveToDept);
        console.log('rrr rejctDept', rejctDept);

        var approved1 = approved;
        var deferred = $q.defer();
        var subId = 0;
        var cavityColum = '';
        var tableName = constantService.tableName + tableNameId;
        var promisecall = [];
        var flag = true;
        promisecall.push(
            angular.forEach(tableRec, function(value, key) {
                if (value.Type == 'table') {
                    var col = [];
                    angular.forEach(value.Value.table, function(value1, key1) {
                        if (value1.columns != '') {
                            angular.forEach(value1.columns, function(value2, key2) {
                                if (value2.cavity) cavityColum = value2.Value;
                                else col.push(value2.Value);
                            });
                        }
                        if (value1.rows != '') {
                            angular.forEach(value1.rows, function(value2, key2) {
                                var rowId = 0;
                                var setRowId = true;
                                var cavityValue = '';
                                var cavityExist = false;
                                angular.forEach(value2.cells, function(value3, key3) {
                                    if (setRowId) {
                                        rowId = value3.Value;
                                        setRowId = false;
                                    }
                                    if (col[key3] == cavityColum.split('_')[0] || value3.cavity == true) {
                                        cavityValue = cavityValue + "_" + value3.Value;
                                        cavityExist = true;
                                    } else {
                                        if (value3.Edited && flag) {
                                            updateInnerTableRecord(tableNameId, subId, rowId, col[key3], value3.Value);
                                        }
                                    }
                                    if (key3 == value2.cells.length - 1 && cavityExist && flag) {
                                        updateInnerTableRecord(tableNameId, subId, rowId, cavityColum.split('_')[0], cavityValue.trim().slice(1));
                                    }

                                    if (value3.NewAdded && flag) {
                                        addInnerTable(tableNameId, subId, rowId, value.Value.table)
                                        flag = false;
                                    }
                                });
                            });
                        }
                    });
                    ++subId;
                } else if (value.Type == 'checkbox' && value.Edited == true) {
                    var query = '';
                    if (value.Type == 'checkbox') query = "UPDATE " + tableName + " SET " + value.InternalName + "= '" + JSON.stringify(value.Value) + "' WHERE " + tableRowName + "=" + tablRowId;
                    else query = "UPDATE " + tableName + " SET " + value.InternalName + "='" + value.Value + "' WHERE " + tableRowName + "=" + tablRowId;

                    if (1) {
                        var dynareq = {
                            method: 'POST',
                            url: constantService.serverURL + 'CreateDynamicTable',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            data: {
                                'query': query,
                                'table_name': tableName,
                                'type': 'UPDATE'
                            }
                        }
                        $http(dynareq).then(function(response) {
                            console.log('response', response);
                        }, function(error) {
                            console.log('error###', error);
                        });
                    } else {
                        $cordovaSQLite.execute(constantService.db, query);
                    }
                } else if (value.Edited == true) {
                    var query = "UPDATE " + tableName + " SET " + value.InternalName + "='" + value.Value + "' WHERE " + tableRowName + "=" + tablRowId;
                    console.log("query update ", query)
                    if (1) {
                        var dynareq = {
                            method: 'POST',
                            url: constantService.serverURL + 'CreateDynamicTable',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            data: {
                                'query': query,
                                'table_name': tableName,
                                'type': 'UPDATE'
                            }
                        }
                        $http(dynareq).then(function(response) {
                            console.log('response after upadting 1', response);
                            console.log('constantService.approveTable', tableName);
                            // console.log('query', query);
                        }, function(error) {
                            console.log('error###', error);
                        });
                    } else {
                        $cordovaSQLite.execute(constantService.db, query);
                    }
                } else {}
            }))


        $q.all(promisecall).then(function(data) {
            if (approved == 'return' || approved == 'transfer') {
                if (1) {
                    var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                    console.log('query (0000)  ', query)
                    var dynareq = {
                        method: 'POST',
                        url: constantService.serverURL + 'CreateDynamicTable',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            'query': query,
                            'table_name': constantService.approveTable,
                            'type': 'UPDATE'
                        }
                    }
                    $http(dynareq).then(function(response) {
                        console.log('response after upadting 2', response);
                        console.log('constantService.approveTable', constantService.approveTable);
                    }, function(error) {
                        console.log('error###', error);
                    });
                } else {
                    $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='reject', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                }
            } else {
                if (approved != undefined) {
                    if (1) {
                        var query = "SELECT * FROM " + constantService.approveTable + " WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                        console.log("query @@@@@ ", query)
                        var dynareq = {
                            method: 'POST',
                            url: constantService.serverURL + 'SelectDynamicTable',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            data: {
                                'query': query,
                                'table_name': constantService.approveTable,
                                'type': 'SELECT'
                            }
                        }
                        $http(dynareq).then(function(response) {
                            var data = [];
                            data.rows = response.data;
                            deferred.resolve(data);
                            approved = approved1;

                            if (data.rows != '' && data.rows.length > 0) {
                                var rejectedBy = '';
                                if (approved != 'approved') {
                                    if (1) {
                                        var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                                        console.log("query ", query);
                                        var dynareq = {
                                            method: 'POST',
                                            url: constantService.serverURL + 'CreateDynamicTable',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            data: {
                                                'query': query,
                                                'table_name': constantService.approveTable,
                                                'type': 'UPDATE'
                                            }
                                        }
                                        $http(dynareq).then(function(response) {
                                            console.log('response after upadting 3', response);
                                            console.log('constantService.approveTable', constantService.approveTable);
                                        }, function(error) {
                                            console.log('error###', error);
                                        });
                                    } else {
                                        $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='reject',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                                    }
                                } else {
                                    var approved = 'approved';
                                    if (moveToDept == undefined) {
                                        moveToDept = 'done';
                                        approved = 'done';
                                    }
                                    if (1) {
                                        var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                                        console.log("query 1 ", query);
                                        var dynareq = {
                                            method: 'POST',
                                            url: constantService.serverURL + 'CreateDynamicTable',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            data: {
                                                'query': query,
                                                'table_name': constantService.approveTable,
                                                'type': 'UPDATE'
                                            }
                                        }
                                        $http(dynareq).then(function(response) {}, function(error) {
                                            console.log('error###', error);
                                        });
                                    } else {
                                        $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='approved', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                                    }
                                }
                            } else {
                                updateApproveTable(approved, tableNameId, tablRowId, formName, moveToDept);
                            }
                        }, function(error) {
                            console.log('error###', error);
                        });
                    }
                    //LOCAL STORAGE COED COMMENTED BY CHETHA 
                    /*else {
                       console.log("2222222222222222222222222222222222222222222222222222222222222222222222222222222222");
                        $cordovaSQLite.execute(constantService.db, "SELECT * FROM " + constantService.approveTable + " WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'").then(function(aaa) {
                            if (aaa.rows.length > 0) {
                                var rejectedBy = '';
                                if (approved != 'approved') {
                                    if (1) {
                                        var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                                        var dynareq = {
                                            method: 'POST',
                                            url: constantService.serverURL + 'CreateDynamicTable',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            data: {
                                                'query': query,
                                                'table_name': constantService.approveTable,
                                                'type': 'UPDATE'
                                            }
                                        }
                                        $http(dynareq).then(function(response) {
                                            console.log('response', response);
                                        }, function(error) {
                                            console.log('error###', error);
                                        });
                                    } else {
                                        console.log("UPDATE " + constantService.approveTable + " SET approved='" + approved + "',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'")
                                        $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='reject',  deptName='" + rejctDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                                    }
                                } else {
                                    var approved = 'approved';
                                    if (moveToDept == undefined) {
                                        moveToDept = 'done';
                                        approved = 'done';
                                    }
                                    if (1) {
                                        var query = "UPDATE " + constantService.approveTable + " SET approved='" + approved + "', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
                                        var dynareq = {
                                            method: 'POST',
                                            url: constantService.serverURL + 'CreateDynamicTable',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            data: {
                                                'query': query,
                                                'table_name': constantService.approveTable,
                                                'type': 'UPDATE'
                                            }
                                        }
                                        $http(dynareq).then(function(response) {
                                            console.log('response', response);
                                        }, function(error) {
                                            console.log('error###', error);
                                        });
                                    } else {
                                        console.log("UPDATE " + constantService.approveTable + " SET approved='" + approved + "', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'")
                                        $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET approved='approved', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'");
                                    }
                                }
                            } else {
                                console.log("updateApproveTable 22222222222222222222 approved", approved)
                                updateApproveTable(approved, tableNameId, tablRowId, formName, moveToDept);
                            }
                        });
                    }*/
                }
            }
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function returnApproveal(tableNameId, tablRowId, formName) {
        var deferred = $q.defer();
        var rejectedBy = '';
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        if (1) {
            var query = "UPDATE " + constantService.approveTable + " SET approved='approved', deptName='" + moveToDept + "', updatedBy='" + $window.localStorage.userDept + "' WHERE tableName='" + tableNameId + "' AND recordId ='" + tablRowId + "'";
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'UPDATE'
                }
            }
            $http(dynareq).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET   approved='reject' WHERE tableName='" + tableNameId + "' AND formName ='" + formName + "' AND status='A'").then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function transformApproveal(tableNameId, tablRowId, formName, transDept) {
        var deferred = $q.defer();
        var rejectedBy = '';
        //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
        if (1) {
            var query = "UPDATE " + constantService.approveTable + " SET   approved='reject' WHERE tableName='" + tableNameId + "' AND tablRowId ='" + tablRowId + "' AND status='A' AND deptName'" + transDept + "'";
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'UPDATE'
                }
            }
            $http(dynareq).then(function(response) {
                console.log('response', response);
                deferred.resolve(response);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "UPDATE " + constantService.approveTable + " SET   approved='reject' WHERE tableName='" + tableNameId + "' AND tablRowId ='" + tablRowId + "' AND status='A' AND deptName'" + transDept + "'").then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function updateActiveRecord(tableId) {
        var deferred = $q.defer();
        var tableName = constantService.tableName + tableId;
        if (1) {
            var query = "SELECT rowid,* FROM  " + tableName;
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': tableName,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, "SELECT rowid,* FROM  " + tableName).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function updateApproveTable(approved, tableName, rowid, formName, moveToDept) {
        console.log("approved in updateApproveTable ", approved)
        var deferred = $q.defer();
        var rejectedBy = '';
        rejectedBy = $window.localStorage.userDept;
        if (moveToDept == undefined) moveToDept = $window.localStorage.userDept;;
        if (1) {
            var query = "INSERT INTO " + constantService.approveTable + " (approved , tableName , recordId , deptName , formName, updatedBy, status) VALUES ('" + approved + "','" + tableName + "','" + rowid + "','" + moveToDept + "','" + formName + "','" + rejectedBy + "','a')";
            console.log('query', query);
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'INSERT'
                }
            }
            $http(dynareq).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, 'INSERT INTO ' + constantService.approveTable + ' (approved , tableName , recordId , deptName , formName, updatedBy, status) VALUES ("' + approved + '","' + tableName + '","' + rowid + '","' + moveToDept + '","' + formName + '","' + rejectedBy + '","a")').then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function updateAprrovalProcess(formlevel, formName, status, comment, recordId) {
        var deferred = $q.defer();
        var query = "INSER INTO " + constantService.trackApproval + " (userName , userId , role , formLevel , formName, status, comment ,recordId) VALUES ('" + $window.localStorage.empName + "','" + $window.localStorage.empCode + "','" + $window.localStorage.userDept + "','" + formlevel + "','" + formName + "','" + status + "','" + comment + "','" + recordId + "')";
        console.log('query insert track approval ', query);
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'CreateDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'INSERT'
                }
            }
            $http(dynareq).then(function(response) {
                console.log('constantService.approveTable updateAprrovalProcess', constantService.approveTable);
                console.log('constantService.approveTable trackApproval', constantService.trackApproval);
                console.log('constantService.db updateAprrovalProcess', constantService.db);
                // SendEmailService.sendmailNotifiction(recordId, );
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
        } else {

            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function getCompletedRecordinOneLevel(query) {
        var deferred = $q.defer();
        console.log('query insert track approval ', query);
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {

            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function addInnerTable(tableNameId, subId, rowId, tablerecord) {
        var tableName = constantService.innerTableName + tableNameId + "_" + subId;
        var query = "delete from " + tableName + " where  tableid in (select tableid from " + tableName + " where rowid='" + rowId + "')";
        var dynareq = {
            method: 'POST',
            url: constantService.serverURL + 'CreateDynamicTable',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'query': query,
                'table_name': tableName,
                'type': 'SELECT'
            }
        }
        $http(dynareq).then(function(response) {

            var inTblecolm = "";
            var inTbleValues = "";
            angular.forEach(tablerecord[0].columns, function(value, key) {
                if (!value.cavity && !value.Value.includes('rowid')) {
                    inTblecolm = inTblecolm + value.Value + ",";
                }
            })
            angular.forEach(tablerecord[1].rows, function(value, key) {
                angular.forEach(value.cells, function(value1, key1) {
                    var aa = tablerecord[0].columns[key1].Value;
                    var bb = value1.Value == undefined ? '' : value1.Value;
                    if (!tablerecord[0].columns[key1].cavity && !aa.includes('rowid')) {
                        inTbleValues = inTbleValues + "'" + bb + "',";
                    } else if (!aa.includes('rowid')) {
                        inTbleValues = inTbleValues.trim().slice(0, -2) + "_" + bb + "',";
                    }
                })
                var query = "INSERT INTO " + tableName + " ( " + inTblecolm.trim().slice(0, -1) + ") VALUES (" + inTbleValues.trim().slice(0, -1) + ")";
                var dynareq = {
                    method: 'POST',
                    url: constantService.serverURL + 'CreateDynamicTable',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        'query': query,
                        'table_name': tableName,
                        'type': 'INSERT'
                    }
                }
                $http(dynareq).then(function(response) {}, function(error) {});

                inTbleValues = "";
            })

        }, function(error) {});
    }

    function getApproverDetails(tableId, rowId) {

        var deferred = $q.defer();
        var queryForGetApproverDetails = "select * from " + constantService.tableName + tableId + " where rowid=" + rowId + "";
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': queryForGetApproverDetails,
                    'table_name': constantService.tableName + tableId,
                    'type': 'SELECT'
                }
            }
            console.log("queryForGetApproverDetails  ", queryForGetApproverDetails)
            $http(dynareq).then(function(response) {
                    console.log('queryForGetApproverDetails response', response);
                    console.log('getDataPersonDetails(response)', getDataPersonDetails(response));
                    deferred.resolve(getDataPersonDetails(response));
                },
                function(error) {
                    console.log('error###', error);
                    deferred.reject(error);
                });
        } else {
            $cordovaSQLite.execute(constantService.db, queryForGetApproverDetails).then(function(data) {
                deferred.resolve(getDataPersonDetails(response));
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    function getDataPersonDetails(response) {
        if (response.data.length > 0) {
            var result = response.data[0];
            var data = {},
                email = {};
            var qainspector = '',
                operator = '',
                supervisor = '',
                operatorEmail = '',
                supervisorEmail = '',
                qainspectorEmail = '';
            if (result.operator || result.senioroperator || result.operatorname || result.preparedby)
                operator = result.operator || result.operatorname || result.senioroperator || result.preparedby;
            if (result.supervisor || result.supervisorname || result.inspectedby|| result.qaincharge)
                supervisor = result.supervisor || result.supervisorname || result.inspectedby|| result.qaincharge;
            if (result.qainspector || result.qaleaktest || result.qagumming1 || result.qabushcasting || result.approvedby )
                qainspector = result.qainspector || result.qaleaktest || result.qagumming1 || result.qabushcasting || result.approvedby ;
            if(result.approverby && result.recordName.includes('EPSfI'))
                supervisor=result.approverby;
            return data = {
                    firstPerson: operator,
                    secondPerson: supervisor,
                    thirdPerson: qainspector
                }
                // angular.forEach(data,function(val,keyyy){
                //     loginService.getUserDetail(val.replace(/[^0-9]/g, '')).then(function (resUserDetail) {
                //         if(keyyy=="firstPerson")
                //             operatorEmail=resUserDetail.data[0].email;
                //         else if(keyyy=="secondPerson")
                //             supervisorEmail=resUserDetail.data[0].email;
                //         else if(keyyy=="thirdPerson")
                //             qainspectorEmail=resUserDetail.data[0].email;
                //     });
                // });
                // return data = {
                //     firstPerson: operator,
                //     firstPersonEmail: operatorEmail,
                //     secondPerson: supervisor,
                //     secondPersonEmail: supervisorEmail,
                //     thirdPerson: qainspector,
                //     thirdPersonEmail: qainspectorEmail
                // }
        }
    }

    function getStatusInformation(tableId, recordId) {
        var deferred = $q.defer();
        var query = "SELECT  * FROM  " + constantService.approveTable + " WHERE tableName='" + tableId + "' and recordId='" + recordId + "'";

        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                var data = [];
                data.rows = response.data;
                deferred.resolve(data);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }
}

angular.module('amaraja').factory('getFormsService', getFormsService);

getFormsService.$inject = ['$q', '$http', '$window', '$cordovaSQLite', 'constantService', '$cordovaNetwork'];

function getFormsService($q, $http, $window, $cordovaSQLite, constantService, $cordovaNetwork) {
  var service = {
    getFormsService: getFormsService,
    addTableRecord: addTableRecord,
    addInnerTableRecord: addInnerTableRecord,
    getWorkFlowTableList: getWorkFlowTableList,
    getApproved: getApproved,
    getRejected: getRejected,
    getTableRecordCount: getTableRecordCount,
    getRejectedList: getRejectedList,
    getTableName: getTableName,
    getInFLowList: getInFLowList,
    getCompletedList: getCompletedList,
    getWorkFlowLevel: getWorkFlowLevel,
    updateApprovalTable: updateApprovalTable,
    getBasedOnColumnName: getBasedOnColumnName,
    getEngneeRecords: getEngneeRecords,
    getEngneeRecord: getEngneeRecord,
    getInnerTable: getInnerTable,
    getTableRecordBasedOnQuery: getTableRecordBasedOnQuery,
    getRecordByMachinPatNo: getRecordByMachinPatNo
  }
  return service;

  function getWorkFlowTableList(dept) {
    var deferred = $q.defer();
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "SELECT * FROM " + constantService.workFlowItems + " WHERE department = '" + dept + "'";
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.workFlowItems,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      console.log('SELECT * FROM ' + constantService.workFlowItems + ' WHERE department = "' + dept + '"');
      $cordovaSQLite.execute(constantService.db, 'SELECT * FROM ' + constantService.workFlowItems + ' WHERE department = "' + dept + '"').then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getApproved(formName) {
    var deferred = $q.defer();
    //console.log( 'SELECT * FROM '+constantService.workFlowItems +' WHERE levelform = "'+formName+'" AND level ="'+level+'"');
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "SELECT DISTINCT  formName, approved FROM " + constantService.approveTable + " WHERE formName = '" + formName + "' AND status ='a' AND deptName='" + $window.localStorage.userDept + "'";
      console.log("query 1212  ", query)
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.approveTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      /*$cordovaSQLite.execute(constantService.db, "SELECT DISTINCT  formName, approved FROM "+constantService.approveTable +" WHERE formName = '"+formName+"' AND status ='a' AND deptName='"+$window.localStorage.userDept+"'").then(function(data) {
       
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });*/
    }
    return deferred.promise;
  }

  function getRejected(formName, level) {
    /* AND status='a'*/
    var deferred = $q.defer();
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "SELECT * FROM " + constantService.workFlowItems + " WHERE levelform = '" + formName + "' AND lev ='" + level + "'";
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.workFlowItems,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;

        var inquery = "SELECT * FROM " + constantService.approveTable + " WHERE deptName = '" + data.rows[0].department + "' AND formName ='" + data.rows[0].levelform + "' AND approved='reject'";
        var dynainreq = {
          method: 'POST',
          url: constantService.serverURL + 'SelectDynamicTable',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            'query': inquery,
            'table_name': constantService.approveTable,
            'type': 'SELECT'
          }
        }
        $http(dynainreq).then(function(response) {
          var data1 = [];
          data1.rows = response.data;
          deferred.resolve(data1);
        }, function(error) {
          deferred.reject(error);
        });
      }, function(error) {});
    } else {
      console.log('SELECT * FROM ' + constantService.workFlowItems + ' WHERE levelform = "' + formName + '" AND level ="' + level + '"');
      $cordovaSQLite.execute(constantService.db, 'SELECT * FROM ' + constantService.workFlowItems + ' WHERE levelform = "' + formName + '" AND lev ="' + level + '"').then(function(data) {
        if (data.rows.length > 0) {
          console.log("SELECT * FROM " + constantService.approveTable + " WHERE deptName = '" + data.rows[0].department + "' AND formName ='" + data.rows[0].levelform + "' AND approved='reject'")
          $cordovaSQLite.execute(constantService.db, "SELECT * FROM " + constantService.approveTable + " WHERE deptName = '" + data.rows[0].department + "' AND formName ='" + data.rows[0].levelform + "' AND approved='reject'").then(function(res) {
            deferred.resolve(res);
          });
        }
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getFormsService() {
    var deferred = $q.defer();
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "INSERT INTO " + constantService.mainTable + "(tableName, tableRecord) VALUES ('" + formName + "','" + JSON.stringify({ 'data': record }) + "')";
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'INSERT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, 'INSERT INTO ' + constantService.mainTable + '(tableName, tableRecord) VALUES (?,?)', [formName, JSON.stringify({
        'data': record
      })]).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function addTableRecord(tableId, tableCol, colvalues) {
    var deferred = $q.defer();
    var tableName = constantService.tableName + tableId;
    var query = "INSERT INTO " + tableName + " (" + tableCol + ") VALUES (" + colvalues + ")";
    console.log('query##################################### ', query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': tableName,
          'type': 'INSERT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        //$cordovaSQLite.execute(db, 'UPDATE '+tableName+' set ('+tableCol+') VALUES ('+colvalues+')')
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;

  }

  function addInnerTableRecord(id, subid, tableid, tableCol, colvalues) {
    var deferred = $q.defer();
    var tableName = constantService.innerTableName + id + "_" + subid;
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "INSERT INTO " + tableName + " ( tableid," + tableCol + ") VALUES (" + tableid + "," + colvalues + ")";
      console.log("quert new  ", query)
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': tableName,
          'type': 'INSERT'
        }
      }
      $http(dynareq).then(function(response) {
        console.log("response ", response)
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      console.log('INSERT INTO ' + tableName + ' ( tableid,' + tableCol + ') VALUES (' + tableid + ',' + colvalues + ')')
      $cordovaSQLite.execute(constantService.db, 'INSERT INTO ' + tableName + ' ( tableid,' + tableCol + ') VALUES (' + tableid + ',' + colvalues + ')').then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getTableRecordCount(tableId) {
    var deferred = $q.defer();
    var tableName = constantService.tableName + tableId;
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "SELECT COUNT(*) as dbcount FROM " + tableName;
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': tableName,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, 'SELECT COUNT(*) FROM ' + tableName).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;

  }

  function getRejectedList(deptList) {
    var deferred = $q.defer();
    console.log('rejectList deptList', deptList)
    var query = "select DISTINCT formName,tableName from DF_TBL_APR where approved='reject'";
    if (1) {
      console.log("query ", query);
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.approveTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }


  function getTableName(rowid) {
    var deferred = $q.defer();
    var query = "SELECT tableName FROM " + constantService.mainTable + " where rowid=" + rowid;
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getInFLowList(dept) {
    var deferred = $q.defer();
    var query = '';
    // if (dept != "QA")
    //   query = "select DISTINCT formName from DF_TBL_FMH where  status != 'reject' and role in('" + dept + "')";
    // else
      query="select DISTINCT formName,tableName from DF_TBL_APR where updatedBy='" + dept + "' and approved!='done'";
      console.log("query inflow ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }


  function getCompletedList() {
    var deferred = $q.defer();
    var query = "select DISTINCT formName from DF_TBL_APR where approved='done'";
    console.log("query completed 2  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getWorkFlowLevel(id) {
    var deferred = $q.defer();
    var query = " select lev from DF_WORKFLOW_I where  levelForm=(select tableName from DF_DYNTABLES where rowid='" + id + "') and department ='" + $window.localStorage.userDept + "'";
    console.log("++query ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function updateApprovalTable(query) {
    var deferred = $q.defer();
    console.log("++query ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'INSERT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;


  }

  function getBasedOnColumnName(tablename, columnName, filterCol, filterColVal, refFor) {
    var deferred = $q.defer();
    var query = '';
    var tableeName = '';
    var columnName1 = ''
    if (isNaN(tablename)) {
      tableeName = tablename;
    } else {
      tableeName = constantService.tableName + tablename;
    }
    if (columnName.trim() == 'partno') {
      columnName1 = ' rowid,' + columnName;
    } else {
      columnName1 = columnName;
    }
    if (refFor == 'table') {
      if (filterCol != null) {
        if (columnName.trim() == 'partno') {
          //query =  "SELECT "+columnName1+" FROM ( SELECT "+columnName1+", ROW_NUMBER() OVER(PARTITION BY "+columnName+" ORDER BY rowid) rn FROM "+tableeName+" where "+filterCol+" like '"+filterColVal+"' ) a WHERE rn = 1 order by rowid asc"
          query = "select  " + columnName1 + " from " + tableeName + " where " + filterCol + " like '" + filterColVal + "'";
        } else {
          query = "select DISTINCT " + columnName1 + " from " + tableeName + " where " + filterCol + " like '" + filterColVal + "'";
        }
      } else {
        query = "select DISTINCT " + columnName1 + " from " + tableeName;
      }
    } else {

      var tableeName = '';
      if (isNaN(tablename)) {
        tableeName = tablename;
      } else {
        tableeName = constantService.tableName + tablename;
      }

      if (filterCol != null) {
        if (columnName.trim() == 'partno') {
          query = "SELECT " + columnName1 + " FROM ( SELECT " + columnName1 + ", ROW_NUMBER() OVER(PARTITION BY " + columnName + " ORDER BY rowid) rn FROM " + tableeName + " where " + filterCol + " like '" + filterColVal + "' ) a WHERE rn = 1 order by rowid asc"
        } else {
          query = "select DISTINCT " + columnName1 + " from " + tableeName + " where " + filterCol + " like '" + filterColVal + "'";
        }
      } else {
        query = "select DISTINCT " + columnName1 + " from " + tableeName;
      }
    }


    console.log("query completed --------------------  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getEngneeRecord(tablename, colName, recorvalue) {
    var tableeName = '';
    if (isNaN(tablename)) {
      tableeName = tablename;
    } else {
      tableeName = constantService.tableName + tablename;
    }
    var deferred = $q.defer();
    var query = "select *  from " + tableeName + " where " + colName + " like '" + recorvalue + "'";
    console.log("query completed 3  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getRecordByMachinPatNo(tablename, colName, recorvalue, formname, machine) {
    var tableeName = '';
    if (isNaN(tablename)) {
      tableeName = tablename;
    } else {
      tableeName = constantService.tableName + tablename;
    }
    var deferred = $q.defer();
    var query = '';
    if (formname.toUpperCase().includes('IN-PROCE')) {
      query = "select *  from " + tableeName + " where " + colName + " like '" + recorvalue + "' and machine='" + machine + "'";
    } else {
      query = "select *  from " + tableeName + " where " + colName + " like '" + recorvalue + "'";
    }
    console.log("query completed 3  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getInnerTable(tablename, tableid, val, formName) {
    var deferred = $q.defer();
    if (formName.toUpperCase().includes("MATERIAL PRE")) {
      var query = "select *  from " + constantService.innerTableName + tablename + "_" + val + " where tableid = '" + tableid + "'  order by column1  ASC";
    } else {
      var query = "select *  from " + constantService.innerTableName + tablename + "_" + val + " where tableid = '" + tableid + "'  order by column1 * 1 ASC";
    }

    console.log("query dyntabResponse 1  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.innerTableName + tablename + "_0",
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getTableRecordBasedOnQuery(query) {
    var deferred = $q.defer();
    console.log("query dyntabResponse  2  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }






  function getEngneeRecords(tablename, partval, col) {
    var deferred = $q.defer();
    var tableeName = '';
    if (isNaN(tablename)) {
      tableeName = tablename;
    } else {
      tableeName = constantService.tableName + tablename;
    }


    var query = "select *  from " + tableeName + " where " + col + " like '" + partval.trim() + "'";
    console.log("query 11 ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

}

angular.module('amaraja').factory('loginService', loginService);

loginService.$inject = ['$q', '$http', '$cordovaSQLite', 'constantService'];

function loginService($q, $http, $cordovaSQLite, constantService) {
 	
 	var service = {
 		userLogin:userLogin,
    getUserDetail:getUserDetail
 	}
 	return service;

 	function userLogin(user){
 		var deferred = $q.defer();
    console.log("login ", user)
 		var request = {
                method: 'POST',
                url: constantService.serverURL + 'Login',
                headers: {
                  'Content-Type': 'application/json'
                },
                data: user
               }
    $http(request).then(function(response){
      var result = response.data.LoginResult[0];
      //$cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_LGN (empCode, empName, deptName, role, supervisor, email, mobile, plant, inActive) VALUES ("' + result.empCode +'", "'+ result.empName +'", "'+ result.deptName +'", "'+ result.role +'", "'+ result.supervisor +'", "'+ result.email +'", "'+ result.mobile +'", "'+ result.plant +'", "'+ result.inActive + '")');
      deferred.resolve(result);
   }, function(error){
       deferred.reject(error);
   });

 		/*console.log("--------------------"+'Select * FROM '+constantService.loginTable+' WHERE userName = "'+user.userName+'" AND password= "'+user.password+'"');
    $cordovaSQLite.execute(constantService.db, 'Select * FROM '+constantService.loginTable+' WHERE userName = "'+user.userName+'" AND password= "'+user.password+'"').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    });*/
    return deferred.promise;
 	}

  function getUserDetail(user) {

        var deferred = $q.defer();
        var queryUserDetails = "select * from " + constantService.usersTable + " where empCode=" + user + "";
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': queryUserDetails,
                    'table_name': constantService.usersTable,
                    'type': 'SELECT'
                }
            }
            console.log("queryUserDetails  ", queryUserDetails)
            $http(dynareq).then(function(response) {
                    deferred.resolve(response);
                },
                function(error) {
                    console.log('error###', error);
                    deferred.reject(error);
                });
        } else {
            $cordovaSQLite.execute(constantService.db, queryUserDetails).then(function(data) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
  }

}
angular.module('amaraja').factory('mainService', mainService);

mainService.$inject = ['$q', '$http', '$cordovaSQLite'];

function mainService($q, $http, $cordovaSQLite) {
	var service = {
		addRecord: addRecord,
		addRecordJSON: addRecordJSON,
		getRecord: getRecord,
		createTable: createTable
	}
	return service;

	function createTable(tableRow) {
		var deferred = $q.defer();
		console.log("service ", tableRow);
		var daaa = '';
		angular.forEach(tableRow, function(value, key) {
			daaa = ' ' + daaa + value + " " + 'TEXT,';
		})
		db = window.openDatabase("amaraja.db", "1", "Sqlite", "2000");
		$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS aaaa3 (id INTEGER PRIMARY KEY AUTOINCREMENT, ' + daaa.slice(0, -1) + ')');
	}

	function addRecord(record) {
		console.log("service menu");
		var deferred = $q.defer();
		$cordovaSQLite.execute(db, 'INSERT INTO dept (dpt, dpt2) VALUES (?,?)', [record.c1, record.c6]).then(function(data) {
			console.log("Save successful", data);
			deferred.resolve(data);
		}, function(error) {
			console.log("error successful", error);
			deferred.reject(error);
		})

		return deferred.promise;
	}

	function addRecordJSON(record) {
		console.log("service menu");

		var jsondata = {
			'value': [{
				"label": "Do you have a website?",
				"field_type": "text",
				"required": false,
				"field_options": {},
				"cid": "c1"
			}, {
				"label": "Please enter your clearance number",
				"field_type": "text",
				"required": true,
				"field_options": {},
				"cid": "c6"
			}, {
				"label": "Security personnel #82?",
				"field_type": "radio",
				"required": true,
				"field_options": {
					"options": [{
						"label": "Yes",
						"checked": false
					}, {
						"label": "No",
						"checked": false
					}],
					"include_other_option": true
				},
				"cid": "c10"
			}, {
				"label": "Medical history",
				"field_type": "file",
				"required": true,
				"field_options": {},
				"cid": "c14"
			}]
		}
		var deferred = $q.defer();
		$cordovaSQLite.execute(db, 'INSERT INTO dept1 (dpt, dpt2) VALUES (?,?)', ['chethan2', JSON.stringify(jsondata)]).then(function(data) {
			console.log("Save successful", data);
			deferred.resolve(data);
		}, function(error) {
			console.log("error successful", error);
			deferred.reject(error);
		})

		return deferred.promise;
	}

	function getRecord() {
		console.log("service menu");
		var deferred = $q.defer();
		$cordovaSQLite.execute(db, 'select * from dept1 where dpt = "chethan2"').then(function(data) {
			console.log("Save successful", data);
			deferred.resolve(data);
		}, function(error) {
			console.log("error successful", error);
			deferred.reject(error);
		})

		return deferred.promise;
	}



}
angular
	.module('amaraja')
	.service('notificationService', notificationService);

notificationService.$inject = ['$ionicPopup','$ionicLoading']

function notificationService($ionicPopup, $ionicLoading) {
	var vm = this;
	vm.alert = alert;
	vm.showSpinner =showSpinner;
	vm.hideLoad = hideLoad;
	vm.confirm = confirm;

	function alert(title, alertMsg, okAction) {
		$ionicPopup.alert({
			title: title,
			template: alertMsg
		}).then(okAction);
	}

	function showSpinner(){
     $ionicLoading.show({
     template: '<ion-spinner icon="dots"></ion-spinner>'
   });
  }
	function hideLoad(){
	  $ionicLoading.hide();
	}

	 function confirm(alertMsg, cancelText, confirmText, cancelAction, confirmAction) {
    var confirmPopup = $ionicPopup.confirm({
      title: '',
      template: alertMsg,
      buttons: [{
        text: cancelText,
        type: 'button-positive',
        onTap: cancelAction

      }, {
          text: confirmText,
          type: 'button-positive',
          onTap: confirmAction
        }]
    });
  }

}
angular.module('amaraja').factory('recordDetailsService', recordDetailsService);

recordDetailsService.$inject = ['$q', '$http', '$window', '$cordovaSQLite', 'constantService', '$cordovaNetwork'];

function recordDetailsService($q, $http, $window, $cordovaSQLite, constantService, $cordovaNetwork) {

    var service = {
        getStatusInformation: getStatusInformation

    }
    return service;

    function getStatusInformation(tableId, recordId) {
        var deferred = $q.defer();
        var query = "SELECT  * FROM  " + constantService.approveTable + " WHERE tableName='" + tableId + "' and recordId='" + recordId + "'";
        console.log('getStatusInformation query', query);
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': query,
                    'table_name': constantService.approveTable,
                    'type': 'SELECT'
                }
            }
            $http(dynareq).then(function(response) {
                deferred.resolve(response.data[0]);
            }, function(error) {
                console.log('error###', error);
                deferred.reject(error);
            });
        } else {
            $cordovaSQLite.execute(constantService.db, query).then(function(data) {
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    // function getApprovalInformation(tableId, recordId) {
    //     var deferred = $q.defer();
    //     var query = "SELECT  * FROM  " + constantService.approveTable + " WHERE tableName='" + tableId + "' and recordId='" + recordId + "'";
    //     console.log('getStatusInformation query', query);
    //     if (1) {
    //         var dynareq = {
    //             method: 'POST',
    //             url: constantService.serverURL + 'SelectDynamicTable',
    //             headers: {
    //                 'Content-Type': 'application/json'
    //             },
    //             data: {
    //                 'query': query,
    //                 'table_name': constantService.approveTable,
    //                 'type': 'SELECT'
    //             }
    //         }
    //         $http(dynareq).then(function(response) {
    //             var data = [];
    //             data.rows = response.data;
    //             deferred.resolve(data);
    //         }, function(error) {
    //             console.log('error###', error);
    //             deferred.reject(error);
    //         });
    //     } else {
    //         $cordovaSQLite.execute(constantService.db, query).then(function(data) {
    //             deferred.resolve(data);
    //         }, function(error) {
    //             deferred.reject(error);
    //         });
    //     }
    //     return deferred.promise;
    // }
}

angular.module('amaraja').factory('reportService', reportService);

reportService.$inject = ['$q', '$http', '$cordovaSQLite','constantService'];

function reportService($q, $http, $cordovaSQLite,constantService) {

	var service ={
		getTableRecords: getTableRecords,
		getInnerTableRecords: getInnerTableRecords,
        getByRowName: getByRowName,
        getByRowNamewithcondition: getByRowNamewithcondition
	}
	return service;

	 function getTableRecords(tableId, condition) {
        var deferred = $q.defer();
        var tableName = constantService.tableName + tableId;
        var query = '';
        if(condition != '' && condition != undefined){
            query = "SELECT rowid,* FROM  " + tableName+" where "+condition;
        }else{
            query = "SELECT rowid,* FROM  " + tableName;
        }
        console.log("q2uery ", query)
        var dynareq = {
            method: 'POST',
            url: constantService.serverURL + 'SelectDynamicTable',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'query': query,
                'table_name': tableName,
                'type': 'SELECT'
            }
        }
        $http(dynareq).then(function(response) {
            var data = [];
            data.rows = response.data;
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error);
        });										
      
        return deferred.promise;
    }


	function getInnerTableRecords(TableNameId, subId, tableid){
		 var deferred = $q.defer();
		 var tableName = constantService.innerTableName + TableNameId + "_" + subId;
          var query = '';
         if(TableNameId != '3097' && TableNameId != '2093' && TableNameId != '2096'){
            query = "SELECT  rowid,* FROM  " + tableName + " WHERE tableid ='"+tableid+"'  order by column1 * 1 ASC";
         }else{
            query = "SELECT  rowid,* FROM  " + tableName + " WHERE tableid ='"+tableid+"'  order by column1   ASC";
         }
		 console.log("11query ", query)
     var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            'query': query,
            'table_name': tableName,
            'type': 'SELECT'
        }
    }
    $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
    }, function(error) {
        deferred.reject(error);
    });
    return deferred.promise;
	}

    function getByRowName(tableId, rowname){
         var deferred = $q.defer();
         var tableName = constantService.tableName + tableId;
         var query = "SELECT distinct  "+rowname+" FROM  " + tableName ;
         console.log("111query ", query)
         var dynareq = {
            method: 'POST',
            url: constantService.serverURL + 'SelectDynamicTable',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'query': query,
                'table_name': tableName,
                'type': 'SELECT'
            }
        }
        $http(dynareq).then(function(response) {
            var data = [];
            data.rows = response.data;
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getByRowNamewithcondition(tableId, rowname, colname,  colValue){
        var deferred = $q.defer();
         var tableName = constantService.tableName + tableId;
         var query = "SELECT distinct  "+rowname+" FROM  " + tableName +" Where "+colname+"='"+colValue+"'";
         console.log("113query ", query)
         var dynareq = {
            method: 'POST',
            url: constantService.serverURL + 'SelectDynamicTable',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'query': query,
                'table_name': tableName,
                'type': 'SELECT'
            }
        }
        $http(dynareq).then(function(response) {
            var data = [];
            data.rows = response.data;
            deferred.resolve(data);
        }, function(error) {
            console.log('error###', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

}
angular.module('amaraja').factory('SendEmailService', SendEmailService);

SendEmailService.$inject = ['$q', '$http', '$rootScope', '$window', '$cordovaSQLite', 'constantService', 'recordDetailsService', 'loginService'];

function SendEmailService($q, $http, $rootScope, $window, $cordovaSQLite, constantService, recordDetailsService, loginService) {

  var service = {
    sendEmail: sendEmail,
    sendmailNotifiction: sendmailNotifiction
  }
  return service;

  function sendEmail(user) {
    var deferred = $q.defer();
    console.log("sendmailNotifiction msg", user);
    var request = {
      method: 'POST',
      url: constantService.serverURL + 'SendEmail',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      data: user
    }
    $http(request).then(function(response) {
      deferred.resolve(response);
    }, function(error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  function sendmailNotifiction(recordInformation, statusInformation) {
    console.log("sendmailNotifiction $window ", $window.localStorage);
    console.log("sendmailNotifiction $rootScope.codeNameOfRecord ", $rootScope.codeNameOfRecord);
    console.log("sendmailNotifiction recordInformation.serviceFirstPerson ", recordInformation.serviceFirstPerson);
    console.log("sendmailNotifiction recordInformation.serviceSecondPerson ", recordInformation.serviceSecondPerson);
    console.log("sendmailNotifiction recordInformation.serviceThirdPerson ", recordInformation.serviceThirdPerson);
    console.log("statusInformation.formName.toLowerCase().includes('shift releiving sheet')", statusInformation.formName.toLowerCase().includes('shift releiving sheet'));
    if (!$window.localStorage.empName.toLowerCase().includes('admin') && !statusInformation.formName.toLowerCase().includes('shift releiving sheet')) {
      var codeNameOfRecord = $rootScope.codeNameOfRecord + recordInformation.serviceRecordId;
      var consultPerson = '',
        fromPerson = '',
        status = statusInformation.approved,
        email = '',
        mailingPerson = '';
      if ((status == "done" || status == "reject") && statusInformation.updatedBy == "Production") {
        console.log('entering production done or reject');
        consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
        mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
        fromPerson = $window.localStorage.empName;
      }else if ((status == "done" || status == "reject") && statusInformation.updatedBy == "QA") {
        if (statusInformation.formName.toLowerCase().includes('pack note cum pre delivery inspection report')) {
          consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
          mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
        } else if (statusInformation.formName.toLowerCase() == "inspection report"){
          consultPerson = recordInformation.serviceThirdPerson.replace(/[0-9]/g, '');
          mailingPerson = recordInformation.serviceThirdPerson.replace(/[^0-9]/g, '');
        }else {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
        }
        fromPerson = $window.localStorage.empName;
      } else if (statusInformation.deptName == "Production") {
        console.log('entering production udation');
        if (status == "return" && statusInformation.updatedBy == "Production") {
          consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
          console.log('status return');
          console.log('Consult person',consultPerson);
          console.log('Mailing person',mailingPerson);
        }
        else if (status == "return" && statusInformation.updatedBy == "QA") {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');

        } else {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
        }
      } else if (statusInformation.deptName == "QA") {
        if (status == "return" && statusInformation.formName.toLowerCase() != "inspection report") {
          consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
        } else if (statusInformation.formName.toLowerCase() == "inspection report" && $window.localStorage.userType == "Operator") {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
        } else {
          consultPerson = recordInformation.serviceThirdPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceThirdPerson.replace(/[^0-9]/g, '');
        }
      } else if ($window.localStorage.userDept == "Engineering") {
        if ($window.localStorage.userType == "Operator") {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
          status = "raised";
        } else {
          console.log('eng Process else');
          consultPerson = recordInformation.serviceFirstPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceFirstPerson.replace(/[^0-9]/g, '');
        }
      }else if (statusInformation.deptName == "Mold Maintainance") {
        if (statusInformation.updatedBy == "Production") {
          consultPerson = recordInformation.serviceThirdPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceThirdPerson.replace(/[^0-9]/g, '');
        } else {
          consultPerson = recordInformation.serviceSecondPerson.replace(/[0-9]/g, '');
          fromPerson = $window.localStorage.empName;
          mailingPerson = recordInformation.serviceSecondPerson.replace(/[^0-9]/g, '');
        }
      }
      loginService.getUserDetail(mailingPerson).then(function(resUserDetail) {
        email = resUserDetail.data[0].email;
        if (email == '' || email == null || email == undefined)
          email = "vck@amararaja.co.in";
        if (($window.localStorage.userDept == "Production" && $window.localStorage.userType == "Operator" && status == "approved") ||
          (status == "approved" && (statusInformation.formName.toLowerCase() == "flammability report" || statusInformation.formName.toLowerCase() == "inspection report") && $window.localStorage.userDept == "QA" && $window.localStorage.userType == "Operator"))
          status = "raised";
        else if (status == "done")
          status = "approved";
        if (consultPerson.trim() == '' || consultPerson == null || consultPerson == undefined)
          consultPerson = "Sir/Madam";
        var msg = "Dear " + consultPerson + " \n\n The document " + codeNameOfRecord + " has been " + status.toUpperCase();
        if (fromPerson.trim() != '' && fromPerson != null && fromPerson != undefined)
          msg += " by " + fromPerson;
        msg += ". Please proceed.\n(This is automatic mail from  Online Process Approval Request ,Don’t Reply to This MailID)\n\n Thanks";
        var sub = "Online Process Request";
        var req = { "email": email, "subj": sub, "msg": msg }
        sendEmail(req).then(function(aa) {});
        console.log('sendmailNotifiction recordInformation', recordInformation);
        console.log('sendmailNotifiction statusInformation', statusInformation);
        console.log('sendmailNotifiction email', email);
        console.log('sendmailNotifiction codeNameOfRecord', codeNameOfRecord);
        console.log('sendmailNotifiction consultPerson', consultPerson);
        console.log('sendmailNotifiction fromPerson', fromPerson);
      });
    }
  }
}

angular.module('amaraja').factory('trackRecordService', trackRecordService);

trackRecordService.$inject = ['$q', '$http', '$cordovaSQLite', 'constantService'];

function trackRecordService($q, $http, $cordovaSQLite, constantService) {
		var service = {
			getFormNames: getFormNames,
			getWorkflow: getWorkflow,
			getFormRecords: getFormRecords,
			getRecordDetail: getRecordDetail
		}
	return service;

	function getFormNames() {
		var deferred = $q.defer();
		var query = "SELECT  rowid, tableName FROM "+constantService.mainTable;
     if(1){
      var dynareq = {
          method: 'POST',
          url: constantService.serverURL+ 'SelectDynamicTable',
          headers: {'Content-Type': 'application/json' },
          data: { 'query': query,
                  'table_name': constantService.mainTable,
                  'type': 'SELECT'
                }
         }
      $http(dynareq).then(function(response){
        console.log('response-',response);
        deferred.resolve(response);   
      }, function(error){
        console.log('error###',error);
        deferred.reject(error);
      });
    }else{
      
    }
    return deferred.promise;
	}


	function getWorkflow() {
		var deferred = $q.defer();		
		var query = "SELECT workflowId, levelForm from DF_WORKFLOW_I group by workflowId, levelForm having count(workflowId) > 0" ;
     if(1){
      var dynareq = { method: 'POST',
                  url: constantService.serverURL+ 'SelectDynamicTable',
                  headers: {'Content-Type': 'application/json' },
                  data: { 'query': query, 'table_name': constantService.workFlowItems, 'type': 'SELECT' }
                }
      $http(dynareq).then(function(response){
        console.log('response-',response);
        deferred.resolve(response);   
      }, function(error){
        console.log('error###',error);
        deferred.reject(error);
      });
    }else{
      
    }
    return deferred.promise;
	}

	function getFormRecords(tableid) {
		var deferred = $q.defer();		
		var query = "SELECT * from DF_TBL_PGR_"+tableid;
		console.log("query 3 ",query);
    if(1){
      var dynareq = { method: 'POST',
                  url: constantService.serverURL+ 'SelectDynamicTable',
                  headers: {'Content-Type': 'application/json' },
                  data: { 'query': query, 'table_name': constantService.workFlowItems, 'type': 'SELECT' }
                }
      $http(dynareq).then(function(response){
        console.log('response-',response);
        deferred.resolve(response);   
      }, function(error){
        console.log('error###',error);
        deferred.reject(error);
      });
    }else{
      
    }
    return deferred.promise;
	}

	function getRecordDetail(formId, recordId) {
		var deferred = $q.defer();		
		var query = "select * from DF_TBL_FMH where formName ='"+formId+"' and recordId='"+recordId+"'"
		console.log("query 4 ",query);
     if(1){
      var dynareq = { method: 'POST',
                  url: constantService.serverURL+ 'SelectDynamicTable',
                  headers: {'Content-Type': 'application/json' },
                  data: { 'query': query, 'table_name': constantService.workFlowItems, 'type': 'SELECT' }
                 }
      $http(dynareq).then(function(response){
        console.log('response-',response);
        deferred.resolve(response);   
      }, function(error){
        console.log('error###',error);
        deferred.reject(error);
      });
    }else{
      
    }
    return deferred.promise;
	}

}	
(function() {
	'use strict';

	angular
		.module('amaraja')
		.factory('workflowService', workflowService);

	workflowService.$inject = ['$q', '$cordovaSQLite','constantService', '$cordovaNetwork', '$http'];

	function workflowService($q, $cordovaSQLite, constantService, $cordovaNetwork, $http) {
		var service = {
			getDepartments: getDepartments,
			getForms: getForms,
			submitworkflow: submitworkflow,
			getWorkflow: getWorkflow,
			getWorkflowData: getWorkflowData,
			updateworkflow: updateworkflow,
			getAllDetails: getAllDetails

		};

		return service;

		function getDepartments() {
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT * FROM "+ constantService.deptTable;
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.deptTable,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	    /*}else{
				$cordovaSQLite.execute(constantService.db, 'SELECT * FROM '+ constantService.deptTable).then(function(result) {console.log('result',result);
					def.resolve(result);
				}, function(error) {console.log('error',error);
					def.reject(error);
				});
			}*/
			return def.promise;
		}

		function getForms() {
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT tableName FROM "+constantService.mainTable;
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.mainTable,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	    /*}else{
				$cordovaSQLite.execute(constantService.db, 'SELECT tableName FROM '+constantService.mainTable).then(function(result) {
					def.resolve(result);
				}, function(error) {
					def.reject(error);
				});
			}*/
			return def.promise;
		}

		function submitworkflow(request) {
			var def = $q.defer();
			var promiseColl = [];

			var query = "INSERT INTO "+constantService.workFlowMaster+" (workflowName, levels) VALUES ('" + request.workflowname+ "',"+ request.levels+ ")";
	    console.log('query',query);
	    var dynareq = {
	                method: 'POST',
	                url: constantService.serverURL+ 'CreateDynamicTable',
	                headers: {
	                  'Content-Type': 'application/json'
	                },
	                data: { 'query': query,
	                        'table_name': constantService.workFlowMaster,
	                        'type': 'INSERT'
	                      }
	               }
	    $http(dynareq).then(function(response){
	      promiseColl.push(
					angular.forEach(request.departments, function(piece, index) {
						var count = parseInt(index) + 1;
						var levelno = 'level ' + count ;

						var insquery = "INSERT INTO "+constantService.workFlowItems+" (workflowId, department, levelForm, lev, levelName) VALUES ("+response.data.CreateDynamicTableResult[0].rowid+",'"+ piece + "','" + request.forms[index] + "','" + levelno + "','" + request.formname[index]+"')";
			      console.log('insquery',insquery);
			      var dynainsreq = {
			        method: 'POST',
			        url: constantService.serverURL+ 'CreateDynamicTable',
			        headers: {
			          'Content-Type': 'application/json'
			        },
			        data: { 'query': insquery,
			                'table_name': constantService.workFlowItems,
			                'type': 'INSERT'
			              }
			       }
			      $http(dynainsreq).then(function(response){
			        console.log('response',response);
			      }, function(error){
			        console.log('error###',error);
			      });
					})
				)
				$q.all(promiseColl).then(function(data) {
					def.resolve(data);
				}, function(error) {
					def.reject(error);
				});
	    }, function(error){
	      console.log('error###',error);
	    });

			/*$cordovaSQLite.execute(constantService.db, 'INSERT INTO '+constantService.workFlowMaster+' (workflowname, levels) VALUES (?,?)', [request.workflowname, request.levels]).then(function(data) {
				promiseColl.push(
					angular.forEach(request.departments, function(piece, index) {
						var count = parseInt(index) + 1;
						var levelno = 'level ' + count ;
						$cordovaSQLite.execute(constantService.db, 'INSERT INTO '+constantService.workFlowItems+' (workflowid, department, levelform, level, levelname) VALUES ('+data.insertId+',"'+ piece+'","'+ request.forms[index]+'","'+ levelno+'","'+ request.formname[index]+'")').then(function(data) {
							console.log("asdded")
						}, function(error) {
							console.log("error ", error)
						});
					})
				)
				$q.all(promiseColl).then(function(data) {
					def.resolve(data);
				}, function(error) {
					def.reject(error);
				});
			}, function(error) {
			});*/

			return def.promise;
		}

		function getWorkflow() {
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT * FROM "+constantService.workFlowMaster;
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.workFlowMaster,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	    /*}else{
				$cordovaSQLite.execute(constantService.db, 'SELECT * FROM '+constantService.workFlowMaster).then(function(result) {console.log('result',result);
					def.resolve(result);
				}, function(error) {
					def.reject(error);
				});
			}*/

			return def.promise;
		}

		function getWorkflowData(id){
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT  rowid,* FROM "+constantService.workFlowItems+" WHERE workflowid =" + id;
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.workFlowItems,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	      	console.log('response',response);
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	    /*}else{
				console.log('select  rowid,* from '+constantService.workFlowItems+' where workflowid =' + id )
				$cordovaSQLite.execute(constantService.db, 'SELECT  rowid,* FROM '+constantService.workFlowItems+' WHERE workflowid =' + id ).then(function(result) {console.log('result',result);
					def.resolve(result);
				}, function(error) {
					def.reject(error);
				});
			}*/
			return def.promise;
		}

		function getAllDetails() {
			console.log('service');
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT * FROM "+constantService.workFlowItems + " AS A JOIN "+ constantService.workFlowMaster + " AS B ON A.workflowId = B.workflowId ORDER BY B.workflowName";
	      console.log('query',query);
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.workFlowItems,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	
			return def.promise;
		}


		function updateworkflow(request, removeElements){
			var def = $q.defer();
			var promiseColl = [];
			promiseColl.push(
				angular.forEach(request, function(value, index){
					if(value.create != undefined && value.create == true){
						//if($cordovaNetwork.isOnline()){
							var query = "INSERT INTO "+constantService.workFlowItems+" (workflowId, department, levelForm, lev, levelName) VALUES (" + value.workflowid + ",'"+ value.department + "','" + value.levelform +"',"+ value.level + ",'"+ value.levelname + "')";
				      var dynareq = {
				                  method: 'POST',
				                  url: constantService.serverURL+ 'CreateDynamicTable',
				                  headers: {
				                    'Content-Type': 'application/json'
				                  },
				                  data: { 'query': query,
				                          'table_name': constantService.workFlowItems,
				                          'type': 'INSERT'
				                        }
				                 }
				      $http(dynareq).then(function(response){
				        console.log('response',response);
				      }, function(error){
				        console.log('error###',error);
				      });
				    /*}else{
						console.log('INSERT INTO '+constantService.workFlowItems+' (workflowid, department, levelform, lev, levelname) VALUES (?,?,?,?,?)', [value.workflowid, value.department, value.levelform, value.level, value.levelname])
							$cordovaSQLite.execute(constantService.db, 'INSERT INTO '+constantService.workFlowItems+' (workflowid, department, levelform, level, levelname) VALUES (?,?,?,?,?)', [value.workflowid, value.department, value.levelform, value.level, value.levelname]).then(function(data) {
							}, function(error) {
							});
						}*/
					}else if(value.edited != undefined && value.edited == true){
						//if($cordovaNetwork.isOnline()){
							var query = "UPDATE "+constantService.workFlowItems+" SET department = '"+ value.department + "', levelform = '" + value.levelform + "', lev = '"+ value.level + "', levelname = '"+ value.levelname + "' WHERE  rowid = "+ value.rowid;
				      var dynareq = {
				                  method: 'POST',
				                  url: constantService.serverURL+ 'CreateDynamicTable',
				                  headers: {
				                    'Content-Type': 'application/json'
				                  },
				                  data: { 'query': query,
				                          'table_name': constantService.workFlowItems,
				                          'type': 'UPDATE'
				                        }
				                 }
				      $http(dynareq).then(function(response){
				        console.log('response',response);
				      }, function(error){
				        console.log('error###',error);
				      });
			      /*}else{
							$cordovaSQLite.execute(constantService.db, 'UPDATE '+constantService.workFlowItems+' SET department = "'+ value.department + '", levelform = "'+ value.levelform + '", level = "'+ value.level + '", levelname = "'+ value.levelname + '" WHERE  rowid = '+ value.rowid).then(function(data) {
								console.log('data',data);
							}, function(error) {
								console.log('error',error);
							});
						}*/
					}else{
					}
	      })
      )
      if(removeElements.length > 0){
				angular.forEach(removeElements, function(value, index){
					//if($cordovaNetwork.isOnline()){
						var delquery = "DELETE FROM "+constantService.workFlowItems+" WHERE rowid = "+ value.rowid;
			      var dynadelreq = {
			                  method: 'POST',
			                  url: constantService.serverURL+ 'CreateDynamicTable',
			                  headers: {
			                    'Content-Type': 'application/json'
			                  },
			                  data: { 'query': delquery,
			                          'table_name': constantService.workFlowItems,
			                          'type': 'DELETE'
			                        }
			                 }
			      $http(dynadelreq).then(function(response){
			        console.log('response',response);
			        var query = "UPDATE "+constantService.workFlowMaster+" SET levels = ((SELECT levels from "+ constantService.workFlowMaster + " WHERE workflowId = "+ value.workflowid+")-1) WHERE workflowId = "+ value.workflowid;
				      var dynareq = {
				                  method: 'POST',
				                  url: constantService.serverURL+ 'CreateDynamicTable',
				                  headers: {
				                    'Content-Type': 'application/json'
				                  },
				                  data: { 'query': query,
				                          'table_name': constantService.workFlowMaster,
				                          'type': 'UPDATE'
				                        }
				                 }
				      $http(dynareq).then(function(response){
				        console.log('response',response);
				      }, function(error){
				        console.log('error###',error);
				      });
				      }, function(error){
				        console.log('error###',error);
				      });
			    /*}else{
						$cordovaSQLite.execute(constantService.db, 'DELETE FROM '+constantService.workFlowItems+' WHERE rowid = '+ value.rowid).then(function(data) {
							$cordovaSQLite.execute(constantService.db, 'UPDATE '+constantService.workFlowMaster+' SET levels = ((SELECT levels from workflowMaster WHERE workflowid = '+ value.workflowid+')-1) WHERE workflowid = '+ value.workflowid);
						}, function(error) {
							console.log('error',error);
						});
					}*/
				})
			}

			$q.all(promiseColl).then(function(data) {
				def.resolve(data);
			}, function(error) {
				def.reject(error);
			});
			return def.promise;
		}
	}
})();