angular.module('amaraja').factory('createFormsService', createFormsService);

createFormsService.$inject = ['$q', '$http', '$cordovaSQLite', 'constantService'];

function createFormsService($q, $http, $cordovaSQLite, constantService) {

  var service = {
    addTableJSON: addTableJSON,
    getTableJSON: getTableJSON,
    getTableByName: getTableByName,
    updateTableJSON: updateTableJSON,
    getColumnValues: getColumnValues,
    createTable: createTable,
    createInnerTable: createInnerTable,
    getTableMaster: getTableMaster,
    getMasterByName: getMasterByName,
    updateFormName: updateFormName,
    getColumnValuesCondition: getColumnValuesCondition,
    getDynamicTableColName: getDynamicTableColName
  }
  return service;

  function addTableJSON(formName, record) {
    var deferred = $q.defer();
    var query = "INSERT INTO " + constantService.mainTable + " (tableName, tableRecord) VALUES ('" + formName + "','" + JSON.stringify({ 'data': record }) + "')";
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'CreateDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.mainTable,
        'type': 'INSERT'
      }
    }
    $http(dynareq).then(function(response) {
      console.log('response', response);
      var result = response.data.CreateDynamicTableResult[0];
      deferred.resolve(result);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'INSERT INTO Tables (tableName, tableRecord) VALUES (?,?)', [formName, JSON.stringify({
      'data': record
    })]).then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;*/
  }

  function createTable(tableId, tableColun) {
    var deferred = $q.defer();
    var col = "";
    var tableName = constantService.tableName + tableId;
    //var flag = true;
    angular.forEach(tableColun, function(value, key) {
      if (value.Type == 'table') {
        col = ' ' + col + value.ColName + " " + 'INTEGER,';
        //flag = false;
      } else if (value.Type == 'date') {
        col = ' ' + col + value.ColName + " " + 'DATETIME,';
      } else if (value.Type == 'checkbox') {
        col = ' ' + col + value.ColName + " " + 'VARCHAR(50),';
      } else if (value.Type == 'textarea') {
        col = ' ' + col + value.ColName + " " + 'VARCHAR(MAX),';
      } else {
        col = ' ' + col + value.ColName + " " + 'VARCHAR(50),';
      }
    });
    /*$cordovaSQLite.execute(constantService.db, 'CREATE TABLE IF NOT EXISTS '+tableName+' ( ' + col.slice(0, -1) + ')').then(function(data) {
       deferred.resolve(data);
     }, function(error) {
       deferred.reject(error);
     });
     return deferred.promise;*/

    var query = "CREATE TABLE " + tableName + " ( rowid INTEGER PRIMARY KEY IDENTITY(1,1), recordName VARCHAR(50), " + col.slice(0, -1).trim() + ")";
    console.log(" query create table ", query)
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'CreateDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': tableName,
        'type': 'CREATE'
      }
    }
    $http(dynareq).then(function(response) {
      //console.log('response',response);
      var result = response.data.CreateDynamicTableResult[0];
      deferred.resolve(result);
    }, function(error) {
      //console.log('error###',error);
      deferred.reject(error);
    });
    return deferred.promise;
  }

  function createInnerTable(pTableID, tableID, record) {
    var deferred = $q.defer();
    var col = "";
    var index = 0;
    angular.forEach(record, function(value, key) {
      if (value.Type == 'table') {
        var tableName = constantService.innerTableName + pTableID + "_" + index;
        angular.forEach(value.Settings, function(value1, key1) {
          if (value1.Type == 'table') {
            angular.forEach(value1.PossibleValue[0].columns, function(value2, key2) {
              col = ' ' + col + value2.value + " " + 'VARCHAR(50),';
              //tableCol = tableCol +" " + value2.value+",";
            })
            var query = "CREATE TABLE " + tableName + " (rowid INTEGER PRIMARY KEY IDENTITY(1,1), tableid INTEGER , " + col.slice(0, -1) + ")";
            var dynareq = {
              method: 'POST',
              url: constantService.serverURL + 'CreateDynamicTable',
              headers: {
                'Content-Type': 'application/json'
              },
              data: {
                'query': query,
                'table_name': tableName,
                'type': 'CREATE'
              }
            }
            $http(dynareq).then(function(response) {
              var tableCol = ''
              angular.forEach(value1.PossibleValue[0].columns, function(value2, key2) {
                tableCol = tableCol + " " + value2.value + ",";
              })
              angular.forEach(value1.PossibleValue[0].rows, function(value2, key2) {
                var colvalues = "";
                angular.forEach(value2.cells, function(value3, key3) {
                  colvalues = colvalues + " '" + value3.value + "',";

                });
                console.log("tableCol ", tableCol)
                var insertquery = "INSERT INTO " + tableName + " (tableid," + tableCol.slice(0, -1) + ") VALUES (0," + colvalues.slice(0, -1) + ")";
                console.log("insert inner query ", insertquery)
                var dynainsreq = {
                  method: 'POST',
                  url: constantService.serverURL + 'CreateDynamicTable',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  data: {
                    'query': insertquery,
                    'table_name': tableName,
                    'type': 'INSERT'
                  }
                }
                $http(dynainsreq).then(function(response) {

                  deferred.resolve(response);
                }, function(error) {
                  console.log('error###', error);
                });
              });
            }, function(error) {
              console.log('error###', error);
            });
          }
        });
        ++index;
        col = '';
      }
    })
    return deferred.promise;
  }

  function updateTableJSON(id, formName, record) {

    var deferred = $q.defer();

    getTableRecord(id).then(function(tableRecord) {
      console.log('tableRecord', tableRecord)
      var updatequery = "UPDATE " + constantService.mainTable + " SET tableName = '" + formName + "', tableRecord = '" + JSON.stringify({ 'data': record }) + "' WHERE rowid = " + id;
      console.log('updatequery', updatequery);
      var dynaupreq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': updatequery,
          'table_name': constantService.mainTable,
          'type': 'UPDATE'
        }
      }
      $http(dynaupreq).then(function(response) {
        console.log("response update form ", response);

        getTableColName(id).then(function(colName) {
          var firstLoop = '',
            secondLoop = '';

          if (colName.data.length <= record.length + 2) {
            firstLoop = record;
            secondLoop = colName.data;
            console.log('firstLoop : Record, secondLoop : colName')
            console.log('record', record)
            console.log('colName', secondLoop)
          } else
          if (colName.data.length > record.length + 2) {
            secondLoop = record;
            firstLoop = colName.data;
            console.log('firstLoop : colName, secondLoop : Record')
            console.log('record', record)
            console.log('colName', firstLoop)
          }

          for (j = 0; j < firstLoop.length; j++) {
            console.log('tableRecordValue', tableRecordValue)
            var flag = true;
            var colNameAsInTable = '',
              tableRecordValue = '',
              isFound = '',
              checkingVal = '',
              checkingObj = '';
            // console.log('colNameAsInTable',colNameAsInTable);
            for (i = 0; i < secondLoop.length; i++) {

              if (colName.data.length <= record.length + 2) {
                colNameAsInTable = secondLoop[i];
                tableRecordValue = firstLoop[j];
                checkingObj = colName;
              } else {
                tableRecordValue = secondLoop[i];
                colNameAsInTable = firstLoop[j];
                checkingObj = record;
              }

              if (colNameAsInTable.COLUMN_NAME == tableRecordValue.Settings[1].Value) {
                flag = false;
                break;
              }
            }

            if (colName.data.length <= record.length + 2) {
              checkingVal = tableRecordValue.Settings[1].Value;
            } else {
              if (colNameAsInTable.COLUMN_NAME != 'rowid' && colNameAsInTable.COLUMN_NAME != 'recordName') {
                checkingVal = colNameAsInTable.COLUMN_NAME;
              }
            }
                isFound = _isContains(checkingObj, checkingVal);

            console.log('contains', isFound, checkingVal);

            if (flag && !isFound) {
              var col = '',
                alterquery = '';

              if (colName.data.length <= record.length + 2) {
                if (tableRecordValue.Type == 'table') {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'INTEGER';
                } else if (tableRecordValue.Type == 'date') {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'DATETIME';
                } else if (tableRecordValue.Type == 'checkbox') {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(50)';
                } else if (tableRecordValue.Type == 'textarea') {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(MAX)';
                } else {
                  col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(50)';
                }
                console.log('add col alter table')
                alterquery = "ALTER TABLE " + constantService.tableName + id + " add " + col;
              } else {
                console.log('drop col alter table')
                alterquery = "ALTER TABLE " + constantService.tableName + id + " drop COLUMN " + checkingVal;
              }
              if (alterquery != '') {
                var alterQueryReq = {
                  method: 'POST',
                  url: constantService.serverURL + 'CreateDynamicTable',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  data: {
                    'query': alterquery,
                    'table_name': constantService.tableName + id,
                    'type': 'Update'
                  }
                }

                $http(alterQueryReq).then(function(response) {
                  console.log('alterQueryReq response', response)
                });
              }
            }
          }

          // angular.forEach(colName.data, function(colNameAsInTable) {
          //   var flag = true;
          //   var tableRecordValue = '';
          //   // console.log('colNameAsInTable',colNameAsInTable);
          //   for (i = 0; i < JSON.parse(tableRecord.rows[0].tableRecord).data.length; i++) {

          //     tableRecordValue = JSON.parse(tableRecord.rows[0].tableRecord).data[i];
          //     if (colNameAsInTable.COLUMN_NAME == tableRecordValue.Settings[1].Value) {
          //       flag = false;
          //       break;
          //     }
          //   }
          //   // if (tableRecordValue.Settings[1].Value == "rowid" || tableRecordValue.Settings[1].Value == "recordName" || colNameAsInTable.COLUMN_NAME == "rowid" || colNameAsInTable.COLUMN_NAME == "recordName")
          //   //   flag = false;
          //   if (flag) {
          //     console.log('flag', flag);
          //     console.log('val.Settings[1].Value', tableRecordValue.Settings[1].Value);
          //     console.log('colNameAsInTable.COLUMN_NAME', colNameAsInTable.COLUMN_NAME);
          //     var col = '';
          //     if (tableRecordValue.Type == 'table') {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'INTEGER';
          //     } else if (tableRecordValue.Type == 'date') {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'DATETIME';
          //     } else if (tableRecordValue.Type == 'checkbox') {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(50)';
          //     } else if (tableRecordValue.Type == 'textarea') {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(MAX)';
          //     } else {
          //       col = ' ' + col + tableRecordValue.Settings[1].Value + " " + 'VARCHAR(50)';
          //     }

          //     var alterquery = "ALTER TABLE " + constantService.tableName + id + " add " + col;
          //     var alterQueryReq = {
          //       method: 'POST',
          //       url: constantService.serverURL + 'CreateDynamicTable',
          //       headers: {
          //         'Content-Type': 'application/json'
          //       },
          //       data: {
          //         'query': alterquery,
          //         'table_name': constantService.tableName + id,
          //         'type': 'Update'
          //       }
          //     }

          //     $http(alterQueryReq).then(function(response) {
          //       console.log('alterQueryReq response', response)
          //     });
          //   }
          // });
        });


        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });

      /*$cordovaSQLite.execute(constantService.db, 'UPDATE Tables SET tableName = ?, tableRecord = ? WHERE rowid = ?',[formName, JSON.stringify({'data': record}), id]).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });*/
    });
    return deferred.promise;
  }

  function getTableJSON() {
    var deferred = $q.defer();

    var query = "SELECT  rowid,tableName from " + constantService.mainTable;
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.mainTable,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select  rowId,tableName from Tables').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function getTableMaster() {
    var deferred = $q.defer();
    var query = "select  DISTINCT tableName from DF_MSTRS";
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': 'DF_MSTRS',
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select  DISTINCT tableName from DF_MSTRS').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function getTableByName(recordId) {
    var deferred = $q.defer();

    var query = "SELECT * from " + constantService.mainTable + " where rowid =" + recordId;
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.mainTable,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select  * from Tables where rowid =' + '"' + recordId + '"').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function getMasterByName(masterName) {
    var deferred = $q.defer();

    var query = "SELECT colName from DF_MSTRS where tableName =" + "'" + masterName + "'";
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': 'DF_MSTRS',
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select colName from DF_MSTRS where tableName =' + '"' + masterName + '"').then(function(data) {console.log('data----',data);
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function getColumnValues(column, table) {
    var deferred = $q.defer();

    var query = "SELECT DISTINCT " + column + " from " + table;
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': table,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /* $cordovaSQLite.execute(constantService.db, 'SELECT DISTINCT '+ column + ' from '+ table).then(function(data) {
       deferred.resolve(data);
     }, function(error) {
       deferred.reject(error);
     })
     return deferred.promise;*/
  }

  function getColumnValuesCondition(column, table, ConditionData) {
    var deferred = $q.defer();
    var subQuery = "(";
    angular.forEach(ConditionData, function(value, index) {
      subQuery += value.condColumn + " " + value.condEquals + " " + "(SELECT DISTINCT " + value.condColumnName + " FROM " + value.condTableName + " WHERE " + value.condColumnName + " = '" + value.condValue + "') " + value.condChange + " ";
    })
    subQuery += ")"
      //console.log('SELECT '+ column + ' from '+ table + ' WHERE ' + condColumn + ' IN (SELECT ' + condColumn + ' FROM '+ condTable +' WHERE '+ condColumn +' = "'+ condValue + '")');
    if (ConditionData[0].condColumn != "") {

      var query = '';
      if (column.includes('empName')) {
        query = "SELECT " + column + ",empCode from " + table + " WHERE " + subQuery;
      } else {
        query = "SELECT " + column + " from " + table + " WHERE " + subQuery;
      }
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': table,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        console.log('responssssssssss', response);
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        console.log('error###', error);
        deferred.reject(error);
      });

      /*$cordovaSQLite.execute(constantService.db, 'SELECT '+ column + ' from '+ table + ' WHERE ' + subQuery).then(function(data) {
        console.log('data',data);
        deferred.resolve(data);
      }, function(error) {
        console.log('err',error);
        deferred.reject(error);
      })*/
    } else {

      var query = '';
      if (column.includes('empName')) {
        query = "SELECT " + column + ",empCode  from " + table;
      } else {
        query = "SELECT " + column + " from " + table;
      }

      // var query = "SELECT "+ column + " from "+ table;
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': table,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        console.log('error###', error);
        deferred.reject(error);
      });
      /* $cordovaSQLite.execute(constantService.db, 'SELECT '+ column + ' from '+ table).then(function(data) {
         console.log('data',data);
         deferred.resolve(data);
       }, function(error) {
         console.log('err',error);
         deferred.reject(error);
       })*/
    }

    return deferred.promise;
  }

  function updateFormName(formName, previousName) {
    var deferred = $q.defer();

    var query = "UPDATE " + constantService.workFlowItems + " SET levelform = '" + formName + "' WHERE levelform = '" + previousName + "'";
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'CreateDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.workFlowItems,
        'type': 'UPDATE'
      }
    }
    $http(dynareq).then(function(response) {
      var updquery = "UPDATE " + constantService.approveTable + " SET formName = '" + formName + "' WHERE formName = '" + previousName + "'";
      var dynaupdreq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': updquery,
          'table_name': constantService.approveTable,
          'type': 'UPDATE'
        }
      }
      $http(dynaupdreq).then(function(response) {}, function(error) {
        console.log('error###', error);
      });
      deferred.resolve(response);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    /*$cordovaSQLite.execute(constantService.db, 'UPDATE '+constantService.workFlowItems+' SET levelform = "'+ formName +'" WHERE levelform = "'+ previousName + '"').then(function(data) {
      $cordovaSQLite.execute(constantService.db, 'UPDATE '+constantService.approveTable+' SET formName = "'+ formName +'" WHERE formName = "'+ previousName + '"');
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    });*/
    return deferred.promise;
  }

  function getDynamicTableColName(rowid) {
    var deferred = $q.defer();
    var query = '';
    console.log("rowid ", rowid, isNaN(rowid))
    if (isNaN(parseInt(rowid))) {
      query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + rowid + "'";
    } else {
      query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + constantService.tableName + rowid + "'";
    }
    console.log("dynamiv query ", query)
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.workFlowItems,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      deferred.resolve(response);
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  function getTableColName(tableId) {
    var deferred = $q.defer();
    var query = '';
    query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + constantService.tableName + tableId + "'";
    console.log("getTableColName query ", query)
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.tableName + tableId,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      console.log('getTableColName response', response);
      deferred.resolve(response);
    }, function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }

  function getTableRecord(id) {
    var deferred = $q.defer();
    var query = "SELECT tableRecord from " + constantService.mainTable + " where rowid=" + id;
    var dynareq = {
      method: 'POST',
      url: constantService.serverURL + 'SelectDynamicTable',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'query': query,
        'table_name': constantService.mainTable,
        'type': 'SELECT'
      }
    }
    $http(dynareq).then(function(response) {
      var data = [];
      console.log('response getTableRecord', response);
      data.rows = response.data;
      deferred.resolve(data);
    }, function(error) {
      console.log('error###', error);
      deferred.reject(error);
    });
    return deferred.promise;

    /*$cordovaSQLite.execute(constantService.db, 'select  rowId,tableName from Tables').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    })
    return deferred.promise;*/
  }

  function _isContains(json, value) {
    let contains = false;
    Object.keys(json).some(key => {
      contains = typeof json[key] === 'object' ? _isContains(json[key], value) : json[key] === value;
      return contains;
    });
    return contains;
  }
}
