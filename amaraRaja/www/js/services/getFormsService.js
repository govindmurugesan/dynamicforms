angular.module('amaraja').factory('getFormsService', getFormsService);

getFormsService.$inject = ['$q', '$http', '$window', '$cordovaSQLite', 'constantService', '$cordovaNetwork'];

function getFormsService($q, $http, $window, $cordovaSQLite, constantService, $cordovaNetwork) {
  var service = {
    getFormsService: getFormsService,
    addTableRecord: addTableRecord,
    addInnerTableRecord: addInnerTableRecord,
    getWorkFlowTableList: getWorkFlowTableList,
    getApproved: getApproved,
    getRejected: getRejected,
    getTableRecordCount: getTableRecordCount,
    getRejectedList: getRejectedList,
    getTableName: getTableName,
    getInFLowList: getInFLowList,
    getCompletedList: getCompletedList,
    getWorkFlowLevel: getWorkFlowLevel,
    updateApprovalTable: updateApprovalTable,
    getBasedOnColumnName: getBasedOnColumnName,
    getEngneeRecords: getEngneeRecords,
    getEngneeRecord: getEngneeRecord,
    getInnerTable: getInnerTable,
    getTableRecordBasedOnQuery: getTableRecordBasedOnQuery,
    getRecordByMachinPatNo: getRecordByMachinPatNo
  }
  return service;

  function getWorkFlowTableList(dept) {
    var deferred = $q.defer();
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "SELECT * FROM " + constantService.workFlowItems + " WHERE department = '" + dept + "'";
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.workFlowItems,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      console.log('SELECT * FROM ' + constantService.workFlowItems + ' WHERE department = "' + dept + '"');
      $cordovaSQLite.execute(constantService.db, 'SELECT * FROM ' + constantService.workFlowItems + ' WHERE department = "' + dept + '"').then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getApproved(formName) {
    var deferred = $q.defer();
    //console.log( 'SELECT * FROM '+constantService.workFlowItems +' WHERE levelform = "'+formName+'" AND level ="'+level+'"');
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "SELECT DISTINCT  formName, approved FROM " + constantService.approveTable + " WHERE formName = '" + formName + "' AND status ='a' AND deptName='" + $window.localStorage.userDept + "'";
      console.log("query 1212  ", query)
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.approveTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      /*$cordovaSQLite.execute(constantService.db, "SELECT DISTINCT  formName, approved FROM "+constantService.approveTable +" WHERE formName = '"+formName+"' AND status ='a' AND deptName='"+$window.localStorage.userDept+"'").then(function(data) {
       
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });*/
    }
    return deferred.promise;
  }

  function getRejected(formName, level) {
    /* AND status='a'*/
    var deferred = $q.defer();
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "SELECT * FROM " + constantService.workFlowItems + " WHERE levelform = '" + formName + "' AND lev ='" + level + "'";
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.workFlowItems,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;

        var inquery = "SELECT * FROM " + constantService.approveTable + " WHERE deptName = '" + data.rows[0].department + "' AND formName ='" + data.rows[0].levelform + "' AND approved='reject'";
        var dynainreq = {
          method: 'POST',
          url: constantService.serverURL + 'SelectDynamicTable',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            'query': inquery,
            'table_name': constantService.approveTable,
            'type': 'SELECT'
          }
        }
        $http(dynainreq).then(function(response) {
          var data1 = [];
          data1.rows = response.data;
          deferred.resolve(data1);
        }, function(error) {
          deferred.reject(error);
        });
      }, function(error) {});
    } else {
      console.log('SELECT * FROM ' + constantService.workFlowItems + ' WHERE levelform = "' + formName + '" AND level ="' + level + '"');
      $cordovaSQLite.execute(constantService.db, 'SELECT * FROM ' + constantService.workFlowItems + ' WHERE levelform = "' + formName + '" AND lev ="' + level + '"').then(function(data) {
        if (data.rows.length > 0) {
          console.log("SELECT * FROM " + constantService.approveTable + " WHERE deptName = '" + data.rows[0].department + "' AND formName ='" + data.rows[0].levelform + "' AND approved='reject'")
          $cordovaSQLite.execute(constantService.db, "SELECT * FROM " + constantService.approveTable + " WHERE deptName = '" + data.rows[0].department + "' AND formName ='" + data.rows[0].levelform + "' AND approved='reject'").then(function(res) {
            deferred.resolve(res);
          });
        }
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getFormsService() {
    var deferred = $q.defer();
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "INSERT INTO " + constantService.mainTable + "(tableName, tableRecord) VALUES ('" + formName + "','" + JSON.stringify({ 'data': record }) + "')";
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'INSERT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, 'INSERT INTO ' + constantService.mainTable + '(tableName, tableRecord) VALUES (?,?)', [formName, JSON.stringify({
        'data': record
      })]).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function addTableRecord(tableId, tableCol, colvalues) {
    var deferred = $q.defer();
    var tableName = constantService.tableName + tableId;
    var query = "INSERT INTO " + tableName + " (" + tableCol + ") VALUES (" + colvalues + ")";
    console.log('query##################################### ', query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': tableName,
          'type': 'INSERT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        //$cordovaSQLite.execute(db, 'UPDATE '+tableName+' set ('+tableCol+') VALUES ('+colvalues+')')
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;

  }

  function addInnerTableRecord(id, subid, tableid, tableCol, colvalues) {
    var deferred = $q.defer();
    var tableName = constantService.innerTableName + id + "_" + subid;
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "INSERT INTO " + tableName + " ( tableid," + tableCol + ") VALUES (" + tableid + "," + colvalues + ")";
      console.log("quert new  ", query)
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': tableName,
          'type': 'INSERT'
        }
      }
      $http(dynareq).then(function(response) {
        console.log("response ", response)
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      console.log('INSERT INTO ' + tableName + ' ( tableid,' + tableCol + ') VALUES (' + tableid + ',' + colvalues + ')')
      $cordovaSQLite.execute(constantService.db, 'INSERT INTO ' + tableName + ' ( tableid,' + tableCol + ') VALUES (' + tableid + ',' + colvalues + ')').then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getTableRecordCount(tableId) {
    var deferred = $q.defer();
    var tableName = constantService.tableName + tableId;
    //if(window.cordova != undefined && $cordovaNetwork.isOnline()){
    if (1) {
      var query = "SELECT COUNT(*) as dbcount FROM " + tableName;
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': tableName,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, 'SELECT COUNT(*) FROM ' + tableName).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;

  }

  function getRejectedList(deptList) {
    var deferred = $q.defer();
    console.log('rejectList deptList', deptList)
    var query = "select DISTINCT formName,tableName from DF_TBL_APR where approved='reject'";
    if (1) {
      console.log("query ", query);
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.approveTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }


  function getTableName(rowid) {
    var deferred = $q.defer();
    var query = "SELECT tableName FROM " + constantService.mainTable + " where rowid=" + rowid;
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getInFLowList(dept) {
    var deferred = $q.defer();
    var query = '';
    // if (dept != "QA")
    //   query = "select DISTINCT formName from DF_TBL_FMH where  status != 'reject' and role in('" + dept + "')";
    // else
      query="select DISTINCT formName,tableName from DF_TBL_APR where updatedBy='" + dept + "' and approved!='done'";
      console.log("query inflow ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }


  function getCompletedList() {
    var deferred = $q.defer();
    var query = "select DISTINCT formName from DF_TBL_APR where approved='done'";
    console.log("query completed 2  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getWorkFlowLevel(id) {
    var deferred = $q.defer();
    var query = " select lev from DF_WORKFLOW_I where  levelForm=(select tableName from DF_DYNTABLES where rowid='" + id + "') and department ='" + $window.localStorage.userDept + "'";
    console.log("++query ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function updateApprovalTable(query) {
    var deferred = $q.defer();
    console.log("++query ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'CreateDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'INSERT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;


  }

  function getBasedOnColumnName(tablename, columnName, filterCol, filterColVal, refFor) {
    var deferred = $q.defer();
    var query = '';
    var tableeName = '';
    var columnName1 = ''
    if (isNaN(tablename)) {
      tableeName = tablename;
    } else {
      tableeName = constantService.tableName + tablename;
    }
    if (columnName.trim() == 'partno') {
      columnName1 = ' rowid,' + columnName;
    } else {
      columnName1 = columnName;
    }
    if (refFor == 'table') {
      if (filterCol != null) {
        if (columnName.trim() == 'partno') {
          //query =  "SELECT "+columnName1+" FROM ( SELECT "+columnName1+", ROW_NUMBER() OVER(PARTITION BY "+columnName+" ORDER BY rowid) rn FROM "+tableeName+" where "+filterCol+" like '"+filterColVal+"' ) a WHERE rn = 1 order by rowid asc"
          query = "select  " + columnName1 + " from " + tableeName + " where " + filterCol + " like '" + filterColVal + "'";
        } else {
          query = "select DISTINCT " + columnName1 + " from " + tableeName + " where " + filterCol + " like '" + filterColVal + "'";
        }
      } else {
        query = "select DISTINCT " + columnName1 + " from " + tableeName;
      }
    } else {

      var tableeName = '';
      if (isNaN(tablename)) {
        tableeName = tablename;
      } else {
        tableeName = constantService.tableName + tablename;
      }

      if (filterCol != null) {
        if (columnName.trim() == 'partno') {
          query = "SELECT " + columnName1 + " FROM ( SELECT " + columnName1 + ", ROW_NUMBER() OVER(PARTITION BY " + columnName + " ORDER BY rowid) rn FROM " + tableeName + " where " + filterCol + " like '" + filterColVal + "' ) a WHERE rn = 1 order by rowid asc"
        } else {
          query = "select DISTINCT " + columnName1 + " from " + tableeName + " where " + filterCol + " like '" + filterColVal + "'";
        }
      } else {
        query = "select DISTINCT " + columnName1 + " from " + tableeName;
      }
    }


    console.log("query completed --------------------  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getEngneeRecord(tablename, colName, recorvalue) {
    var tableeName = '';
    if (isNaN(tablename)) {
      tableeName = tablename;
    } else {
      tableeName = constantService.tableName + tablename;
    }
    var deferred = $q.defer();
    var query = "select *  from " + tableeName + " where " + colName + " like '" + recorvalue + "'";
    console.log("query completed 3  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getRecordByMachinPatNo(tablename, colName, recorvalue, formname, machine) {
    var tableeName = '';
    if (isNaN(tablename)) {
      tableeName = tablename;
    } else {
      tableeName = constantService.tableName + tablename;
    }
    var deferred = $q.defer();
    var query = '';
    if (formname.toUpperCase().includes('IN-PROCE')) {
      query = "select *  from " + tableeName + " where " + colName + " like '" + recorvalue + "' and machine='" + machine + "'";
    } else {
      query = "select *  from " + tableeName + " where " + colName + " like '" + recorvalue + "'";
    }
    console.log("query completed 3  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getInnerTable(tablename, tableid, val, formName) {
    var deferred = $q.defer();
    if (formName.toUpperCase().includes("MATERIAL PRE")) {
      var query = "select *  from " + constantService.innerTableName + tablename + "_" + val + " where tableid = '" + tableid + "'  order by column1  ASC";
    } else {
      var query = "select *  from " + constantService.innerTableName + tablename + "_" + val + " where tableid = '" + tableid + "'  order by column1 * 1 ASC";
    }

    console.log("query dyntabResponse 1  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.innerTableName + tablename + "_0",
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

  function getTableRecordBasedOnQuery(query) {
    var deferred = $q.defer();
    console.log("query dyntabResponse  2  ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }






  function getEngneeRecords(tablename, partval, col) {
    var deferred = $q.defer();
    var tableeName = '';
    if (isNaN(tablename)) {
      tableeName = tablename;
    } else {
      tableeName = constantService.tableName + tablename;
    }


    var query = "select *  from " + tableeName + " where " + col + " like '" + partval.trim() + "'";
    console.log("query 11 ", query)
    if (1) {
      var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'query': query,
          'table_name': constantService.mainTable,
          'type': 'SELECT'
        }
      }
      $http(dynareq).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });
    } else {
      $cordovaSQLite.execute(constantService.db, query).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
    }
    return deferred.promise;
  }

}
