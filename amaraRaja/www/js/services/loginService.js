angular.module('amaraja').factory('loginService', loginService);

loginService.$inject = ['$q', '$http', '$cordovaSQLite', 'constantService'];

function loginService($q, $http, $cordovaSQLite, constantService) {
 	
 	var service = {
 		userLogin:userLogin,
    getUserDetail:getUserDetail
 	}
 	return service;

 	function userLogin(user){
 		var deferred = $q.defer();
    console.log("login ", user)
 		var request = {
                method: 'POST',
                url: constantService.serverURL + 'Login',
                headers: {
                  'Content-Type': 'application/json'
                },
                data: user
               }
    $http(request).then(function(response){
      var result = response.data.LoginResult[0];
      //$cordovaSQLite.execute(db, 'INSERT INTO DF_MSTR_LGN (empCode, empName, deptName, role, supervisor, email, mobile, plant, inActive) VALUES ("' + result.empCode +'", "'+ result.empName +'", "'+ result.deptName +'", "'+ result.role +'", "'+ result.supervisor +'", "'+ result.email +'", "'+ result.mobile +'", "'+ result.plant +'", "'+ result.inActive + '")');
      deferred.resolve(result);
   }, function(error){
       deferred.reject(error);
   });

 		/*console.log("--------------------"+'Select * FROM '+constantService.loginTable+' WHERE userName = "'+user.userName+'" AND password= "'+user.password+'"');
    $cordovaSQLite.execute(constantService.db, 'Select * FROM '+constantService.loginTable+' WHERE userName = "'+user.userName+'" AND password= "'+user.password+'"').then(function(data) {
      deferred.resolve(data);
    }, function(error) {
      deferred.reject(error);
    });*/
    return deferred.promise;
 	}

  function getUserDetail(user) {

        var deferred = $q.defer();
        var queryUserDetails = "select * from " + constantService.usersTable + " where empCode=" + user + "";
        if (1) {
            var dynareq = {
                method: 'POST',
                url: constantService.serverURL + 'SelectDynamicTable',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'query': queryUserDetails,
                    'table_name': constantService.usersTable,
                    'type': 'SELECT'
                }
            }
            console.log("queryUserDetails  ", queryUserDetails)
            $http(dynareq).then(function(response) {
                    deferred.resolve(response);
                },
                function(error) {
                    console.log('error###', error);
                    deferred.reject(error);
                });
        } else {
            $cordovaSQLite.execute(constantService.db, queryUserDetails).then(function(data) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
  }

}