angular.module('amaraja').factory('mainService', mainService);

mainService.$inject = ['$q', '$http', '$cordovaSQLite'];

function mainService($q, $http, $cordovaSQLite) {
	var service = {
		addRecord: addRecord,
		addRecordJSON: addRecordJSON,
		getRecord: getRecord,
		createTable: createTable
	}
	return service;

	function createTable(tableRow) {
		var deferred = $q.defer();
		console.log("service ", tableRow);
		var daaa = '';
		angular.forEach(tableRow, function(value, key) {
			daaa = ' ' + daaa + value + " " + 'TEXT,';
		})
		db = window.openDatabase("amaraja.db", "1", "Sqlite", "2000");
		$cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS aaaa3 (id INTEGER PRIMARY KEY AUTOINCREMENT, ' + daaa.slice(0, -1) + ')');
	}

	function addRecord(record) {
		console.log("service menu");
		var deferred = $q.defer();
		$cordovaSQLite.execute(db, 'INSERT INTO dept (dpt, dpt2) VALUES (?,?)', [record.c1, record.c6]).then(function(data) {
			console.log("Save successful", data);
			deferred.resolve(data);
		}, function(error) {
			console.log("error successful", error);
			deferred.reject(error);
		})

		return deferred.promise;
	}

	function addRecordJSON(record) {
		console.log("service menu");

		var jsondata = {
			'value': [{
				"label": "Do you have a website?",
				"field_type": "text",
				"required": false,
				"field_options": {},
				"cid": "c1"
			}, {
				"label": "Please enter your clearance number",
				"field_type": "text",
				"required": true,
				"field_options": {},
				"cid": "c6"
			}, {
				"label": "Security personnel #82?",
				"field_type": "radio",
				"required": true,
				"field_options": {
					"options": [{
						"label": "Yes",
						"checked": false
					}, {
						"label": "No",
						"checked": false
					}],
					"include_other_option": true
				},
				"cid": "c10"
			}, {
				"label": "Medical history",
				"field_type": "file",
				"required": true,
				"field_options": {},
				"cid": "c14"
			}]
		}
		var deferred = $q.defer();
		$cordovaSQLite.execute(db, 'INSERT INTO dept1 (dpt, dpt2) VALUES (?,?)', ['chethan2', JSON.stringify(jsondata)]).then(function(data) {
			console.log("Save successful", data);
			deferred.resolve(data);
		}, function(error) {
			console.log("error successful", error);
			deferred.reject(error);
		})

		return deferred.promise;
	}

	function getRecord() {
		console.log("service menu");
		var deferred = $q.defer();
		$cordovaSQLite.execute(db, 'select * from dept1 where dpt = "chethan2"').then(function(data) {
			console.log("Save successful", data);
			deferred.resolve(data);
		}, function(error) {
			console.log("error successful", error);
			deferred.reject(error);
		})

		return deferred.promise;
	}



}