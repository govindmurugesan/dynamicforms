angular
	.module('amaraja')
	.service('notificationService', notificationService);

notificationService.$inject = ['$ionicPopup','$ionicLoading']

function notificationService($ionicPopup, $ionicLoading) {
	var vm = this;
	vm.alert = alert;
	vm.showSpinner =showSpinner;
	vm.hideLoad = hideLoad;
	vm.confirm = confirm;

	function alert(title, alertMsg, okAction) {
		$ionicPopup.alert({
			title: title,
			template: alertMsg
		}).then(okAction);
	}

	function showSpinner(){
     $ionicLoading.show({
     template: '<ion-spinner icon="dots"></ion-spinner>'
   });
  }
	function hideLoad(){
	  $ionicLoading.hide();
	}

	 function confirm(alertMsg, cancelText, confirmText, cancelAction, confirmAction) {
    var confirmPopup = $ionicPopup.confirm({
      title: '',
      template: alertMsg,
      buttons: [{
        text: cancelText,
        type: 'button-positive',
        onTap: cancelAction

      }, {
          text: confirmText,
          type: 'button-positive',
          onTap: confirmAction
        }]
    });
  }

}