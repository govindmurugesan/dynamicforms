angular.module('amaraja').factory('reportService', reportService);

reportService.$inject = ['$q', '$http', '$cordovaSQLite','constantService'];

function reportService($q, $http, $cordovaSQLite,constantService) {

	var service ={
		getTableRecords: getTableRecords,
		getInnerTableRecords: getInnerTableRecords,
        getByRowName: getByRowName,
        getByRowNamewithcondition: getByRowNamewithcondition
	}
	return service;

	 function getTableRecords(tableId, condition) {
        var deferred = $q.defer();
        var tableName = constantService.tableName + tableId;
        var query = '';
        if(condition != '' && condition != undefined){
            query = "SELECT rowid,* FROM  " + tableName+" where "+condition;
        }else{
            query = "SELECT rowid,* FROM  " + tableName;
        }
        console.log("q2uery ", query)
        var dynareq = {
            method: 'POST',
            url: constantService.serverURL + 'SelectDynamicTable',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'query': query,
                'table_name': tableName,
                'type': 'SELECT'
            }
        }
        $http(dynareq).then(function(response) {
            var data = [];
            data.rows = response.data;
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error);
        });										
      
        return deferred.promise;
    }


	function getInnerTableRecords(TableNameId, subId, tableid){
		 var deferred = $q.defer();
		 var tableName = constantService.innerTableName + TableNameId + "_" + subId;
          var query = '';
         if(TableNameId != '3097' && TableNameId != '2093' && TableNameId != '2096'){
            query = "SELECT  rowid,* FROM  " + tableName + " WHERE tableid ='"+tableid+"'  order by column1 * 1 ASC";
         }else{
            query = "SELECT  rowid,* FROM  " + tableName + " WHERE tableid ='"+tableid+"'  order by column1   ASC";
         }
		 console.log("11query ", query)
     var dynareq = {
        method: 'POST',
        url: constantService.serverURL + 'SelectDynamicTable',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            'query': query,
            'table_name': tableName,
            'type': 'SELECT'
        }
    }
    $http(dynareq).then(function(response) {
        var data = [];
        data.rows = response.data;
        deferred.resolve(data);
    }, function(error) {
        deferred.reject(error);
    });
    return deferred.promise;
	}

    function getByRowName(tableId, rowname){
         var deferred = $q.defer();
         var tableName = constantService.tableName + tableId;
         var query = "SELECT distinct  "+rowname+" FROM  " + tableName ;
         console.log("111query ", query)
         var dynareq = {
            method: 'POST',
            url: constantService.serverURL + 'SelectDynamicTable',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'query': query,
                'table_name': tableName,
                'type': 'SELECT'
            }
        }
        $http(dynareq).then(function(response) {
            var data = [];
            data.rows = response.data;
            deferred.resolve(data);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getByRowNamewithcondition(tableId, rowname, colname,  colValue){
        var deferred = $q.defer();
         var tableName = constantService.tableName + tableId;
         var query = "SELECT distinct  "+rowname+" FROM  " + tableName +" Where "+colname+"='"+colValue+"'";
         console.log("113query ", query)
         var dynareq = {
            method: 'POST',
            url: constantService.serverURL + 'SelectDynamicTable',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                'query': query,
                'table_name': tableName,
                'type': 'SELECT'
            }
        }
        $http(dynareq).then(function(response) {
            var data = [];
            data.rows = response.data;
            deferred.resolve(data);
        }, function(error) {
            console.log('error###', error);
            deferred.reject(error);
        });
        return deferred.promise;
    }

}