angular.module('amaraja').factory('trackRecordService', trackRecordService);

trackRecordService.$inject = ['$q', '$http', '$cordovaSQLite', 'constantService'];

function trackRecordService($q, $http, $cordovaSQLite, constantService) {
		var service = {
			getFormNames: getFormNames,
			getWorkflow: getWorkflow,
			getFormRecords: getFormRecords,
			getRecordDetail: getRecordDetail
		}
	return service;

	function getFormNames() {
		var deferred = $q.defer();
		var query = "SELECT  rowid, tableName FROM "+constantService.mainTable;
     if(1){
      var dynareq = {
          method: 'POST',
          url: constantService.serverURL+ 'SelectDynamicTable',
          headers: {'Content-Type': 'application/json' },
          data: { 'query': query,
                  'table_name': constantService.mainTable,
                  'type': 'SELECT'
                }
         }
      $http(dynareq).then(function(response){
        console.log('response-',response);
        deferred.resolve(response);   
      }, function(error){
        console.log('error###',error);
        deferred.reject(error);
      });
    }else{
      
    }
    return deferred.promise;
	}


	function getWorkflow() {
		var deferred = $q.defer();		
		var query = "SELECT workflowId, levelForm from DF_WORKFLOW_I group by workflowId, levelForm having count(workflowId) > 0" ;
     if(1){
      var dynareq = { method: 'POST',
                  url: constantService.serverURL+ 'SelectDynamicTable',
                  headers: {'Content-Type': 'application/json' },
                  data: { 'query': query, 'table_name': constantService.workFlowItems, 'type': 'SELECT' }
                }
      $http(dynareq).then(function(response){
        console.log('response-',response);
        deferred.resolve(response);   
      }, function(error){
        console.log('error###',error);
        deferred.reject(error);
      });
    }else{
      
    }
    return deferred.promise;
	}

	function getFormRecords(tableid) {
		var deferred = $q.defer();		
		var query = "SELECT * from DF_TBL_PGR_"+tableid;
		console.log("query 3 ",query);
    if(1){
      var dynareq = { method: 'POST',
                  url: constantService.serverURL+ 'SelectDynamicTable',
                  headers: {'Content-Type': 'application/json' },
                  data: { 'query': query, 'table_name': constantService.workFlowItems, 'type': 'SELECT' }
                }
      $http(dynareq).then(function(response){
        console.log('response-',response);
        deferred.resolve(response);   
      }, function(error){
        console.log('error###',error);
        deferred.reject(error);
      });
    }else{
      
    }
    return deferred.promise;
	}

	function getRecordDetail(formId, recordId) {
		var deferred = $q.defer();		
		var query = "select * from DF_TBL_FMH where formName ='"+formId+"' and recordId='"+recordId+"'"
		console.log("query 4 ",query);
     if(1){
      var dynareq = { method: 'POST',
                  url: constantService.serverURL+ 'SelectDynamicTable',
                  headers: {'Content-Type': 'application/json' },
                  data: { 'query': query, 'table_name': constantService.workFlowItems, 'type': 'SELECT' }
                 }
      $http(dynareq).then(function(response){
        console.log('response-',response);
        deferred.resolve(response);   
      }, function(error){
        console.log('error###',error);
        deferred.reject(error);
      });
    }else{
      
    }
    return deferred.promise;
	}

}	