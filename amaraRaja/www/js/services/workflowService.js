(function() {
	'use strict';

	angular
		.module('amaraja')
		.factory('workflowService', workflowService);

	workflowService.$inject = ['$q', '$cordovaSQLite','constantService', '$cordovaNetwork', '$http'];

	function workflowService($q, $cordovaSQLite, constantService, $cordovaNetwork, $http) {
		var service = {
			getDepartments: getDepartments,
			getForms: getForms,
			submitworkflow: submitworkflow,
			getWorkflow: getWorkflow,
			getWorkflowData: getWorkflowData,
			updateworkflow: updateworkflow,
			getAllDetails: getAllDetails

		};

		return service;

		function getDepartments() {
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT * FROM "+ constantService.deptTable;
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.deptTable,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	    /*}else{
				$cordovaSQLite.execute(constantService.db, 'SELECT * FROM '+ constantService.deptTable).then(function(result) {console.log('result',result);
					def.resolve(result);
				}, function(error) {console.log('error',error);
					def.reject(error);
				});
			}*/
			return def.promise;
		}

		function getForms() {
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT tableName FROM "+constantService.mainTable;
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.mainTable,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	    /*}else{
				$cordovaSQLite.execute(constantService.db, 'SELECT tableName FROM '+constantService.mainTable).then(function(result) {
					def.resolve(result);
				}, function(error) {
					def.reject(error);
				});
			}*/
			return def.promise;
		}

		function submitworkflow(request) {
			var def = $q.defer();
			var promiseColl = [];

			var query = "INSERT INTO "+constantService.workFlowMaster+" (workflowName, levels) VALUES ('" + request.workflowname+ "',"+ request.levels+ ")";
	    console.log('query',query);
	    var dynareq = {
	                method: 'POST',
	                url: constantService.serverURL+ 'CreateDynamicTable',
	                headers: {
	                  'Content-Type': 'application/json'
	                },
	                data: { 'query': query,
	                        'table_name': constantService.workFlowMaster,
	                        'type': 'INSERT'
	                      }
	               }
	    $http(dynareq).then(function(response){
	      promiseColl.push(
					angular.forEach(request.departments, function(piece, index) {
						var count = parseInt(index) + 1;
						var levelno = 'level ' + count ;

						var insquery = "INSERT INTO "+constantService.workFlowItems+" (workflowId, department, levelForm, lev, levelName) VALUES ("+response.data.CreateDynamicTableResult[0].rowid+",'"+ piece + "','" + request.forms[index] + "','" + levelno + "','" + request.formname[index]+"')";
			      console.log('insquery',insquery);
			      var dynainsreq = {
			        method: 'POST',
			        url: constantService.serverURL+ 'CreateDynamicTable',
			        headers: {
			          'Content-Type': 'application/json'
			        },
			        data: { 'query': insquery,
			                'table_name': constantService.workFlowItems,
			                'type': 'INSERT'
			              }
			       }
			      $http(dynainsreq).then(function(response){
			        console.log('response',response);
			      }, function(error){
			        console.log('error###',error);
			      });
					})
				)
				$q.all(promiseColl).then(function(data) {
					def.resolve(data);
				}, function(error) {
					def.reject(error);
				});
	    }, function(error){
	      console.log('error###',error);
	    });

			/*$cordovaSQLite.execute(constantService.db, 'INSERT INTO '+constantService.workFlowMaster+' (workflowname, levels) VALUES (?,?)', [request.workflowname, request.levels]).then(function(data) {
				promiseColl.push(
					angular.forEach(request.departments, function(piece, index) {
						var count = parseInt(index) + 1;
						var levelno = 'level ' + count ;
						$cordovaSQLite.execute(constantService.db, 'INSERT INTO '+constantService.workFlowItems+' (workflowid, department, levelform, level, levelname) VALUES ('+data.insertId+',"'+ piece+'","'+ request.forms[index]+'","'+ levelno+'","'+ request.formname[index]+'")').then(function(data) {
							console.log("asdded")
						}, function(error) {
							console.log("error ", error)
						});
					})
				)
				$q.all(promiseColl).then(function(data) {
					def.resolve(data);
				}, function(error) {
					def.reject(error);
				});
			}, function(error) {
			});*/

			return def.promise;
		}

		function getWorkflow() {
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT * FROM "+constantService.workFlowMaster;
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.workFlowMaster,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	    /*}else{
				$cordovaSQLite.execute(constantService.db, 'SELECT * FROM '+constantService.workFlowMaster).then(function(result) {console.log('result',result);
					def.resolve(result);
				}, function(error) {
					def.reject(error);
				});
			}*/

			return def.promise;
		}

		function getWorkflowData(id){
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT  rowid,* FROM "+constantService.workFlowItems+" WHERE workflowid =" + id;
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.workFlowItems,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	      	console.log('response',response);
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	    /*}else{
				console.log('select  rowid,* from '+constantService.workFlowItems+' where workflowid =' + id )
				$cordovaSQLite.execute(constantService.db, 'SELECT  rowid,* FROM '+constantService.workFlowItems+' WHERE workflowid =' + id ).then(function(result) {console.log('result',result);
					def.resolve(result);
				}, function(error) {
					def.reject(error);
				});
			}*/
			return def.promise;
		}

		function getAllDetails() {
			console.log('service');
			var def = $q.defer();
			//if($cordovaNetwork.isOnline()){
	      var query = "SELECT * FROM "+constantService.workFlowItems + " AS A JOIN "+ constantService.workFlowMaster + " AS B ON A.workflowId = B.workflowId ORDER BY B.workflowName";
	      console.log('query',query);
	      var dynareq = {
	        method: 'POST',
	        url: constantService.serverURL+ 'SelectDynamicTable',
	        headers: {
	          'Content-Type': 'application/json'
	        },
	        data: { 'query': query,
	                'table_name': constantService.workFlowItems,
	                'type': 'SELECT'
	              }
	      }
	      $http(dynareq).then(function(response){
	        var data =[];
	        data.rows = response.data;
	        def.resolve(data);
	      }, function(error){
	        console.log('error###',error);
	        def.reject(error);
	      });
	
			return def.promise;
		}


		function updateworkflow(request, removeElements){
			var def = $q.defer();
			var promiseColl = [];
			promiseColl.push(
				angular.forEach(request, function(value, index){
					if(value.create != undefined && value.create == true){
						//if($cordovaNetwork.isOnline()){
							var query = "INSERT INTO "+constantService.workFlowItems+" (workflowId, department, levelForm, lev, levelName) VALUES (" + value.workflowid + ",'"+ value.department + "','" + value.levelform +"',"+ value.level + ",'"+ value.levelname + "')";
				      var dynareq = {
				                  method: 'POST',
				                  url: constantService.serverURL+ 'CreateDynamicTable',
				                  headers: {
				                    'Content-Type': 'application/json'
				                  },
				                  data: { 'query': query,
				                          'table_name': constantService.workFlowItems,
				                          'type': 'INSERT'
				                        }
				                 }
				      $http(dynareq).then(function(response){
				        console.log('response',response);
				      }, function(error){
				        console.log('error###',error);
				      });
				    /*}else{
						console.log('INSERT INTO '+constantService.workFlowItems+' (workflowid, department, levelform, lev, levelname) VALUES (?,?,?,?,?)', [value.workflowid, value.department, value.levelform, value.level, value.levelname])
							$cordovaSQLite.execute(constantService.db, 'INSERT INTO '+constantService.workFlowItems+' (workflowid, department, levelform, level, levelname) VALUES (?,?,?,?,?)', [value.workflowid, value.department, value.levelform, value.level, value.levelname]).then(function(data) {
							}, function(error) {
							});
						}*/
					}else if(value.edited != undefined && value.edited == true){
						//if($cordovaNetwork.isOnline()){
							var query = "UPDATE "+constantService.workFlowItems+" SET department = '"+ value.department + "', levelform = '" + value.levelform + "', lev = '"+ value.level + "', levelname = '"+ value.levelname + "' WHERE  rowid = "+ value.rowid;
				      var dynareq = {
				                  method: 'POST',
				                  url: constantService.serverURL+ 'CreateDynamicTable',
				                  headers: {
				                    'Content-Type': 'application/json'
				                  },
				                  data: { 'query': query,
				                          'table_name': constantService.workFlowItems,
				                          'type': 'UPDATE'
				                        }
				                 }
				      $http(dynareq).then(function(response){
				        console.log('response',response);
				      }, function(error){
				        console.log('error###',error);
				      });
			      /*}else{
							$cordovaSQLite.execute(constantService.db, 'UPDATE '+constantService.workFlowItems+' SET department = "'+ value.department + '", levelform = "'+ value.levelform + '", level = "'+ value.level + '", levelname = "'+ value.levelname + '" WHERE  rowid = '+ value.rowid).then(function(data) {
								console.log('data',data);
							}, function(error) {
								console.log('error',error);
							});
						}*/
					}else{
					}
	      })
      )
      if(removeElements.length > 0){
				angular.forEach(removeElements, function(value, index){
					//if($cordovaNetwork.isOnline()){
						var delquery = "DELETE FROM "+constantService.workFlowItems+" WHERE rowid = "+ value.rowid;
			      var dynadelreq = {
			                  method: 'POST',
			                  url: constantService.serverURL+ 'CreateDynamicTable',
			                  headers: {
			                    'Content-Type': 'application/json'
			                  },
			                  data: { 'query': delquery,
			                          'table_name': constantService.workFlowItems,
			                          'type': 'DELETE'
			                        }
			                 }
			      $http(dynadelreq).then(function(response){
			        console.log('response',response);
			        var query = "UPDATE "+constantService.workFlowMaster+" SET levels = ((SELECT levels from "+ constantService.workFlowMaster + " WHERE workflowId = "+ value.workflowid+")-1) WHERE workflowId = "+ value.workflowid;
				      var dynareq = {
				                  method: 'POST',
				                  url: constantService.serverURL+ 'CreateDynamicTable',
				                  headers: {
				                    'Content-Type': 'application/json'
				                  },
				                  data: { 'query': query,
				                          'table_name': constantService.workFlowMaster,
				                          'type': 'UPDATE'
				                        }
				                 }
				      $http(dynareq).then(function(response){
				        console.log('response',response);
				      }, function(error){
				        console.log('error###',error);
				      });
				      }, function(error){
				        console.log('error###',error);
				      });
			    /*}else{
						$cordovaSQLite.execute(constantService.db, 'DELETE FROM '+constantService.workFlowItems+' WHERE rowid = '+ value.rowid).then(function(data) {
							$cordovaSQLite.execute(constantService.db, 'UPDATE '+constantService.workFlowMaster+' SET levels = ((SELECT levels from workflowMaster WHERE workflowid = '+ value.workflowid+')-1) WHERE workflowid = '+ value.workflowid);
						}, function(error) {
							console.log('error',error);
						});
					}*/
				})
			}

			$q.all(promiseColl).then(function(data) {
				def.resolve(data);
			}, function(error) {
				def.reject(error);
			});
			return def.promise;
		}
	}
})();